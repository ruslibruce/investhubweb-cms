import { putEvent } from "@/shared/api/mutation/event";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutEvent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putEvent,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["event"]] });
          notification.success({
            message: "Event berhasil diupdate",
          });
        }
      },
    },
  });

  const handleUpdateEvent = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleUpdateEvent,
  };
};

export default usePutEvent;
