import { fetchLocation } from "@/shared/api/fetch/location";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchLocation();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetLocation = (initialData?: any) => {
  const fetchDataLocationID = fetchLocation();

  const fetchGetLocation = useFetchHook({
    keys: fetchDataLocationID.key,
    api: fetchDataLocationID.api,
    initialData,
    options: {},
  });

  return {
    fetchGetLocation,
  };
};

export default useGetLocation;
