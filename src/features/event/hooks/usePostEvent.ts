import { addEvent } from "@/shared/api/mutation/event";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostEvent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: addEvent,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["event"]] });
          notification.success({
            message: "Event berhasil dibuat",
          });
        }
      },
    },
  });

  const handlePostEvent = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostEvent,
  };
};

export default usePostEvent;
