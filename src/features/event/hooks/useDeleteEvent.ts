import { deleteEvent } from "@/shared/api/mutation/event";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteEvent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteEvent,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["event"]] });
          notification.success({
            message: "The event was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteEvent = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteEvent,
  };
};

export default useDeleteEvent;
