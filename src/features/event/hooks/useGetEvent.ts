import { fetchEvent } from "@/shared/api/fetch/event";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchEvent();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetEvent = (initialData?: any, params?: string) => {
  const fetchDataEvent = fetchEvent(params);

  const fetchQuery = useFetchHook({
    keys: [fetchDataEvent.key, params],
    api: fetchDataEvent.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetEvent;
