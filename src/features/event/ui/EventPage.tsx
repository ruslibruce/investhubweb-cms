import {
  ArrowRightOutlined,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  EyeTwoTone,
  FolderOpenTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Flex,
  Input,
  Modal,
  Row,
  Select,
  Space,
  Spin,
  Upload,
  message,
} from "antd";

import { DASHBOARD_EVENT_DETAIL } from "@/shared/constants/path";
import { useRouter } from "next/router";

import HamburgerCard from "@/components/HamburgerCard";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import useDeleteEvent from "../hooks/useDeleteEvent";
import useGetEvent from "../hooks/useGetEvent";
import useGetEventCategory from "../hooks/useGetEventCategory";
import usePostEvent from "../hooks/usePostEvent";
import usePutEvent from "../hooks/usePutEvent";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import {
  CATEGORY_STATUS,
  IMAGE_FORMAT,
  TOOLBAR_OPTION_QUILL,
  USER,
} from "@/shared/constants/storageStatis";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import {
  checkIsEmptyObject,
  getStatusColor,
  isEmptyContent,
  ReactQuillNoSSR,
} from "@/shared/utils/helper";
import dayjs from "dayjs";
import dynamic from "next/dynamic";
import Image from "next/image";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useGetLocation from "../hooks/useGetLocation";
import { paramsToString } from "@/shared/utils/string";
import NoData from "@/shared/components/NoData";
import { imagePORTAL } from "@/shared/constants/imageUrl";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
const { Dragger } = Upload;
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  category: "",
  location: "",
  start_time: "",
  end_time: "",
  title: "",
  description: "",
  cover_image: "",
  is_active: "draft",
};
const EventPage = () => {
  const dataUser = storageCheck(USER);
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const { t: translate } = useTranslation();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [dataFilter, setDataFilter] = React.useState({
    category: "",
    is_active: "",
    title: "",
    location: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState({
    category: "",
    is_active: "",
    title: "",
    location: "",
  });
  const { fetchQuery } = useGetEvent(
    [],
    paramsToString({ page: currentPage, ...dataFilterApi })
  );
  const { fetchQuery: fetchCategory } = useGetEventCategory({});
  const { data: dataFetchCategory } = fetchCategory;
  const { handleDeleteEvent } = useDeleteEvent();
  const { mutationQuery: mutationPost, handlePostEvent } = usePostEvent();
  const { isPending: isLoadingPostEvent, isSuccess: isSuccessPostEvent } =
    mutationPost;
  const { mutationQuery: mutationUpdate, handleUpdateEvent } = usePutEvent();
  const { isPending: isLoadingUpdateEvent, isSuccess: isSuccessUpdateEvent } =
    mutationUpdate;
  const { data: dataResultEvent, isLoading } = fetchQuery;
  const { data: eventData } = dataResultEvent;
  console.log("eventData", eventData);

  const navigate = useRouter();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [formDataEvent, setFormDataEvent] = useState<any>(initialFormData);
  const [fontSize, setFontSize] = useState("initial");
  const { fetchGetLocation } = useGetLocation([]);
  const { data: dataLocation } = fetchGetLocation;
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessPostEvent) {
      setIsModalVisible(false);
    }

    if (isSuccessUpdateEvent) {
      setIsEditModalVisible(false);
    }
  }, [isSuccessPostEvent, isSuccessUpdateEvent]);

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const handleStartTimeChange = (
    date: dayjs.Dayjs,
    dateString: string | string[],
    setFieldValue: any
  ) => {
    setFieldValue("start_time", dateString);
  };

  const handleEndTimeChange = (
    date: dayjs.Dayjs,
    dateString: string | string[],
    setFieldValue: any
  ) => {
    setFieldValue("end_time", dateString);
  };

  const handleAddEventSubmit = (values: any) => {
    handlePostEvent(values);
  };

  const handleDeleteEventSubmit = (id: string) => {
    handleDeleteEvent(id);
  };

  const showModal = () => {
    setFormDataEvent(initialFormData);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = (event: any) => {
    setFormDataEvent({
      category: event.category,
      location: event.location,
      start_time: event.start_time,
      end_time: event.end_time,
      title: event.title,
      description: event.description,
      cover_image: event.cover_image.split("/").pop(),
      is_active: event.is_active,
      id: event.id,
    });
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handlePublish = (values: any, params: string) => {
    console.log("values", values);
    let category = dataFetchCategory.find((item: any) =>
      typeof values.category == "string"
        ? item.name == values.category
        : item.id == values.category
    );
    console.log("dataFetchCategory", dataFetchCategory);
    handleUpdateEvent(values.id, {
      ...values,
      is_active: params,
      category: category.id,
      cover_image: values.cover_image.split("/").pop(),
    });
  };

  const handleEvent = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_EVENT_DETAIL}/${value.id}`);
  };

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter({
      ...dataFilter,
      category: value,
    });
  };

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    if (value === "all") {
      setDataFilter({
        ...dataFilter,
        is_active: "",
      });
      return;
    }
    setDataFilter({
      ...dataFilter,
      is_active: value,
    });
  };

  const handleChangeLocation = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter({
      ...dataFilter,
      location: value,
    });
  };

  const handleSetFilter = () => {
    console.log("dataFilter", dataFilter);
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };

  const renderCards = () => {
    if (!eventData) return <LoaderSpinGif size="large" />;
    if (eventData && eventData.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );

    return eventData?.map((card: any, index: any) => (
      <Card key={card.id}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(card.is_active),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {card.is_active}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleEvent(e, { id: card.id })}
                />
              </span>
              {!isKurator && (
                <span style={{ marginRight: 10 }}>
                  <EditTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={() => editModal(card)}
                  />
                </span>
              )}
              {!isKurator && (
                <span style={{ marginRight: 10 }}>
                  <DeleteTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={() => {
                      Modal.confirm({
                        title: "Delete Data",
                        content: "Are you sure you want to delete this data?",
                        onOk: () => handleDeleteEventSubmit(card.id),
                        okButtonProps: {
                          icon: <DeleteOutlined />,
                          style: { backgroundColor: "#CB0B0C" },
                        },
                      });
                    }}
                  />
                </span>
              )}
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleEvent(e, { id: card.id })}
                onEdit={() => editModal(card)}
                onDelete={() => {
                  Modal.confirm({
                    title: "Delete Data",
                    content: "Are you sure you want to delete this data?",
                    onOk: () => handleDeleteEventSubmit(card.id),
                    okButtonProps: {
                      icon: <DeleteOutlined />,
                      style: { backgroundColor: "#CB0B0C" },
                    },
                  });
                }}
                arrayItems={["1", "2", "3"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p
              style={{ fontWeight: "bold" }}
              dangerouslySetInnerHTML={{ __html: card.title }}
            />
            <p
              style={{ color: "gray" }}
              dangerouslySetInnerHTML={{
                __html: isMobile
                  ? truncateTextToHalf(card.description)
                  : card.description,
              }}
            />
          </div>
        </div>
      </Card>
    ));
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    category: yup.string().required(translate("CategoryRequired")),
    location: yup.string().required(translate("LocationRequired")),
    start_time: yup.string().required(translate("StartTimeRequired")),
    end_time: yup.string().required(translate("EndTimeRequired")),
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
    description: yup.string().required(translate("DescriptionRequired")),
    cover_image: yup.string().required(translate("CoverPictureRequired")),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Event</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Event",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          {!isKurator && (
            <Button
              size={formSize}
              type="primary"
              onClick={showModal}
              style={{ backgroundColor: "#CB0B0C" }}
            >
              <PlusOutlined />
              Create Event
            </Button>
          )}
        </div>
      </div>
      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search Event</p>
            <p style={{ marginTop: 18 }}>Title</p>
            <Input
              size={formSize}
              placeholder="Title"
              onChange={(e) =>
                setDataFilter({
                  ...dataFilter,
                  title: e.target.value,
                })
              }
            />
            <p style={{ marginTop: "18px" }}>Category</p>
            <Select
              size={formSize}
              defaultValue="Category"
              style={{ width: "100%", marginTop: 10 }}
              onChange={handleChange}
              options={dataFetchCategory}
            />
            <p style={{ marginTop: 18 }}>Location</p>
            <Select
              size={formSize}
              defaultValue="Location"
              style={{ width: "100%", marginTop: 10 }}
              onChange={handleChangeLocation}
              options={dataLocation}
            />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              size={formSize}
              defaultValue="All"
              style={{ width: "100%", marginTop: 10 }}
              onChange={handleChangeStatus}
              options={CATEGORY_STATUS}
            />
            <Button
              size={formSize}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
              onClick={handleSetFilter}
            >
              Submit
            </Button>
          </Card>
        </div>

        <div className="category-table">
          {renderCards()}

          {/* Pagination */}
          {eventData && eventData.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <Row>
                <Col>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultEvent?.records}
                    limit={dataResultEvent?.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultEvent?.records}
                    limit={dataResultEvent?.limit}
                  />
                </Col>
              </Row>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Event"
        open={isModalVisible}
        width={1200}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleAddEventSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={10}
              >
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    options={dataFetchCategory}
                    value={values.category}
                    size={formSize}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Location</label>
                  <Select
                    placeholder="Select Location"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("location", value)
                    }
                    options={dataLocation}
                    value={values.location}
                    size={formSize}
                  />
                  {errors.location && (
                    <div className={"error"}>{errors.location.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Start Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleStartTimeChange(date, dateString, setFieldValue)
                    }
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={values.start_time ? dayjs(values.start_time) : null}
                    size={formSize}
                  />
                  {errors.start_time && (
                    <div className={"error"}>
                      {errors.start_time.toString()}
                    </div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">End Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleEndTimeChange(date, dateString, setFieldValue)
                    }
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={values.end_time ? dayjs(values.end_time) : null}
                    size={formSize}
                  />
                  {errors.end_time && (
                    <div className={"error"}>{errors.end_time.toString()}</div>
                  )}
                </Col>
              </Row>
              <label className="required" style={{ marginBottom: 12 }}>
                Title
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("title", "");
                    return;
                  }
                  setFieldValue("title", value);
                }}
                value={values.title}
              />
              {errors.title && (
                <div className={"error"}>{errors.title.toString()}</div>
              )}
              <label className="required" style={{ marginBottom: 12 }}>
                Description
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("description", "");
                    return;
                  }
                  setFieldValue("description", value);
                }}
                value={values.description}
              />
              {errors.description && (
                <div className={"error"}>{errors.description.toString()}</div>
              )}
              {!values.cover_image ? (
                <>
                  <label className="required">Upload files (Max. 2MB)</label>
                  <Dragger
                    {...{
                      name: "file",
                      // multiple: true,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                </>
              ) : (
                <Col span={12}>
                  <div>
                    <label className="required">Image Cover</label>
                  </div>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={`${imagePORTAL}/${values.cover_image}`}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingPostEvent}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update Event"
        open={isEditModalVisible}
        width={1200}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataEvent}
          enableReinitialize
          onSubmit={(values) => {
            console.log("values", values);
            handlePublish(values, "pending");
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={10}
              >
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    size={formSize}
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    options={dataFetchCategory}
                    value={values.category}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Location</label>
                  <Select
                    placeholder="Select Location"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("location", value)
                    }
                    options={dataLocation}
                    value={values.location}
                    size={formSize}
                  />
                  {errors.location && (
                    <div className={"error"}>{errors.location.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Start Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleStartTimeChange(date, dateString, setFieldValue)
                    }
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={values.start_time ? dayjs(values.start_time) : null}
                    size={formSize}
                  />
                  {errors.start_time && (
                    <div className={"error"}>
                      {errors.start_time.toString()}
                    </div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">End Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleEndTimeChange(date, dateString, setFieldValue)
                    }
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={values.end_time ? dayjs(values.end_time) : null}
                    size={formSize}
                  />
                  {errors.end_time && (
                    <div className={"error"}>{errors.end_time.toString()}</div>
                  )}
                </Col>
              </Row>
              <label className="required" style={{ marginBottom: 12 }}>
                Title
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("title", "");
                    return;
                  }
                  setFieldValue("title", value);
                }}
                value={values.title}
              />
              {errors.title && (
                <div className={"error"}>{errors.title.toString()}</div>
              )}
              <label className="required" style={{ marginBottom: 12 }}>
                Description
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("description", "");
                    return;
                  }
                  setFieldValue("description", value);
                }}
                value={values.description}
              />
              {errors.description && (
                <div className={"error"}>{errors.description.toString()}</div>
              )}
              {!values.cover_image ? (
                <>
                  <label className="required">Upload files (Max. 2MB)</label>
                  <Dragger
                    {...{
                      name: "file",
                      // multiple: true,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                </>
              ) : (
                <Col span={12}>
                  <div>
                    <label className="required">Image Cover</label>
                  </div>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={`${imagePORTAL}/${values.cover_image}`}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingUpdateEvent}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default EventPage;
