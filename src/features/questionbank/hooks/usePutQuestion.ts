import { putQuestionBank } from "@/shared/api/mutation/question";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutQuestion = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER);

  const mutationQuery = useMutationHook({
    api: putQuestionBank,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["question"] });
          notification.success({
            message: "Question anda berhasil diupdate",
          });
        }
      },
    },
  });

  const handlePutQuestionBank = (id: string, data: {}) => {
    mutationQuery.mutate({ id, data });
  };

  return {
    mutationQuery,
    handlePutQuestionBank,
  };
};

export default usePutQuestion;
