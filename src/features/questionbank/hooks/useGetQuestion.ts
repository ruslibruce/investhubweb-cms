import { fetchQuestionApi } from "@/shared/api/fetch/question";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchQuestionApi();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */

const useGetQuestion = (initialData?: any) => {
  const fetchDataNews = fetchQuestionApi();

  const fetchQuestion = useFetchHook({
    keys: fetchDataNews.key,
    api: fetchDataNews.api,
    initialData,
    options: {},
  });

  return {
    fetchQuestion,
  };
};

export default useGetQuestion;
