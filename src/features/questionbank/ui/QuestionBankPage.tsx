import {
  DeleteTwoTone,
  EditTwoTone,
  MailOutlined,
  PlusOutlined,
  SaveOutlined,
  SearchOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type {
  InputRef,
  MenuProps,
  TableColumnsType,
  TableColumnType,
} from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Input,
  Menu,
  Modal,
  Row,
  Select,
  Space,
  Table,
} from "antd";
import { useRouter } from "next/router";

import { TOOLBAR_OPTION_QUILL } from "@/shared/constants/storageStatis";
import {
  checkIsEmptyObject,
  isEmptyContent,
  ReactQuillNoSSR,
} from "@/shared/utils/helper";
import React, { useRef, useState } from "react";
import "react-quill/dist/quill.snow.css";
import useGetQuestion from "../hooks/useGetQuestion";
import usePostQuestion from "../hooks/usePostQuestion";
import usePutQuestion from "../hooks/usePutQuestion";
import { FilterDropdownProps } from "antd/es/table/interface";
import { COLORS } from "@/shared/styles/color";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const items: MenuProps["items"] = [
  {
    label: "Question Bank",
    key: "question",
    icon: <MailOutlined />,
  },
];

const initialformQuestion = {
  question: "",
  options: [
    { option: "a", value: "" },
    { option: "b", value: "" },
    { option: "c", value: "" },
    { option: "d", value: "" },
  ],
  answer: "",
  score_if_correct: "",
  score_if_wrong: "",
};

const QuestionBankPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [current, setCurrent] = useState("question");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const { fetchQuestion } = useGetQuestion();
  const { data: dataQuestion } = fetchQuestion;
  const [answer, setAnswer] = useState([
    { value: "a", label: "a" },
    { value: "b", label: "b" },
    { value: "c", label: "c" },
    { value: "d", label: "d" },
  ]);
  const [formQuestion, setFormQuestion] = useState<any>(initialformQuestion);
  const { handlePostQuestionBank, mutationQuery: mutationQueryPostQuestion } =
    usePostQuestion();
  const { isPending: isLoadingPost, isSuccess: isSuccessPost } =
    mutationQueryPostQuestion;
  const { handlePutQuestionBank, mutationQuery: mutationQueryPutQuestion } =
    usePutQuestion();
  const { isPending: isLoadingPut, isSuccess: isSuccessPut } =
    mutationQueryPutQuestion;
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef<InputRef>(null);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessPut) {
      setIsEditModalVisible(false);
    }

    if (isSuccessPost) {
      setIsModalVisible(false);
    }
  }, [isSuccessPut, isSuccessPost]);

  type DataType = {
    key: React.Key;
    no: number;
    question: string;
    options: string;
    answer: string;
    score_if_correct: number;
    score_if_wrong: number;
  };

  type DataIndex = keyof DataType;

  const handleSearch = (
    selectedKeys: string[],
    confirm: FilterDropdownProps["confirm"],
    dataIndex: DataIndex
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (
    clearFilters: () => void,
    confirm: FilterDropdownProps["confirm"]
  ) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const getColumnSearchProps = (
    dataIndex: DataIndex
  ): TableColumnType<DataType> => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div style={{ padding: 8 }} onKeyDown={(e) => e.stopPropagation()}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(selectedKeys as string[], confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(selectedKeys as string[], confirm, dataIndex)
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90, backgroundColor: COLORS.PRIMARY }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters, confirm)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
            style={{color: COLORS.PRIMARY}}
          >
            Filter
          </Button> */}
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
            style={{ color: COLORS.PRIMARY }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined
        style={{ color: filtered ? COLORS.PRIMARY : undefined }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <strong dangerouslySetInnerHTML={{ __html: text }} />
      ) : (
        <span dangerouslySetInnerHTML={{ __html: text }} />
      ),
  });

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      width: 60,
      dataIndex: "no",
    },
    {
      title: "Question",
      dataIndex: "question",
      width: 250,
      ...getColumnSearchProps("question"),
    },
    {
      title: "Options",
      dataIndex: "options",
      width: 300,
      render(value, record, index) {
        return {
          children: [
            <>
              {value.map((item: any, index: any) => {
                return (
                  <Flex key={index}>
                    <span>{item.option}</span>
                    <span style={{ marginRight: 12 }}>{"."}</span>
                    <span
                      style={{ marginRight: 12, textAlign: "justify" }}
                      dangerouslySetInnerHTML={{ __html: item.value }}
                    />
                  </Flex>
                );
              })}
            </>,
          ],
        };
      },
    },
    {
      title: "Answer",
      dataIndex: "answer",
      width: 70,
    },
    {
      title: "Score If Correct",
      dataIndex: "score_if_correct",
      width: 70,
    },
    {
      title: "Score If Wrong",
      dataIndex: "score_if_wrong",
      width: 70,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      width: 60,
      render: (value) => (
        <>
          <span style={{ marginRight: 12 }}>
            <EditTwoTone
              twoToneColor="#e5e500"
              onClick={() => editModal(value)}
            />
          </span>
          {/* <span style={{ marginRight: 12 }}>
            <DeleteTwoTone
              twoToneColor="#ff0000"
              onClick={() =>
                Modal.confirm({
                  title: "Delete User",
                  content: "Are you sure you want to delete this user?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleOnDelete(value.id),
                  okButtonProps: {
                    icon: <DeleteFilled />,
                    style: { backgroundColor: "#CB0B0C" },
                  },
                })
              }
            />
          </span> */}
        </>
      ),
    },
  ];

  const handleChangeAnswer = (value: string, setFieldValue: any) => {
    console.log(`selected ${value}`);
    setFieldValue("answer", value);
  };
  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  const showModal = () => {
    setFormQuestion(initialformQuestion);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = (value: any) => {
    console.log("edit", value);
    setFormQuestion({
      id: value.id,
      question: value.question,
      options: value.options,
      answer: value.answer,
      score_if_correct: value.score_if_correct,
      score_if_wrong: value.score_if_wrong,
    });
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleAddOptions = (values: any, setFieldValue: any) => {
    setAnswer([
      ...answer,
      {
        value: String.fromCharCode(answer.length + 97),
        label: String.fromCharCode(answer.length + 97),
      },
    ]);
    setFieldValue("options", {
      ...values,
      options: [
        ...values.options,
        {
          option: String.fromCharCode(values.options.length + 97),
          value: "",
        },
      ],
    });
  };

  const handleDeleteOptions = (values: any, setFieldValue: any) => {
    if (values.options.length === 2) {
      return;
    }
    const options = values.options.slice(0, values.options.length - 1);
    const answers = answer.slice(0, values.options.length - 1);
    setFieldValue("options", options);
    setAnswer(answers);
  };

  const handleSubmitCreateQuestion = (values: any) => {
    handlePostQuestionBank(values);
  };

  const handleSubmitUpdateQuestion = (values: any) => {
    handlePutQuestionBank(values.id, values);
  };

  const handleOnClickCreate = (e: any) => {
    setFormQuestion((prev: {}) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  const isEveryOptionValueEmpty = (values: any) => {
    return values.options.some(
      (option: any) => !option.value || option.value.trim() === ""
    );
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    question: yup
      .string()
      .required(translate("QuestionRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("QuestionLength")} ${max} ${translate("Characters")}`
      ),
    answer: yup
      .string()
      .required(translate("AnswerRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("AnswerLength")} ${max} ${translate("Characters")}`
      ),
    score_if_correct: yup
      .number()
      .required(translate("PassingScoreRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("PassingScoreLength")} ${max} ${translate("Characters")}`
      ),
    score_if_wrong: yup
      .number()
      .required(translate("WrongScoreRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("WrongScoreLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "40px" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Question Bank</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Question Bank",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C" }}
          >
            <PlusOutlined />
            Create Question
          </Button>
        </div>
      </div>
      <div className="category-content">
        <div className="category-table">
          <Menu
            style={{ marginTop: 28 }}
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            items={items}
          />
          <Table
            columns={columns}
            dataSource={dataQuestion}
            scroll={{ x: 1000 }}
            style={{ padding: 16, width: "100%" }}
            loading={!dataQuestion ? true : false}
          />
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Add Question"
        open={isModalVisible}
        width={1000}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialformQuestion}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleSubmitCreateQuestion(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <label className="required" style={{ marginBottom: 16 }}>
                Question
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                value={values.question}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("question", "");
                    return;
                  }
                  setFieldValue("question", value);
                }}
              />
              {errors.question && (
                <div className={"error"}>{errors.question.toString()}</div>
              )}
              <label className="required" style={{ marginBottom: 16 }}>
                Correct Answer
              </label>
              <Select
                placeholder="Select Answer"
                style={{ width: 950, marginBottom: 12 }}
                value={values.answer}
                onChange={(value) => handleChangeAnswer(value, setFieldValue)}
                options={answer}
                size={formSize}
              />
              {errors.answer && (
                <div className={"error"}>{errors.answer.toString()}</div>
              )}
              <Card style={{ width: 950, background: "#f2f2f2" }}>
                <Row style={{ marginBottom: 16 }}>
                  <Col span={2}>Options</Col>
                  <Col span={20}>
                    <p style={{ marginLeft: 350 }}>Value</p>
                  </Col>
                  <Col span={2}>
                    <p style={{ marginLeft: 45 }}>#</p>
                  </Col>
                </Row>
                {values.options.map((item: any) => {
                  return (
                    <Row key={item.option} style={{ marginBottom: 16 }}>
                      <Col span={2}>
                        <p style={{ marginTop: 25 }}>{item.option}</p>
                      </Col>
                      <Col span={20}>
                        <ReactQuillNoSSR
                          modules={TOOLBAR_OPTION_QUILL}
                          style={{ marginBottom: 12 }}
                          value={item.value}
                          onChange={(value: string) => {
                            if (isEmptyContent(value)) {
                              setFieldValue(
                                "options",
                                values.options.map((option: any) => {
                                  if (option.option === item.option) {
                                    return {
                                      ...option,
                                      value: "",
                                    };
                                  }
                                  return option;
                                })
                              );
                              return;
                            }
                            setFieldValue(
                              "options",
                              values.options.map((option: any) => {
                                if (option.option === item.option) {
                                  return {
                                    ...option,
                                    value: value,
                                  };
                                }
                                return option;
                              })
                            );
                          }}
                        />
                      </Col>
                      <Col span={2}>
                        <DeleteTwoTone
                          twoToneColor="#a6a6a6"
                          style={{
                            marginLeft: 45,
                            marginTop: 25,
                            cursor: "pointer",
                          }}
                          onClick={() =>
                            handleDeleteOptions(values, setFieldValue)
                          }
                        />
                      </Col>
                    </Row>
                  );
                })}
                <Button
                  type="primary"
                  style={{ backgroundColor: "#CB0B0C" }}
                  icon={<PlusOutlined />}
                  onClick={() => handleAddOptions(values, setFieldValue)}
                >
                  Add Options
                </Button>
              </Card>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col span={8}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Correct Score"}
                    title={"Correct Score"}
                    type={"number"}
                    name={"score_if_correct"}
                  />
                </Col>
                <Col span={8}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Wrong Score"}
                    title={"Wrong Score"}
                    type={"number"}
                    name={"score_if_wrong"}
                  />
                </Col>
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid
                      ? "#a6a6a6"
                      : isEveryOptionValueEmpty(values)
                      ? "#a6a6a6"
                      : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPost}
                  disabled={!isValid || isEveryOptionValueEmpty(values)}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Edit Question"
        open={isEditModalVisible}
        width={1000}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formQuestion}
          enableReinitialize
          onSubmit={(values) => {
            handleSubmitUpdateQuestion(values);
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <label className="required" style={{ marginBottom: 16 }}>
                Question
              </label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                style={{ marginBottom: 12 }}
                value={values.question}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("question", "");
                    return;
                  }
                  setFieldValue("question", value);
                }}
              />
              {errors.question && (
                <div className={"error"}>{errors.question.toString()}</div>
              )}
              <label className="required" style={{ marginBottom: 16 }}>
                Correct Answer
              </label>
              <Select
                placeholder="Select Answer"
                style={{ width: 950, marginBottom: 12 }}
                value={values.answer}
                onChange={(value) => handleChangeAnswer(value, setFieldValue)}
                options={answer}
                size={formSize}
              />
              {errors.answer && (
                <div className={"error"}>{errors.answer.toString()}</div>
              )}
              <Card style={{ width: 950, background: "#f2f2f2" }}>
                <Row style={{ marginBottom: 16 }}>
                  <Col span={2}>Options</Col>
                  <Col span={20}>
                    <p style={{ marginLeft: 350 }}>Value</p>
                  </Col>
                  <Col span={2}>
                    <p style={{ marginLeft: 45 }}>#</p>
                  </Col>
                </Row>
                {values.options.map((item: any) => {
                  return (
                    <Row key={item.option} style={{ marginBottom: 16 }}>
                      <Col span={2}>
                        <p style={{ marginTop: 25 }}>{item.option}</p>
                      </Col>
                      <Col span={20}>
                        <ReactQuillNoSSR
                          modules={TOOLBAR_OPTION_QUILL}
                          style={{ marginBottom: 12 }}
                          value={item.value}
                          onChange={(value: string) => {
                            if (isEmptyContent(value)) {
                              setFieldValue(
                                "options",
                                values.options.map((option: any) => {
                                  if (option.option === item.option) {
                                    return {
                                      ...option,
                                      value: "",
                                    };
                                  }
                                  return option;
                                })
                              );
                              return;
                            }
                            setFieldValue(
                              "options",
                              values.options.map((option: any) => {
                                if (option.option === item.option) {
                                  return {
                                    ...option,
                                    value: value,
                                  };
                                }
                                return option;
                              })
                            );
                          }}
                        />
                      </Col>
                      <Col span={2}>
                        <DeleteTwoTone
                          twoToneColor="#a6a6a6"
                          style={{
                            marginLeft: 45,
                            marginTop: 25,
                            cursor: "pointer",
                          }}
                          onClick={() =>
                            handleDeleteOptions(values, setFieldValue)
                          }
                        />
                      </Col>
                    </Row>
                  );
                })}
                <Button
                  type="primary"
                  style={{ backgroundColor: "#CB0B0C" }}
                  icon={<PlusOutlined />}
                  onClick={() => handleAddOptions(values, setFieldValue)}
                >
                  Add Options
                </Button>
              </Card>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col span={8}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Correct Score"}
                    title={"Correct Score"}
                    type={"number"}
                    name={"score_if_correct"}
                  />
                </Col>
                <Col span={8}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Wrong Score"}
                    title={"Wrong Score"}
                    type={"number"}
                    name={"score_if_wrong"}
                  />
                </Col>
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid
                      ? "#a6a6a6"
                      : isEveryOptionValueEmpty(values)
                      ? "#a6a6a6"
                      : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPut}
                  disabled={!isValid || isEveryOptionValueEmpty(values)}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default QuestionBankPage;
