import CustomButton from "@/shared/components/CustomButton";
import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import { Form, Input, Space } from "antd";
import TextWelcomeLogin from "./components/TextWelcomeLogin";
import TextLoginComponent from "./components/TextLoginComponent";
import useRegisterForm from "../hooks/useRegisterForm";

const FormRegister = () => {
  const { mutationQuery, handleOnSubmit } = useRegisterForm();
  // eslint-disable-next-line no-unused-vars
  const { isPending, isError, error, isSuccess } = mutationQuery;

  const onFinish = (values: any) => {
    handleOnSubmit(values.email, values.password);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Space direction="vertical" size={40} style={{ display: 'flex', marginTop: '20px' }}>
      <TextWelcomeLogin />
      <Space direction="vertical" size={0} style={{ display: 'flex' }}>
        <Form
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <CustomFormItem
            label="Email"
            name="email"
            rules={[{ type: 'email', required: true, message: 'Please input your email!' }]}
          >
            <Input />
          </CustomFormItem>

          <CustomFormItem
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}
          >
            <Input.Password />
          </CustomFormItem>

          <TextLoginComponent />

          <CustomFormItem>
            <CustomButton 
              typeButton="primary"
              loading={isPending}
              spellCheck={true}
              block
              htmlType="submit">
              Register
            </CustomButton>
          </CustomFormItem>
        </Form>
      </Space>
    </Space>
  );
};

export default FormRegister;