import { register } from "@/shared/api/mutation/register";
import { DASHBOARD_LOGIN } from "@/shared/constants/path";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { notification } from "antd";
import { useRouter } from "next/router";

const useRegisterForm = () => {
  const router = useRouter();

  const mutationQuery = useMutationHook({
    api: register,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        if (data) {
          notification.success({
            message: "Berhasil registrasi. Silahkan Login",
          });
          router.replace(DASHBOARD_LOGIN);
        }
      },
    },
  });

  const handleOnSubmit = (email: string, password: string) => {
    mutationQuery.mutate({
      email,
      password,
    });
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useRegisterForm;
