import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Input,
  Select,
  message,
  Upload,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import type { UploadProps } from "antd";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { useRouter } from "next/router";
import { IMAGE_FORMAT } from "@/shared/constants/storageStatis";

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};

const propsForum: UploadProps = {
  name: "file",
  action: "https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188",
  headers: {
    authorization: "authorization-text",
  },
  accept: IMAGE_FORMAT,
  beforeUpload: (file) => {
    console.log("file beforeUpload", file);
    const isSize = file.size / 1024 / 1024 < 2;

    if (!isSize) {
      message.error(`${file.name} size must be less than 2 MB`);
    }

    return (isSize) || Upload.LIST_IGNORE;
  },
  onChange(info) {
    const { status, response } = info.file;
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({ error: response, text: "Upload Image" });
      }
    }
  },
};

const ForumDetailPage = () => {
  const navigate = useRouter();
  const { id } = navigate.query;
  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Forum</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "Forum",
          },
          {
            title: "Detail Forum",
          },
        ]}
      />
      <Card>
        <p style={{ fontWeight: "bold", fontSize: 18 }}>Detail Forum</p>
        <p style={{ marginTop: 50 }}>
          IP-001
          <Button
            ghost
            disabled
            style={{
              color: "#66ef66",
              background: "#d1fad1",
              marginLeft: 12,
              fontWeight: "bold",
            }}
          >
            Active
          </Button>
        </p>
        <b>Category</b>
        <p style={{ color: "gray" }}>Sekuritas</p>
        <b>Company Name</b>
        <p style={{ color: "gray" }}>BNI Sekuritas</p>
        <b>Publish Date</b>
        <p style={{ color: "gray" }}>Jan 26,2024</p>
        <b>Description</b>
        <p style={{ color: "gray" }}>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus
          veritatis corrupti officia ipsum porro veniam illo vel quis. Dolores
          minima autem nam dolorum architecto culpa alias nisi iure aperiam
          pariatur.
        </p>
        <p style={{ fontWeight: "bold" }}>Upload Images</p>
        <Upload {...propsForum}>
          <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>
      </Card>
    </Space>
  );
};

export default ForumDetailPage;
