import {
  deleteContentProvider,
  updateContentProvider,
} from "@/shared/api/mutation/content-provider";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteContentProvider = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteContentProvider,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["content-provider"] });
          notification.success({
            message: "Profile Content Provider berhasil dihapus",
          });
        }
      },
    },
  });

  const handleDeleteContentProvider = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    handleDeleteContentProvider,
  };
};

export default useDeleteContentProvider;
