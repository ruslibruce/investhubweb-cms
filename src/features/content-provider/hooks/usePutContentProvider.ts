import { updateContentProvider } from "@/shared/api/mutation/content-provider";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutContentProvider = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: updateContentProvider,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["content-provider"] });
          notification.success({
            message: "Profile Content Provider berhasil diupdate",
          });
        }
      },
    },
  });

  const handleUpdateContentProvider = (data: {}, id: string) => {
    mutationQuery.mutate({ data, id });
  };

  return {
    mutationQuery,
    handleUpdateContentProvider,
  };
};

export default usePutContentProvider;
