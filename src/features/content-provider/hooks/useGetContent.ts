import { fetchContentProvider } from "@/shared/api/fetch/content-provider";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchContentProvider();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDataContent = (initialData?: any) => {
  const fetchDataContentProvider = fetchContentProvider();

  const fetchQuery = useFetchHook({
    keys: fetchDataContentProvider.key,
    api: fetchDataContentProvider.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetDataContent;
