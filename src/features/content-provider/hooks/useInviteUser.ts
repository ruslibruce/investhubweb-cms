import { inviteUser } from "@/shared/api/mutation/content-provider";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useInviteUser = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: inviteUser,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["cms-admin"] });
        if (data) {
          notification.success({
            message: "Content Provider berhasil diinvite",
          });
        }
      },
    },
  });

  const handlePostContentProviderInvite = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostContentProviderInvite,
  };
};

export default useInviteUser;
