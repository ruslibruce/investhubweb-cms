import { addContentProvider } from "@/shared/api/mutation/content-provider";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostContentProvider = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: addContentProvider,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["content-provider"] });
        if (data) {
          notification.success({
            message: "Content Provider berhasil dibuat",
          });
        }
      },
    },
  });

  const handlePostContentProvider = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostContentProvider,
  };
};

export default usePostContentProvider;
