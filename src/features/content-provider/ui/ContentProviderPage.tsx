import FormInputFormik from "@/components/FormInputFormik";
import usePutContentProvider from "@/features/content-provider/hooks/usePutContentProvider";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  regexEmail,
  regexFacebookUrl,
  regexInstagramUrl,
  regexPhoneNumber,
  regexTiktokUrl,
  regexTwitterUrl,
  regexUrl,
  regexYoutubeUrl,
} from "@/shared/utils/helper";
import {
  DeleteFilled,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  FolderOpenTwoTone,
  MailTwoTone,
  PlusOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { TableColumnsType } from "antd";
import {
  Button,
  Card,
  Col,
  Flex,
  Input,
  message,
  Modal,
  Row,
  Select,
  Space,
  Table,
  Upload,
} from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import * as yup from "yup";
import useGetContent from "../hooks/useGetContent";
import useGetContentProviderType from "../hooks/useGetContentProviderType";
import useInviteUser from "../hooks/useInviteUser";
import usePostContentProvider from "../hooks/usePostContentProvider";
import { imageIAM } from "@/shared/constants/imageUrl";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import useDeleteContentProvider from "../hooks/useDeleteContentProvider";
const { Dragger } = Upload;

const initialCP = {
  name: "",
  description: "",
  website: "",
  contact_name: "",
  contact_email: "",
  content_provider_type: "",
};

const initialInviteCP = {
  name: "",
  email: "",
  content_provider_id: "",
  role: "cp-admin",
};

const ContentProviderPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const { fetchQuery } = useGetContent();
  const { isLoading, data: dataContentProvider } = fetchQuery;
  const { mutationQuery, handlePostContentProvider } = usePostContentProvider();
  const { isSuccess, isPending: isLoadingAddCP } = mutationQuery;
  const { mutationQuery: mutationInvite, handlePostContentProviderInvite } =
    useInviteUser();
  const { isSuccess: isSuccessInvite, isPending: isLoadingInvite } =
    mutationInvite;
  const { handleUpdateContentProvider, mutationQuery: mutationQueryUpdateCP } =
    usePutContentProvider();
  const { isSuccess: isSuccessUpdateCP, isPending: isLoadingUpdateCP } =
    mutationQueryUpdateCP;
  const { handleDeleteContentProvider, mutationQuery: mutationQueryDelete } =
    useDeleteContentProvider();
  const { isPending: isLoadingDelete } = mutationQueryDelete;
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isInviteModalVisible, setIsInviteModalVisible] = useState(false);
  const [formData, setFormData] = useState<any>(initialCP);
  const [formDataInvite, setFormDataInvite] = useState<any>(initialInviteCP);
  const { fetchQueryGetContentProviderType } = useGetContentProviderType([]);
  const { data: dataContentProviderType } = fetchQueryGetContentProviderType;
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccess) {
      setIsAddModalVisible(false);
    }

    if (isSuccessInvite) {
      setIsInviteModalVisible(false);
    }

    if (isSuccessUpdateCP) {
      setIsEditModalVisible(false);
    }
  }, [isSuccess, isSuccessInvite, isSuccessUpdateCP]);

  const handleTableRowClick = (value: any) => {
    setFormDataInvite({
      ...initialInviteCP,
      content_provider_id: value.id,
    });
    setIsInviteModalVisible(true);
  };

  const handleInviteUser = (values: any) => {
    handlePostContentProviderInvite(values);
  };

  type DataType = {
    key: React.Key;
    no: number;
    name: string;
    website: string;
    contact_name: string;
    contact_email: string;
    content_keywords: string;
    is_active: boolean;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      width: 60,
      dataIndex: "no",
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Website",
      width: 200,
      dataIndex: "website",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Contact Name",
      dataIndex: "contact_name",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Contact Email",
      dataIndex: "contact_email",
      // defaultSortOrder: "descend",
      width: 200,
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Active",
      dataIndex: "is_active",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
      render: (value) => (
        <span
          style={{
            color: value ? "green" : "red",
            fontWeight: "bold",
          }}
        >
          {value ? "Active" : "Inactive"}
        </span>
      ),
    },
    {
      title: "Content Keywords",
      dataIndex: "content_keywords",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      width: 200,
      render: (value) => (
        <Flex gap={10}>
          <span>
            <MailTwoTone onClick={() => handleTableRowClick(value)} />
          </span>
          <span>
            <EditTwoTone
              twoToneColor="#e5e500"
              onClick={() => editModal(value)}
            />
          </span>
          <span style={{ marginRight: 12 }}>
            <DeleteTwoTone
              twoToneColor="#ff0000"
              onClick={() =>
                Modal.confirm({
                  title: "Delete User",
                  content: "Are you sure you want to delete this user?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleDeleteContentProvider(value.id),
                  okButtonProps: {
                    icon: <DeleteFilled />,
                    style: { backgroundColor: "#CB0B0C" },
                    loading: isLoadingDelete,
                  },
                })
              }
            />
          </span>
        </Flex>
      ),
    },
  ];

  const addModal = () => {
    setIsAddModalVisible(true);
  };

  const handleAddContentProviderSubmit = (values: any) => {
    let contentProviderType = dataContentProviderType.find(
      (item: any) => item.name == values.content_provider_type
    );
    handlePostContentProvider({
      ...values,
      content_provider_type: contentProviderType.id,
    });
  };

  const handleEditContentProviderSubmit = (values: any) => {
    let contentProviderType = dataContentProviderType.find(
      (item: any) => item.name == values.content_provider_type
    );
    handleUpdateContentProvider(
      {
        ...values,
        content_provider_type: contentProviderType.id,
      },
      values.id
    );
  };

  const editModal = (value: any) => {
    console.log("value", value);
    let contentProviderType = dataContentProviderType.find(
      (item: any) => item.id == value.content_provider_type
    );
    console.log("contentProviderType", contentProviderType);
    setFormData({
      ...initialCP,
      name: value.name,
      description: value.description,
      website: value.website,
      contact_name: value.contact_name,
      contact_email: value.contact_email,
      whatsapp: value.whatsapp,
      facebook: value.facebook,
      instagram: value.instagram,
      twitter: value.twitter,
      youtube: value.youtube,
      tiktok: value.tiktok,
      content_keywords: value.content_keywords,
      id: value.id,
      content_provider_type: contentProviderType?.name,
    });
    setIsEditModalVisible(true);
  };
  const handleCancel = () => {
    setIsAddModalVisible(false);
  };
  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };
  const handleInviteCancel = () => {
    setIsInviteModalVisible(false);
  };

  const inviteValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        100,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
  });

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        100,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
    content_provider_type: yup.string().required(translate("CPRequired")),
    description: yup
      .string()
      .required(translate("DescriptionRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("DescriptionLength")} ${max} ${translate("Characters")}`
      ),
    website: yup
      .string()
      .matches(regexUrl(), translate("WebsiteMatches"))
      .max(
        100,
        ({ max }) =>
          `${translate("WebsiteLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("WebsiteRequired")),
    contact_name: yup
      .string()
      .required(translate("ContactNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("ContactNameRequired")} ${max} ${translate(
            "Characters"
          )}`
      ),
    contact_email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("ContactEmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("ContactEmailRequired")),
    whatsapp: yup
      .string()
      .max(
        15,
        ({ max }) =>
          `${translate("WhatsappLength")} ${max} ${translate("Characters")}`
      )
      .matches(
        regexPhoneNumber(),
        `Whatsapp ${translate("PhoneNumberMatches")}`
      )
      .nullable(),
    facebook: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("FacebookLength")} ${max} ${translate("Characters")}`
      )
      .matches(regexFacebookUrl(), `Facebook ${translate("URLMatches")}`)
      .nullable(),
    instagram: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("InstagramLength")} ${max} ${translate("Characters")}`
      )
      .matches(regexInstagramUrl(), `Instagram ${translate("URLMatches")}`)
      .nullable(),
    twitter: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("TwitterLength")} ${max} ${translate("Characters")}`
      )
      .matches(regexTwitterUrl(), `Twitter ${translate("URLMatches")}`)
      .nullable(),
    youtube: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("YoutubeLength")} ${max} ${translate("Characters")}`
      )
      .matches(regexYoutubeUrl(), `Youtube ${translate("URLMatches")}`)
      .nullable(),
    tiktok: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("TiktokLength")} ${max} ${translate("Characters")}`
      )
      .matches(regexTiktokUrl(), `Tiktok ${translate("URLMatches")}`)
      .nullable(),
    content_keywords: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("ContentKeywordLength")} ${max} ${translate(
            "Characters"
          )}`
      )
      .nullable(),
    photo: yup.string().nullable(),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row
        style={{
          justifyContent: "space-between",
        }}
      >
        <Col className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>
            Content Provider
          </p>
        </Col>
        <Col className="button-post-category">
          <Button
            size={formSize}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
            icon={<PlusOutlined />}
          >
            Add Content Provider
          </Button>
        </Col>
      </Row>
      <div className="category-content">
        {/* <div>
          <Card style={{ width: 200 }} className="category-search">
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>
              Search Content Provider
            </p>
            <p style={{ marginTop: 18 }}>Role Name</p>
            <Input
              size={formSize}
              placeholder="Role Name"
            />

            <Button
              size={formSize}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Submit
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          <Card style={{ width: "100%" }}>
            <b style={{ fontSize: 18 }}>Content Provider</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataContentProvider}
              scroll={{ x: 1800 }}
              loading={!dataContentProvider ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Content Provider"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialCP}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddContentProviderSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Name"}
                title={"Name"}
                type={"text"}
                name={"name"}
              />
              <label
                className="required"
                style={{ marginBottom: 6, marginTop: 18 }}
              >
                Content Provider Type
              </label>
              <Select
                placeholder="Content Provider Type"
                style={{ width: "100%" }}
                value={values.content_provider_type}
                onChange={(value) => {
                  setFieldValue("content_provider_type", value);
                }}
                options={dataContentProviderType}
                size={formSize}
              />
              {errors.content_provider_type && (
                <div className={"error"}>
                  {errors.content_provider_type.toString()}
                </div>
              )}
              <Field
                component={FormInputFormik}
                placeholder={"Description"}
                title={"Description"}
                type={"text"}
                textArea={true}
                name={"description"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"URL Website"}
                title={"URL Website"}
                type={"url"}
                name={"website"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Contact Name"}
                title={"Contact Name"}
                type={"text"}
                name={"contact_name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Contact Email"}
                title={"Contact Email"}
                type={"email"}
                name={"contact_email"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAddCP}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Content Provider"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formData}
          enableReinitialize
          onSubmit={(values) => {
            handleEditContentProviderSubmit(values);
          }}
        >
          {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Name"}
                title={"Name"}
                type={"text"}
                name={"name"}
              />
              <label
                className="required"
                style={{ marginBottom: 6, marginTop: 18 }}
              >
                Content Provider Type
              </label>
              <Select
                size={formSize}
                placeholder="Content Provider Type"
                style={{ width: "100%" }}
                value={values.content_provider_type}
                onChange={(value) =>
                  setFieldValue("content_provider_type", value)
                }
                options={dataContentProviderType}
              />
              {errors.content_provider_type && (
                <div className={"error"}>
                  {errors.content_provider_type.toString()}
                </div>
              )}
              <Field
                component={FormInputFormik}
                placeholder={"Description"}
                title={"Description"}
                type={"text"}
                textArea={true}
                name={"description"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"URL Website"}
                title={"URL Website"}
                type={"url"}
                name={"website"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Contact Name"}
                title={"Contact Name"}
                type={"text"}
                name={"contact_name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Contact Email"}
                title={"Contact Email"}
                type={"email"}
                name={"contact_email"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Whatsapp"}
                title={"Whatsapp"}
                type={"text"}
                name={"whatsapp"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Facebook"}
                title={"Facebook"}
                type={"url"}
                name={"facebook"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Instagram"}
                title={"Instagram"}
                type={"url"}
                name={"instagram"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Twitter"}
                title={"Twitter"}
                type={"url"}
                name={"twitter"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Youtube"}
                title={"Youtube"}
                type={"url"}
                name={"youtube"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Tiktok"}
                title={"Tiktok"}
                type={"url"}
                name={"tiktok"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={"pasar modal, saham ..."}
                title={"Content Keywords"}
                type={"text"}
                name={"content_keywords"}
                isRequired={false}
              />
              {!values.photo ? (
                <>
                  <p style={{ marginTop: 18 }}>Upload Photo (Max. 2MB)</p>
                  <Dragger
                    {...{
                      name: "file",
                      multiple: true,
                      maxCount: 1,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-iam/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser?.token}`,
                      },
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        let filterType = file.type.split("/")[0];
                        const isImage = filterType === "image";
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isImage) {
                          message.error(`${file.name} is not a image file`);
                        }

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return (isImage && isSize) || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "photo"),
                      onDrop(e) {
                        console.log("Dropped files", e.dataTransfer.files);
                      },
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                </>
              ) : (
                <Col span={12} style={{ marginTop: 10 }}>
                  <p>Photo Profile</p>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={
                        values.photo.includes("https")
                          ? values.photo
                          : `${imageIAM}/${values.photo}`
                      }
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("photo", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              {errors.photo && (
                <div className={"error"}>{errors.photo.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingUpdateCP}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Invite User */}
      <Modal
        centered
        title="Invite User"
        open={isInviteModalVisible}
        width={800}
        onCancel={handleInviteCancel}
        footer={null}
      >
        <Formik
          validationSchema={inviteValidationSchema}
          initialValues={formDataInvite}
          enableReinitialize
          onSubmit={(values) => {
            handleInviteUser(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Name"}
                title={"Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
              />
              <Button
                type="primary"
                style={{ backgroundColor: "#CB0B0C", marginTop: 20 }}
                block
                spellCheck={true}
                loading={isLoadingInvite}
                htmlType="submit"
                onClick={(event: any) => handleSubmit(event)}
                disabled={!isValid}
              >
                {translate("Invite")}
              </Button>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default ContentProviderPage;
