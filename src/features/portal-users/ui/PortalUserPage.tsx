import useDeleteUser from "@/features/cms-admin/hooks/useDeleteUser";
import { USER_MANAGEMENT_PORTAL_USERS_DETAIL } from "@/shared/constants/path";
import { COLORS } from "@/shared/styles/color";
import {
  DeleteFilled,
  DeleteTwoTone,
  EyeOutlined,
  SaveOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import type { InputRef, TableColumnsType, TableColumnType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Select,
  Space,
  Table,
} from "antd";
import { FilterDropdownProps } from "antd/es/table/interface";
import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import Highlighter from "react-highlight-words";
import "react-quill/dist/quill.snow.css";
import useGetPortalUsers from "../hooks/useGetPortalUsers";

const PortalUserPage = () => {
  const navigate = useRouter();
  const { fetchQuery } = useGetPortalUsers();
  const { isLoading, data: dataPortalUsers } = fetchQuery;

  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const { handleOnDelete } = useDeleteUser();
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef<InputRef>(null);
  console.log("portalUsers", dataPortalUsers);

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
  };

  const handleUserDetail = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    navigate.push(`${USER_MANAGEMENT_PORTAL_USERS_DETAIL}/${value.id}`);
  };

  type DataType = {
    key: React.Key;
    no: number;
    name: string;
    email: string;
    contactNumber: string;
    role: string;
  };

  type DataIndex = keyof DataType;

  const handleSearch = (
    selectedKeys: string[],
    confirm: FilterDropdownProps["confirm"],
    dataIndex: DataIndex
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (
    clearFilters: () => void,
    confirm: FilterDropdownProps["confirm"]
  ) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const getColumnSearchProps = (
    dataIndex: DataIndex
  ): TableColumnType<DataType> => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div style={{ padding: 8 }} onKeyDown={(e) => e.stopPropagation()}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(selectedKeys as string[], confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(selectedKeys as string[], confirm, dataIndex)
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90, backgroundColor: COLORS.PRIMARY }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters, confirm)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
            style={{color: COLORS.PRIMARY}}
          >
            Filter
          </Button> */}
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
            style={{ color: COLORS.PRIMARY }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined
        style={{ color: filtered ? COLORS.PRIMARY : undefined }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
      ...getColumnSearchProps("name"),
    },
    {
      title: "Email",
      dataIndex: "email",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Contact Number",
      dataIndex: "phone",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Role",
      dataIndex: "role",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (value) => (
        <Flex gap={10}>
          <span style={{ marginLeft: 12 }}>
            <EyeOutlined onClick={(e) => handleUserDetail(e, value)} />
          </span>
          <span style={{ marginRight: 12 }}>
            <DeleteTwoTone
              twoToneColor="#ff0000"
              onClick={() =>
                Modal.confirm({
                  title: "Delete User",
                  content: "Are you sure you want to delete this user?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleOnDelete(value.id),
                  okButtonProps: {
                    icon: <DeleteFilled />,
                    style: { backgroundColor: "#CB0B0C" },
                  },
                })
              }
            />
          </span>
        </Flex>
      ),
    },
  ];

  const addModal = () => {
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const editModal = (value: any) => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleDeleteContentProviderSubmit = () => {};

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Portal Users</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "User Management",
              },
              {
                title: "Portal Users",
              },
            ]}
          />
        </div>
      </div>
      <div className="category-content">
        {/* <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>Search User</p>
            <p>Name</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Name" />
            <p style={{ marginTop: "18px" }}>Contact Number</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Contact Number" />
            <p style={{ marginTop: 18 }}>Role</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Role" />
            <p style={{ marginTop: 12 }}>Status</p>
            <Select
              size={isSmallScreen ? "small" : "middle"}
              defaultValue="active"
              style={{ width: "100%" }}
              onChange={handleChangeStatus}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-active", label: "Non-Active" },
              ]}
            />
            <Button size={isSmallScreen ? "small" : "middle"} type="primary" style={{ backgroundColor: "#CB0B0C", marginTop: 25, width: "100%" }}>
              Submit
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>User List</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataPortalUsers}
              scroll={{ x: 200 }}
              loading={!dataPortalUsers ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add User"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            // onClick={handleAddNewsSubmit}
          >
            Save
          </Button>,
        ]}
      >
        <p style={{ marginBottom: 6 }}>Name</p>
        <Input style={{ height: 50 }} placeholder="Name" />
        <p style={{ marginTop: 18 }}>Email</p>
        <Input style={{ height: 50 }} placeholder="Email" />
        <p style={{ marginTop: 18 }}>Name Content Provider</p>
        <Input style={{ height: 50 }} placeholder="Name Content Provider" />
        <p style={{ marginTop: 18 }}>Role</p>
        <Select
          placeholder="Select Role"
          style={{ width: 750, height: 50 }}
          onChange={handleChangeStatus}
          options={[
            { value: "superadmin", label: "Super Admin" },
            { value: "portal admin", label: "Portal Admin" },
            { value: "kurator", label: "Kurator" },
            { value: "cp", label: "CP Admin" },
          ]}
        />
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Edit News"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            onClick={handleCancel}
          >
            Save
          </Button>,
        ]}
      >
        <p style={{ marginBottom: 6 }}>Name</p>
        <Input style={{ height: 50 }} placeholder="Name" />
        <p style={{ marginTop: 18 }}>Email</p>
        <Input style={{ height: 50 }} placeholder="Email" />
        <p style={{ marginTop: 18 }}>Name Content Provider</p>
        <Input style={{ height: 50 }} placeholder="Name Content Provider" />
        <p style={{ marginTop: 18 }}>Role</p>
        <Select
          placeholder="Select Role"
          style={{ width: 750, height: 50 }}
          onChange={handleChangeStatus}
          options={[
            { value: "superadmin", label: "Super Admin" },
            { value: "portal admin", label: "Portal Admin" },
            { value: "kurator", label: "Kurator" },
            { value: "cp", label: "CP Admin" },
          ]}
        />
        <p style={{ marginTop: 18 }}>Status</p>
        <Button
          ghost
          disabled
          style={{
            color: "#66ef66",
            background: "#d1fad1",
            fontWeight: "bold",
            marginTop: 12,
          }}
        >
          Active
        </Button>
      </Modal>
    </Space>
  );
};

export default PortalUserPage;
