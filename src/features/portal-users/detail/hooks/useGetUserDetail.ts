
import { getDetailUser } from "@/shared/api/fetch/user";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getDetailUser();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetUserDetail = (id: string, initialData?: any) => {
  const fetchDataCollectionUsers = getDetailUser(id);
  const fetchQueryUserDetail = useFetchHook({
    keys: fetchDataCollectionUsers.key,
    api: fetchDataCollectionUsers.api,
    initialData,
    options: {}
  });

  return {
    fetchQueryUserDetail
  };
};

export default useGetUserDetail;
