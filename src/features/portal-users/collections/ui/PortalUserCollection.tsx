import React, { useState } from "react";
import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Input,
  Modal,
  message,
  Flex,
  Menu,
} from "antd";
import {
  CloseOutlined,
  CheckOutlined,
  UserOutlined,
  InboxOutlined,
  BookOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
import iconBookMark from "@/shared/images/icon/bookmark.png";
import Image from "next/image";
import { useRouter } from "next/router";
import useGetCollection from "../hooks/useGetCollection";

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};

const items: MenuProps["items"] = [
  {
    label: "Profile Info",
    key: "profile",
    icon: <UserOutlined />,
  },
  {
    label: "Course",
    key: "course",
    icon: <BookOutlined />,
  },
  {
    label: "Collections",
    key: "collection",
    icon: <InboxOutlined />,
  },
];

const PortalUserCollection = () => {
  const navigate = useRouter();
  const {id} = navigate.query;
  const { fetchQuery } = useGetCollection(id as string);

  const { isLoading, data } = fetchQuery;
  const [dataCollection, setDataCollection] = useState([]);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [current, setCurrent] = useState("profile");

  React.useEffect(() => {
    setDataCollection(data);
  }, [data]);

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = () => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "0px" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Detail User</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "User Management",
          },
          {
            title: "Portal Users",
          },
          {
            title: "Collection Detail",
          },
        ]}
      />

      <Row gutter={2}>
        {dataCollection?.map((card: any) => (
          <Col span={7} key={card.key}>
            <Card
              hoverable
              style={{ width: 400, marginTop: 15 }}
              cover={
                <img
                  alt="example"
                  src={
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s"
                  }
                />
              }
            >
              <Row>
                {/* <Col span={8}> */}
                {/* <Image height={300} width={300} style={{marginLeft:15,marginTop:4}} src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s"} alt="Bookmark" /> */}
                {/* </Col> */}
                {/* <Col span={16}> */}
                <b style={{ fontSize: 18 }}>{card.title}</b>
                <p style={{ fontSize: 14 }}>{card.description}</p>
                {/* </Col> */}
              </Row>
            </Card>
          </Col>
        ))}
      </Row>
    </Space>
  );
};

export default PortalUserCollection;
