import { fetchCollectionDetail } from "@/shared/api/fetch/portal-user-collection";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchCollectionDetail();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetCollection = (id: string, initialData?: any) => {
  const fetchDataCollectionUsers = fetchCollectionDetail(id);
  const fetchQuery = useFetchHook({
    keys: fetchDataCollectionUsers.key,
    api: fetchDataCollectionUsers.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetCollection;
