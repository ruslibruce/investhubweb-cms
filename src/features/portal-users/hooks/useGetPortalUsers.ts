import { fetchPortalUsers } from "@/shared/api/fetch/portal-users";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchPortalUsers();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDataCmsAdmin = (initialData?: any) => {
  const fetchDataPortalUsers = fetchPortalUsers();

  const fetchQuery = useFetchHook({
    keys: fetchDataPortalUsers.key,
    api: fetchDataPortalUsers.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetDataCmsAdmin;
