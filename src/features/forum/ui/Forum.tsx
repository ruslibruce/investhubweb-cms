import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Input,
  Upload,
  Select,
  message,
  Modal,
  Flex,
  DatePicker,
} from "antd";
import {
  PlusOutlined,
  EditTwoTone,
  DownOutlined,
  EyeTwoTone,
  DeleteTwoTone,
  SaveOutlined,
  FolderOpenTwoTone,
} from "@ant-design/icons";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { DASHBOARD_FORUM_DETAIL } from "@/shared/constants/path";
import { useRouter } from "next/router";
import type { DatePickerProps, UploadProps } from "antd";
import HamburgerCard from "@/components/HamburgerCard";

import React, { useState } from "react";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { IMAGE_FORMAT } from "@/shared/constants/storageStatis";
const { Dragger } = Upload;

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};
const ForumPage = () => {
  const navigate = useRouter();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };

  const propsForum: UploadProps = {
    name: "file",
    multiple: true,
    action: "https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188",
    accept: IMAGE_FORMAT,
    beforeUpload: (file) => {
      console.log("file beforeUpload", file);
      const isSize = file.size / 1024 / 1024 < 2;

      if (!isSize) {
        message.error(`${file.name} size must be less than 2 MB`);
      }

      return isSize || Upload.LIST_IGNORE;
    },
    onChange(info) {
      const { status, response } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        console.log(
          `${info.file.name} file upload failed because of ${info.file.error}`
        );
        if (response && response?.message == "Unauthorized") {
          handlingErrors({ error: response, text: "Upload Image" });
        }
      }
    },
  };
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = () => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleForum = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_FORUM_DETAIL}/${value.id}`);
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Forum</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Forum",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={isSmallScreen ? "small" : "middle"}
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C" }}
          >
            <PlusOutlined />
            Create Forum
          </Button>
        </div>
      </div>
      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search Forum</p>
            <p>Code</p>
            <Input
              size={isSmallScreen ? "small" : "middle"}
              placeholder="Ex:N-001"
            />
            <p style={{ marginTop: "18px" }}>Category</p>
            <Input
              size={isSmallScreen ? "small" : "middle"}
              placeholder="Category"
            />
            <p style={{ marginTop: 18 }}>Company Name</p>
            <Input
              size={isSmallScreen ? "small" : "middle"}
              placeholder="Company Name"
            />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              size={isSmallScreen ? "small" : "middle"}
              defaultValue="active"
              style={{ width: "100%" }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-active", label: "Non-Active" },
              ]}
            />
            <Button
              size={isSmallScreen ? "small" : "middle"}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Submit
            </Button>
          </Card>
        </div>
        <div className="category-table">
          <Card
            className="card"
            style={{
              width: "100%",
              marginTop: "30px",
            }}
          >
            <div className="management-portal-card">
              <div className="management-portal-content-card">
                <p style={{ fontWeight: "bold", color: "green" }}>Active</p>

                <div>
                  {/* <p style={{ fontWeight: "bold" }}>{card.id}</p> */}
                  <p>IP-002</p>
                  <p style={{ fontWeight: "bold" }}>Mandiri Sekuritas</p>
                </div>
              </div>
              <div>
                <div className="curd-button">
                  <span>
                    <EyeTwoTone
                      twoToneColor="#a6a6a6"
                      onClick={(e) => handleForum(e, { id: "apa" })}
                    />
                  </span>
                  <span>
                    <EditTwoTone twoToneColor="#a6a6a6" onClick={editModal} />
                  </span>
                  <span>
                    <DeleteTwoTone twoToneColor="#a6a6a6" />
                  </span>
                  {/* <Button onClick={(e) => handleNews(e, { id: card.id })} icon={<DownOutlined style={{ color: "#cc0000", height: 15, width: 15 }} />}></Button> */}
                </div>
                <div className="hamburger-curd-button">
                  <HamburgerCard />
                </div>
              </div>
            </div>
          </Card>
          {/* <Card
            style={{
              width: 900,
              height: 190,
              marginTop: "12px",
            }}
          >
            <Row>
              <Col span={22}>
                <p style={{ color: "#e5e500", fontWeight: "bold" }}>Draft</p>
              </Col>
              <Col span={2}>
                <span style={{ marginRight: "10px" }}>
                  <EyeTwoTone twoToneColor="#a6a6a6" onClick={(e) => handleForum(e, { id: "apa" })} />
                </span>
                <span style={{ marginRight: 10 }}>
                  <EditTwoTone twoToneColor="#a6a6a6" onClick={editModal} />
                </span>
                <span>
                  <DeleteTwoTone twoToneColor="#a6a6a6" />
                </span>
              </Col>
            </Row>
            <Row>
              <Col span={22}>
                <p style={{}}>IP-002</p>
                <p style={{ fontWeight: "bold" }}>Mandiri Sekuritas</p>
              </Col>
              <Col span={2}>
                <Button style={{ height: 38, marginTop: 24 }} icon={<DownOutlined style={{ color: "#cc0000", height: 15, width: 15 }} />}></Button>
              </Col>
            </Row>
          </Card> */}
          {/* <Card
            style={{
              width: 900,
              height: 190,
              marginTop: "12px"
            }}>
            <Row>
              <Col span={22}>
                <p style={{ color: "red", fontWeight: "bold" }}>Non-Active</p>
              </Col>
              <Col span={2}>
                <span style={{ marginRight: "10px" }}>
                  <EyeTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={(e) => handleForum(e, { id: "apa" })}
                  />
                </span>
                <span style={{ marginRight: 10 }}>
                  <EditTwoTone twoToneColor="#a6a6a6" onClick={editModal} />
                </span>
                <span>
                  <DeleteTwoTone twoToneColor="#a6a6a6" />
                </span>
              </Col>
            </Row>
            <Row>
              <Col span={22}>
                <p style={{}}>IP-003</p>
                <p style={{ fontWeight: "bold" }}>Ciptadana Sekuritas Asia</p>
               
              </Col>
              <Col span={2}>
                <Button
                  style={{ height: 38, marginTop: 24 }}
                  icon={
                    <DownOutlined
                      style={{ color: "#cc0000", height: 15, width: 15 }}
                    />
                  }></Button>
              </Col>
            </Row>
          </Card> */}
          <Card
            style={{
              width: "100%",
              height: 70,
              marginTop: "12px",
            }}
          >
            <Row>
              <Col span={19}>
                <span>Showing 1 - 4 from 100</span>
              </Col>
              <Col span={5}>
                <div className="bottom-pagination-card">
                  <div className="pagination-card">
                    <IoIosArrowBack className="ic-next-preliminary" />
                  </div>
                  <div className={`pagination-card pagination-card-active`}>
                    1
                  </div>
                  <div className={`pagination-card`}>2</div>
                  <div className={`pagination-card`}>3</div>
                  <div className={`pagination-card`}>...</div>
                  <div className={`pagination-card`}>
                    <IoIosArrowForward className="ic-next-preliminary" />
                  </div>
                </div>
              </Col>
            </Row>
          </Card>
        </div>
      </div>
      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Forum"
        open={isModalVisible}
        width={1200}
        onCancel={handleCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            onClick={handleCancel}
          >
            Submit
          </Button>,
        ]}
      >
        <Row
          style={{ marginBottom: 24, marginTop: 32, alignItems: "center" }}
          gutter={[8, 8]}
        >
          <Col span={6}>
            {" "}
            <p>Code</p>
            <Input style={{ height: 50 }} placeholder="Input Code" />
          </Col>
          <Col span={6}>
            <p>Category</p>
            <Select
              placeholder="Select Category"
              style={{ width: 280, height: 50 }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-Active", label: "Non-Active" },
              ]}
            />
          </Col>
          <Col span={6}>
            <p>Company Name</p>
            <Select
              placeholder="Select Company Name"
              style={{ width: 280, height: 50 }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Location 1" },
                { value: "draft", label: "Location 2" },
                { value: "non-Active", label: "Location 3" },
              ]}
            />
          </Col>
          <Col span={6}>
            <p>Publish Date</p>
            <DatePicker
              onChange={onChange}
              style={{ width: 280, height: 50 }}
            />
          </Col>
        </Row>
        <p>Upload files</p>
        <Dragger {...propsForum}>
          <p className="ant-upload-drag-icon">
            <FolderOpenTwoTone twoToneColor="#737373" />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
          <p className="ant-upload-hint"></p>
        </Dragger>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update Forum"
        open={isEditModalVisible}
        width={1200}
        onCancel={handleEditCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            onClick={handleCancel}
          >
            Submit
          </Button>,
        ]}
      >
        <Row
          style={{ marginBottom: 24, marginTop: 32, alignItems: "center" }}
          gutter={[8, 8]}
        >
          <Col span={6}>
            {" "}
            <p>Code</p>
            <Input style={{ height: 50 }} placeholder="Input Code" />
          </Col>
          <Col span={6}>
            <p>Category</p>
            <Select
              placeholder="Select Category"
              style={{ width: 280, height: 50 }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-Active", label: "Non-Active" },
              ]}
            />
          </Col>
          <Col span={6}>
            <p>Company Name</p>
            <Select
              placeholder="Select Company Name"
              style={{ width: 280, height: 50 }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-Active", label: "Non-Active" },
              ]}
            />
          </Col>
          <Col span={6}>
            <p>Publish Date</p>
            <DatePicker
              onChange={onChange}
              style={{ width: 280, height: 50 }}
            />
          </Col>
        </Row>
        <p>Upload files</p>
        <Dragger {...propsForum}>
          <p className="ant-upload-drag-icon">
            <FolderOpenTwoTone twoToneColor="#737373" />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
          <p className="ant-upload-hint"></p>
        </Dragger>
      </Modal>
    </Space>
  );
};

export default ForumPage;
