import { postResetPassword } from "@/shared/api/mutation/forgotPass";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostResetPassword = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postResetPassword,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
      },
    },
  });

  const handlePostReset = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handlePostReset,
  };
};

export default usePostResetPassword;
