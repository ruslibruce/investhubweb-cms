import { DASHBOARD_LOGIN } from "@/shared/constants/path";
import IconLogo from "@/shared/images/logo/logo_investhub.webp";
import { Button, Space } from "antd";
import { Field, Formik } from "formik";
import Cookies from "js-cookie";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import * as yup from "yup";
import YupPassword from "yup-password";
import usePostResetPassword from "./hooks/usePostResetPassword";
import FormInputFormik from "@/components/FormInputFormik";
import Modal from "@/components/Modal";
YupPassword(yup); // extend yup with password method

function ResetPasswordForm() {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);
  const validateValue = JSON.parse(Cookies.get("validateTemp") || "{}");
  const { handlePostReset, mutationQuery } = usePostResetPassword();
  const { isSuccess, isPending } = mutationQuery;
  console.log("validateValue", validateValue);

  React.useEffect(() => {
    if (isSuccess) {
      setModalVisible(true);
    }
  }, [isSuccess]);

  const handleHiddenModal = () => {
    Cookies.remove("validateTemp");
    setModalVisible(!modalVisible);
    navigate.push(DASHBOARD_LOGIN);
  };

  const handleForgotPasswordForm = (values: any) => {
    let data = { ...values, ...validateValue };
    handlePostReset(data);
  };

  const forgotValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword"))
      .minLowercase(1, translate("Lower_Password"))
      .minUppercase(1, translate("Upper_Password"))
      .minNumbers(1, translate("Number_Password"))
      .minWords(1, translate("Word_Password"))
      .minSymbols(1, translate("Symbol_Password")),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password")], translate("Match_Password"))
      .required(translate("Confirm_Password")),
  });
  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "20px" }}>
      <div className="text-title">{translate("ForgotPasswordForm_Title")}</div>
      <div className="text-title-info">
        {translate("ForgotPasswordForm_Desc")}
      </div>
      <Space direction="vertical" size={0} style={{ display: "flex" }}>
        <Formik
          validationSchema={forgotValidationSchema}
          initialValues={{ password: "", password_confirmation: "" }}
          onSubmit={(values) => {
            handleForgotPasswordForm(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterPassword")}
                title={translate("LabelPasswordCreate")}
                type={"password"}
                name={"password"}
                styleForm={{marginTop: 20}}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterConfirmPassword")}
                title={translate("LabelConfirmPassword")}
                type={"password"}
                name={"password_confirmation"}
              />
              <Button
                type="primary"
                style={{ backgroundColor: "#CB0B0C", marginTop: 20 }}
                block
                spellCheck={true}
                loading={isPending}
                htmlType="submit"
                onClick={(event: any) => handleSubmit(event)}
                disabled={!isValid}
              >
                {translate("Send")}
              </Button>
            </>
          )}
        </Formik>
      </Space>
      {modalVisible && (
        <Modal
          emailSent={true}
          handleHiddenModal={handleHiddenModal}
          title={translate("Title_Modal_ForgotPasswordForm")}
          desc={translate("Desc_Modal_ForgotPasswordForm")}
          btn1={translate("BacktoLogin")}
          btn2={""}
        />
      )}
    </Space>
  );
}

export default ResetPasswordForm;
