import { getStatusColor } from "@/shared/utils/helper";
import { Breadcrumb, Button, Card, Space } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import useGetInvestmentDetail from "../hooks/useGetInvestmentDetail";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";

const InvestmentPartnerDetailPage = () => {
  const navigate = useRouter();
  const { id } = navigate.query;

  // Get
  const { fetchQuery } = useGetInvestmentDetail(id as string, {});
  const { data: investmentDetail, isLoading } = fetchQuery;

  console.log("data nih:", investmentDetail)

  if(isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(`${investmentDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(!investmentDetail) {
    return <NoData />
  }

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Investment Partner</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "Investment Partner",
          },
          {
            title: "Detail Investment Partner",
          },
        ]}
      />
      <Card loading={investmentDetail ? false : true}>
        <p style={{ fontWeight: "bold", fontSize: 18 }}>Detail Investment Partner</p>
        <p style={{ marginTop: 50 }}>
          <Button
            ghost
            disabled
            style={{
              color: "white",
              background: getStatusColor(investmentDetail?.is_active),
              fontWeight: "bold",
              marginBottom: 12,
            }}
          >
            {investmentDetail?.is_active}
          </Button>
        </p>
        <b>Category</b>
        <p style={{ color: "gray" }}>{investmentDetail?.category}</p>
        <b>Company Name</b>
        <p style={{ color: "gray" }}>{investmentDetail?.name}</p>
        <b>Link</b>
        <p style={{ color: "gray" }}>{investmentDetail?.url}</p>
        <b>Image</b>
        <Image
        src={investmentDetail?.cover_image}
        alt={investmentDetail?.name || "dummy"}
        className="flag-icon"
          width={300}
          height={300}
        />
        {/* <p style={{ fontWeight: "bold" }}>Upload Images</p>
        <Upload {...props}>
          <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload> */}
      </Card>
    </Space>
  );
};

export default InvestmentPartnerDetailPage;
