import { fetchInvestmentDetail } from "@/shared/api/fetch/investmentDetail";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchInvestmentDetail();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetInvestmentDetail = (id: string, initialData?: any) => {
  const fetchDataInvestmentDetail = fetchInvestmentDetail(id);
  const fetchQuery = useFetchHook({
    keys: fetchDataInvestmentDetail.key,
    api: fetchDataInvestmentDetail.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetInvestmentDetail;
