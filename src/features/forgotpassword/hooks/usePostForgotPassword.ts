import { postForgotPassword } from "@/shared/api/mutation/forgotPass";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostForgotPassword = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postForgotPassword,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
      },
    },
  });

  const handlePostForgot = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handlePostForgot,
  };
};

export default usePostForgotPassword;
