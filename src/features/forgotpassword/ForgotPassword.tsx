import { DASHBOARD_LOGIN, DASHBOARD_VALIDATE } from "@/shared/constants/path";
import IconBack from "@/shared/images/icon/arrow-left.webp";
import IconLogo from "@/shared/images/logo/logo_investhub.webp";
import { Button, notification, Space } from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import usePostForgotPassword from "./hooks/usePostForgotPassword";
import Modal from "@/components/Modal";
import * as yup from "yup";
import FormInputFormik from "@/components/FormInputFormik";
import { regexEmail } from "@/shared/utils/helper";
import ReCAPTCHA from "react-google-recaptcha";

function ForgotPassword() {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);
  const { handlePostForgot, mutationQuery } = usePostForgotPassword();
  const { isPending, data } = mutationQuery;
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);
  const [timeLeft, setTimeLeft] = React.useState<number>(120);
  const [isTimerRun, setIsTimerRun] = React.useState<boolean>(false);

  // Convert timeLeft into minutes and seconds
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;

  React.useEffect(() => {
    if (!isTimerRun) {
      return;
    }
    // Exit early when we reach 0
    if (timeLeft === 0) {
      setIsTimerRun(false);
      setTimeLeft(120);
      return;
    }

    // Save intervalId to clear the interval when the component re-renders
    const intervalId = setInterval(() => {
      // Decrease time left by one second
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // Clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // Add timeLeft as a dependency to re-run the effect when we update it
  }, [timeLeft, isTimerRun]);

  React.useEffect(() => {
    console.log(data);
    let dataSukses = data as any;

    if (dataSukses?.status === "success") {
      setModalVisible(true);
    }
  }, [data]);

  const handleHiddenModal = () => {
    setToken("");
    navigate.push(DASHBOARD_VALIDATE);
    setModalVisible(!modalVisible);
  };

  const handleForgotPassword = (values: any) => {
    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: translate("Recaptcha"),
      });
      return;
    }

    setToken("");
    // setIsTimerRun(true);
    handlePostForgot({
      ...values,
      "g-recaptcha-response": captchaRef.current.getValue(),
    });
    captchaRef.current.reset();
  };

  const forgotValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
  });

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    // alert(tokenCaptcha);
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: translate("Recaptcha_Expired"),
    });
  };

  return (
    <Space
      direction="vertical"
      size={40}
      style={{ display: "flex", marginTop: "20px" }}
    >
      <Image
        onClick={() => navigate.push(DASHBOARD_LOGIN)}
        src={IconBack}
        alt="icon-back"
        className="icon-back"
      />
      <div className="text-title">{translate("ForgotPassword_Title")}</div>
      <div className="text-title-info">{translate("Desc_ForgotPassword")}</div>
      <Space direction="vertical" size={0} style={{ display: "flex" }}>
        <Formik
          validationSchema={forgotValidationSchema}
          initialValues={{ email: "" }}
          onSubmit={(values) => {
            handleForgotPassword(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
                styleForm={{ marginTop: 20 }}
              />
              <div className={"position-recaptcha"}>
                <ReCAPTCHA
                  sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
                  onChange={onVerify}
                  onExpired={onExpire}
                  ref={captchaRef}
                />
              </div>
              <Button
                type="primary"
                style={{ backgroundColor: "#CB0B0C", marginTop: 20 }}
                block
                spellCheck={true}
                loading={isPending}
                htmlType="submit"
                onClick={(event: any) => handleSubmit(event)}
                disabled={isPending || !isValid}
              >
                {isTimerRun
                  ? `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`
                  : translate("Send")}
              </Button>
            </>
          )}
        </Formik>
      </Space>
      {modalVisible && (
        <Modal
          emailSent={true}
          handleHiddenModal={handleHiddenModal}
          title={translate("Title_Modal_ForgotPassword")}
          desc={translate("Desc_Modal_ForgotPassword")}
          btn1={translate("Finished")}
          btn2={""}
        />
      )}
    </Space>
  );
}

export default ForgotPassword;
