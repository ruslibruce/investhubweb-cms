import { fetchDetailNews } from "@/shared/api/fetch/newsDetail";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailNews();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDetailNews = (id: string, initialData?: any) => {
  const fetchDataColors = fetchDetailNews(id);
  const fetchQuery = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetDetailNews;
