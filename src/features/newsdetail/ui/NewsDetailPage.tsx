import useGetNewsCategory from "@/features/news/hooks/useGetNewsCategory";
import { DASHBOARD_NEWS } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { checkIsEmptyObject, getStatusColor } from "@/shared/utils/helper";
import {
  ArrowRightOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Row,
  Space,
  Spin,
  Typography,
} from "antd";
import moment from "moment";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetDetailNews from "../hooks/useGetDetailNews";
import useUpdateNewsDetail from "../hooks/useUpdateNewsDetail";
import Image from "next/image";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
const { Text } = Typography;

const NewsDetailPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { id } = navigate.query;
  const { fetchQuery } = useGetDetailNews(id as string, {});
  const { data: newsDetail, isLoading } = fetchQuery;
  console.log("newsDetail", newsDetail);
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const { fetchQuery: fetchCategory } = useGetNewsCategory([]);
  const { isLoading: isLoadingCategory, data: dataFetchCategory } =
    fetchCategory;
  const { handleUpdateNews, mutationQuery } = useUpdateNewsDetail();
  const { isPending: isLoadingUpdateNews, isSuccess: isSuccessUpdateNews } =
    mutationQuery;

  // Note
  const [editNoteData, editNoteFormData] = useState<any>({
    note: "",
    id: "",
  });
  const [isNoteModalVisible, setIsNoteModalVisible] = useState(false);
  const handleNoteCancel = () => {
    setIsNoteModalVisible(false);
  };

  React.useEffect(() => {
    if (isSuccessUpdateNews) {
      setIsNoteModalVisible(false);
    }
  }, [isSuccessUpdateNews]);

  const noteModal = (newsDetail: any) => {
    editNoteFormData({
      note: newsDetail.note,
      id: newsDetail.id,
    });
    setIsNoteModalVisible(true);
  };

  const handleSubmitNote = async (values: any) => {
    handleUpdateNews(values.id, {
      ...values,
      status: "rejected",
    });
  };

  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    event.preventDefault();
    console.log("query", JSON.stringify(value, null, 2));
    let category = dataFetchCategory.find(
      (item: any) => item.name == value.category
    );
    handleUpdateNews(value.id, {
      ...value,
      status: params,
      category: category.id,
      publish_time: moment(value.publish_time).format("YYYY-MM-DD"),
      cover_image: value.cover_image.split("/").pop(),
    });
  };

  if (isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (`${newsDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (!newsDetail) {
    return <NoData />;
  }

  const noteValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    note: yup
      .string()
      .required(translate("NoteRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("NoteLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Detail News</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "News",
            href: `/cms/${DASHBOARD_NEWS}`,
          },
          {
            title: "Detail News",
          },
        ]}
      />
      <Card>
        <Row
          style={{ justifyContent: "space-between", justifyItems: "center" }}
        >
          <Text style={{ fontWeight: "bold", fontSize: 18 }}>Detail News</Text>
          <Button
            ghost
            disabled
            style={{
              color: "white",
              background: getStatusColor(newsDetail?.status),
              fontWeight: "bold",
              borderRadius: 5,
            }}
          >
            {newsDetail?.status}
          </Button>
        </Row>
        <Space direction="vertical">
          <Text style={{ fontWeight: "bold" }}>Category</Text>
          <Text style={{ color: "gray", marginBottom: 13 }}>
            {newsDetail?.category}
          </Text>
          <Text style={{ fontWeight: "bold" }}>Publish Date</Text>
          <Text style={{ color: "gray", marginBottom: 13 }}>
            {moment(newsDetail?.publish_time).format("DD MMMM YYYY")}
          </Text>
          {newsDetail?.link && (
            <>
              <Text style={{ fontWeight: "bold" }}>{"Source"}</Text>
              <Link href={newsDetail?.link} target="_blank">
                <Text
                  style={{
                    color: "gray",
                    marginBottom: 13,
                  }}
                >
                  {newsDetail?.link}
                </Text>
              </Link>
            </>
          )}
          <Text style={{ fontWeight: "bold" }}>Title</Text>
          <article
            style={{ color: "gray", marginBottom: 13 }}
            dangerouslySetInnerHTML={{ __html: newsDetail?.subject }}
          />
          <Text style={{ fontWeight: "bold" }}>Description</Text>
          <article
            style={{ color: "gray", marginBottom: 13 }}
            dangerouslySetInnerHTML={{ __html: newsDetail?.body }}
          />
          <Text style={{ fontWeight: "bold" }}>Images</Text>
          {newsDetail?.source_id ? (
            <img
              src={newsDetail?.cover_image}
              alt="flag-icon"
              className="flag-icon"
              width={300}
              height={300}
              style={{ borderRadius: 10 }}
            />
          ) : (
            <Image
              src={newsDetail?.cover_image}
              alt="flag-icon"
              className="flag-icon"
              width={300}
              height={300}
              style={{ borderRadius: 10 }}
            />
          )}
          {newsDetail?.note && newsDetail?.status !== "pending" && (
            <>
              <Text style={{ fontWeight: "bold" }}>Note</Text>
              <article
                style={{ color: "gray", marginBottom: 13 }}
                dangerouslySetInnerHTML={{ __html: newsDetail?.note }}
              />
            </>
          )}
          <Flex gap="small" justify="flex-end">
            {newsDetail?.status === "unpublished" && !isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("published"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, newsDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {newsDetail?.status === "draft" && !isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, newsDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {newsDetail?.status === "rejected" && !isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, newsDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {newsDetail?.status === "unpublished" && !isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, newsDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {newsDetail?.status === "pending" && isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("published"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, newsDetail, "published")}
              >
                Publish
              </Button>
            )}
            {newsDetail?.status === "pending" && isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  height: 38,
                  marginTop: 24,
                  backgroundColor: getStatusColor("rejected"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                type="primary"
                iconPosition="end"
                onClick={() => noteModal(newsDetail)}
              >
                Rejected
              </Button>
            )}
            {newsDetail?.status === "published" && isKurator && (
              <Button
                loading={isLoadingUpdateNews}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  height: 38,
                  marginTop: 24,
                  backgroundColor: getStatusColor("rejected"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                type="primary"
                iconPosition="end"
                onClick={() => noteModal(newsDetail)}
              >
                Rejected
              </Button>
            )}
          </Flex>
        </Space>
      </Card>

      {/* modal note */}
      <Modal
        centered
        title="Note"
        open={isNoteModalVisible}
        width={800}
        onCancel={handleNoteCancel}
        footer={null}
      >
        <Formik
          validationSchema={noteValidationSchema}
          initialValues={editNoteData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleSubmitNote(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Enter your note here"}
                title={"Notes"}
                type={"text"}
                name={"note"}
                textArea={true}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingUpdateNews}
                >
                  {translate("Submit")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default NewsDetailPage;
// function setIsEditModalVisible(arg0: boolean) {
//   throw new Error("Function not implemented.");
// }
