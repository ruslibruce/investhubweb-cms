import { SaveOutlined, SendOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Input,
  Modal,
  Row,
  Space,
  Tooltip,
  Typography,
} from "antd";
import { useRouter } from "next/router";

import { DASHBOARD_COURSE } from "@/shared/constants/path";
import { BUTTON_COLOR, USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { checkIsEmptyObject } from "@/shared/utils/helper";
import Image from "next/image";
import React, { useState } from "react";
import { TbNewSection } from "react-icons/tb";
import "react-quill/dist/quill.snow.css";
import useGetCourseDetail from "../hooks/useGetCourseDetail";
import ListCard from "./ListCard";
import useAddSection from "../hooks/useAddSection";
import NoData from "@/shared/components/NoData";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
const CourseDetailPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id = slug?.[0];
  const page = slug?.[1];

  const initialFormCourse = {
    title: "",
    parent_type: "C",
  };
  const dataUser = storageCheck(USER);
  const { fetchQuery } = useGetCourseDetail(id as string);
  const { data: dataCourseDetail, isLoading } = fetchQuery;
  const { addSection, mutationQuery: mutationAdd } = useAddSection();
  const { isSuccess: isSuccessAdd, isPending: isLoadingAdd } = mutationAdd;
  const [isModalCreateSection, setIsModalCreateSection] = useState(false);
  const [formDataSection, setFormDataSection] =
    React.useState<any>(initialFormCourse);
  console.log("dataCourseDetail", dataCourseDetail);
  const isKurator = dataUser?.user?.role === "kurator";
  const { Text } = Typography;

  React.useEffect(() => {
    if (isSuccessAdd) {
      setIsModalCreateSection(false);
    }
  }, [isSuccessAdd]);

  const handleAddSection = (values: any) => {
    addSection({
      ...values,
      parent_id: id,
    });
  };

  const handleShowModalCreateSection = () => {
    setFormDataSection(initialFormCourse);
    setIsModalCreateSection(true);
  };

  if (isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (`${dataCourseDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (!dataCourseDetail) {
    return <NoData />;
  }

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row>
        <Col span={20}>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Detail Course</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Course",
                onClick: () => {
                  navigate.push({
                    pathname: DASHBOARD_COURSE,
                    query: { page: page },
                  });
                },
                href: "#",
              },
              {
                title: "Detail",
              },
            ]}
          />
        </Col>
        <Col span={4}>
          {/* <Button
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C" }}>
            <PlusOutlined />
            Create Course
          </Button> */}
        </Col>
      </Row>
      <Card>
        <Card style={{ width: "100%", background: "#f2f2f2" }}>
          <Space direction="vertical">
            <Text style={{ fontSize: 28, fontWeight: "bold" }}>
              {dataCourseDetail.title}
            </Text>
            <Text style={{ color: "gray" }}>
              {dataCourseDetail.description}
            </Text>
            <Image
              src={dataCourseDetail.cover_image_file}
              alt="cover-image"
              width={300}
              height={100}
              style={{
                borderRadius: 10,
                objectFit: "cover",
                marginTop: 10,
              }}
            />
          </Space>
        </Card>

        <Space
          direction="vertical"
          style={{
            marginTop: 24,
            width: "100%",
          }}
        >
          <Flex justify="end">
            {!isKurator && (
              <Tooltip title="Add Section">
                <Button
                  type="primary"
                  onClick={handleShowModalCreateSection}
                  style={{
                    backgroundColor: BUTTON_COLOR[0].color,
                    marginTop: 5,
                    marginRight: 10,
                  }}
                  icon={<TbNewSection />}
                />
              </Tooltip>
            )}
          </Flex>
          <Text style={{ fontSize: 24, marginTop: 10, fontWeight: "bold" }}>
            Course Content
          </Text>
        </Space>
        {dataCourseDetail?.sections?.length === 0 && <NoData />}

        {dataCourseDetail?.sections?.map((item: any) => (
          <ListCard
            contentType={dataCourseDetail.content_type}
            key={item.id}
            item={item}
            idDetailCourse={id as string}
            isParentSection={true}
          />
        ))}
      </Card>

      {/* Modal Create Section */}
      <Modal
        centered
        title="Form Create Section"
        open={isModalCreateSection}
        width={1200}
        onCancel={() => setIsModalCreateSection(false)}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormCourse}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddSection(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Input Title"}
                title={"Title"}
                type={"text"}
                name={"title"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAdd}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CourseDetailPage;
