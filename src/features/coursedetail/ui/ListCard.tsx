import FormInputFormik from "@/components/FormInputFormik";
import { imageLMS } from "@/shared/constants/imageUrl";
import { DASHBOARD_TEST_QUESTION } from "@/shared/constants/path";
import {
  AUDIO_FORMAT,
  BUTTON_COLOR,
  DOCUMENT_FORMAT,
  SUBTITLE_FORMAT,
  USER,
  VIDEO_FORMAT,
} from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import {
  regexUrl
} from "@/shared/utils/helper";
import {
  DeleteOutlined,
  FolderOpenTwoTone,
  MenuOutlined,
  SendOutlined
} from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Divider,
  Flex,
  Modal,
  Popover,
  Row,
  Select,
  Space,
  Tooltip,
  Typography,
  Upload,
  message
} from "antd";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React from "react";
import { AiFillDelete, AiOutlineSelect } from "react-icons/ai";
import { FaNoteSticky } from "react-icons/fa6";
import { LuSubtitles } from "react-icons/lu";
import { MdPermMedia } from "react-icons/md";
import { RiEditLine } from "react-icons/ri";
import * as yup from "yup";
import useAddContent from "../hooks/useAddContent";
import useAddSection from "../hooks/useAddSection";
import useAddSubtitle from "../hooks/useAddSubtitle";
import useDeleteContent from "../hooks/useDeleteContent";
import useDeleteSection from "../hooks/useDeleteSection";
import useDeleteTestCourse from "../hooks/useDeleteTestCourse";
import useEditContent from "../hooks/useEditContent";
import useEditSection from "../hooks/useEditSection";
import useGetTestTypeCourse from "../hooks/useGetTestTypeCourse";
import usePostTestCourse from "../hooks/usePostTestCourse";
import usePutTestCourse from "../hooks/usePutTestCourse";
const { Dragger } = Upload;

type ListCardProps = {
  item: any;
  contentType: string;
  idDetailCourse: string;
  isParentSection?: true;
  idParentSection?: string;
};

const initialFormDataSection = {
  title: "",
  parent_type: "S",
};

const initialFormDataContent = {
  title: "",
  content_file: "",
};

const initialFormDataTest = {
  name: "",
  duration: "",
  passing_score: "",
  test_type_id: "",
  parent_id: "",
  course_id: "",
  isPostTest: false,
  isPreTest: false,
};

const initialFormDataSubtitle = {
  language: "",
  filename: "",
};

export default function ListCard({
  item,
  contentType,
  idDetailCourse,
  isParentSection,
  idParentSection,
}: ListCardProps) {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const dataUser = storageCheck(USER);
  const [formDataSection, setFormDataSection] = React.useState<any>(
    initialFormDataSection
  );
  const [formDataContent, setFormDataContent] = React.useState<any>(
    initialFormDataContent
  );
  const [formDataSubtitle, setFormDataSubtitle] = React.useState<any>(
    initialFormDataSubtitle
  );
  const [formDataTest, setFormDataTest] =
    React.useState<any>(initialFormDataTest);
  const [sourceType, setSourceType] = React.useState("");
  const { fetchCategoryType } = useGetTestTypeCourse([]);
  const { data: dataTestType } = fetchCategoryType;
  const [isModalCreateSection, setIsModalCreateSection] = React.useState(false);
  const [isModalCreateContent, setIsModalCreateContent] = React.useState(false);
  const [isModalCreateSubtitle, setIsModalCreateSubtitle] =
    React.useState(false);
  const [isModalVisibleAddTest, setisModalVisibleAddTest] =
    React.useState(false);
  const [isModalVisibleEditTest, setisModalVisibleEditTest] =
    React.useState(false);
  const [isModalVisibleEditContent, setisModalVisibleEditContent] =
    React.useState(false);
  const [isModalEditSection, setIsModalEditSection] = React.useState(false);
  const { addContent, mutationQuery: mutationAddContent } = useAddContent();
  const { isPending: isLoadingAddContent, isSuccess: isSuccessAddContent } =
    mutationAddContent;
  const { addSubtitle, mutationQuery: mutationAddSubtitle } = useAddSubtitle();
  const { isPending: isLoadingAddSubtitle, isSuccess: isSuccessAddSubtitle } =
    mutationAddSubtitle;
  const { editContent, mutationQuery: mutationEditContent } = useEditContent();
  const { isPending: isLoadingEditContent, isSuccess: isSuccessEditContent } =
    mutationEditContent;
  const { deleteContent } = useDeleteContent();
  const { handleUpdateTest, mutationQuery: mutationUpdateTest } =
    usePutTestCourse();
  const { isPending: isLoadingUpdateTest, isSuccess: isSuccessUpdateTest } =
    mutationUpdateTest;
  const { handlePostTest, mutationQuery: mutationPostTest } =
    usePostTestCourse();
  const { isPending: isLoadingPostTest, isSuccess: isSuccessPostTest } =
    mutationPostTest;
  const { runDeleteTest } = useDeleteTestCourse();
  const { addSection, mutationQuery: mutationAddSection } = useAddSection();
  const { isPending: isLoadingAddSection, isSuccess: isSuccessAddSection } =
    mutationAddSection;
  const { editSection, mutationQuery: mutationEditSection } = useEditSection();
  const { isPending: isLoadingEditSection, isSuccess: isSuccessEditSection } =
    mutationEditSection;
  const { deleteSection } = useDeleteSection();
  console.log("item.contents", item.contents);
  const [formSize, setFormSize] = React.useState<SizeType>("large");
  const isKurator = dataUser?.user?.role === "kurator";
  const { Text } = Typography;

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessAddContent) {
      setIsModalCreateContent(false);
    }

    if (isSuccessAddSubtitle) {
      setIsModalCreateSubtitle(false);
      setFormDataSubtitle(initialFormDataSubtitle);
    }

    if (isSuccessEditContent) {
      setisModalVisibleEditContent(false);
    }

    if (isSuccessUpdateTest) {
      setisModalVisibleEditTest(false);
    }

    if (isSuccessPostTest) {
      setisModalVisibleAddTest(false);
    }

    if (isSuccessAddSection) {
      setIsModalCreateSection(false);
    }

    if (isSuccessEditSection) {
      setIsModalEditSection(false);
    }
  }, [
    isSuccessAddContent,
    isSuccessEditContent,
    isSuccessUpdateTest,
    isSuccessPostTest,
    isSuccessAddSection,
    isSuccessEditSection,
    isSuccessAddSubtitle,
  ]);

  const handleShowCreateContent = () => {
    setFormDataContent(initialFormDataContent);
    setIsModalCreateContent(true);
  };

  const handleShowCreateSubtitle = (item: any) => {
    setFormDataSubtitle((prev: any) => ({
      ...prev,
      content_id: item.id,
    }));
    setIsModalCreateSubtitle(true);
  };

  const handleShowCreateSection = () => {
    setFormDataSection(initialFormDataSection);
    setIsModalCreateSection(true);
  };

  const handleAddSection = (values: any) => {
    addSection({
      ...formDataSection,
      parent_id: item.id,
    });
  };

  const showEditSection = (item: any) => {
    console.log("id", item);

    setFormDataSection((prev: {}) => ({
      ...prev,
      parent_id: idParentSection,
      id: item.id,
      title: item.title,
    }));
    setIsModalEditSection(true);
  };

  const handleEditSection = (values: any) => {
    editSection(
      {
        ...values,
        parent_type: isParentSection ? "C" : "S",
        parent_id: isParentSection ? idDetailCourse : values.parent_id,
      },
      values.id
    );
  };

  const handleDeleteSection = (id: string) => {
    deleteSection(id);
  };

  const showAddTest = (item: any) => {
    let dataTest: any = {};
    dataTestType.map((value: any) => {
      if (item.pre_test && value.label === "Course Post Test") {
        dataTest = value;
      } else if (item.post_test && value.label === "Course Pre Test") {
        dataTest = value;
      }
    });
    setFormDataTest({
      ...initialFormDataTest,
      parent_id: item.id,
      course_id: idDetailCourse,
      isPostTest: item.post_test ? true : false,
      isPreTest: item.pre_test ? true : false,
      name:
        !item.post_test && !item.pre_test
          ? ""
          : item.post_test
          ? "Course Pre Test"
          : "Course Post Test",
      test_type_id: !item.post_test && !item.pre_test ? "" : dataTest.value,
    });
    setisModalVisibleAddTest(true);
  };

  const handleSubmitAddTest = (values: any) => {
    handlePostTest(values);
  };

  const showEditTest = (item: any, parent_id: string) => {
    console.log("id", item);

    setFormDataTest((prev: {}) => ({
      ...prev,
      parent_id: parent_id,
      course_id: idDetailCourse,
      id: item.id,
      test_type_id: item.test_type_id,
      passing_score: item.passing_score,
      duration: item.duration,
      name: item.name,
    }));
    setisModalVisibleEditTest(true);
  };

  const handleSubmitEditTest = (values: any) => {
    handleUpdateTest(values, values.id);
  };

  const handleDeleteTest = (id: string) => {
    runDeleteTest(id);
  };

  const handleAddContent = (values: any) => {
    addContent({
      ...values,
      section_id: item.id,
      content_type: contentType.toLowerCase(),
      content_file: values.content_file.split("/").pop(),
    });
  };

  const handleAddSubtitle = (values: any) => {
    console.log("formDataSubtitle", values);
    addSubtitle({ ...values, filename: values.filename.split("/").pop() });
  };

  const showEditContent = (item: any, parent_id: string) => {
    console.log("id", item);

    setFormDataContent((prev: {}) => ({
      ...prev,
      parent_id: parent_id,
      course_id: idDetailCourse,
      id: item.id,
      title: item.title,
      content_file: item.content_file,
    }));
    setSourceType("link");
    setisModalVisibleEditContent(true);
  };

  const handleEditContent = (values: any) => {
    editContent(
      {
        ...values,
        content_file: values.content_file.split("/").pop(),
      },
      values.id
    );
  };

  const handleDeleteContent = (id: string) => {
    deleteContent(id);
  };

  const handleCancelTest = () => {
    setisModalVisibleAddTest(false);
  };

  const handleCancelEditTest = () => {
    setisModalVisibleEditTest(false);
  };

  const handleChangeSelect = (value: string, setFieldValue: any) => {
    console.log(`selected ${value}`);
    dataTestType.map((item: any) => {
      if (item.value === value) {
        setFieldValue("name", item.label);
      }
    });
    setFieldValue("test_type_id", value);
  };

  const handleSelectQuestion = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    navigate.push(
      `${DASHBOARD_TEST_QUESTION}/${value.id}/${
        value.name
      }/${idDetailCourse}/${true}`
    );
  };

  const contentValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
    content_file: yup
      .string()
      .required(translate("LinkRequired"))
      .matches(regexUrl(), translate("LinkMatches"))
      .max(
        150,
        ({ max }) =>
          `${translate("LinkLength")} ${max} ${translate("Characters")}`
      ),
  });

  const sectionValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
  });

  const testValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    test_type_id: yup.string().required(translate("TestTypeRequired")),
    duration: yup
      .number()
      .required(translate("DurationRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("DurationLength")} ${max} ${translate("Characters")}`
      ),
    passing_score: yup
      .number()
      .required(translate("PassingScoreRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("PassingScoreLength")} ${max} ${translate("Characters")}`
      ),
  });

  const subtitleValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    language: yup.string().required(translate("LanguageRequired")),
    filename: yup.string().required(translate("FileRequired")),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, `${imageLMS}/${dataFile.response.data.filename}`);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <>
      <Card
        style={{
          marginTop: 20,
          marginLeft: idParentSection ? 15 : 0,
          boxShadow: idParentSection ? "" : "0 4px 8px rgba(0, 0, 0, 0.1)",
        }}
      >
        {window.innerWidth < 600 && !isKurator ? (
          <Popover
            placement="bottomRight"
            content={() => {
              return (
                <Col>
                  <Tooltip title="Add Section">
                    <Button
                      size="small"
                      type="primary"
                      onClick={handleShowCreateSection}
                      style={{
                        backgroundColor: BUTTON_COLOR[1].color,
                        marginRight: 10,
                      }}
                      icon={<MenuOutlined />}
                    />
                  </Tooltip>
                  {item.post_test && item.pre_test ? null : (
                    <Tooltip title="Add Test">
                      <Button
                        size="small"
                        type="primary"
                        onClick={() => showAddTest(item)}
                        style={{
                          backgroundColor: BUTTON_COLOR[2].color,
                          marginRight: 10,
                        }}
                        icon={<FaNoteSticky />}
                      />
                    </Tooltip>
                  )}
                  <Tooltip title="Add Content">
                    <Button
                      size="small"
                      type="primary"
                      onClick={handleShowCreateContent}
                      style={{
                        backgroundColor: BUTTON_COLOR[3].color,
                        marginRight: 10,
                      }}
                      icon={<MdPermMedia />}
                    />
                  </Tooltip>
                  <Tooltip title="Edit Section">
                    <Button
                      size="small"
                      type="primary"
                      onClick={() => showEditSection(item)}
                      style={{
                        backgroundColor: BUTTON_COLOR[4].color,
                        marginRight: 10,
                      }}
                      icon={<RiEditLine />}
                    />
                  </Tooltip>
                  <Tooltip title="Delete Section">
                    <Button
                      size="small"
                      type="primary"
                      onClick={() =>
                        Modal.confirm({
                          title: "Delete Data",
                          content:
                            "Are you sure you want to delete this Section?",
                          okText: "Yes",
                          cancelText: "No",
                          onOk: () => handleDeleteSection(item.id),
                          okButtonProps: {
                            icon: <AiFillDelete />,
                            style: { backgroundColor: "#CB0B0C" },
                          },
                        })
                      }
                      style={{
                        backgroundColor: BUTTON_COLOR[6].color,
                        marginRight: 10,
                      }}
                      icon={<AiFillDelete style={{ color: "white" }} />}
                    />
                  </Tooltip>
                </Col>
              );
            }}
          >
            <Flex justify="end">
              <Button size="small" type="primary" icon={<MenuOutlined />} />
            </Flex>
          </Popover>
        ) : (
          <Flex justify="end">
            <Tooltip title="Add Section">
              <Button
                size="small"
                type="primary"
                onClick={handleShowCreateSection}
                style={{
                  backgroundColor: BUTTON_COLOR[1].color,
                  marginRight: 10,
                }}
                icon={<MenuOutlined />}
              />
            </Tooltip>
            {item.post_test && item.pre_test ? null : (
              <Tooltip title="Add Test">
                <Button
                  size="small"
                  type="primary"
                  onClick={() => showAddTest(item)}
                  style={{
                    backgroundColor: BUTTON_COLOR[2].color,
                    marginRight: 10,
                  }}
                  icon={<FaNoteSticky />}
                />
              </Tooltip>
            )}
            <Tooltip title="Add Content">
              <Button
                size="small"
                type="primary"
                onClick={handleShowCreateContent}
                style={{
                  backgroundColor: BUTTON_COLOR[3].color,
                  marginRight: 10,
                }}
                icon={<MdPermMedia />}
              />
            </Tooltip>
            <Tooltip title="Edit Section">
              <Button
                size="small"
                type="primary"
                onClick={() => showEditSection(item)}
                style={{
                  backgroundColor: BUTTON_COLOR[4].color,
                  marginRight: 10,
                }}
                icon={<RiEditLine />}
              />
            </Tooltip>
            <Tooltip title="Delete Section">
              <Button
                size="small"
                type="primary"
                onClick={() =>
                  Modal.confirm({
                    title: "Delete Data",
                    content: "Are you sure you want to delete this Section?",
                    okText: "Yes",
                    cancelText: "No",
                    onOk: () => handleDeleteSection(item.id),
                    okButtonProps: {
                      icon: <AiFillDelete />,
                      style: { backgroundColor: "#CB0B0C" },
                    },
                  })
                }
                style={{
                  backgroundColor: BUTTON_COLOR[6].color,
                  marginRight: 10,
                }}
                icon={<AiFillDelete style={{ color: "white" }} />}
              />
            </Tooltip>
          </Flex>
        )}
        <Text style={{ fontWeight: "bold", marginTop: 10 }}>{item.title}</Text>
        {item.pre_test?.length > 0 && (
          <>
            <Divider />
            <Text
              style={{
                fontWeight: "bold",
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 15,
              }}
            >
              {"Pre Test"}
            </Text>
            {item.pre_test?.map((test: any) => (
              <Card style={{ marginLeft: 15, marginTop: 10 }}>
                {window.innerWidth < 600 && !isKurator ? (
                  <Popover
                    content={() => {
                      return (
                        <Col>
                          <Tooltip title="Select Question">
                            <Button
                              size="small"
                              type="primary"
                              style={{
                                backgroundColor: BUTTON_COLOR[2].color,
                                marginRight: 10,
                              }}
                              icon={<AiOutlineSelect />}
                              onClick={(e) => handleSelectQuestion(e, test)}
                            />
                          </Tooltip>
                          <Tooltip title="Edit Question">
                            <Button
                              size="small"
                              type="primary"
                              style={{
                                backgroundColor: BUTTON_COLOR[4].color,
                                marginRight: 10,
                              }}
                              icon={<RiEditLine />}
                              onClick={() => showEditTest(test, item.id)}
                            />
                          </Tooltip>
                          <Tooltip title="Delete Test">
                            <Button
                              size="small"
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: "Delete Data",
                                  content:
                                    "Are you sure you want to delete this Test?",
                                  okText: "Yes",
                                  cancelText: "No",
                                  onOk: () => handleDeleteTest(test.id),
                                  okButtonProps: {
                                    icon: <AiFillDelete />,
                                    style: { backgroundColor: "#CB0B0C" },
                                  },
                                })
                              }
                              style={{
                                backgroundColor: BUTTON_COLOR[6].color,
                                marginRight: 10,
                              }}
                              icon={<AiFillDelete />}
                            />
                          </Tooltip>
                        </Col>
                      );
                    }}
                  >
                    <Flex justify="end">
                      <Button
                        size="small"
                        type="primary"
                        icon={<MenuOutlined />}
                      />
                    </Flex>
                  </Popover>
                ) : (
                  <Flex justify="end">
                    <Tooltip title="Select Question">
                      <Button
                        size="small"
                        type="primary"
                        style={{
                          backgroundColor: BUTTON_COLOR[2].color,
                          marginRight: 10,
                        }}
                        icon={<AiOutlineSelect />}
                        onClick={(e) => handleSelectQuestion(e, test)}
                      />
                    </Tooltip>
                    <Tooltip title="Edit Question">
                      <Button
                        size="small"
                        type="primary"
                        style={{
                          backgroundColor: BUTTON_COLOR[4].color,
                          marginRight: 10,
                        }}
                        icon={<RiEditLine />}
                        onClick={() => showEditTest(test, item.id)}
                      />
                    </Tooltip>
                    <Tooltip title="Delete Test">
                      <Button
                        size="small"
                        type="primary"
                        onClick={() =>
                          Modal.confirm({
                            title: "Delete Data",
                            content:
                              "Are you sure you want to delete this Test?",
                            okText: "Yes",
                            cancelText: "No",
                            onOk: () => handleDeleteTest(test.id),
                            okButtonProps: {
                              icon: <AiFillDelete />,
                              style: { backgroundColor: "#CB0B0C" },
                            },
                          })
                        }
                        style={{
                          backgroundColor: BUTTON_COLOR[6].color,
                          marginRight: 10,
                        }}
                        icon={<AiFillDelete />}
                      />
                    </Tooltip>
                  </Flex>
                )}
                <Space direction="vertical" style={{ marginTop: 10 }}>
                  <Row>
                    <Text style={{ width: 100 }}>{"Test Name"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.name}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Passing Score"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.passing_score}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Test Type"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.test_type}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Duration"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.duration}</Text>
                  </Row>
                </Space>
              </Card>
            ))}
          </>
        )}
        {item.contents?.length > 0 && (
          <>
            <Divider />
            <Text
              style={{
                fontWeight: "bold",
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 15,
              }}
            >
              {"Content"}
            </Text>
            {item.contents?.map((content: any) => (
              <Card style={{ marginLeft: 15, marginTop: 10 }}>
                {window.innerWidth < 600 && !isKurator ? (
                  <Popover
                    content={() => {
                      return (
                        <Col>
                          <Tooltip title="Edit Content">
                            <Button
                              size="small"
                              type="primary"
                              style={{
                                backgroundColor: BUTTON_COLOR[4].color,
                                marginRight: 10,
                              }}
                              icon={<RiEditLine />}
                              onClick={() => showEditContent(content, item.id)}
                            />
                          </Tooltip>
                          <Tooltip title="Add Subtitle">
                            <Button
                              size="small"
                              type="primary"
                              onClick={() => handleShowCreateSubtitle(content)}
                              style={{
                                backgroundColor: BUTTON_COLOR[3].color,
                                marginRight: 10,
                              }}
                              icon={<LuSubtitles />}
                            />
                          </Tooltip>
                          <Tooltip title="Delete Content">
                            <Button
                              size="small"
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: "Delete Data",
                                  content:
                                    "Are you sure you want to delete this Content?",
                                  okText: "Yes",
                                  cancelText: "No",
                                  onOk: () => handleDeleteContent(content.id),
                                  okButtonProps: {
                                    icon: <AiFillDelete />,
                                    style: { backgroundColor: "#CB0B0C" },
                                  },
                                })
                              }
                              style={{
                                backgroundColor: BUTTON_COLOR[6].color,
                                marginRight: 10,
                              }}
                              icon={<AiFillDelete />}
                            />
                          </Tooltip>
                        </Col>
                      );
                    }}
                  >
                    <Flex justify="end">
                      <Button
                        size="small"
                        type="primary"
                        icon={<MenuOutlined />}
                      />
                    </Flex>
                  </Popover>
                ) : (
                  <Flex justify="end">
                    <Tooltip title="Edit Content">
                      <Button
                        size="small"
                        type="primary"
                        style={{
                          backgroundColor: BUTTON_COLOR[4].color,
                          marginRight: 10,
                        }}
                        icon={<RiEditLine />}
                        onClick={() => showEditContent(content, item.id)}
                      />
                    </Tooltip>
                    <Tooltip title="Add Subtitle">
                      <Button
                        size="small"
                        type="primary"
                        onClick={() => handleShowCreateSubtitle(content)}
                        style={{
                          backgroundColor: BUTTON_COLOR[3].color,
                          marginRight: 10,
                        }}
                        icon={<LuSubtitles />}
                      />
                    </Tooltip>
                    <Tooltip title="Delete Content">
                      <Button
                        size="small"
                        type="primary"
                        onClick={() =>
                          Modal.confirm({
                            title: "Delete Data",
                            content:
                              "Are you sure you want to delete this Content?",
                            okText: "Yes",
                            cancelText: "No",
                            onOk: () => handleDeleteContent(content.id),
                            okButtonProps: {
                              icon: <AiFillDelete />,
                              style: { backgroundColor: "#CB0B0C" },
                            },
                          })
                        }
                        style={{
                          backgroundColor: BUTTON_COLOR[6].color,
                          marginRight: 10,
                        }}
                        icon={<AiFillDelete />}
                      />
                    </Tooltip>
                  </Flex>
                )}
                <Space direction="vertical" style={{ marginTop: 10 }}>
                  <Row>
                    <Text style={{ width: 120 }}>{"Content Name"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{content.title}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 120 }}>{"Content Url"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{content.content_file}</Text>
                  </Row>
                  {content.subtitles?.length > 0 &&
                    content.subtitles.map((subtitle: any) => (
                      <>
                        <Row>
                          <Text style={{ width: 120 }}>{"Url Language"}</Text>
                          <Text style={{ marginLeft: 5, marginRight: 5 }}>
                            {":"}
                          </Text>
                          <Text>{subtitle.filename}</Text>
                        </Row>
                        <Row>
                          <Text style={{ width: 120 }}>{"Language Name"}</Text>
                          <Text style={{ marginLeft: 5, marginRight: 5 }}>
                            {":"}
                          </Text>
                          <Text>{subtitle.language}</Text>
                        </Row>
                      </>
                    ))}
                </Space>
              </Card>
            ))}
          </>
        )}
        {item.post_test?.length > 0 && (
          <>
            <Divider />
            <Text
              style={{
                fontWeight: "bold",
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 15,
              }}
            >
              {"Post Test"}
            </Text>
            {item.post_test?.map((test: any) => (
              <Card style={{ marginLeft: 15, marginTop: 10 }}>
                {window.innerWidth < 600 && !isKurator ? (
                  <Popover
                    content={() => {
                      return (
                        <Col>
                          <Tooltip title="Select Question">
                            <Button
                              size="small"
                              type="primary"
                              style={{
                                backgroundColor: BUTTON_COLOR[2].color,
                                marginRight: 10,
                              }}
                              icon={<AiOutlineSelect />}
                              onClick={(e) => handleSelectQuestion(e, test)}
                            />
                          </Tooltip>
                          <Tooltip title="Edit Question">
                            <Button
                              size="small"
                              type="primary"
                              style={{
                                backgroundColor: BUTTON_COLOR[4].color,
                                marginRight: 10,
                              }}
                              icon={<RiEditLine />}
                              onClick={() => showEditTest(test, item.id)}
                            />
                          </Tooltip>
                          <Tooltip title="Delete Test">
                            <Button
                              size="small"
                              type="primary"
                              onClick={() =>
                                Modal.confirm({
                                  title: "Delete Data",
                                  content:
                                    "Are you sure you want to delete this Test?",
                                  okText: "Yes",
                                  cancelText: "No",
                                  onOk: () => handleDeleteTest(test.id),
                                  okButtonProps: {
                                    icon: <AiFillDelete />,
                                    style: { backgroundColor: "#CB0B0C" },
                                  },
                                })
                              }
                              style={{
                                backgroundColor: BUTTON_COLOR[6].color,
                                marginRight: 10,
                              }}
                              icon={<AiFillDelete />}
                            />
                          </Tooltip>
                        </Col>
                      );
                    }}
                  >
                    <Flex justify="end">
                      <Button
                        size="small"
                        type="primary"
                        icon={<MenuOutlined />}
                      />
                    </Flex>
                  </Popover>
                ) : (
                  <Flex justify="end">
                    <Tooltip title="Select Question">
                      <Button
                        size="small"
                        type="primary"
                        style={{
                          backgroundColor: BUTTON_COLOR[2].color,
                          marginRight: 10,
                        }}
                        icon={<AiOutlineSelect />}
                        onClick={(e) => handleSelectQuestion(e, test)}
                      />
                    </Tooltip>
                    <Tooltip title="Edit Question">
                      <Button
                        size="small"
                        type="primary"
                        style={{
                          backgroundColor: BUTTON_COLOR[4].color,
                          marginRight: 10,
                        }}
                        icon={<RiEditLine />}
                        onClick={() => showEditTest(test, item.id)}
                      />
                    </Tooltip>
                    <Tooltip title="Delete Test">
                      <Button
                        size="small"
                        type="primary"
                        onClick={() =>
                          Modal.confirm({
                            title: "Delete Data",
                            content:
                              "Are you sure you want to delete this Test?",
                            okText: "Yes",
                            cancelText: "No",
                            onOk: () => handleDeleteTest(test.id),
                            okButtonProps: {
                              icon: <AiFillDelete />,
                              style: { backgroundColor: "#CB0B0C" },
                            },
                          })
                        }
                        style={{
                          backgroundColor: BUTTON_COLOR[6].color,
                          marginRight: 10,
                        }}
                        icon={<AiFillDelete />}
                      />
                    </Tooltip>
                  </Flex>
                )}
                <Space direction="vertical" style={{ marginTop: 10 }}>
                  <Row>
                    <Text style={{ width: 100 }}>{"Test Name"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.name}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Passing Score"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.passing_score}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Test Type"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.test_type}</Text>
                  </Row>
                  <Row>
                    <Text style={{ width: 100 }}>{"Duration"}</Text>
                    <Text style={{ marginLeft: 5, marginRight: 5 }}>{":"}</Text>
                    <Text>{test.duration}</Text>
                  </Row>
                </Space>
              </Card>
            ))}
          </>
        )}
        {item.sections?.length > 0 && (
          <>
            {item.sections?.map((section: any) => (
              <ListCard
                contentType={contentType}
                key={section.id}
                item={section}
                idDetailCourse={idDetailCourse}
                idParentSection={item.id}
              />
            ))}
          </>
        )}
      </Card>

      {/* Modal Create Content */}
      <Modal
        centered
        title="Form Create Content"
        open={isModalCreateContent}
        width={1200}
        onCancel={() => setIsModalCreateContent(false)}
        footer={null}
      >
        <Formik
          validationSchema={contentValidationSchema}
          initialValues={initialFormDataContent}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddContent(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, setFieldValue, values }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Title"}
                    title={"Title"}
                    type={"text"}
                    name={"title"}
                  />
                </Col>
                <Col>
                  <Text>Source Type</Text>
                  <Select
                    placeholder="Select Source Type"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) => {
                      setSourceType(value);
                      setFieldValue("content_file", "");
                    }}
                    options={[
                      { value: "link", label: "Link/URL" },
                      { value: "file", label: "Attachment File" },
                    ]}
                    value={sourceType}
                    size={formSize}
                  />
                </Col>
                {sourceType === "link" && (
                  <Col>
                    <Field
                      component={FormInputFormik}
                      placeholder={"Input URL"}
                      title={"URL"}
                      type={"url"}
                      name={"content_file"}
                    />
                  </Col>
                )}
              </Row>
              {sourceType === "file" && (
                <>
                  {values.content_file ? (
                    <Row>
                      <label className="required">File Name</label>
                      <Col
                        span={24}
                        style={{
                          alignItems: "center",
                          justifyItems: "center",
                          marginTop: 10,
                          marginBottom: 20,
                        }}
                      >
                        <Text>{values.content_file}</Text>
                        <DeleteOutlined
                          style={{
                            fontSize: 20,
                            color: "red",
                            position: "absolute",
                            top: 10,
                            right: 10,
                            backgroundColor: "white",
                            padding: 5,
                            borderRadius: 100,
                          }}
                          type="primary"
                          onClick={() => {
                            setFieldValue("content_file", "");
                          }}
                        />
                      </Col>
                    </Row>
                  ) : (
                    <>
                      <label className="required">
                        Upload File Content (Max. 100MB)
                      </label>
                      <Dragger
                        {...{
                          name: "file",
                          multiple: false,
                          maxCount: 1,
                          action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                          headers: {
                            authorization: `Bearer ${dataUser.token}`,
                          },
                          accept:
                            contentType.toLowerCase() === "video"
                              ? VIDEO_FORMAT
                              : contentType.toLowerCase() === "article"
                              ? DOCUMENT_FORMAT
                              : AUDIO_FORMAT,
                          beforeUpload: (file) => {
                            console.log("file beforeUpload", file);
                            // console.log("file type", file.type.split("/")[0]);
                            // console.log("contentType", contentType);
                            const isSize = file.size / 1024 / 1024 < 100;

                            if (!isSize) {
                              message.error(
                                `${file.name} size must be less than 100 MB`
                              );
                            }

                            return isSize || Upload.LIST_IGNORE;
                          },
                          onChange: (info) =>
                            handleUploadChange(
                              info,
                              setFieldValue,
                              "content_file"
                            ),
                        }}
                        style={{ marginTop: 10 }}
                      >
                        <Text className="ant-upload-drag-icon">
                          <FolderOpenTwoTone twoToneColor="#737373" />
                        </Text>
                        <Text className="ant-upload-text">
                          Click or drag file to this area to upload
                        </Text>
                        <Text className="ant-upload-hint"></Text>
                      </Dragger>
                      {errors.content_file && (
                        <div className={"error"}>
                          {translate("FileRequired")}
                        </div>
                      )}
                    </>
                  )}
                </>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAddContent}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit Content */}
      <Modal
        centered
        title="Form Edit Content"
        open={isModalVisibleEditContent}
        width={1200}
        onCancel={() => setisModalVisibleEditContent(false)}
        footer={null}
      >
        <Formik
          validationSchema={contentValidationSchema}
          initialValues={formDataContent}
          enableReinitialize
          onSubmit={(values) => {
            handleEditContent(values);
          }}
        >
          {({ handleSubmit, isValid, errors, setFieldValue, values }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col span={6}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Title"}
                    title={"Title"}
                    type={"text"}
                    name={"title"}
                  />
                </Col>
                <Col span={6}>
                  <Text>Source Type </Text>
                  <Select
                    placeholder="Select Source Type"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) => {
                      setSourceType(value);
                      setFieldValue("content_file", "");
                    }}
                    options={[
                      { value: "link", label: "Link/URL" },
                      { value: "file", label: "Attachment File" },
                    ]}
                    value={sourceType}
                    size={formSize}
                  />
                </Col>
                {sourceType === "link" && (
                  <Col span={6}>
                    <Field
                      component={FormInputFormik}
                      placeholder={"Input URL"}
                      title={"URL"}
                      type={"url"}
                      name={"content_file"}
                    />
                  </Col>
                )}
              </Row>
              {sourceType === "file" && (
                <>
                  {values.content_file ? (
                    <Row>
                      <label className="required">File Name</label>
                      <Col
                        span={24}
                        style={{
                          alignItems: "center",
                          justifyItems: "center",
                          marginTop: 10,
                          marginBottom: 20,
                        }}
                      >
                        <Text>{values.content_file}</Text>
                        <DeleteOutlined
                          style={{
                            fontSize: 20,
                            color: "red",
                            position: "absolute",
                            top: 10,
                            right: 10,
                            backgroundColor: "white",
                            padding: 5,
                            borderRadius: 100,
                          }}
                          type="primary"
                          onClick={() => {
                            setFieldValue("content_file", "");
                          }}
                        />
                      </Col>
                    </Row>
                  ) : (
                    <>
                      <label className="required">
                        Upload File Content (Max. 100MB)
                      </label>
                      <Dragger
                        {...{
                          name: "file",
                          multiple: false,
                          maxCount: 1,
                          action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                          headers: {
                            authorization: `Bearer ${dataUser.token}`,
                          },
                          accept:
                            contentType.toLowerCase() === "video"
                              ? VIDEO_FORMAT
                              : contentType.toLowerCase() === "article"
                              ? DOCUMENT_FORMAT
                              : AUDIO_FORMAT,
                          beforeUpload: (file) => {
                            console.log("file beforeUpload", file);
                            // console.log("file type", file.type.split("/")[0]);
                            // console.log("contentType", contentType);
                            const isSize = file.size / 1024 / 1024 < 100;

                            if (!isSize) {
                              message.error(
                                `${file.name} size must be less than 100 MB`
                              );
                            }

                            return isSize || Upload.LIST_IGNORE;
                          },
                          onChange: (info) =>
                            handleUploadChange(
                              info,
                              setFieldValue,
                              "content_file"
                            ),
                        }}
                        style={{ marginTop: 10 }}
                      >
                        <Text className="ant-upload-drag-icon">
                          <FolderOpenTwoTone twoToneColor="#737373" />
                        </Text>
                        <Text className="ant-upload-text">
                          Click or drag file to this area to upload
                        </Text>
                        <Text className="ant-upload-hint"></Text>
                      </Dragger>
                      {errors.content_file && (
                        <div className={"error"}>
                          {errors.content_file.toString()}
                        </div>
                      )}
                    </>
                  )}
                </>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingEditContent}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Create Section */}
      <Modal
        centered
        title="Form Create Section"
        open={isModalCreateSection}
        width={500}
        onCancel={() => setIsModalCreateSection(false)}
        footer={null}
      >
        <Formik
          validationSchema={sectionValidationSchema}
          initialValues={initialFormDataSection}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddSection(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Input Title"}
                title={"Title"}
                type={"text"}
                name={"title"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAddSection}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit Section */}
      <Modal
        centered
        title="Form Edit Section"
        open={isModalEditSection}
        width={500}
        onCancel={() => setIsModalEditSection(false)}
        footer={null}
      >
        <Formik
          validationSchema={sectionValidationSchema}
          initialValues={formDataSection}
          enableReinitialize
          onSubmit={(values) => {
            handleEditSection(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Input Title"}
                title={"Title"}
                type={"text"}
                name={"title"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingEditSection}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Create Test Course */}
      <Modal
        centered
        title="Form Add Test"
        open={isModalVisibleAddTest}
        width={1200}
        onCancel={handleCancelTest}
        footer={null}
      >
        <Formik
          validationSchema={testValidationSchema}
          initialValues={formDataTest}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleSubmitAddTest(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, values, errors, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
              >
                <Col span={24}>
                  <label className="required">Test Type</label>
                  {!values.isPostTest && !values.isPreTest ? (
                    <Select
                      placeholder="Select Test Type"
                      style={{
                        width: "100%",
                        marginTop: 10,
                        marginBottom: 15,
                      }}
                      onChange={(value) =>
                        handleChangeSelect(value, setFieldValue)
                      }
                      options={dataTestType}
                      value={values.test_type_id}
                      size={formSize}
                    />
                  ) : (
                    <Text style={{ fontWeight: "bold" }}>{values.name}</Text>
                  )}
                  {errors.test_type_id && (
                    <div className={"error"}>
                      {errors.test_type_id.toString()}
                    </div>
                  )}
                </Col>
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Duration (Minute)"}
                    title={"Duration (Minute)"}
                    type={"number"}
                    name={"duration"}
                  />
                </Col>
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Duration (Minute)"}
                    title={"Passing Score"}
                    type={"number"}
                    name={"passing_score"}
                  />
                </Col>
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostTest}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit Test Course */}
      <Modal
        centered
        title="Form Edit Test"
        open={isModalVisibleEditTest}
        width={1200}
        onCancel={handleCancelEditTest}
        footer={null}
      >
        <Formik
          validationSchema={testValidationSchema}
          initialValues={formDataTest}
          enableReinitialize
          onSubmit={(values) => {
            handleSubmitEditTest(values);
          }}
        >
          {({ handleSubmit, isValid, values, errors, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
              >
                <Col span={24}>
                  <label className="required">Test Type</label>
                  <Text style={{ fontWeight: "bold" }}>{values.name}</Text>
                  {errors.test_type_id && (
                    <div className={"error"}>
                      {errors.test_type_id.toString()}
                    </div>
                  )}
                </Col>
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Duration (Minute)"}
                    title={"Duration (Minute)"}
                    type={"number"}
                    name={"duration"}
                  />
                </Col>
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Duration (Minute)"}
                    title={"Passing Score"}
                    type={"number"}
                    name={"passing_score"}
                  />
                </Col>
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingUpdateTest}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Create Subtitle */}
      <Modal
        centered
        title="Form Create Subtitle"
        open={isModalCreateSubtitle}
        width={1200}
        onCancel={() => setIsModalCreateSubtitle(false)}
        footer={null}
      >
        <Formik
          validationSchema={subtitleValidationSchema}
          initialValues={formDataSubtitle}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddSubtitle(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, values, errors, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
              >
                <Col span={24} style={{ marginBottom: 10 }}>
                  <div>
                    <label className="required">Language</label>
                  </div>
                  <Select
                    placeholder="Select Source Type"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("language", value)
                    }
                    options={[
                      { value: "en", label: "English" },
                      { value: "id", label: "Indonesia" },
                    ]}
                    value={values.language}
                    size={formSize}
                  />
                  {errors.language && (
                    <div className={"error"}>{errors.language.toString()}</div>
                  )}
                </Col>
              </Row>
              {values.filename ? (
                <Row>
                  <label className="required">File Name</label>
                  <Col
                    span={24}
                    style={{
                      alignItems: "center",
                      justifyItems: "center",
                      marginTop: 10,
                      marginBottom: 20,
                    }}
                  >
                    <Text>{values.filename}</Text>
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("filename", "");
                      }}
                    />
                  </Col>
                </Row>
              ) : (
                <>
                  <label className="required">
                    Upload File Subtitle (Format VTT) (Max. 2MB)
                  </label>
                  <Dragger
                    {...{
                      name: "file",
                      multiple: false,
                      maxCount: 1,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: SUBTITLE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        // console.log("file type", file.type.split("/")[0]);
                        // console.log("contentType", contentType);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "filename"),
                    }}
                    style={{ marginTop: 10 }}
                  >
                    <Text className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </Text>
                    <Text className="ant-upload-text">
                      Click or drag file to this area to upload
                    </Text>
                    <Text className="ant-upload-hint"></Text>
                  </Dragger>
                </>
              )}
              {errors.filename && (
                <div className={"error"}>{errors.filename.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingAddContent}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </>
  );
}
