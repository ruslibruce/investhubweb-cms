import { putTest } from "@/shared/api/fetch/test";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutTestCourse = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putTest,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["courseDetail"] });
          notification.success({
            message: "Test berhasil ditambahkan",
          });
        }
      },
    },
  });

  const handleUpdateTest = (data: {}, id: string) => {
    mutationQuery.mutate({data, id});
  };

  return {
    mutationQuery,
    handleUpdateTest,
  };
};

export default usePutTestCourse;
