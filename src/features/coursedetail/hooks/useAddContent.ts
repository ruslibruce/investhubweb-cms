import { postCreateCourseContent } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useAddContent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postCreateCourseContent,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["courseDetail"] });
          notification.success({
            message: `Content berhasil dibuat`,
          });
        }
      },
    },
  });

  const addContent = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    addContent,
  };
};

export default useAddContent;
