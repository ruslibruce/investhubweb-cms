import { deleteCourseSection } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteSection = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteCourseSection,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["courseDetail"] });
          notification.success({
            message: `Section berhasil dihapus`,
          });
        }
      },
    },
  });

  const deleteSection = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    deleteSection,
  };
};

export default useDeleteSection;
