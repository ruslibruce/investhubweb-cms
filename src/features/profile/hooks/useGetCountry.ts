import { getCountry } from "@/shared/api/fetch/portal-users";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = getCountry();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetCountry = (initialData?: any) => {
  const fetchDataCountry = getCountry();

  const fetchQuery = useFetchHook({
    keys: fetchDataCountry.key,
    api: fetchDataCountry.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetCountry;
