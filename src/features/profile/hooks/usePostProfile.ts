import { updateProfile } from "@/shared/api/mutation/user";
import { imageIAM } from "@/shared/constants/imageUrl";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostProfile = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER);

  const mutationQuery = useMutationHook({
    api: updateProfile,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["user"] });
          notification.success({
            message: "Profile anda berhasil diupdate",
          });
          let dataUserResult = {
            ...dataUser,
            user: {
              ...dataUser.user,
              ...variables,
              photo: `${imageIAM}/${variables?.photo}`,
            },
          };
          storageSet(USER, dataUserResult);
        }
      },
    },
  });

  const handleUpdateProfile = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleUpdateProfile,
  };
};

export default usePostProfile;
