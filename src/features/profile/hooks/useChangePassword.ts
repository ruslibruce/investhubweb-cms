import { changePass } from "@/shared/api/mutation/changePass";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useChangePassword = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER)

  const mutationQuery = useMutationHook({
    api: changePass,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);
        if (data) {
          const temp = {
            ...dataUser,
            user: {
              ...dataUser.user,
              first_login: false,
            }
          }
          console.log('temp ganti password', temp)
          storageSet(USER, temp)
          notification.success({
            message: "Password baru anda berhasil diupdate",
          });
        }
      },
    },
  });

  const postChangePassword = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    postChangePassword,
  };
};

export default useChangePassword;
