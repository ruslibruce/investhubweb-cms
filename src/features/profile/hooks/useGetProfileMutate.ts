import { getProfileInfoMutation } from "@/shared/api/mutation/user";
import {
  DASHBOARD_HOME,
  DASHBOARD_PROFILE,
  DASHBOARD_TERMS,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageSet } from "@/shared/utils/clientStorageUtils";
import { notification } from "antd";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
const useGetProfileMutate = () => {
  const navigate = useRouter();
  const { t: translate } = useTranslation();
  const mutationQuery = useMutationHook({
    api: getProfileInfoMutation,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        console.log("data", data);
        // console.log("variables", variables);
        // console.log("context", context);

        const { user, token, cp } = variables;
        // console.log("decodedToken", decodedToken);
        if (data) {
          const dataUser = {
            user: {
              ...user,
              ...data,
            },
            token: token,
            profile: cp,
            fcmToken: null,
          };
          console.log("dataUser get profile", dataUser);
          storageSet(USER, dataUser);
          if (!dataUser.profile?.is_active) {
            navigate.replace(DASHBOARD_TERMS);
          } else {
            if (dataUser.user?.first_login) {
              navigate.replace(DASHBOARD_PROFILE);
              return;
            }
            navigate.replace(DASHBOARD_HOME);
          }
        }
      },
    },
  });

  const handleGetProfile = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleGetProfile,
  };
};

export default useGetProfileMutate;
