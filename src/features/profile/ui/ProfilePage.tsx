import FormInputFormik from "@/components/FormInputFormik";
import { imageIAM } from "@/shared/constants/imageUrl";
import { IMAGE_FORMAT, USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { regexPhoneNumber } from "@/shared/utils/helper";
import {
  CopyOutlined,
  DeleteOutlined,
  FolderOpenTwoTone,
  SendOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Flex,
  Modal,
  Radio,
  Row,
  Select,
  Space,
  Upload,
  message,
  theme,
} from "antd";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React, { useState } from "react";
import { FaUserCircle } from "react-icons/fa";
import * as yup from "yup";
import YupPassword from "yup-password";
import useChangePassword from "../hooks/useChangePassword";
import useGetCountry from "../hooks/useGetCountry";
import usePostProfile from "../hooks/usePostProfile";
const { Dragger } = Upload;
YupPassword(yup); // extend yup with password method

type DataType = {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  totalUser: string;
  inProgress: string;
  completed: string;
  course: string;
};

const initialUserData = {
  name: "",
  date_of_birth: "",
  phone: "",
  gender: "",
  email: "",
  address: "",
  country: "",
  institution_id: "",
  photo: "",
};

const initialPassword = {
  old_password: "",
  new_password: "",
  new_password_confirmation: "",
};

const ProfilePage = () => {
  const { t: translate } = useTranslation();
  const { token } = theme.useToken();
  const dataUser = storageCheck(USER);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isEditModalChangePass, setIsEditModalChangePass] = useState(false);
  const [formChangePass, setFormChangePass] = useState(initialPassword);
  const [formData, setFormData] = useState<any>(initialUserData);
  const { postChangePassword, mutationQuery: mutationChangePassword } =
    useChangePassword();
  const {
    isPending: isLoadingChangePassword,
    isSuccess: isSuccessChangePassword,
  } = mutationChangePassword;
  const { fetchQuery } = useGetCountry([]);
  const { data: dataCountry } = fetchQuery;
  const { handleUpdateProfile, mutationQuery } = usePostProfile();
  const { isPending, isSuccess } = mutationQuery;
  const [isPicDeleted, setIsPicDeleted] = useState(false);
  const countryName = dataCountry?.find(
    (item: any) => item.key == dataUser?.user?.country
  );
  console.log("dataUser", dataUser);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (dataUser.user.first_login) {
      message.info({
        content: "Please update your password",
        duration: 5,
        style: {
          marginTop: "20vh",
          padding: 20,
        },
      });
    }
  }, [dataUser]);

  React.useEffect(() => {
    if (isSuccess) {
      setIsEditModalVisible(false);
    }

    if (isSuccessChangePassword) {
      setIsEditModalChangePass(false);
    }
  }, [isSuccess, isSuccessChangePassword]);

  const handleEditCancel = () => {
    setFormData(initialUserData);
    setIsEditModalVisible(false);
  };

  const handleEditProfileSubmit = (values: any) => {
    let idCountry = dataCountry.find(
      (item: any) => item.name == values.country
    );
    console.log("idCountry", idCountry);
    handleUpdateProfile({
      ...values,
      country: idCountry?.key,
    });
  };
  const handleChangePassCancel = () => {
    setIsEditModalChangePass(false);
  };

  const handleChangePassSubmit = (values: any) => {
    postChangePassword(values);
  };

  const handleOpenEdit = () => {
    setFormData({
      ...initialUserData,
      name: dataUser?.user?.name,
      date_of_birth: dataUser?.user?.date_of_birth,
      phone: dataUser?.user?.phone,
      gender: dataUser?.user?.gender,
      email: dataUser?.user?.email,
      address: dataUser?.user?.address,
      country: countryName ? countryName.value : dataUser?.user?.country,
      institution_id: dataUser?.user?.institution_id,
      photo: dataUser?.user?.photo.split("/").pop() ?? "",
    });
    setIsEditModalVisible(true);
  };

  const handleOnChange = (event: any) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleOpenChangePassword = () => {
    setFormChangePass(initialPassword);
    setIsEditModalChangePass(true);
  };

  const changePassValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    old_password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword")),
    new_password: yup
      .string()
      .min(
        8,
        ({ min }) =>
          `${translate("PasswordLength")} ${min} ${translate("Characters")}`
      )
      .required(translate("RequirePassword"))
      .minLowercase(1, translate("Lower_Password"))
      .minUppercase(1, translate("Upper_Password"))
      .minNumbers(1, translate("Number_Password"))
      .minWords(1, translate("Word_Password"))
      .minSymbols(1, translate("Symbol_Password")),
    new_password_confirmation: yup
      .string()
      .oneOf([yup.ref("new_password")], translate("Match_Password"))
      .required(translate("Confirm_New_Password")),
  });

  const profileValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        50,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
    address: yup
      .string()
      .max(
        100,
        ({ max }) =>
          `${translate("AddressLength")} ${max} ${translate("Characters")}`
      ),
    date_of_birth: yup.string(),
    phone: yup
      .string()
      .matches(regexPhoneNumber(), translate("ValidPhone"))
      .max(
        15,
        ({ max }) =>
          `${translate("PhoneNumberLength")} ${max} ${translate("Characters")}`
      ),
    country: yup.string().required(translate("RequireCountry")),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row>
        <Col span={21}>
          <span style={{ fontSize: 24, fontWeight: "bold" }}>
            My Personal Profile
          </span>
        </Col>
      </Row>
      <Row gutter={[8, 8]}>
        <Col span={24}>
          <Card>
            <Row
              style={{ justifyContent: "space-between", alignItems: "center" }}
              gutter={[8, 8]}
            >
              <Col>
                <Space direction="horizontal">
                  {dataUser?.user?.photo ? (
                    <Image
                      width={100}
                      height={100}
                      style={{ borderRadius: 100, width: 100, height: 100 }}
                      src={
                        dataUser?.user?.photo.includes("https")
                          ? dataUser?.user?.photo
                          : `${imageIAM}/${dataUser?.user?.photo}`
                      }
                      alt={"Icon Profile"}
                    />
                  ) : (
                    <FaUserCircle size={110} color="#9F0E0F" />
                  )}
                  <Space direction="vertical" style={{ marginLeft: 10 }}>
                    <p style={{ fontSize: 24, color: "#9F0E0F" }}>
                      {dataUser?.user?.name}
                    </p>
                    <p style={{ fontSize: 17 }}>{dataUser?.user?.email}</p>
                    <p style={{ fontSize: 17 }}>{dataUser?.user?.role}</p>
                  </Space>
                </Space>
              </Col>
              <Col>
                <Button
                  style={{
                    background: "#9F0E0F",
                    color: "white",
                  }}
                  onClick={handleOpenChangePassword}
                  icon={<CopyOutlined />}
                >
                  Change Password
                </Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row gutter={[8, 8]}>
        <Col span={24}>
          <Card>
            <Row
              gutter={[8, 8]}
              style={{ marginBottom: 30, justifyContent: "space-between" }}
            >
              <Col style={{ fontSize: 24, fontWeight: "bold" }}>
                Content Provider Profile
              </Col>
              <Col>
                <Button
                  style={{
                    marginBottom: 8,
                    background: "#9F0E0F",
                    color: "white",
                  }}
                  onClick={handleOpenEdit}
                  icon={<CopyOutlined />}
                >
                  Change
                </Button>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Name</span>
                  <span>{dataUser?.user?.name}</span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Adresss</span>
                  <span>{dataUser?.user?.address}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Country</span>
                  <span>{countryName?.name}</span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Date of Birth</span>
                  <span>{dataUser?.user?.date_of_birth}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Gender</span>
                  <span>
                    {dataUser?.user?.gender === "L" ? "Laki-Laki" : "Perempuan"}
                  </span>
                </Space>
              </Col>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Phone</span>
                  <span>{dataUser?.user?.phone ?? "-"}</span>
                </Space>
              </Col>
            </Row>
            <Row gutter={[8, 8]} style={{ marginBottom: 40 }}>
              <Col span={12}>
                <Space direction="vertical">
                  <span>Institution</span>
                  <span>
                    {dataUser?.user?.institution_id ? "Publik" : "OJK"}
                  </span>
                </Space>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Profile"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={profileValidationSchema}
          initialValues={formData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleEditProfileSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <p>{`${translate("PhotoProfile")} ${translate(
                "Optional"
              )} (Max. 2MB)`}</p>
              {!values.photo ? (
                <Dragger
                  {...{
                    name: "file",
                    multiple: true,
                    maxCount: 1,
                    action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-iam/upload-file`,
                    headers: {
                      authorization: `Bearer ${dataUser?.token}`,
                    },
                    accept: IMAGE_FORMAT,
                    beforeUpload: (file) => {
                      console.log("file beforeUpload", file);
                      const isSize = file.size / 1024 / 1024 <= 2;

                      if (!isSize) {
                        message.error(
                          `${file.name} size must be less than 2 MB`
                        );
                      }

                      return isSize || Upload.LIST_IGNORE;
                    },
                    onChange: (info) =>
                      handleUploadChange(info, setFieldValue, "photo"),
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    <FolderOpenTwoTone twoToneColor="#737373" />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to this area to upload
                  </p>
                  <p className="ant-upload-hint"></p>
                </Dragger>
              ) : (
                <Space
                  style={{ position: "relative", marginTop: 5 }}
                  direction="horizontal"
                >
                  <Image
                    alt="image edit"
                    width={200}
                    height={200}
                    style={{ width: 400, height: 200, borderRadius: 10 }}
                    src={`${imageIAM}/${formData.photo}`}
                  />
                  <DeleteOutlined
                    style={{
                      fontSize: 20,
                      color: "red",
                      position: "absolute",
                      top: 10,
                      right: 10,
                      backgroundColor: "white",
                      padding: 5,
                      borderRadius: 100,
                    }}
                    type="primary"
                    onClick={() => {
                      setFieldValue("photo", "");
                    }}
                  />
                </Space>
              )}
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterFullName")}
                title={translate("LabelFullName")}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterDateOfBirth")}
                title={`${translate("LabelDateOfBirth")} ${translate(
                  "Optional"
                )}`}
                type={"date"}
                name={"date_of_birth"}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterPhone")}
                title={`${translate("LabelPhone")} ${translate("Optional")}`}
                type={"text"}
                name={"phone"}
                isRequired={false}
              />
              <p style={{ marginBottom: 6, marginTop: 18 }}>
                {translate("LabelGender")}
              </p>
              <Radio.Group
                onChange={(event) =>
                  setFieldValue("gender", event.target.value)
                }
                name="gender"
                value={values.gender}
                style={{ marginBottom: 20 }}
              >
                <Radio value={"L"}>Male</Radio>
                <Radio value={"F"}>Female</Radio>
              </Radio.Group>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
                readOnly={true}
                isRequired={false}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterAddress")}
                title={`${translate("LabelAddress")} ${translate("Optional")}`}
                type={"text"}
                name={"address"}
                textArea={true}
                isRequired={false}
              />
              <label
                className="required"
                style={{ marginBottom: 6, marginTop: 18 }}
              >
                {translate("LabelCountry")}
              </label>
              <Select
                style={{ width: "100%", marginTop: 10 }}
                onChange={(e) => setFieldValue("country", e)}
                showSearch
                value={values.country}
                placeholder={translate("EnterCountry")}
                options={dataCountry}
                size={formSize}
              />
              {errors.country && (
                <div className={"error"}>{errors.country.toString()}</div>
              )}
              <p style={{ marginBottom: 6, marginTop: 18 }}>
                {translate("LabelIns")}
              </p>
              <Select
                style={{ width: "100%" }}
                onChange={(e) => setFieldValue("institution_id", e)}
                value={values.institution_id}
                placeholder={translate("EnterIns")}
                options={[
                  { value: "1", label: "Public" },
                  { value: "2", label: "OJK" },
                ]}
                size={formSize}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isPending}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Change Password */}
      <Modal
        centered
        title="Form Change Password"
        open={isEditModalChangePass}
        width={800}
        onCancel={handleChangePassCancel}
        footer={null}
      >
        <Formik
          validationSchema={changePassValidationSchema}
          initialValues={{
            old_password: "",
            new_password: "",
            new_password_confirmation: "",
          }}
          enableReinitialize
          onSubmit={(values) => {
            handleChangePassSubmit(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterOldPassword")}
                title={translate("LabelOldPassword")}
                type={"password"}
                name={"old_password"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterNewPassword")}
                title={translate("LabelNewPassword")}
                type={"password"}
                name={"new_password"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterNewsConfirmPassword")}
                title={translate("LabelNewsConfirmPassword")}
                type={"password"}
                name={"new_password_confirmation"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingChangePassword}
                  disabled={!isValid}
                >
                  Save
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default ProfilePage;
