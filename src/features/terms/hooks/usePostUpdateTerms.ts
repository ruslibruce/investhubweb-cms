import { postTerms } from "@/shared/api/mutation/terms";
import { DASHBOARD_HOME, DASHBOARD_PROFILE } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageCheck, storageSet } from "@/shared/utils/clientStorageUtils";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";

const usePostUpdateTerms = () => {
  const queryClient = useQueryClient();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: postTerms,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        if (data) {
          notification.success({
            message: "Terms anda berhasil diupdate",
          });
          let temp = {
            ...dataUser,
            profile: {
              ...dataUser.profile,
              is_active: true,
            },
          };
          storageSet(USER, temp);
          queryClient.invalidateQueries({ queryKey: ["user"] });
          if (dataUser.user?.first_login) {
            navigate.replace(DASHBOARD_PROFILE);
            return;
          }
          navigate.replace(DASHBOARD_HOME);
        }
      },
    },
  });

  const handleUpdateTerms = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleUpdateTerms,
  };
};

export default usePostUpdateTerms;
