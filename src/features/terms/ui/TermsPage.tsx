import { USER } from "@/shared/constants/storageStatis";
import useLogoutUser from "@/shared/hooks/useLogoutUser";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import type { CheckboxProps } from "antd";
import { Button, Card, Checkbox, Col, Row, Space, notification } from "antd";
import React, { useState } from "react";
import usePostUpdateTerms from "../hooks/usePostUpdateTerms";
import useGetTerms from "../hooks/useGetTerms";

const TermsPage = () => {
  const dataUser = storageCheck(USER);
  const { handleUpdateTerms } = usePostUpdateTerms();
  const [isChecked, setIsChecked] = useState(false);
  const { handleSignout } = useLogoutUser();
  const { fetchQuery } = useGetTerms({});
  const { data: dataPrivacy } = fetchQuery;
  console.log('dataUser', dataUser);

  const onChangeCheckbox: CheckboxProps["onChange"] = (e) => {
    console.log(`checked = ${e.target.checked}`);
    setIsChecked(e.target.checked);
  };

  const handleAcceptTerms = () => {
    if (!isChecked) {
      notification.info({
        message: "Harap centang syarat dan ketentuan yang berlaku",
      });
      return;
    }
    handleUpdateTerms({});
  };

  React.useEffect(() => {
    if (!dataUser.token) {
      notification.info({
        message: "Harap login terlebih dahulu",
      });
      handleSignout();
    }
  }, []);

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row>
        <Col span={24} style={{ textAlign: "center", marginTop: 20 }}>
          <span style={{ fontSize: 24, fontWeight: "bold" }}>
            TERMS CONDITION
          </span>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card>
            <Row
              style={{
                marginBottom: 30,
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Col span={24} style={{ fontSize: 24, fontWeight: "bold" }}>
                {dataPrivacy?.title}
              </Col>
            </Row>
            <Row style={{ marginBottom: 40 }}>
              <Col span={24}>
                <span
                  dangerouslySetInnerHTML={{
                    __html: dataPrivacy?.body,
                  }}
                />
              </Col>
            </Row>
            <Row gutter={16} style={{ marginBottom: 30 }}>
              <Col span={24} style={{ fontSize: 24, fontWeight: "bold" }}>
                <Checkbox onChange={onChangeCheckbox}>
                  Saya menyetujui syarat dan ketentuan yang berlaku
                </Checkbox>
              </Col>
            </Row>
            <Row
              gutter={16}
              style={{
                marginBottom: 30,
              }}
            >
              <Col span={8}>
                <Button
                  style={{
                    marginBottom: 8,
                    background: "#9F0E0F",
                    color: "white",
                  }}
                  onClick={handleAcceptTerms}
                >
                  Setuju
                </Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </Space>
  );
};

export default TermsPage;
