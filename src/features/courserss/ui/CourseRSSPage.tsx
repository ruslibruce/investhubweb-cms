import HamburgerCard from "@/components/HamburgerCard";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  getStatusColor,
  isYoutubeUrl,
  validateUrl,
} from "@/shared/utils/helper";
import {
  ArrowRightOutlined,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
} from "@ant-design/icons";
import type { DatePickerProps } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Divider,
  Flex,
  Input,
  message,
  Modal,
  Space,
  Spin,
} from "antd";
import moment from "moment";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useDeleteNewsRss from "../hooks/useDeleteNewsRss";
import useGetNewsRss from "../hooks/useGetNewsRss";
import usePostNewsRSS from "../hooks/usePostNewsRSS";
import usePostUpdateNewsRSS from "../hooks/usePostUpdateNewsRSS";
import NoData from "@/shared/components/NoData";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";

const initialFormData = {
  media_name: "",
  rss_link: "",
  is_active: "pending",
};

const CourseRSSPage = () => {
  const dataUser = storageCheck(USER);
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const { handlePostNewsRSS, mutationQuery: mutationPost } = usePostNewsRSS();
  const { isPending: isLoadingPostNewsRSS, isSuccess: isSuccessPostNewsRSS } =
    mutationPost;
  const { handlePostUpdateNewsRSS, mutationQuery: mutationUpdate } =
    usePostUpdateNewsRSS();
  const {
    isPending: isLoadingPostUpdateNewsRSS,
    isSuccess: isSuccessPostUpdateRSS,
  } = mutationUpdate;
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [uploadedFileName, setUploadedFileName] = useState("");
  const { fetchQuery } = useGetNewsRss([], currentPage);
  const { isLoading, data: dataRSS } = fetchQuery;
  const { data: newsDataRss } = dataRSS;
  const { mutationQuery: mutationDelete, handleDeleteNewsRss } =
    useDeleteNewsRss();
  const [formDataNews, setFormDataNews] = useState<any>(initialFormData);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [fontSize, setFontSize] = useState("initial");
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";

  React.useEffect(() => {
    if (isSuccessPostNewsRSS) {
      setIsAddModalVisible(false);
    }
    if (isSuccessPostUpdateRSS) {
      setIsEditModalVisible(false);
    }
  }, [isSuccessPostUpdateRSS, isSuccessPostNewsRSS]);

  const handleAddNewsRssSubmit = () => {
    handlePostNewsRSS(formDataNews);
  };

  const handleDeleteNewsRssSubmit = (id: string) => {
    handleDeleteNewsRss(id);
  };

  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };

  type DataType = {
    key: React.Key;
    no: number;
    category: string;
    link: string;
    publishDate: string;
    title: string;
    description: string;
    file: string;
  };

  const addModal = () => {
    setFormDataNews(initialFormData);
    setIsAddModalVisible(true);
  };

  const editModal = (value: any) => {
    setIsEditModalVisible(true);
    setFormDataNews({
      id: value.id,
      media_name: value.media_name,
      rss_link: value.rss_link,
      is_active: value.is_active,
    });
  };

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };
  }, []);

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const checkIsEmptyObject = (object: any) => {
    return Object.values(object).some((x) => x === null || x === "");
  };

  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    event.preventDefault();
    console.log("query", JSON.stringify(value, null, 2));
    handlePostUpdateNewsRSS(value.id, {
      ...value,
      is_active: params,
      publish_time: moment().format("YYYY-MM-DD"),
    });
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleEditNewsSubmit = () => {
    handlePostUpdateNewsRSS(formDataNews.id, {
      ...formDataNews,
      is_active: "draft",
    }); // Panggil dengan dua argumen
    setIsEditModalVisible(false);
  };

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const renderCards = () => {
    if (!newsDataRss) return <LoaderSpinGif size="large" />;
    if (newsDataRss && newsDataRss.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return newsDataRss?.map((card: any, index: any) => (
      <Card key={card.id}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(card.is_active),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {card.is_active}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              {/* <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleNews(e, { id: card.id })}
                />
              </span> */}
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => editModal(card)}
                />
              </span>
              <span style={{ marginRight: 10 }}>
                <DeleteTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => {
                    Modal.confirm({
                      title: "Delete Data",
                      content: "Are you sure you want to delete this data?",
                      onOk: () => handleDeleteNewsRssSubmit(card.id),
                      okButtonProps: {
                        icon: <DeleteOutlined />,
                        style: { backgroundColor: "#CB0B0C" },
                      },
                    });
                  }}
                />
              </span>
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                isView={false}
                onEdit={() => editModal(card)}
                onDelete={() => {
                  Modal.confirm({
                    title: "Delete Data",
                    content: "Are you sure you want to delete this data?",
                    onOk: () => handleDeleteNewsRssSubmit(card.id),
                    okButtonProps: {
                      icon: <DeleteOutlined />,
                      style: { backgroundColor: "#CB0B0C" },
                    },
                  });
                }}
                arrayItems={["1", "2"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p style={{ fontWeight: "bold" }}>{card.id}</p>
            <p style={{ fontWeight: "bold" }}>{card.media_name}</p>
            <p style={{ color: "gray" }}>
              {isMobile ? truncateTextToHalf(card.rss_link) : card.rss_link}
            </p>
          </div>
        </div>
      </Card>
    ));
  };

  const handleSetFormData = (event: any) => {
    // if (event.target.name === "rss_link" && !validateUrl(event.target.value)) {
    //   setFormDataNews((prev: any) => ({
    //     ...prev,
    //     [event.target.name]: "",
    //   }));
    //   return;
    // }

    // if (isYoutubeUrl(event.target.value)) {
    //   setFormDataNews((prev: any) => ({
    //     ...prev,
    //     [event.target.name]: "",
    //   }));
    //   return;
    // }

    setFormDataNews((prev: any) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Course Rss</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Course Rss",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={isSmallScreen ? "small" : "middle"}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
          >
            <PlusOutlined />
            Create Course Rss
          </Button>
        </div>
      </div>
      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search RSS Course</p>
            <p style={{ marginTop: "18px" }}>RSS Course Name</p>
            <Input
              size={isSmallScreen ? "small" : "middle"}
              placeholder="RSS Name"
            />
            <p style={{ marginTop: 18 }}>Link RSS</p>
            <Input.TextArea
              size={isSmallScreen ? "small" : "middle"}
              placeholder="Link RSS"
            />
            <Button
              size={isSmallScreen ? "small" : "middle"}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Search
            </Button>
          </Card>
        </div>
        <div className="category-table">
          {renderCards()}
          {newsDataRss && newsDataRss.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <div>
                <div>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataRSS?.records}
                    limit={dataRSS?.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataRSS?.records}
                    limit={dataRSS?.limit}
                  />
                </div>
              </div>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add News Rss"
        open={isAddModalVisible}
        width={1100}
        onCancel={handleCancel}
        footer={[
          <Button
            loading={isLoadingPostNewsRSS}
            icon={<SaveOutlined />}
            style={{
              backgroundColor: checkIsEmptyObject(formDataNews)
                ? "#a6a6a6"
                : "#CB0B0C",
              color: "white",
            }}
            type="primary"
            disabled={checkIsEmptyObject(formDataNews)}
            onClick={handleAddNewsRssSubmit}
          >
            Create
          </Button>,
        ]}
      >
        <p>RSS Name</p>
        <Input
          onChange={handleSetFormData}
          style={{ marginBottom: 12, marginTop: 8 }}
          name="media_name"
        />
        <p>Link RSS</p>
        <Input
          onChange={handleSetFormData}
          name="rss_link"
          style={{ marginBottom: 12, marginTop: 8 }}
          onBlur={(event) => {
            if (!validateUrl(event.target.value)) {
              message.error("Format content harus berupa link");
              setFormDataNews((prev: {}) => ({
                ...prev,
                rss_link: "",
              }));
            }

            if (isYoutubeUrl(event.target.value)) {
              message.error("Link tidak boleh berupa youtube link");
              setFormDataNews((prev: {}) => ({
                ...prev,
                rss_link: "",
              }));
            }

            // if (event.target.value.length > 50) {
            //   message.error("Format link harus kurang dari 50 karakter");
            //   setFormDataNews((prev: {}) => ({
            //     ...prev,
            //     rss_link: "",
            //   }));
            // }
          }}
        />
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Edit News"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={[
          <Flex gap="small">
            {formDataNews.is_active !== "draft" && !isKurator && (
              <Button
                loading={isLoadingPostUpdateNewsRSS}
                icon={<SaveOutlined />}
                style={{
                  backgroundColor: checkIsEmptyObject(formDataNews)
                    ? "#a6a6a6"
                    : getStatusColor("draft"),
                  color: "white",
                }}
                disabled={checkIsEmptyObject(formDataNews)}
                type="primary"
                onClick={handleEditNewsSubmit}
              >
                Save As Draft
              </Button>
            )}
            {formDataNews.is_active === "draft" && !isKurator && (
              <Button
                loading={isLoadingPostUpdateNewsRSS}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  backgroundColor: checkIsEmptyObject(formDataNews)
                    ? "#a6a6a6"
                    : getStatusColor("pending"),
                  color: "white",
                }}
                disabled={checkIsEmptyObject(formDataNews)}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, formDataNews, "pending")}
              >
                Publish
              </Button>
            )}
            {formDataNews.is_active === "pending" && isKurator && (
              <Button
                loading={isLoadingPostUpdateNewsRSS}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  backgroundColor: checkIsEmptyObject(formDataNews)
                    ? "#a6a6a6"
                    : getStatusColor("published"),
                  color: "white",
                }}
                disabled={checkIsEmptyObject(formDataNews)}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, formDataNews, "published")}
              >
                Publish
              </Button>
            )}
            {formDataNews.is_active === "published" && isKurator && (
              <Button
                loading={isLoadingPostUpdateNewsRSS}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  backgroundColor: checkIsEmptyObject(formDataNews)
                    ? "#a6a6a6"
                    : getStatusColor("unpublished"),
                  color: "white",
                }}
                disabled={checkIsEmptyObject(formDataNews)}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, formDataNews, "unpublished")}
              >
                UnPublish
              </Button>
            )}
            {formDataNews.is_active === "pending" ||
              (formDataNews.is_active === "published" && isKurator && (
                <Button
                  loading={isLoadingPostUpdateNewsRSS}
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                  style={{
                    backgroundColor: checkIsEmptyObject(formDataNews)
                      ? "#a6a6a6"
                      : getStatusColor("rejected"),
                    color: "white",
                  }}
                  disabled={checkIsEmptyObject(formDataNews)}
                  type="primary"
                  iconPosition="end"
                  onClick={(e) => handlePublish(e, formDataNews, "rejected")}
                >
                  Rejected
                </Button>
              ))}
          </Flex>,
        ]}
      >
        <p>RSS Name</p>
        <Input
          onChange={handleSetFormData}
          value={formDataNews.media_name}
          style={{ marginBottom: 12, marginTop: 8 }}
          name="media_name"
        />
        <p>Link RSS</p>
        <Input
          onChange={handleSetFormData}
          value={formDataNews.rss_link}
          name="rss_link"
          style={{ marginBottom: 12, marginTop: 8 }}
        />
      </Modal>
    </Space>
  );
};

export default CourseRSSPage;
