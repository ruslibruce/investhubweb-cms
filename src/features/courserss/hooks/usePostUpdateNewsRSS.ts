import { updateNewsRss } from "@/shared/api/mutation/newsRss";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";

const usePostUpdateNewsRSS = () => {
  const queryClient = useQueryClient();
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: updateNewsRss,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["rss"]] });
          notification.success({
            message: "The news RSS was successfully updated",
          });
        }
      },
    },
  });

  const handlePostUpdateNewsRSS = (id: string, data: {}) => {
    mutationQuery.mutate({ id, data });
  };

  return {
    mutationQuery,
    handlePostUpdateNewsRSS,
  };
};

export default usePostUpdateNewsRSS;
