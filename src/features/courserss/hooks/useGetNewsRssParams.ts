import { getCategoryNewsRss } from "@/shared/api/mutation/newsRss";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const useGetNewsRssParams = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: getCategoryNewsRss,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({ queryKey: ["investment"] });
      }
    }
  });

  const handleGetNewsRssParams = (value: string) => {
    mutationQuery.mutate({
      value
    });
  };

  return {
    mutationQuery,
    handleGetNewsRssParams
  };
};

export default useGetNewsRssParams;
