import { fetchNewsRss } from "@/shared/api/mutation/newsRss";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchNewsRss();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */


const useGetNewsRss = (initialData?: any, page?: number) => {
  const fetchDataNewsRss = fetchNewsRss(`?page=${page ?? 1}`);
  const fetchQuery = useFetchHook({
    keys: [fetchDataNewsRss.key, page],
    api: fetchDataNewsRss.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetNewsRss;
