import { editEventCategory } from "@/shared/api/mutation/eventCategory";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useEditEventCategory = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: editEventCategory,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["category-event"] });
          notification.success({
            message: "Kategori event berhasil diupdate",
          });
        }
      },
    },
  });

  const handleEditEventCategory = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleEditEventCategory,
  };
};

export { useEditEventCategory };
