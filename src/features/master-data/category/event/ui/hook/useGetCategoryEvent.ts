import { fetchEventCategory } from "@/shared/api/fetch/eventCategory";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchEventCategory();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */

const useGetEventCategory = (initialData?: any) => {
  const fetchDataEventCategory = fetchEventCategory();

  const fetchQuery = useFetchHook({
    keys: fetchDataEventCategory.key,
    api: fetchDataEventCategory.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetEventCategory;
