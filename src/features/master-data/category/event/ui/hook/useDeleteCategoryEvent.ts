import { deleteEventCategory } from "@/shared/api/mutation/eventCategory";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteEventCategory = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteEventCategory,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["category-event"] });
          notification.success({
            message: "The event category was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteEventCategory = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteEventCategory,
  };
};

export { useDeleteEventCategory };
