import {
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { TableColumnsType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Space,
  Table,
} from "antd";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";

import { checkIsEmptyObject } from "@/shared/utils/helper";
import { useDeleteEventCategory } from "./hook/useDeleteCategoryEvent";
import { useEditEventCategory } from "./hook/useEditCategoryEvent";
import useGetEventCategory from "./hook/useGetCategoryEvent";
import { usePostEventCategory } from "./hook/usePostCategoryEvent";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  name: "",
  // description: "",
};

const CategoryEventPage = () => {
  const { t: translate } = useTranslation();
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  // Get
  const { fetchQuery } = useGetEventCategory();
  const { data: dataEventCategory } = fetchQuery;
  const { mutationQuery: mutationPost, handlePostEventCategory } =
    usePostEventCategory();
  const { isPending: isLoadingPostEventCategory, isSuccess: isSuccessPost } =
    mutationPost;
  console.log("dataEventCategory", dataEventCategory);

  // Delete
  const { handleDeleteEventCategory } = useDeleteEventCategory();

  // Edit
  const { mutationQuery: mutationUpdate, handleEditEventCategory } =
    useEditEventCategory();
  const { isPending: isLoadingEditEventCategory, isSuccess: isSuccessEdit } =
    mutationUpdate;
  const [formDataEvent, setFormDataEvent] = useState<any>(initialFormData);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessEdit) {
      setIsEditModalVisible(false);
    }
    if (isSuccessPost) {
      setIsAddModalVisible(false);
    }
  }, [isSuccessEdit, isSuccessPost]);

  const editModal = (value: any) => {
    setFormDataEvent({
      id: value.id,
      name: value.name,
      description: value.name,
    });
    setIsEditModalVisible(true);
  };

  const handleEditSave = async (values: any) => {
    handleEditEventCategory(values.id, values);
  };

  const handleAddEventCategory = async (values: any) => {
    handlePostEventCategory({
      ...values,
      description: values.name,
    });
  };

  const handleDeleteEvent = (id: string) => {
    handleDeleteEventCategory(id);
  };

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormDataEvent((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleEditInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormDataEvent((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  type DataType = {
    id: string;
    key: React.Key;
    no: number;
    categoryName: string;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Category Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (value) => (
        <>
          <span style={{ marginRight: 12 }}>
            <EditTwoTone
              twoToneColor="#e5e500"
              onClick={() => editModal(value)}
            />
          </span>
          <DeleteTwoTone
            twoToneColor="#a6a6a6"
            onClick={() => {
              Modal.confirm({
                title: "Delete Data",
                content: "Are you sure you want to delete this data?",
                onOk: () => handleDeleteEvent(value.id),
                okButtonProps: {
                  icon: <DeleteOutlined />,
                  style: { backgroundColor: "#CB0B0C" },
                },
              });
            }}
          />
        </>
      ),
    },
  ];

  const addModal = () => {
    setFormDataEvent(initialFormData);
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
  });
  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p>Master Data</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Master Data",
              },
              {
                title: "Category",
              },
              {
                title: "Event",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={formSize}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
          >
            <PlusOutlined />
            Create Category
          </Button>
        </div>
      </div>

      <div className="category-content">
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>Event List</b>
            <Table
              scroll={{ x: 200 }}
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataEventCategory}
              loading={!dataEventCategory ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Category Event"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddEventCategory(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostEventCategory}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Category Event"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataEvent}
          enableReinitialize
          onSubmit={(values) => {
            handleEditSave(values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingEditEventCategory}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CategoryEventPage;
