import { editNewsCategory } from "@/shared/api/mutation/newsCategory";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useEditNewsCategory = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: editNewsCategory,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["category-news"] });
          notification.success({
            message: "Kategori berita berhasil diupdate",
          });
        }
      },
    },
  });

  const handleEditNewsCategory = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleEditNewsCategory,
  };
};

export { useEditNewsCategory };
