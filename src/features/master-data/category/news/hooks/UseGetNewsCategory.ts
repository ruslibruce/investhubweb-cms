import { fetchNewsCategory } from "@/shared/api/fetch/newsCategory";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchNewsCategory();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */

const useGetNewsCategory = (initialData?: any) => {
  const fetchDataNewsCategory = fetchNewsCategory();

  const fetchQuery = useFetchHook({
    keys: fetchDataNewsCategory.key,
    api: fetchDataNewsCategory.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetNewsCategory;
