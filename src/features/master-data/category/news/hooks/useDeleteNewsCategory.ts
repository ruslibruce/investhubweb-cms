import { deleteNewsCategory } from "@/shared/api/mutation/newsCategory";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteNewsCategory = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteNewsCategory,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["category-news"] });
          notification.success({
            message: "The news category was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteNewsCategory = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteNewsCategory,
  };
};

export { useDeleteNewsCategory };
