import { checkIsEmptyObject } from "@/shared/utils/helper";
import {
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import type { TableColumnsType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Space,
  Table,
} from "antd";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import { useDeleteNewsCategory } from "../hooks/useDeleteNewsCategory";
import { useEditNewsCategory } from "../hooks/useEditNewsCategory";
import useGetNewsCategory from "../hooks/UseGetNewsCategory";
import { usePostNewsCategory } from "../hooks/usePostNewsCategory";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  name: "",
};

const CategoryNewsPage = () => {
  const { t: translate } = useTranslation();
  const queryClient = new QueryClient();
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [formDataNews, setFormDataNews] = useState<any>(initialFormData);
  const { fetchQuery } = useGetNewsCategory();
  const { data: dataNewsCategory } = fetchQuery;
  const { mutationQuery: mutationPost, handlePostNewsCategory } =
    usePostNewsCategory();
  const { isPending: isLoadingPostNewsCategory, isSuccess: isSuccessPostNews } =
    mutationPost;
  console.log("dataNewsCategory", dataNewsCategory);

  // Delete
  const { mutationQuery: mutationDelete, handleDeleteNewsCategory } =
    useDeleteNewsCategory();
  const { isPending: isLoadingDeleteNewsCategory } = mutationDelete;

  // Edit
  const { mutationQuery: mutationUpdate, handleEditNewsCategory } =
    useEditNewsCategory();
  const { isPending: isLoadingEditNewsCategory, isSuccess: isSuccessEditNews } =
    mutationUpdate;
    const [formSize, setFormSize] = React.useState<SizeType>("large");
  
    React.useEffect(() => {
      const handleResize = () => {
        const screenWidth = window.innerWidth;
        if (screenWidth >= 1200) {
          setFormSize("middle");
        } else {
          setFormSize("large"); // Default size if needed
        }
      };
  
      window.addEventListener("resize", handleResize);
  
      // Call the function initially to set the correct form size on load
      handleResize();
  
      return () => {
        window.removeEventListener("resize", handleResize);
      };
    }, []);

  React.useEffect(() => {
    if (isSuccessEditNews) {
      setIsEditModalVisible(false);
    }
    if (isSuccessPostNews) {
      setIsAddModalVisible(false);
    }
  }, [isSuccessEditNews, isSuccessPostNews]);

  const editModal = (value: any) => {
    setFormDataNews({
      id: value.id,
      name: value.name,
      description: value.name,
    });
    setIsEditModalVisible(true);
  };

  const handleEditSave = async (values: any) => {
    handleEditNewsCategory(values.id, values);
  };

  type DataType = {
    id: string;
    key: React.Key;
    no: number;
    categoryName: string;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Category Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (value) => (
        <>
          <span style={{ marginRight: 12 }}>
            <EditTwoTone
              twoToneColor="#e5e500"
              onClick={() => editModal(value)}
            />
          </span>
          <DeleteTwoTone
            twoToneColor="#a6a6a6"
            onClick={() => {
              Modal.confirm({
                title: "Delete Data",
                content: "Are you sure you want to delete this data?",
                onOk: () => handleDeleteNews(value.id),
                okButtonProps: {
                  icon: <DeleteOutlined />,
                  style: { backgroundColor: "#CB0B0C" },
                  loading: isLoadingDeleteNewsCategory,
                },
              });
            }}
          />
        </>
      ),
    },
  ];

  const addModal = () => {
    setFormDataNews(initialFormData);
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleAddNewsCategory = async (values: any) => {
    handlePostNewsCategory({
      ...values,
      description: values.name,
    });
  };

  const handleDeleteNews = (id: string) => {
    handleDeleteNewsCategory(id);
  };

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormDataNews((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleEditInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormDataNews((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <QueryClientProvider client={queryClient}>
      <Space direction="vertical" style={{ display: "flex" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="indicator">
            <p>Master Data</p>
            <Breadcrumb
              separator=">"
              items={[
                {
                  title: "Master Data",
                },
                {
                  title: "Category",
                },
                {
                  title: "News",
                },
              ]}
            />
          </div>
          <div className="button-post-category">
            <Button
              size={formSize}
              type="primary"
              style={{ backgroundColor: "#CB0B0C" }}
              onClick={addModal}
            >
              <PlusOutlined />
              Create Category
            </Button>
          </div>
        </div>
        <div className="category-content">
          <div className="category-table">
            <Card>
              <b style={{ fontSize: 18 }}>Category News List</b>
              <Table
                style={{ marginTop: 20 }}
                columns={columns}
                dataSource={dataNewsCategory}
                scroll={{ x: 200 }}
                loading={!dataNewsCategory ? true : false}
              />
            </Card>
          </div>
        </div>

        {/* Modal Create */}
        <Modal
          centered
          title="Form Add Category News"
          open={isAddModalVisible}
          width={800}
          onCancel={handleCancel}
          footer={null}
        >
          <Formik
            validationSchema={addValidationSchema}
            initialValues={initialFormData}
            enableReinitialize
            onSubmit={(values, { resetForm }) => {
              handleAddNewsCategory(values);
              resetForm();
            }}
          >
            {({ handleSubmit, isValid }) => (
              <>
                <Field
                  component={FormInputFormik}
                  placeholder={"Category Name"}
                  title={"Category Name"}
                  type={"text"}
                  name={"name"}
                />
                <Flex justify="flex-end">
                  <Button
                    icon={<SendOutlined />}
                    style={{
                      marginTop: 18,
                      backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                      color: "white",
                    }}
                    type="primary"
                    onClick={(event: any) => handleSubmit(event)}
                    loading={isLoadingPostNewsCategory}
                    disabled={!isValid}
                  >
                    {translate("Create")}
                  </Button>
                </Flex>
              </>
            )}
          </Formik>
        </Modal>

        {/* Modal Edit */}
        <Modal
          centered
          title="Form Update Category News"
          open={isEditModalVisible}
          width={800}
          onCancel={handleEditCancel}
          footer={null}
        >
          <Formik
            validationSchema={addValidationSchema}
            initialValues={formDataNews}
            enableReinitialize
            onSubmit={(values) => {
              handleEditSave(values);
            }}
          >
            {({ handleSubmit, isValid }) => (
              <>
                <Field
                  component={FormInputFormik}
                  placeholder={"Category Name"}
                  title={"Category Name"}
                  type={"text"}
                  name={"name"}
                />
                <Flex justify="flex-end">
                  <Button
                    icon={<SendOutlined />}
                    style={{
                      marginTop: 18,
                      backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                      color: "white",
                    }}
                    type="primary"
                    onClick={(event: any) => handleSubmit(event)}
                    loading={isLoadingEditNewsCategory}
                    disabled={!isValid}
                  >
                    {translate("Update")}
                  </Button>
                </Flex>
              </>
            )}
          </Formik>
        </Modal>
      </Space>
    </QueryClientProvider>
  );
};

export default CategoryNewsPage;
