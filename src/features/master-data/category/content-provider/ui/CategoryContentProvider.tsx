import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Input,
  Select,
  Modal,
  Flex,
  Table,
} from "antd";
import {
  PlusOutlined,
  EditTwoTone,
  DeleteTwoTone,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";
import { DASHBOARD_NEWS_DETAIL } from "@/shared/constants/path";
import type { DatePickerProps, TableColumnsType } from "antd";
import { useRouter } from "next/router";
import "react-quill/dist/quill.snow.css";
import useGetContentProviderType from "@/features/content-provider/hooks/useGetContentProviderType";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";

const CategoryCPPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [uploadedFileName, setUploadedFileName] = useState("");
  // const [newsData, setNewsData] = useState<any[]>([]);
  const [category, setCategory] = useState("");
  const [status, setStatus] = useState("");
  const [publishDate, setPublishDate] = useState<string | string[]>([]);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [file, setFile] = useState(null);
  const { fetchQueryGetContentProviderType } = useGetContentProviderType();
  const { data: dataContentProviderType } = fetchQueryGetContentProviderType;
  console.log("dataContentProviderType", dataContentProviderType);

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    setStatus(value);
  };

  type DataType = {
    key: React.Key;
    no: number;
    name: string;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "id",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Category Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
    },
  ];

  const handleDatePickerChange: DatePickerProps["onChange"] = (
    date,
    dateString
  ) => {
    setPublishDate(dateString);
  };

  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };

  const handleNews = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_NEWS_DETAIL}/${value.id}`);
  };

  const addModal = () => {
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const editModal = () => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
    name_en: yup
      .string()
      .required(translate("CategoryNameENRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameENLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p>Master Data</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Master Data",
              },
              {
                title: "Category",
              },
              {
                title: "Content Provider",
              },
            ]}
          />
        </div>
        {/* <div className="button-post-category">
          <Button size={isSmallScreen ? "small" : "middle"} type="primary" style={{ backgroundColor: "#CB0B0C" }} onClick={addModal}>
            <PlusOutlined />
            Create Category
          </Button>
        </div> */}
      </div>
      <div className="category-content">
        {/* <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>Search Category Course</p>
            <p>Category Name</p>
            <Input className="category-search-input" size={isSmallScreen ? "small" : "middle"} placeholder="Category Name" />
            <Button size={isSmallScreen ? "small" : "middle"} type="primary" style={{ backgroundColor: "#CB0B0C", width: "100%" }}>
              Submit
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>Category Content Provider List</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataContentProviderType}
              scroll={{ x: 200 }}
              loading={!dataContentProviderType ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Category Course"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={{
            name: "",
            name_en: "",
          }}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Category Name (English)"}
                title={"Category Name (English)"}
                type={"text"}
                name={"name_en"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Category Course"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={{
            name: "",
            name_en: "",
          }}
          enableReinitialize
          onSubmit={(values) => {
            console.log("values", values);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Category Name (English)"}
                title={"Category Name (English)"}
                type={"text"}
                name={"name_en"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CategoryCPPage;
