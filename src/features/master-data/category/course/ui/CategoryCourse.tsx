import useGetCourseCategory from "@/features/course/hooks/useGetCourseCategory";
import { checkIsEmptyObject } from "@/shared/utils/helper";
import {
  DeleteFilled,
  DeleteTwoTone,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { TableColumnsType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Space,
  Table,
} from "antd";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import { useCreateCategoryCourse } from "../hooks/useCreateCategoryCourse";
import { useDeleteCategoryCourse } from "../hooks/useDeleteCategoryCourse";
import { useEditCategoryCourse } from "../hooks/useEditCategoryCourse";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  name: "",
  name_en: "",
};

const CategoryCoursePage = () => {
  const { t: translate } = useTranslation();
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [formDataCourse, setFormDataCourse] = useState<any>(initialFormData);
  const { createCourseCategory, mutationQuery: mutationQueryCreate } =
    useCreateCategoryCourse();
  const { isSuccess: isSuccessCreate, isPending: isLoadingCreate } =
    mutationQueryCreate;
  const { runDeleteCourseCategory, mutationQuery: mutationQueryDelete } =
    useDeleteCategoryCourse();
  const { isSuccess: isSuccessDelete, isPending: isLoadingDelete } =
    mutationQueryDelete;
  const { editCourseCategory, mutationQuery: mutationQueryEdit } =
    useEditCategoryCourse();
  const { isSuccess: isSuccessEdit, isPending: isLoadingEdit } =
    mutationQueryEdit;

  // GET
  const { fetchQuery } = useGetCourseCategory();
  const { data: dataCourseCategory } = fetchQuery;
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  type DataType = {
    key: React.Key;
    no: number;
    categoryName: string;
    name: string;
    name_en: string;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Category Name (English)",
      dataIndex: "name_en",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Category Name (Indonesia)",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (value) => (
        <>
          <span style={{ marginRight: 12 }}>
            <EditTwoTone
              twoToneColor="#e5e500"
              onClick={() => editModal(value)}
            />
          </span>
          <DeleteTwoTone
            onClick={() =>
              Modal.confirm({
                title: "Delete Data",
                content: "Are you sure you want to delete this Course?",
                okText: "Yes",
                cancelText: "No",
                onOk: () => runDeleteCourseCategory(value.id),
                okButtonProps: {
                  icon: <DeleteFilled />,
                  style: { backgroundColor: "#CB0B0C" },
                  loading: isLoadingDelete,
                },
              })
            }
            twoToneColor="#ff0000"
          />
        </>
      ),
    },
  ];

  const addModal = () => {
    setFormDataCourse(initialFormData);
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const editModal = (values: any) => {
    setFormDataCourse({
      ...initialFormData,
      id: values.id,
      name: values.name,
      name_en: values.name_en,
    });
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  React.useEffect(() => {
    if (isSuccessCreate) {
      setIsAddModalVisible(false);
    }
    if (isSuccessEdit) {
      setIsEditModalVisible(false);
    }
  }, [isSuccessCreate, isSuccessEdit]);

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
    name_en: yup
      .string()
      .required(translate("CategoryNameENRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameENLength")} ${max} ${translate(
            "Characters"
          )}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p>Master Data</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Master Data",
              },
              {
                title: "Category",
              },
              {
                title: "Course",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={formSize}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
          >
            <PlusOutlined />
            Create Category
          </Button>
        </div>
      </div>
      <div className="category-content">
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>Category Course List</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataCourseCategory}
              scroll={{ x: 200 }}
              loading={!dataCourseCategory ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Category Course"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            createCourseCategory(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Category Name (English)"}
                title={"Category Name (English)"}
                type={"text"}
                name={"name_en"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingCreate}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Category Course"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataCourse}
          enableReinitialize
          onSubmit={(values) => {
            editCourseCategory(values, values.id);
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Category Name"}
                title={"Category Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Category Name (English)"}
                title={"Category Name (English)"}
                type={"text"}
                name={"name_en"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingEdit}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CategoryCoursePage;
