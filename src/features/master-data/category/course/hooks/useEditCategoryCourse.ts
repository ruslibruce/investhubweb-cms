import { putCourseCategory } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useEditCategoryCourse = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putCourseCategory,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["course-categories"] });
          notification.success({
            message: "Kategori Course berhasil diupdate",
          });
        }
      },
    },
  });

  const editCourseCategory = (data: {}, id: string) => {
    mutationQuery.mutate({ data, id });
  };

  return {
    mutationQuery,
    editCourseCategory,
  };
};

export { useEditCategoryCourse };
