import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Input,
  Select,
  Modal,
  Flex,
  DatePicker,
  message,
  Upload,
  Table,
} from "antd";
import {
  PlusOutlined,
  EditTwoTone,
  DeleteTwoTone,
  SaveOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";
import { DASHBOARD_NEWS_DETAIL } from "@/shared/constants/path";
import type { DatePickerProps, TableColumnsType } from "antd";
import { useRouter } from "next/router";
import "react-quill/dist/quill.snow.css";

const RolePage = () => {
  const navigate = useRouter();
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  type DataType = {
    key: React.Key;
    no: number;
    roleName: string;
    description: string;
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Role Name",
      dataIndex: "roleName",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Description",
      dataIndex: "description",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    // {
    //   title: "Action",
    //   dataIndex: "",
    //   key: "x",
    //   render: () => (
    //     <>
    //       <span style={{ marginRight: 12 }}>
    //         <EditTwoTone twoToneColor="#e5e500" onClick={editModal} />
    //       </span>
    //       <DeleteTwoTone twoToneColor="#ff0000" />
    //     </>
    //   ),
    // },
  ];

  const data: DataType[] = [
    {
      no: 1,
      key: "1",
      roleName: "Portal Admin",
      description: "Role Portal Admin",
    },
    {
      no: 2,
      key: "2",
      roleName: "Portal User",
      description: "Role Portal User",
    },
    {
      no: 3,
      key: "3",
      roleName: "Komite Kurator Konten",
      description: "Role Komite Kurator Konten",
    },
    {
      no: 4,
      key: "4",
      roleName: "Content Provider Admin",
      description: "Role Content Provider Admin",
    },
    {
      no: 5,
      key: "5",
      roleName: "Admin Content Provider - OJK",
      description: "Role Admin Content Provider - OJK",
    },
    {
      no: 6,
      key: "6",
      roleName: "Learner",
      description: "Learner",
    },
  ];

  const addModal = () => {
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const editModal = () => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Role</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "User Management",
              },
              {
                title: "Role",
              },
            ]}
          />
        </div>
        {/* <div className="button-post-category">
          <Button size={isSmallScreen ? "small" : "middle"} type="primary" style={{ backgroundColor: "#CB0B0C" }} onClick={addModal}>
            <PlusOutlined />
            Create Role
          </Button>
        </div> */}
      </div>
      <div className="category-content">
        {/* <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>Search Role</p>
            <p>Role Name</p>
            <Input className="category-search-input" size={isSmallScreen ? 'small' : 'middle'} placeholder="Role Name" />
            <Button type="primary" size={isSmallScreen ? 'small' : 'middle'} style={{ backgroundColor: "#CB0B0C", width: "100%" }}>
              Submit
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>Role List</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={data}
              scroll={{ x: 200 }}
              loading={!data ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Role"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            // onClick={handleAddNewsSubmit}
          >
            Save
          </Button>,
        ]}
      >
        <p style={{ marginBottom: 6 }}>Role Name</p>
        <Input style={{ height: 50 }} placeholder="Name" />
        <p style={{ marginTop: 18 }}>Description</p>
        <Input.TextArea style={{ height: 50 }} placeholder="Description" />
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Update Role"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={[
          <Button
            icon={<SaveOutlined />}
            style={{ backgroundColor: "#CB0B0C", color: "white" }}
            type="primary"
            onClick={handleCancel}
          >
            Save
          </Button>,
        ]}
      >
        <p style={{ marginBottom: 6 }}>Role Name</p>
        <Input style={{ height: 50 }} placeholder="Name" />
        <p style={{ marginTop: 18 }}>Description</p>
        <Input.TextArea style={{ height: 50 }} placeholder="Description" />
      </Modal>
    </Space>
  );
};

export default RolePage;
