import HamburgerCard from "@/components/HamburgerCard";
import NoData from "@/shared/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { imagePORTAL } from "@/shared/constants/imageUrl";
import { DASHBOARD_NEWS_DETAIL } from "@/shared/constants/path";
import {
  CATEGORY_STATUS,
  IMAGE_FORMAT,
  TOOLBAR_OPTION_QUILL,
  USER,
} from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import {
  checkIsEmptyObject,
  getStatusColor,
  isEmptyContent,
  isYoutubeUrl,
  ReactQuillNoSSR,
  regexUrl,
  validateUrl,
} from "@/shared/utils/helper";
import { paramsToString } from "@/shared/utils/string";
import {
  ArrowRightOutlined,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  EyeTwoTone,
  FolderOpenTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { DatePickerProps, UploadProps } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Flex,
  Input,
  message,
  Modal,
  Row,
  Select,
  Space,
  Spin,
  Upload,
} from "antd";
import dayjs from "dayjs";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useDeleteNews from "../hooks/useDeleteNews";
import useGetNews from "../hooks/useGetNews";
import useGetNewsCategory from "../hooks/useGetNewsCategory";
import useGetRSSToNews from "../hooks/useGetRSSToNews";
import usePostNews from "../hooks/usePostNews";
import useUpdateNews from "../hooks/useUpdateNews";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";
const { Dragger } = Upload;

const initialFormData = {
  category: "",
  publish_time: "",
  subject: "",
  body: "",
  cover_image: "",
  status: "draft",
  link: "",
};

const NewsPage = () => {
  const dataUser = storageCheck(USER);
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const { t: translate } = useTranslation();
  const { mutationQuery: mutationPost, handlePostNews } = usePostNews();
  const { isPending: isLoadingPostNews, isSuccess: isSuccessPostNews } =
    mutationPost;
  const { mutationQuery: mutationDelete, handleDeleteNews } = useDeleteNews();
  // const { isLoading, data } = fetchQuery;
  const { fetchQuery: fetchCategory } = useGetNewsCategory([]);
  const { isLoading: isLoadingCategory, data: dataFetchCategory } =
    fetchCategory;
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const [dataFilter, setDataFilter] = React.useState({
    category: "",
    status: "",
    subject: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState({
    category: "",
    status: "",
    subject: "",
  });
  const { fetchQuery } = useGetNews(
    [],
    paramsToString({ page: currentPage, ...dataFilterApi })
  );

  const { data: dataNewsResult, isLoading } = fetchQuery;
  const { data: dataNews } = dataNewsResult;
  const { handleUpdateNews, mutationQuery: mutationQueryUpdateNews } =
    useUpdateNews();
  const { isPending: isLoadingUpdateNews, isSuccess: isSuccessUpdateNews } =
    mutationQueryUpdateNews;

  const navigate = useRouter();

  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [formDataNews, setFormDataNews] = useState<any>(initialFormData);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [fontSize, setFontSize] = useState("initial");
  const { handleUpdateRSSToNews } = useGetRSSToNews();
  const [dataCategoryNews, setDataCategoryNews] = useState<any>([]);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  console.log("dataNews", dataNews);

  React.useEffect(() => {
    if (dataFetchCategory) {
      const filteredArray = dataFetchCategory.filter(
        (obj: any) => obj.name !== "Rss News"
      );
      setDataCategoryNews(filteredArray);
    }
  }, [dataFetchCategory]);

  React.useEffect(() => {
    if (isSuccessPostNews) {
      setIsAddModalVisible(false);
    }

    if (isSuccessUpdateNews) {
      setIsEditModalVisible(false);
    }
  }, [isSuccessPostNews, isSuccessUpdateNews]);

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };
  }, []);

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const handleAddNewsSubmit = (values: any) => {
    handlePostNews(values);
  };

  const handleDeleteNewsSubmit = (id: string) => {
    handleDeleteNews(id);
  };

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    if (value === "all") {
      setDataFilter({
        ...dataFilter,
        status: "",
      });
      return;
    }
    setDataFilter({
      ...dataFilter,
      status: value,
    });
  };

  const handleDatePickerChange = (
    date: dayjs.Dayjs,
    dateString: string | string[],
    setFieldValue: any
  ) => {
    console.log("dateString", dateString);
    console.log("date", date);
    setFieldValue("publish_time", dateString);
  };

  const onChangeEditDate: DatePickerProps["onChange"] = (date, dateString) => {
    console.log("dateString", dateString);
    setFormDataNews((prev: {}) => ({
      ...prev,
      publish_time: dateString,
    }));
  };

  const handleNews = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_NEWS_DETAIL}/${value.id}`);
  };

  const addModal = () => {
    setFormDataNews(initialFormData);
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const editModal = (news: any) => {
    setFormDataNews({
      category: news.category,
      publish_time: news.publish_time,
      subject: news.subject,
      body: news.body,
      cover_image: news.cover_image.split("/").pop(),
      id: news.id,
      status: news.status,
      link: news.link,
    });
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handlePublish = (value: any, params: string) => {
    console.log("query", JSON.stringify(value, null, 2));
    let category = dataFetchCategory.find((item: any) =>
      typeof value.category === "string"
        ? item.name == value.category
        : item.id == value.category
    );
    handleUpdateNews(value.id, {
      ...value,
      status: params,
      category: category.id,
      cover_image: value.cover_image.split("/").pop(),
    });
  };

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter({
      ...dataFilter,
      category: value,
    });
  };

  const handleSetFilter = () => {
    console.log("dataFilter", dataFilter);
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };

  function checkIsEmptyObjectExceptLink(obj: { [key: string]: any }): boolean {
    for (const key in obj) {
      if (
        key !== "link" &&
        (obj[key] === undefined || obj[key] === null || obj[key] === "")
      ) {
        return true;
      }
    }
    return false;
  }

  const renderCards = () => {
    if (!dataNews) return <LoaderSpinGif size="large" />;

    if (dataNews && dataNews.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );

    return dataNews?.map((card: any, index: any) => (
      <Card key={card.id}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p
            style={{
              color: getStatusColor(card.status),
              fontWeight: "bold",
              textTransform: "capitalize",
              fontSize: fontSize,
            }}
          >
            {card.status}
          </p>

          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleNews(e, { id: card.id })}
                />
              </span>

              {!card.source_id && !isKurator && (
                <span style={{ marginRight: 10 }}>
                  <EditTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={() => editModal(card)}
                  />
                </span>
              )}
              {!isKurator && (
                <span style={{ marginRight: 10 }}>
                  <DeleteTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={() => {
                      Modal.confirm({
                        title: "Delete Data",
                        content: "Are you sure you want to delete this data?",
                        onOk: () => handleDeleteNewsSubmit(card.id),
                        okButtonProps: {
                          icon: <DeleteOutlined />,
                          style: { backgroundColor: "#CB0B0C" },
                        },
                      });
                    }}
                  />
                </span>
              )}
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleNews(e, { id: card.id })}
                onEdit={() => editModal(card)}
                onDelete={() => {
                  Modal.confirm({
                    title: "Delete Data",
                    content: "Are you sure you want to delete this data?",
                    onOk: () => handleDeleteNewsSubmit(card.id),
                    okButtonProps: {
                      icon: <DeleteOutlined />,
                      style: { backgroundColor: "#CB0B0C" },
                    },
                  });
                }}
                arrayItems={["0", "1", "2"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p
              style={{ fontWeight: "bold" }}
              dangerouslySetInnerHTML={{ __html: card.subject }}
            />
            <p
              style={{ color: "gray" }}
              dangerouslySetInnerHTML={{
                __html: isMobile ? truncateTextToHalf(card.body) : card.body,
              }}
            />
            {card.link && (
              <Link href={card.link} target="_blank">
                <p style={{ color: "gray" }}>{card.link}</p>
              </Link>
            )}
          </div>
        </div>
      </Card>
    ));
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    category: yup.string().required(translate("CategoryRequired")),
    cover_image: yup.string().required(translate("CoverPictureRequired")),
    publish_time: yup.string().required(translate("PublishTimeRequired")),
    subject: yup
      .string()
      .required(translate("ContentSubjectRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("ContentSubjectLength")} ${max} ${translate(
            "Characters"
          )}`
      ),
    body: yup.string().required(translate("ContentBodyRequired")),
    link: yup
      .string()
      .matches(regexUrl(), translate("LinkMatches"))
      .max(
        150,
        ({ max }) =>
          `${translate("LinkLength")} ${max} ${translate("Characters")}`
      )
      .nullable(),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>News</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "News",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            loading={isLoadingPostNews}
            size={formSize}
            type="primary"
            style={{ backgroundColor: "green", marginRight: 10 }}
            onClick={handleUpdateRSSToNews}
            icon={<PlusOutlined />}
          >
            Fetch RSS News
          </Button>
          {!isKurator && (
            <Button
              loading={isLoadingPostNews}
              size={formSize}
              type="primary"
              style={{ backgroundColor: "#CB0B0C" }}
              onClick={addModal}
              icon={<PlusOutlined />}
            >
              Create News
            </Button>
          )}
        </div>
      </div>
      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search News</p>
            <p style={{ marginTop: 18 }}>Title</p>
            <Input
              style={{ width: "100%", marginTop: 10 }}
              size={formSize}
              placeholder="Title"
              onChange={(e) =>
                setDataFilter({
                  ...dataFilter,
                  subject: e.target.value,
                })
              }
            />
            <p style={{ marginTop: 18 }}>Category</p>
            <Select
              style={{ width: "100%", marginTop: 10 }}
              size={formSize}
              defaultValue="Category"
              onChange={handleChange}
              options={dataFetchCategory}
            />
            <p style={{ marginTop: 12 }}>Status</p>
            <Select
              style={{ width: "100%", marginTop: 10 }}
              size={formSize}
              defaultValue="All"
              onChange={handleChangeStatus}
              options={CATEGORY_STATUS}
            />
            <Button
              type="primary"
              size={formSize}
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
              onClick={handleSetFilter}
            >
              Submit
            </Button>
          </Card>
        </div>
        <div className="category-table">
          {renderCards()}
          {dataNews && dataNews.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <Row>
                <Col>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataNewsResult.records}
                    limit={dataNewsResult.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataNewsResult?.records}
                    limit={dataNewsResult?.limit}
                  />
                </Col>
              </Row>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add News"
        open={isAddModalVisible}
        width={1100}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleAddNewsSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={20}
              >
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    size={formSize}
                    style={{
                      width: 300,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    options={dataCategoryNews}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    value={values.category}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Publish Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleDatePickerChange(date, dateString, setFieldValue)
                    }
                    size={formSize}
                    style={{
                      width: 300,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={
                      values.publish_time ? dayjs(values.publish_time) : null
                    }
                  />
                  {errors.publish_time && (
                    <div className={"error"}>
                      {errors.publish_time.toString()}
                    </div>
                  )}
                </Col>
                <Col>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Link"}
                    title={"Link (Optional)"}
                    type={"text"}
                    name={"link"}
                    isRequired={false}
                  />
                </Col>
              </Row>
              <label className="required">Subject</label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("subject", "");
                    return;
                  }
                  setFieldValue("subject", value);
                }}
                style={{ marginBottom: 12, marginTop: 8 }}
                value={values.subject}
              />
              {errors.subject && (
                <div className={"error"}>{errors.subject.toString()}</div>
              )}
              <label className="required">Body</label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("body", "");
                    return;
                  }
                  setFieldValue("body", value);
                }}
                style={{ marginBottom: 12, marginTop: 8 }}
                value={values.body}
              />
              {errors.body && (
                <div className={"error"}>{errors.body.toString()}</div>
              )}
              {!values.cover_image ? (
                <>
                  <label className="required" style={{ marginBottom: 10 }}>
                    Upload File(Max. 2MB)
                  </label>
                  <Dragger
                    {...{
                      name: "file",
                      // multiple: true,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                </>
              ) : (
                <Col span={12}>
                  <div>
                    <label className="required">Image Cover</label>
                  </div>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={`${imagePORTAL}/${values.cover_image}`}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              {errors.cover_image && (
                <div className={"error"}>{errors.cover_image.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingPostNews}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Edit News"
        open={isEditModalVisible}
        width={1100}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataNews}
          enableReinitialize
          onSubmit={(values) => {
            console.log("values", values);
            handlePublish(values, "pending");
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={20}
              >
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    size={formSize}
                    style={{
                      width: 300,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    options={dataCategoryNews}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    value={values.category}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col
                  style={{
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Publish Date</label>
                  <DatePicker
                    onChange={(date, dateString) =>
                      handleDatePickerChange(date, dateString, setFieldValue)
                    }
                    size={formSize}
                    style={{
                      width: 300,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    value={
                      values.publish_time ? dayjs(values.publish_time) : null
                    }
                  />
                  {errors.publish_time && (
                    <div className={"error"}>
                      {errors.publish_time.toString()}
                    </div>
                  )}
                </Col>
                <Col>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Link"}
                    title={"Link (Optional)"}
                    type={"text"}
                    name={"link"}
                    isRequired={false}
                  />
                </Col>
              </Row>
              <label className="required">Subject</label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("subject", "");
                    return;
                  }
                  setFieldValue("subject", value);
                }}
                style={{ marginBottom: 12, marginTop: 8 }}
                value={values.subject}
              />
              {errors.subject && (
                <div className={"error"}>{errors.subject.toString()}</div>
              )}
              <label className="required">Body</label>
              <ReactQuillNoSSR
                modules={TOOLBAR_OPTION_QUILL}
                onChange={(value: string) => {
                  if (isEmptyContent(value)) {
                    setFieldValue("body", "");
                    return;
                  }
                  setFieldValue("body", value);
                }}
                style={{ marginBottom: 12, marginTop: 8 }}
                value={values.body}
              />
              {errors.body && (
                <div className={"error"}>{errors.body.toString()}</div>
              )}
              {!values.cover_image ? (
                <>
                  <label className="required" style={{ marginBottom: 10 }}>
                    Upload File (Max. 2MB)
                  </label>
                  <Dragger
                    {...{
                      name: "file",
                      // multiple: true,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                </>
              ) : (
                <Col span={12}>
                  <div>
                    <label className="required">Image Cover</label>
                  </div>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={`${imagePORTAL}/${values.cover_image}`}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              {errors.cover_image && (
                <div className={"error"}>{errors.cover_image.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingUpdateNews}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default NewsPage;
