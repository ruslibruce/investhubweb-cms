import { fetchUpdateRSSToNews } from "@/shared/api/mutation/newsRss";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useGetRSSToNews = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: fetchUpdateRSSToNews,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["news"]] });
          notification.success({
            message: "The news rss was successfully updated to news database",
          });
        }
      },
    },
  });

  const handleUpdateRSSToNews = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleUpdateRSSToNews,
  };
};

export default useGetRSSToNews;
