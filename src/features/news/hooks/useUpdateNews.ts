import { putNews } from "@/shared/api/mutation/news";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useUpdateNews = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putNews,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["news"]] });
          notification.success({
            message: "The news was successfully updated",
          });
        }
      },
    },
  });

  const handleUpdateNews = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleUpdateNews,
  };
};

export default useUpdateNews;
