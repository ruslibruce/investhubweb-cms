import { deleteNews } from "@/shared/api/mutation/news";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteNews = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteNews,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["news"]] });
          notification.success({
            message: "The news was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteNews = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteNews,
  };
};

export default useDeleteNews;
