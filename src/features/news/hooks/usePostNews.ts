import { addNews } from "@/shared/api/mutation/news";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostNews = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: addNews,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["news"]] });
          notification.success({
            message: "The news was successfully created",
          });
        }
      },
    },
  });

  const handlePostNews = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostNews,
  };
};

export default usePostNews;
