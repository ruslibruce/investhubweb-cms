import { fetchNews } from "@/shared/api/fetch/news";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchNews();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */

const useGetNews = (initialData?: any, params?: string) => {
  const fetchDataNews = fetchNews(params);

  const fetchQuery = useFetchHook({
    keys: [fetchDataNews.key, params],
    api: fetchDataNews.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetNews;
