import { DASHBOARD_GENERAL_CONTENT } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Breadcrumb, Card, Space, Spin } from "antd";
import { useRouter } from "next/router";
import useGetDetailNews from "../hooks/useGetDetailGeneralContent";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";

const GeneralContentDetailPage = () => {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { id } = navigate.query;
  const { fetchDetailGeneralContent } = useGetDetailNews(id as string, {});
  const { data: generalContentDetail, isLoading } = fetchDetailGeneralContent;
  console.log("generalContentDetail", generalContentDetail);
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";

  if(isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(`${generalContentDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(!generalContentDetail) {
    return <NoData />
  }

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Detail News</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "General Content",
            href: `/cms/${DASHBOARD_GENERAL_CONTENT}`,
          },
          {
            title: `Detail ${generalContentDetail.title}`,
          },
        ]}
      />
      <Card>
        <p
          style={{ fontWeight: "bold", fontSize: 18, marginBottom: 13}}
        >{`Detail ${generalContentDetail.title}`}</p>
        <b>Title</b>
        <p
          style={{ color: "gray", marginBottom: 13 }}
          dangerouslySetInnerHTML={{ __html: generalContentDetail?.title }}
        />
        <b>Body</b>
        <p
          style={{ color: "gray", marginBottom: 13 }}
          dangerouslySetInnerHTML={{ __html: generalContentDetail?.body }}
        />
      </Card>
    </Space>
  );
};

export default GeneralContentDetailPage;
