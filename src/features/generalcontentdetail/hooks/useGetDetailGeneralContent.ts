import { fetchGeneralContentDetail } from "@/shared/api/fetch/generalContent";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchGeneralContentDetail();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDetailGeneralContent = (id: string, initialData?: any) => {
  const fetchDataColors = fetchGeneralContentDetail(id);
  const fetchDetailGeneralContent = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {}
  });

  return {
    fetchDetailGeneralContent
  };
};

export default useGetDetailGeneralContent;
