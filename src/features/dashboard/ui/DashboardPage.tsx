import {
  Space,
  Row,
  Col,
  Button,
  Card,
  Input,
  Table,
  Calendar,
  Select,
  DatePicker,
  theme,
  Flex,
} from "antd";
import type { CalendarProps } from "antd";
import type { Dayjs } from "dayjs";
import {
  UserOutlined,
  FileTextOutlined,
  TeamOutlined,
  EditOutlined,
  EyeOutlined,
  DownOutlined,
  SlidersOutlined,
} from "@ant-design/icons";
import type { DatePickerProps, TableColumnsType, TableProps } from "antd";
import React, { useState } from "react";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { USER } from "@/shared/constants/storageStatis";

const onChange: DatePickerProps["onChange"] = (date, dateString) => {
  console.log(date, dateString);
};

const onPanelChange = (value: Dayjs, mode: CalendarProps<Dayjs>["mode"]) => {
  console.log(value.format("YYYY-MM-DD"), mode);
};

const columns: TableColumnsType<DataType> = [
  {
    title: "Course",
    dataIndex: "course",
  },
  {
    title: "Total User",
    dataIndex: "totalUser",

    sorter: (a, b) => a.name.length - b.name.length,
    // sortDirections: ["descend"]
  },
  {
    title: "Completed",
    dataIndex: "completed",
    // defaultSortOrder: "descend",
    sorter: (a, b) => a.age - b.age,
  },
  {
    title: "In Progress",
    dataIndex: "inProgress",
    sorter: (a, b) => a.age - b.age,
  },
];

const data: DataType[] = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    course: "Investing 101",
    totalUser: "812 User",
    completed: "610 User",
    inProgress: "202 User",
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    course: "Investing 101",
    totalUser: "1,8K User",
    completed: "1K User",
    inProgress: "800 User",
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sydney No. 1 Lake Park",
    course: "Investing 101",
    totalUser: "12K User",
    completed: "11K User",
    inProgress: "1K User",
  },
  {
    key: "4",
    name: "Jim Red",
    age: 32,
    address: "London No. 2 Lake Park",
    course: "Investing 101",
    totalUser: "9.2K User",
    completed: "200 User",
    inProgress: "9K User",
  },
];

type DataType = {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  totalUser: string;
  inProgress: string;
  completed: string;
  course: string;
};

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};

const DashboardPage = () => {
  const { token } = theme.useToken();
  const dataUser = storageCheck(USER);

  const wrapperStyle: React.CSSProperties = {
    marginTop: 12,
    width: 350,
    border: `1px solid ${token.colorBorderSecondary}`,
    borderRadius: token.borderRadiusLG,
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup function to remove event listener
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div>
          <span style={{ fontSize: 24, fontWeight: "bold" }}>
            Welcome back, {dataUser?.user?.name}
          </span>
          {/* <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p> */}
        </div>
        {/* <div>
          <DatePicker size={isSmallScreen ? "small" : "middle"} onChange={onChange} />
        </div> */}
      </div>
      {/* <div className="dashboard-menu">
        <div>
          <Card>
            <Row>
              <Col span={22}>
                <p style={{ fontSize: 17 }}>Total User</p>
                <p style={{ fontSize: 24, fontWeight: "bold" }}>6,784</p>
                <p>+150 Today</p>
              </Col>
              <Col span={2}>
                <UserOutlined style={{ fontSize: 20 }} />
              </Col>
            </Row>
          </Card>
        </div>
        <div>
          <Card>
            <Row>
              <Col span={22}>
                <p style={{ fontSize: 17 }}>Total Course</p>
                <p style={{ fontSize: 24, fontWeight: "bold" }}>1,920</p>
                <p>+20 Today</p>
              </Col>
              <Col span={2}>
                <FileTextOutlined style={{ fontSize: 20 }} />
              </Col>
            </Row>
          </Card>
        </div>
        <div>
          <Card>
            <Row>
              <Col span={22}>
                <p style={{ fontSize: 17 }}>Active User</p>
                <p style={{ fontSize: 24, fontWeight: "bold" }}>4,412</p>
                <p>+98 Today</p>
              </Col>
              <Col span={2}>
                <UserOutlined style={{ fontSize: 20 }} />
              </Col>
            </Row>
          </Card>
        </div>
        <div>
          <Card>
            <Row>
              <Col span={22}>
                <p style={{ fontSize: 17 }}>Online User</p>
                <p style={{ fontSize: 24, fontWeight: "bold" }}>329</p>
                <p>+29 Today</p>
              </Col>
              <Col span={2}>
                <TeamOutlined style={{ fontSize: 20 }} />
              </Col>
            </Row>
          </Card>
        </div>
      </div> */}
      <div style={{ width: "100%", display: "flex", flexWrap: "wrap" }}>
        {/* <div className="dashboard-table">
          <Card>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", marginBottom: "20px", flexWrap: "wrap" }}>
              <div>Recent Course</div>
              <div>
                <Button size={isSmallScreen ? "small" : "middle"} style={{ marginRight: 8 }} icon={<SlidersOutlined />}>
                  Filters
                </Button>
                <Button
                  size={isSmallScreen ? "small" : "middle"}
                  style={{
                    color: "#ff3333",
                    fontWeight: "bold",
                    backgroundColor: "#ffe5e5",
                  }}
                >
                  See All
                </Button>
              </div>
            </div>
            <Table columns={columns} dataSource={data} scroll={{ x: 200 }} />
          </Card>
        </div> */}
        <Flex className="calendar-container" style={{ paddingTop: "20px" }}>
          <Card>
            <div>
              <Col>Calendar</Col>
            </div>
            <div>
              <Calendar
                style={{ width: "100%" }}
                className="calendar"
                fullscreen={false}
                onPanelChange={onPanelChange}
              />
            </div>
          </Card>
        </Flex>
      </div>
    </Space>
  );
};

export default DashboardPage;
