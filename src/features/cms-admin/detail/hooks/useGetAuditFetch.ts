import { fetchAuditTrail } from "@/shared/api/mutation/audit-trail";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchAuditTrail();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetAuditFetch = (params: string, initialData?: any) => {
  const fetchDataCollectionUsers = fetchAuditTrail(params);
  const fetchQuery = useFetchHook({
    keys: [fetchDataCollectionUsers.key, params],
    api: fetchDataCollectionUsers.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetAuditFetch;
