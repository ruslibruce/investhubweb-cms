import { getExportAuditTrail } from "@/shared/api/mutation/audit-trail";
import { deleteUser } from "@/shared/api/mutation/user";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { Modal, notification } from "antd";

const useGetAuditExportLog = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: getExportAuditTrail,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);

        if (data) {
          queryClient.invalidateQueries({ queryKey: [["audit_trail"]] });
          notification.success({
            message: "berhasil export audit log",
          });
        }
      },
    },
  });

  const handleExportAuditLog = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    handleExportAuditLog,
  };
};

export default useGetAuditExportLog;
