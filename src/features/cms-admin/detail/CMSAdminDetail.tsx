import useGetUserDetail from "@/features/portal-users/detail/hooks/useGetUserDetail";
import NoData from "@/shared/components/NoData";
import {
  USER_MANAGEMENT_CMSADMIN,
  USER_MANAGEMENT_PORTAL_USERS_COLLECTION_DETAIL,
} from "@/shared/constants/path";
import iconBookMark from "@/shared/images/icon/bookmark.png";
import { COLORS } from "@/shared/styles/color";
import { paramsToString } from "@/shared/utils/string";
import {
  BookOutlined,
  DownloadOutlined,
  HistoryOutlined,
  InboxOutlined,
  UserOutlined,
} from "@ant-design/icons";
import type { DatePickerProps, MenuProps, TableColumnsType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  DatePicker,
  Flex,
  Input,
  Menu,
  Row,
  Space,
  Table,
} from "antd";
import dayjs from "dayjs";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetAuditExportLog from "./hooks/useGetAuditExportLog";
import useGetAuditFetch from "./hooks/useGetAuditFetch";
import useGetCollection from "./hooks/useGetCollection";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";

const items: MenuProps["items"] = [
  {
    label: "Profile Info",
    key: "profile",
    icon: <UserOutlined />,
  },
  {
    label: "Course",
    key: "course",
    icon: <BookOutlined />,
  },
  {
    label: "Collections",
    key: "collection",
    icon: <InboxOutlined />,
  },
  {
    label: "Audit Log",
    key: "audit",
    icon: <HistoryOutlined />,
  },
];

const cardDataCourse = [
  {
    title: "Course 1",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "1",
  },
  {
    title: "Course 2",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "2",
  },
  {
    title: "Course 3",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "3",
  },
  {
    title: "Course 4",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "4",
  },
  {
    title: "Course 5",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "5",
  },
  {
    title: "Course 6",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "6",
  },
  {
    title: "Course 7",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "7",
  },
  {
    title: "Course 8",
    description: "Description ",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7KAeP00lNSlAuNUXle4uhPfGvZ33tj9rhXQk56i73eQ&s",
    key: "8",
  },
];

const CMSAdminDetail = () => {
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id = slug?.[0];
  const { fetchQuery } = useGetCollection(id as string);
  const { isLoading: isLoadingCollection, data: dataCollection } = fetchQuery;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [current, setCurrent] = useState("profile");
  const [params, setParams] = useState<any>({
    userid: id as string,
    search: "",
    start_date: "",
    end_date: "",
  });
  const [paramsApi, setParamsApi] = useState({
    userid: id as string,
    page: 1,
  });
  const { fetchQuery: fetchAudit } = useGetAuditFetch(
    paramsToString(paramsApi)
  );
  const { data: dataResultTrailLog, isLoading: isLoadingTrailLog } = fetchAudit;
  const dataTrailLog = dataResultTrailLog?.data;
  const { fetchQueryUserDetail } = useGetUserDetail(id as string);
  const { data: dataUserDetail } = fetchQueryUserDetail;
  const { handleExportAuditLog } = useGetAuditExportLog();
  console.log("dataUserDetail:", dataUserDetail);
  console.log("dataTrailLog:", dataTrailLog);

  const handleCollectionDetail = (e: any, card: any) => {
    console.log("card", card);
    navigate.push(
      `${USER_MANAGEMENT_PORTAL_USERS_COLLECTION_DETAIL}/${card.id}`
    );
  };

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = () => {
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  type DataAudit = {
    key: React.Key;
    no: number;
    action: string;
    ip_address: string;
    datetime: Date;
    description: string;
  };

  const columnsAudit: TableColumnsType<DataAudit> = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Activity",
      dataIndex: "action",
      width: 300,
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Description",
      dataIndex: "description",
      width: 300,
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "IP Address",
      dataIndex: "ip_address",
      width: 300,
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Date Time",
      dataIndex: "datetime",
      width: 300,
      sorter: (a, b) => a.no - b.no,
      render: (text) => moment(text).format("DD MMM YYYY HH:mm"),
    },
  ];

  const handleSubmitSearch = () => {
    console.log("params", params);
    setParamsApi(params);
  };

  const handleStartTimeChange: DatePickerProps["onChange"] = (
    date,
    dateString
  ) => {
    setParams((prev: any) => ({
      ...prev,
      start_date: dateString,
    }));
  };

  const handleEndTimeChange: DatePickerProps["onChange"] = (
    date,
    dateString
  ) => {
    setParams((prev: any) => ({
      ...prev,
      end_date: dateString,
    }));
  };

  const onChange = (event: any) => {
    setParams((prev: any) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>{`Detail User ${
        !dataUserDetail || `${dataUserDetail.id}` !== id
          ? ""
          : dataUserDetail?.name
      }`}</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "User Management",
          },
          {
            title: "CMS Admin",
            href: `/cms/${USER_MANAGEMENT_CMSADMIN}`,
          },
          {
            title: `Detail User ${
              !dataUserDetail || `${dataUserDetail.id}` !== id
                ? ""
                : dataUserDetail?.name
            }`,
          },
        ]}
      />
      <Menu
        onClick={onClick}
        selectedKeys={[current]}
        mode="horizontal"
        items={items}
      />
      {current === "profile" && (
        <Card
          loading={
            !dataUserDetail
              ? true
              : `${dataUserDetail.id}` !== id
              ? true
              : false
          }
        >
          <p style={{ fontWeight: "bold", fontSize: 18, marginBottom: 18 }}>
            Profile Info
          </p>
          <b>Name</b>
          <p style={{ color: "gray", marginBottom: 12 }}>
            {dataUserDetail?.name}
          </p>
          <b>Email</b>
          <p style={{ color: "gray", marginBottom: 12 }}>
            {dataUserDetail?.email}
          </p>
          <b>Contact Number</b>
          <p style={{ color: "gray", marginBottom: 12 }}>
            {dataUserDetail?.phone ?? "-"}
          </p>
          <b>Role</b>
          <p style={{ color: "gray", marginBottom: 12 }}>
            {dataUserDetail?.role}
          </p>
          <b>Created At</b>
          <p style={{ color: "gray", marginBottom: 12 }}>
            {dataUserDetail?.created_at}
          </p>
        </Card>
      )}
      {current === "course" && (
        <Row gutter={2}>
          {cardDataCourse.map((card) => (
            <Col span={6} key={card.key}>
              <Card
                style={{ width: 240, marginTop: 15 }}
                cover={
                  <Image
                    width={100}
                    height={100}
                    alt="example"
                    src={card.imageUrl}
                  />
                }
              >
                <Row>
                  <Col span={10}>
                    <b style={{ fontSize: 18 }}>{card.title}</b>
                    <p style={{ fontSize: 14 }}>{card.description}</p>
                  </Col>
                  <Col span={14}></Col>
                </Row>
              </Card>
            </Col>
          ))}
        </Row>
      )}
      {current === "collection" && (
        <Flex>
          {dataCollection && dataCollection.length === 0 && (
            <Card
              style={{
                marginTop: 15,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <NoData />
            </Card>
          )}
          {isLoadingCollection && (
            <Card
              style={{
                marginTop: 15,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <LoaderSpinGif size="large" />
            </Card>
          )}
          {dataCollection &&
            dataCollection.length > 0 &&
            dataCollection.map((card: any) => (
              <Card
                key={card.key}
                style={{
                  marginTop: 15,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                  display: "flex",
                }}
                // onClick={(e) => handleCollectionDetail(e, card)}
              >
                <Row gutter={10} style={{ alignItems: "center" }}>
                  <Col>
                    <Image src={iconBookMark} alt="Bookmark" />
                  </Col>
                  <Col>
                    <b style={{ fontSize: 18 }}>{card.name}</b>
                  </Col>
                </Row>
              </Card>
            ))}
        </Flex>
      )}
      {current === "audit" && (
        <Row
          style={{
            width: "100%",
            justifyContent: "space-between",
          }}
          gutter={[16, 16]}
          wrap
        >
          <Col>
            <Card style={{ width: "100%" }}>
              <p style={{ fontWeight: "bold" }}>Search Log</p>
              <p style={{ marginTop: "18px" }}>Activity</p>
              <Input
                onChange={onChange}
                name="search"
                placeholder="Input Activity"
              />
              <p>Start Date</p>
              <DatePicker
                value={params.start_date ? dayjs(params.start_date) : null}
                onChange={handleStartTimeChange}
                style={{ width: "100%", height: 50 }}
                allowClear
              />
              <p>End Date</p>
              <DatePicker
                value={params.end_date ? dayjs(params.end_date) : null}
                onChange={handleEndTimeChange}
                style={{ width: "100%", height: 50 }}
                allowClear
              />
              <Button
                type="primary"
                loading={isLoadingTrailLog}
                style={{
                  backgroundColor: "#CB0B0C",
                  marginTop: 20,
                  width: "100%",
                }}
                onClick={handleSubmitSearch}
              >
                Search
              </Button>
            </Card>
          </Col>
          <Col>
            {dataTrailLog && dataTrailLog.length === 0 && (
              <Card
                style={{
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                  display: "flex",
                }}
              >
                <NoData />
              </Card>
            )}
            {isLoadingTrailLog && (
              <Card
                style={{
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                  display: "flex",
                }}
              >
                <LoaderSpinGif size="large" />
              </Card>
            )}
            {dataTrailLog && dataTrailLog.length > 0 && (
              <Table
                title={() => (
                  <Flex justify="end">
                    <Link
                      href={`${
                        process.env.NEXT_PUBLIC_HOST_NAME
                      }/api-log/audit-trail-logs/export${paramsToString(
                        params
                      )}`}
                    >
                      <Button
                        icon={<DownloadOutlined />}
                        iconPosition="end"
                        type="primary"
                        style={{ backgroundColor: COLORS.PRIMARY }}
                        // onClick={() =>
                        //   handleExportAuditLog(paramsToString(paramsApi))
                        // }
                      >
                        Export
                      </Button>
                    </Link>
                  </Flex>
                )}
                style={{
                  width: "100%",
                  backgroundColor: "#fff",
                  padding: 20,
                  borderRadius: 10,
                }}
                columns={columnsAudit}
                dataSource={dataTrailLog}
                scroll={{ x: 830 }}
                loading={isLoadingTrailLog}
                pagination={{
                  total: dataResultTrailLog?.records,
                  current: paramsApi.page,
                  onChange: (page) => setParamsApi({ ...paramsApi, page }),
                  showTotal: (total) => `Total ${total} items`,
                  pageSize: dataResultTrailLog.limit,
                  showSizeChanger: false,
                }}
              />
            )}
          </Col>
        </Row>
      )}
    </Space>
  );
};

export default CMSAdminDetail;
