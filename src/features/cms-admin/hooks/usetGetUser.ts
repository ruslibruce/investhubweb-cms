import { fetchCmsAdmin } from "@/shared/api/fetch/cms-admin";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchCmsAdmin();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};


const useGetDataCmsAdmin = (initialData?: any) => {
  const fetchDataCmsAdmin = fetchCmsAdmin();

  const fetchQuery = useFetchHook({
    keys: fetchDataCmsAdmin.key,
    api: fetchDataCmsAdmin.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetDataCmsAdmin;
