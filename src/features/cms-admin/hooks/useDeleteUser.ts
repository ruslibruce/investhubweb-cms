import { deleteUser } from "@/shared/api/mutation/user";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { Modal, notification } from "antd";

const useDeleteUser = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteUser,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);

        if (data) {
          queryClient.invalidateQueries({
            queryKey: ["portal-users"],
          });
          queryClient.invalidateQueries({ queryKey: ["cms-admin"] });
          notification.success({
            message: "User berhasil dihapus",
          });
        }
      },
    },
  });

  const handleOnDelete = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    handleOnDelete,
  };
};

export default useDeleteUser;
