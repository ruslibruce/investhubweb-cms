export const modelUser = (data: any[], pageSize: number, page: number) => {
  const model: any[] = [
    ...(data as any[]),
  ].map((x, i) => ({
    ...x,
    key: i,
    no: i + 1 + pageSize * (page - 1),
  }));

  return model;
};