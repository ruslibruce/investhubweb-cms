import FormInputFormik from "@/components/FormInputFormik";
import useGetDataContentProvider from "@/features/content-provider/hooks/useGetContent";
import useInviteUser from "@/features/content-provider/hooks/useInviteUser";
import { USER_MANAGEMENT_CMSADMIN_DETAIL } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  DeleteFilled,
  DeleteOutlined,
  DeleteTwoTone,
  EyeOutlined,
  PlusOutlined,
  SearchOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { InputRef, TableColumnsType, TableColumnType } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Select,
  Space,
  Table,
  notification,
} from "antd";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import "react-quill/dist/quill.snow.css";
import * as yup from "yup";
import useGetUser from "../hooks/usetGetUser";
import { regexEmail } from "@/shared/utils/helper";
import useDeleteUser from "../hooks/useDeleteUser";
import { FilterDropdownProps } from "antd/es/table/interface";
import Highlighter from "react-highlight-words";
import { COLORS } from "@/shared/styles/color";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const CMSAdminPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { fetchQuery } = useGetUser();
  const { data: dataCmsAdmin } = fetchQuery;
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const dataUser = storageCheck(USER).user;
  const { fetchQuery: fetchDataCP } = useGetDataContentProvider([]);
  const { data: categoryCP } = fetchDataCP;
  const { mutationQuery: mutationInvite, handlePostContentProviderInvite } =
    useInviteUser();
  const { isPending } = mutationInvite;
  const { handleOnDelete } = useDeleteUser();
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef<InputRef>(null);
  console.log("dataCmsAdmin", dataCmsAdmin);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isPending) {
      setIsAddModalVisible(false);
    }
  }, [isPending]);

  type DataType = {
    key: React.Key;
    no: number;
    name: string;
    email: string;
    contactNumber: string;
    role: string;
  };

  type DataIndex = keyof DataType;

  const handleSearch = (
    selectedKeys: string[],
    confirm: FilterDropdownProps["confirm"],
    dataIndex: DataIndex
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (
    clearFilters: () => void,
    confirm: FilterDropdownProps["confirm"]
  ) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const getColumnSearchProps = (
    dataIndex: DataIndex
  ): TableColumnType<DataType> => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div style={{ padding: 8 }} onKeyDown={(e) => e.stopPropagation()}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(selectedKeys as string[], confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(selectedKeys as string[], confirm, dataIndex)
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90, backgroundColor: COLORS.PRIMARY }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters, confirm)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
            style={{color: COLORS.PRIMARY}}
          >
            Filter
          </Button> */}
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
            style={{ color: COLORS.PRIMARY }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined
        style={{ color: filtered ? COLORS.PRIMARY : undefined }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns: TableColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a, b) => a.no - b.no,
      ...getColumnSearchProps("name"),
    },
    {
      title: "Email",
      dataIndex: "email",
      // defaultSortOrder: "descend",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Institution",
      dataIndex: "institution",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Role",
      dataIndex: "role",
      sorter: (a, b) => a.no - b.no,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "dataCmsAdmin",
      render: (value) => (
        <Flex gap={10}>
          <span style={{ marginLeft: 12 }}>
            <EyeOutlined onClick={(e) => handleUserDetail(e, value)} />
          </span>
          <span style={{ marginRight: 12 }}>
            <DeleteTwoTone
              twoToneColor="#ff0000"
              onClick={() =>
                Modal.confirm({
                  title: "Delete User",
                  content: "Are you sure you want to delete this user?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleOnDelete(value.id),
                  okButtonProps: {
                    icon: <DeleteFilled />,
                    style: { backgroundColor: "#CB0B0C" },
                  },
                })
              }
            />
          </span>
        </Flex>
      ),
    },
  ];

  const addModal = () => {
    setIsAddModalVisible(true);
  };

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const handleInviteUser = (value: any) => {
    if (
      dataUser?.role === "portal-admin" &&
      value.role === "cp-admin" &&
      !value.content_provider_id
    ) {
      notification.warning({
        message: "Please choose Content Provider",
      });
      return;
    }

    handlePostContentProviderInvite(value);
  };

  const handleUserDetail = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    navigate.push(
      `${USER_MANAGEMENT_CMSADMIN_DETAIL}/${value.id}`
    );
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup function to remove event listener
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const inviteValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    email: yup
      .string()
      .matches(regexEmail(), translate("ValidEmail"))
      .max(
        30,
        ({ max }) =>
          `${translate("EmailLength")} ${max} ${translate("Characters")}`
      )
      .required(translate("RequireEmail")),
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        50,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>CMS Admin</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "User Management",
              },
              {
                title: "CMS Admin",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={isSmallScreen ? "small" : "middle"}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
          >
            <PlusOutlined />
            Add User
          </Button>
        </div>
      </div>
      <div className="category-content">
        {/* <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>Search User</p>
            <p>Name</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Name" />
            <p style={{ marginTop: "18px" }}>Contact Number</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Contact Number" />
            <p style={{ marginTop: 18 }}>Role</p>
            <Input size={isSmallScreen ? "small" : "middle"} placeholder="Role" />
            <p style={{ marginTop: 12 }}>Status</p>
            <Select
              size={isSmallScreen ? "small" : "middle"}
              defaultValue="active"
              style={{ width: "100%" }}
              onChange={handleChangeStatus}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-active", label: "Non-Active" },
              ]}
            />
            <Button size={isSmallScreen ? "small" : "middle"} type="primary" style={{ backgroundColor: "#CB0B0C", marginTop: 25, width: "100%" }}>
              Submit
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          <Card>
            <b style={{ fontSize: 18 }}>User List</b>
            <Table
              style={{ marginTop: 20 }}
              columns={columns}
              dataSource={dataCmsAdmin}
              scroll={{ x: 200 }}
              loading={!dataCmsAdmin ? true : false}
            />
          </Card>
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Add CMS Admin User"
        open={isAddModalVisible}
        width={800}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={inviteValidationSchema}
          initialValues={{
            name: "",
            email: "",
            role: "portal-admin",
            content_provider_id: "1",
          }}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleInviteUser(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Name"}
                title={"Name"}
                type={"text"}
                name={"name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={translate("EnterEmail")}
                title={translate("LabelEmail")}
                type={"email"}
                name={"email"}
              />
              {dataUser?.role === "portal-admin" ? (
                <>
                  <p style={{ marginTop: 18 }}>Role</p>
                  <Select
                    placeholder="Select Role"
                    style={{ width: 750 }}
                    value={values.role}
                    onChange={(value) => {
                      setFieldValue("role", value);
                      if (value !== "cp-admin") {
                        setFieldValue("content_provider_id", 1);
                      } else {
                        setFieldValue("content_provider_id", "");
                      }
                    }}
                    options={[
                      { value: "portal-admin", label: "Portal Admin" },
                      { value: "kurator", label: "Kurator" },
                      { value: "cp-admin", label: "CP Admin" },
                    ]}
                    size={formSize}
                  />
                  {errors.role && (
                    <div className={"error"}>{errors.role.toString()}</div>
                  )}
                  {values.role === "cp-admin" ? (
                    <>
                      <p style={{ marginTop: 18 }}>Content Provider</p>
                      <Select
                        placeholder="Select CP Admin"
                        style={{ width: 750 }}
                        value={values.content_provider_id}
                        onChange={(value) => {
                          setFieldValue("content_provider_id", value);
                        }}
                        options={categoryCP}
                        size={formSize}
                      />
                      {errors.content_provider_id && (
                        <div className={"error"}>
                          {errors.content_provider_id.toString()}
                        </div>
                      )}
                    </>
                  ) : null}
                </>
              ) : null}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isPending}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CMSAdminPage;
