import { postTest } from "@/shared/api/fetch/test";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostTest = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postTest,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["test"] });
          notification.success({
            message: "Test berhasil ditambahkan",
          });
        }
      },
    },
  });

  const handlePostTest = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handlePostTest,
  };
};

export default usePostTest;
