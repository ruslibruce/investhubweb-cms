import { DASHBOARD_TEST_QUESTION } from "@/shared/constants/path";
import { PlusOutlined, SendOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  Modal,
  Row,
  Select,
  Space,
} from "antd";
import { useRouter } from "next/router";

import FormInputFormik from "@/components/FormInputFormik";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { BUTTON_COLOR } from "@/shared/constants/storageStatis";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import * as yup from "yup";
import useGetLevelType from "../hooks/useGetLevelType";
import useGetTest from "../hooks/useGetTest";
import useGetTestType from "../hooks/useGetTestType";
import usePostTest from "../hooks/usePostTest";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  name: "",
  duration: "",
  passing_score: "",
  test_type_id: "",
  parent_id: "",
};
const TestPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { fetchQueryAllTest } = useGetTest();
  const { data: dataTest, isLoading } = fetchQueryAllTest;
  const [formData, setFormData] = useState<any>(initialFormData);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { handlePostTest, mutationQuery } = usePostTest();
  const { isSuccess, isPending: isLoadingPost } = mutationQuery;
  const { fetchCategoryType } = useGetTestType([]);
  const { data: dataTestType } = fetchCategoryType;
  const { fetchCategoryLevelType } = useGetLevelType([]);
  const { data: dataLevelType } = fetchCategoryLevelType;
  console.log("dataTest", dataTest);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccess) {
      setIsModalVisible(false);
    }
  }, [isSuccess]);

  const handleInvestment = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    navigate.push(`${DASHBOARD_TEST_QUESTION}/${value.id}/${value.name}`);
  };

  const handleChangeCreate = (e: any) => {
    setFormData((prev: {}) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const showModal = () => {
    setFormData(initialFormData);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleChangeSelect = (value: string, setFieldValue: any) => {
    console.log(`selected ${value}`);
    setFieldValue("test_type_id", value);
  };

  const handleChangeSelectLevel = (value: string, setFieldValue: any) => {
    console.log(`selected ${value}`);
    dataLevelType.map((item: any) => {
      if (item.value === value) {
        setFieldValue("parent_id", value);
        setFieldValue("name", item.label);
      }
    });
  };

  const handleSubmitAddTest = (values: any) => {
    if (values.test_type_id === 4) {
      handlePostTest(values);
    } else {
      delete values["parent_id"];
      handlePostTest(values);
    }
  };

  const renderCards = () => {
    if (!dataTest) return <LoaderSpinGif size="large" />;
    if (dataTest && dataTest.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataTest?.map((card: any, index: any) => (
      <Card className="card" key={card.id}>
        <div className="management-portal-card">
          <div className="management-portal-content-card">
            <div>
              {/* <p style={{ fontWeight: "bold" }}>{card.id}</p> */}
              <p style={{ fontWeight: "bold" }}>
                {index + 1} - {card.name}
              </p>
              <Row>
                <p style={{ color: "gray", marginRight: 5 }}>
                  {"Test Type : "}
                </p>
                <p style={{ color: "gray" }}>{card.test_type}</p>
              </Row>
            </div>
          </div>
          <div>
            <div className="curd-button">
              <Button
                type="primary"
                onClick={(e) => handleInvestment(e, card)}
                style={{
                  backgroundColor: BUTTON_COLOR[1].color,
                }}
                icon={<PlusOutlined />}
              >
                Select Question
              </Button>
            </div>
          </div>
        </div>
      </Card>
    ));
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("CategoryNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("CategoryNameLength")} ${max} ${translate("Characters")}`
      ),
    duration: yup
      .number()
      .required(translate("DurationRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("DurationLength")} ${max} ${translate("Characters")}`
      ),
    passing_score: yup
      .number()
      .required(translate("PassingScoreRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("PassingScoreLength")} ${max} ${translate("Characters")}`
      ),
    test_type_id: yup.string().required(translate("TestTypeRequired")),
  });

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "40px" }}>
      <Row style={{ justifyContent: "space-between" }}>
        <Col>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Test</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Test",
              },
            ]}
          />
        </Col>
        <Col>
          <Button
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C", marginLeft: 150 }}
            icon={<PlusOutlined />}
          >
            Create Test
          </Button>
        </Col>
      </Row>
      <div className="category-content">
        <div className="category-table">{renderCards()}</div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Test"
        open={isModalVisible}
        width={1200}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleSubmitAddTest(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, setFieldValue, values }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
              >
                <Col span={24}>
                  <label className="isRequired">Test Type</label>
                  <Select
                    placeholder="Select Test Type"
                    style={{
                      width: "100%",
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value) =>
                      handleChangeSelect(value, setFieldValue)
                    }
                    options={dataTestType}
                    value={values.test_type_id}
                    size={formSize}
                  />
                  {errors.test_type_id && (
                    <div className={"error"}>
                      {errors.test_type_id.toString()}
                    </div>
                  )}
                </Col>
                {values.test_type_id.toString() !== "4" && (
                  <Col span={24}>
                    <Field
                      component={FormInputFormik}
                      placeholder={"Input Test Name"}
                      title={"Test Name"}
                      type={"text"}
                      name={"name"}
                    />
                  </Col>
                )}
                {values.test_type_id.toString() === "4" && (
                  <Col span={24}>
                    <label className="isRequired">Level Type</label>
                    <Select
                      placeholder="Select Level Type"
                      style={{
                        width: "100%",
                        marginTop: 10,
                        marginBottom: 15,
                      }}
                      onChange={(value) =>
                        handleChangeSelectLevel(value, setFieldValue)
                      }
                      options={dataLevelType}
                      value={values.parent_id}
                      size={formSize}
                    />
                    {errors.parent_id && (
                      <div className={"error"}>
                        {errors.parent_id.toString()}
                      </div>
                    )}
                  </Col>
                )}
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Duration (Minute)"}
                    title={"Duration (Minute)"}
                    type={"number"}
                    name={"duration"}
                  />
                </Col>
                <Col span={24}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Passing Score"}
                    title={"Passing Score"}
                    type={"number"}
                    name={"passing_score"}
                  />
                </Col>
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPost}
                  disabled={!isValid}
                >
                  {translate("Submit")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default TestPage;
