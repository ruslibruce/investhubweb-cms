import { postValidateResetToken } from "@/shared/api/mutation/forgotPass";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";

const usePostValidateToken = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postValidateResetToken,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
      },
    },
  });

  const handlePostValidate = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handlePostValidate,
  };
};

export default usePostValidateToken;
