import { DASHBOARD_EXTERNAL_CERTIFICATE_DETAIL } from "@/shared/constants/path";
import { paramsToString } from "@/shared/utils/string";
import { EyeTwoTone } from "@ant-design/icons";
import { Breadcrumb, Button, Card, Select, Space, Spin } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";

import HamburgerCard from "@/components/HamburgerCard";
import NoData from "@/shared/components/NoData";
import { CERTIFICATE_STATUS, USER } from "@/shared/constants/storageStatis";
import { getStatusColor } from "@/shared/utils/helper";
import useGetCertificate from "../hooks/useGetCertificate";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import { storageCheck } from "@/shared/utils/clientStorageUtils";

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};
const SertifikatPage = () => {
  const dataUser = storageCheck(USER);
  const isKurator = dataUser?.user?.role === "kurator";
  const navigate = useRouter();
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [fontSize, setFontSize] = useState("initial");
  const [currentPage, setCurrentPage] = React.useState<number>(1);

  // Filter
  const [dataFilter, setDataFilter] = React.useState<any>({
    status: isKurator ? "false" : "",
    name: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState<any>({
    status: isKurator ? "false" : "",
    name: "",
  });

  // GET
  const { fetchQuery } = useGetCertificate(
    {},
    paramsToString({ page: currentPage, ...dataFilterApi })
  );
  const { data: dataCertificateResult, isLoading } = fetchQuery;
  const { data: dataCertificate } = dataCertificateResult;

  console.log("Data Certificate:", dataCertificateResult);

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter((prev: {}) => ({
      ...prev,
      status: value,
    }));
  };

  const handleSetFilter = () => {
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const handleCertificate = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_EXTERNAL_CERTIFICATE_DETAIL}/${value.id}`);
  };

  const renderCards = () => {
    if (!dataCertificate) return <LoaderSpinGif size="large" />;
    if (dataCertificate && dataCertificate.length === 0)
      return (
        <Card className="card">
          <NoData />
        </Card>
      );
    return dataCertificate?.map((card: any, index: any) => (
      <Card className="card" key={index}>
        <div className="management-portal-card">
          <div className="management-portal-content-card">
            <p
              style={{
                color: !card.is_verified
                  ? getStatusColor("pending")
                  : card.is_approved
                  ? getStatusColor("published")
                  : getStatusColor("rejected"),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {!card.is_verified
                ? "Pending"
                : card.is_approved
                ? "Approved"
                : "Rejected"}
            </p>

            <div>
              <p style={{ fontWeight: "bold" }}>{card.user_name}</p>
              <p style={{ fontWeight: "bold" }}>{card.provider}</p>
              <p style={{ color: "gray" }}>
                {isMobile
                  ? truncateTextToHalf(card.description)
                  : card.description}
              </p>
            </div>
          </div>
          <div>
            <div className="curd-button">
              <span style={{ marginLeft: "15px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleCertificate(e, { id: card.id })}
                />
              </span>
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleCertificate(e, { id: card.id })}
                arrayItems={["0"]}
              />
            </div>
          </div>
        </div>
      </Card>
    ));
  };

  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>
            External Certificate
          </p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "External Certificate",
              },
            ]}
          />
        </div>
      </div>
      <div className="category-content">
        {!isKurator && (
          <div>
            <Card className="category-search">
              <p style={{ fontWeight: "bold" }}>Search Certificate</p>
              <p style={{ marginTop: 18 }}>Status</p>
              <Select
                size={isSmallScreen ? "small" : "middle"}
                defaultValue="All"
                style={{ width: "100%" }}
                onChange={handleChangeStatus}
                options={CERTIFICATE_STATUS}
              />
              <Button
                size={isSmallScreen ? "small" : "middle"}
                type="primary"
                style={{
                  backgroundColor: "#CB0B0C",
                  marginTop: 25,
                  width: "100%",
                }}
                onClick={handleSetFilter}
              >
                Submit
              </Button>
            </Card>
          </div>
        )}
        <div className="category-table">{renderCards()}</div>
      </div>
    </Space>
  );
};

export default SertifikatPage;
