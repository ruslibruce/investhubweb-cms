import { fetchCertificate } from "@/shared/api/fetch/certificate";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchCertificate();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */

const useGetCertificate = (initialData?: any, params?: string) => {
  const fetchDataCertificate = fetchCertificate(params);

  const fetchQuery = useFetchHook({
    keys: [fetchDataCertificate.key, params],
    api: fetchDataCertificate.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetCertificate;
