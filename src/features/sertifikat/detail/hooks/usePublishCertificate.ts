import { publishCertificate } from "@/shared/api/mutation/certificate";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePublishCertificate = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: publishCertificate,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables: any, context) {
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["certificateDetail"] });
          notification.success({
            message: "Certificate berhasil diupdate",
          });
        }
      },
    },
  });

  const handlePublishCertificate = (data: {}, id: string) => {
    mutationQuery.mutate({ data, id });
  };

  return {
    mutationQuery,
    handlePublishCertificate,
  };
};

export default usePublishCertificate;
