import { fetchDetailCertificate } from "@/shared/api/fetch/certificate";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailCertificate();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDetailCertificate = (id: string, initialData?: any) => {
  const fetchDataCertificate = fetchDetailCertificate(id);
  const fetchQuery = useFetchHook({
    keys: fetchDataCertificate.key,
    api: fetchDataCertificate.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetDetailCertificate;
