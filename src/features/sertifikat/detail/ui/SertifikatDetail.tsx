import { DASHBOARD_EXTERNAL_CERTIFICATE } from "@/shared/constants/path";
import { BUTTON_COLOR, USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { CheckOutlined, SaveOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Checkbox,
  Col,
  Flex,
  Input,
  Modal,
  notification,
  Row,
  Space,
  Spin,
} from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetDetailCertificate from "../hooks/useGetCertificateDetail";
import usePublishCertificate from "../hooks/usePublishCertificate";
import NoData from "@/shared/components/NoData";
import { getStatusColor } from "@/shared/utils/helper";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import PDFViewer from "@/components/PDFViewer";
const { Search } = Input;

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};

const SertifikatPageDetail = () => {
  const navigate = useRouter();
  const { id } = navigate.query;

  //GET
  const { fetchQuery } = useGetDetailCertificate(id as string, {});
  const { data: certificateDetail, isLoading } = fetchQuery;
  const { handlePublishCertificate, mutationQuery } = usePublishCertificate();
  const { isPending: isLoadingMutation } = mutationQuery;
  const [dataChecked, setDataChecked] = useState<any>([]);
  const [search, setSearch] = useState("");
  console.log("data certificateDetail:", certificateDetail);
  console.log("dataCheckedl:", dataChecked);

  const filteredCourse = certificateDetail?.courses?.filter((item: any) => {
    return item.title.toLowerCase().includes(search.toLowerCase());
  });

  React.useEffect(() => {
    console.log("data certificateDetail:", certificateDetail);
    if (certificateDetail) {
      let temp = [] as any;
      certificateDetail.courses?.map((item: any) => {
        temp.push(item);
      });
      console.log("temp get", temp);
      setDataChecked(temp);
    }
  }, [certificateDetail]);

  const onChangeCheckbox = (e: any, card: any) => {
    console.log("dataChecked", card);

    console.log(`checked = ${e.target.checked}`);
    let temp: any = dataChecked.map((item: any) => {
      if (item.id === card.id) {
        item.checked = e.target.checked ? 1 : 0;
        console.log("item", item);
      }
      return item;
    });

    setDataChecked(temp);
  };

  const handleSaveCourse = (is_approved: boolean) => {
    let temp: any = [];
    dataChecked.map((item: any, index: any) => {
      if (item.checked === 1) {
        temp.push(item.id);
      }
    });
    console.log("temp checked", temp);

    if(is_approved === true && temp.length === 0){
      notification.info({
        message: "Please select at least one course",
      })
      return
    }

    handlePublishCertificate(
      {
        courses: is_approved ? temp : [],
        is_approved,
      },
      id as string
    );
  };

  if (isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (`${certificateDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (!certificateDetail) {
    return <NoData />;
  }

  const onSearch = (value: string) => {
    console.log("search:", value);
    setSearch(value);
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>
        External Certificate
      </p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "External Certificate",
            href: `/cms/${DASHBOARD_EXTERNAL_CERTIFICATE}`,
          },
          {
            title: "Detail External Certificate",
          },
        ]}
      />
      <Card loading={certificateDetail ? false : true}>
        <p style={{ fontWeight: "bold", fontSize: 18 }}>
          Detail External Certificate
        </p>
        <p style={{ marginTop: 50 }}>
          Status
          <Button
            size="large"
            ghost
            disabled
            style={{
              color: "white",
              background: !certificateDetail.is_verified
                ? getStatusColor("pending")
                : certificateDetail.is_approved
                ? getStatusColor("published")
                : getStatusColor("rejected"),
              marginLeft: 12,
              marginBottom: 16,
              fontWeight: "bold",
            }}
          >
            {!certificateDetail.is_verified
              ? "Pending"
              : certificateDetail.is_approved
              ? "Approved"
              : "Rejected"}
          </Button>
        </p>
        <b>Title</b>
        <p style={{ color: "gray", marginBottom: 12 }}>
          {certificateDetail?.title}
        </p>
        <b>Duration</b>
        <p style={{ color: "gray", marginBottom: 12 }}>
          {certificateDetail?.duration}
        </p>
        <b>Description</b>
        <p style={{ color: "gray", marginBottom: 12 }}>
          {certificateDetail?.description}
        </p>
        {certificateDetail.file.includes("pdf") ? (
          <>
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>PDF</p>
            <PDFViewer url={certificateDetail.file} />
          </>
        ) : (
          <>
            <p style={{ fontWeight: "bold", marginBottom: 12 }}>Image</p>
            <Image
              src={certificateDetail?.file}
              alt={certificateDetail?.title}
              className="flag-icon"
              width={300}
              height={300}
              style={{ marginBottom: 12 }}
            />
          </>
        )}
        {!certificateDetail.is_verified && (
          <Flex
            style={{ marginTop: 60, marginBottom: 20 }}
            justify="center"
            align="center"
            gap={10}
          >
            <Button
              style={{ backgroundColor: getStatusColor("rejected") }}
              icon={<CheckOutlined />}
              onClick={() =>
                Modal.confirm({
                  title: "Rejected Courses",
                  content: "Are you sure reject this certificate?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleSaveCourse(false),
                  okButtonProps: {
                    icon: <SaveOutlined />,
                    style: { backgroundColor: "#CB0B0C" },
                    loading: isLoadingMutation,
                  },
                })
              }
              type="primary"
              size="large"
            >
              Rejected
            </Button>
            <Button
              style={{ backgroundColor: getStatusColor("published") }}
              icon={<CheckOutlined />}
              onClick={() =>
                Modal.confirm({
                  title: "Verified Courses",
                  content:
                    "Are you sure this certificate is equivalent to with this courses?",
                  okText: "Yes",
                  cancelText: "No",
                  onOk: () => handleSaveCourse(true),
                  okButtonProps: {
                    icon: <SaveOutlined />,
                    style: { backgroundColor: "#CB0B0C" },
                    loading: isLoadingMutation,
                  },
                })
              }
              type="primary"
              size="large"
            >
              Approved
            </Button>
          </Flex>
        )}
        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 12,
          }}
        >
          <Col>
            <p style={{ fontWeight: "bold" }}>Equivalent Courses</p>
          </Col>
          <Col>
            <Search
              placeholder="search courses"
              onSearch={onSearch}
              style={{ width: 300 }}
              allowClear
            />
          </Col>
        </Row>
        {filteredCourse?.length === 0 && <NoData />}
        {filteredCourse?.map((card: any, index: number) => {
          const findChecked = dataChecked.find((item: any) => {
            return item.id == card.id;
          });
          return (
            <Card key={card.id}>
              <Row style={{ display: "flex", alignItems: "center" }}>
                <Col span={22}>
                  {/* <p style={{ fontWeight: "bold" }}>{card.id}</p> */}
                  <p style={{ fontWeight: "bold" }}>
                    {index + 1} - {card.title}
                  </p>
                  <p style={{ color: "gray" }}>{card.title}</p>
                </Col>
                <Col span={2}>
                  <Checkbox
                    style={{ transform: "scale(1.5)" }}
                    checked={findChecked?.checked}
                    onChange={(e) => onChangeCheckbox(e, card)}
                  />
                </Col>
              </Row>
            </Card>
          );
        })}
      </Card>
    </Space>
  );
};

export default SertifikatPageDetail;
