import { publishInvestment } from "@/shared/api/mutation/investment-partner";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePublishInvestment = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: publishInvestment,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables: any, context) {
        queryClient.invalidateQueries({ queryKey: [["investment-partner"]] });
        notification.success({
          message:
            variables.type === "update"
              ? "Investment berhasil diupdate"
              : variables.type === "unpublished"
              ? "Investment berhasil diunpublish"
              : "Investment berhasil dipublish",
        });
      },
    },
  });

  const handlePublishInvestment = (data: {}, id: string, type?: string) => {
    mutationQuery.mutate({ data, id, type });
  };

  return {
    mutationQuery,
    handlePublishInvestment,
  };
};

export default usePublishInvestment;
