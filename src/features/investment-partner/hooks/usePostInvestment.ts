import { addInvestment } from "@/shared/api/mutation/investment-partner";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostInvestment = () => {
    const queryClient = useQueryClient();
  
    const mutationQuery = useMutationHook({
      api: addInvestment,
      options: {
        // eslint-disable-next-line no-unused-vars
        onError(error: any, variables, context) {},
  
        // eslint-disable-next-line no-unused-vars
        onSuccess(data: any, variables, context) {
          // Create Token cookie
          console.log("data", data);
          console.log("variables", variables);
          console.log("context", context);
          queryClient.invalidateQueries({ queryKey: [["investment-partner"]] });
          notification.success({
            message: "The investment partner was successfully created",
          });
        },
      },
    });

    const handlePostInvestment = (object: {}) => {
        mutationQuery.mutate(object);
      };
    
      return {
        mutationQuery,
        handlePostInvestment,
      };

}

export default usePostInvestment;