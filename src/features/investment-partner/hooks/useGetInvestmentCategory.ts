import { fetchInvestmentCategory } from "@/shared/api/fetch/investmentCategory";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchInvestmentCategory();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetInvestmentCategory = (initialData?: any) => {
  const fetchDataInvestment = fetchInvestmentCategory();

  const fetchQuery = useFetchHook({
    keys: fetchDataInvestment.key,
    api: fetchDataInvestment.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetInvestmentCategory;
