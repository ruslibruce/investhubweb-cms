import { putInvestment } from "@/shared/api/mutation/investment-partner";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutInvestment = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putInvestment,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({ queryKey: [["investment-partner"]] });
        notification.success({
          message: "Investment berhasil diupdate",
        });
      },
    },
  });

  const handleUpdateInvestment = (params : string, data: {}) => {
    mutationQuery.mutate({params, data});
  };

  return {
    mutationQuery,
    handleUpdateInvestment,
  };
};

export default usePutInvestment;
