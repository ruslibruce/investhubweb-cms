import { fetchInvestment } from "@/shared/api/fetch/investment-partner";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchInvestment();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetInvestment = (initialData?: any, params?: string) => {
  const fetchDataInvestment = fetchInvestment(params);
  const fetchQuery = useFetchHook({
    keys: [fetchDataInvestment.key, params],
    api: fetchDataInvestment.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetInvestment;
