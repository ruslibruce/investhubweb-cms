import { deleteInvestment } from "@/shared/api/mutation/investment-partner";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteInvestment = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteInvestment,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({ queryKey: [["investment-partner"]] });
        notification.success({
          message: "The investment was successfully deleted",
        });
      },
    },
  });

  const handleDeleteInvestment = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {  
    mutationQuery,
    handleDeleteInvestment,
  };
};

export default useDeleteInvestment;
