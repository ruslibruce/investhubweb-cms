import { DASHBOARD_INVESTMENT_PARTNER_DETAIL } from "@/shared/constants/path";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  checkIsEmptyObject,
  getStatusColor,
  regexUrl,
} from "@/shared/utils/helper";
import { paramsToString } from "@/shared/utils/string";
import {
  ArrowRightOutlined,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  EyeTwoTone,
  FolderOpenTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import type { DatePickerProps, UploadFile, UploadProps } from "antd";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Divider,
  Flex,
  Input,
  message,
  Modal,
  Row,
  Select,
  Space,
  Spin,
  Upload,
} from "antd";
import { useRouter } from "next/router";
import useDeleteInvestment from "../hooks/useDeleteInvestment";
import useGetInvestment from "../hooks/useGetInvestment";
import useGetInvestmentCategory from "../hooks/useGetInvestmentCategory";
import usePostInvestment from "../hooks/usePostInvestment";
import usePublishInvestment from "../hooks/usePublishInvestment";
import usePutInvestment from "../hooks/usePutInvestment";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { IMAGE_FORMAT, USER } from "@/shared/constants/storageStatis";
import dynamic from "next/dynamic";
import Image from "next/image";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import NoData from "@/shared/components/NoData";
import { imagePORTAL } from "@/shared/constants/imageUrl";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
const { Dragger } = Upload;
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import HamburgerCard from "@/components/HamburgerCard";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  name: "",
  url: "",
  category: "",
  cover_image: "",
};

const InvestmentPartnerPage = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const [fontSize, setFontSize] = useState("initial");
  const dataUser = storageCheck(USER);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };

  // Filter
  const [dataFilter, setDataFilter] = React.useState<any>({
    category: "",
    name: "",
    is_active: "",
    url: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState<any>({
    category: "",
    name: "",
    is_active: "",
    url: "",
  });
  const handleSetFilter = () => {
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };
  const handleChangeCategory = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter((prev: {}) => ({
      ...prev,
      category: value,
    }));
  };
  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    setDataFilter((prev: {}) => ({
      ...prev,
      is_active: value,
    }));
  };

  // GET
  const { fetchQuery } = useGetInvestment(
    {},
    paramsToString({ page: currentPage, ...dataFilterApi })
  );
  const { data: dataInvestmenResult, isLoading } = fetchQuery;
  const { data: dataInvestment } = dataInvestmenResult;

  // GET CATEGORY
  const { fetchQuery: fetchCategory } = useGetInvestmentCategory([]);
  const { isLoading: isLoadingCategory, data: dataFetchCategory } =
    fetchCategory;
  const allOption = { value: "", label: "All" };
  const optionsWithAll = [allOption, ...dataFetchCategory];
  console.log("dataInvestment", dataInvestment);

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(
          name,
          `${imagePORTAL}/${dataFile.response.data.filename}`
        );
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  // Post
  const { mutationQuery: mutationPost, handlePostInvestment } =
    usePostInvestment();
  const [formDataInvestment, setFormDataInvestment] =
    useState<any>(initialFormData);
  const { isPending: isLoadingPostInvestment, isSuccess: isSuccessPost } =
    mutationPost;

  React.useEffect(() => {
    if (isSuccessPost) {
      setIsModalVisible(false);
    }
  }, [isSuccessPost]);
  const handleAddDataInvestment = (values: any) => {
    handlePostInvestment({
      ...values,
      cover_image: formDataInvestment.cover_image.split("/").pop(),
    });
  };

  // DELETE
  const { handleDeleteInvestment } = useDeleteInvestment();
  const handleDeleteInvestmentSubmit = (id: string) => {
    handleDeleteInvestment(id);
  };

  // EDIT
  const { mutationQuery: mutationUpdate, handleUpdateInvestment } =
    usePutInvestment();
  const editModal = (item: any) => {
    setFormDataInvestment({
      id: item.id,
      name: item.name,
      url: item.url,
      category: item.category_name,
      cover_image: item.cover_image,
    });
    setIsEditModalVisible(true);
  };

  const handleEditInvestmentSubmit = (values: any) => {
    let chooseCategory = dataFetchCategory.find(
      (item: any) => item.label == values.category
    );
    handleUpdateInvestment(values.id, {
      ...values,
      category: chooseCategory?.value,
      cover_image: values.cover_image.split("/").pop(),
    });
    setIsEditModalVisible(false);
  };

  // Publish
  const { handlePublishInvestment } = usePublishInvestment();
  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    handlePublishInvestment(
      {
        ...value,
        is_active: params,
      },
      value.id,
      params
    );
  };

  const handleInvestment = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    navigate.push(`${DASHBOARD_INVESTMENT_PARTNER_DETAIL}/${value.id}`);
  };

  const showModal = () => {
    setFormDataInvestment(initialFormData);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        100,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
    url: yup
      .string()
      .required(translate("LinkRequired"))
      .matches(regexUrl(), translate("LinkMatches"))
      .max(
        100,
        ({ max }) =>
          `${translate("LinkLength")} ${max} ${translate("Characters")}`
      ),
    category: yup.string().required(translate("CategoryRequired")),
    cover_image: yup.string().required(translate("CoverPictureRequired")),
  });

  const renderCards = () => {
    if (!dataInvestment) return <LoaderSpinGif size="large" />;
    if (dataInvestment && dataInvestment.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataInvestment?.map((card: any, index: any) => (
      <Card
        style={{ marginBottom: "15px", minWidth: 300, width: "100%" }}
        key={index}
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(card.is_active),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {card.is_active}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleInvestment(e, { id: card.id })}
                />
              </span>
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => editModal(card)}
                />
              </span>
              <span>
                <DeleteTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => {
                    Modal.confirm({
                      title: "Delete Data",
                      content: "Are you sure you want to delete this data?",
                      onOk: () => handleDeleteInvestmentSubmit(card.id),
                      okButtonProps: {
                        icon: <DeleteOutlined />,
                        style: { backgroundColor: "#CB0B0C" },
                      },
                    });
                  }}
                />
              </span>
              <div className="hamburger-curd-button">
                <HamburgerCard
                  onView={(e) => handleInvestment(e, { id: card.id })}
                  onEdit={() => editModal(card)}
                  onDelete={() => {
                    Modal.confirm({
                      title: "Delete Data",
                      content: "Are you sure you want to delete this data?",
                      onOk: () => handleDeleteInvestmentSubmit(card.id),
                      okButtonProps: {
                        icon: <DeleteOutlined />,
                        style: { backgroundColor: "#CB0B0C" },
                      },
                    });
                  }}
                  arrayItems={["0", "1", "2"]}
                />
              </div>
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p style={{ fontWeight: "bold" }}>{card.category}</p>
            <p>{card.name}</p>
            <p style={{ color: "gray" }}>{card.url}</p>
          </div>
          <div>
            {card.is_active === "draft" && (
              <Button
                size={formSize}
                // onClick={(e) => handlePublish(e, card, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: "green",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
            {card.is_active === "published" && (
              <Button
                size={formSize}
                onClick={(e) => handlePublish(e, card, "unpublished")}
                style={{
                  marginTop: 24,
                  backgroundColor: "#CB0B0C",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Unpublish
              </Button>
            )}
            {card.is_active === "unpublished" && (
              <Button
                size={formSize}
                onClick={(e) => handlePublish(e, card, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: "green",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
          </div>
        </div>
      </Card>
    ));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row wrap style={{ justifyContent: "space-between" }}>
        <Col>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>
            Investment Partner
          </p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Investment Partner",
              },
            ]}
          />
        </Col>
        <Col>
          <Button
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C", marginLeft: 75 }}
          >
            <PlusOutlined />
            Create Investment Partner
          </Button>
        </Col>
      </Row>
      <Row wrap gutter={8}>
        <Col>
          <Card style={{ width: 300 }}>
            <p style={{ fontWeight: "bold" }}>Search Investment Partner</p>
            <p style={{ marginTop: 18 }}>Company Name</p>
            <Input
              style={{ height: 50 }}
              placeholder="Company Name"
              onChange={(e) =>
                setDataFilter((prev: {}) => ({
                  ...prev,
                  name: e.target.value,
                }))
              }
            />
            <p style={{ marginTop: 18 }}>Category</p>
            <Select
              defaultValue="All"
              style={{ width: 250, height: "50px" }}
              onChange={handleChangeCategory}
              options={optionsWithAll}
            />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              defaultValue="All"
              style={{ width: 250, height: "50px" }}
              onChange={handleChangeStatus}
              options={[
                { value: "", label: "All" },
                { value: "published", label: "Published" },
                { value: "unpublished", label: "Unpublished" },
              ]}
            />
            <Button
              type="primary"
              style={{ backgroundColor: "#CB0B0C", marginTop: 25, width: 250 }}
              onClick={handleSetFilter}
            >
              Submit
            </Button>
          </Card>
        </Col>
        <Col span={17}>
          {renderCards()}
          {dataInvestment && dataInvestment.length > 0 && (
            <Card
              style={{
                height: 70,
                marginTop: "12px",
              }}
            >
              <Row>
                <Col>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataInvestmenResult.records}
                    limit={dataInvestmenResult.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataInvestmenResult?.records}
                    limit={dataInvestmenResult?.limit}
                  />
                </Col>
              </Row>
            </Card>
          )}
        </Col>
      </Row>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Investment Partner"
        open={isModalVisible}
        width={1200}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddDataInvestment(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col span={6}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Company Name"}
                    title={"Company Name"}
                    type={"text"}
                    name={"name"}
                  />
                </Col>
                <Col span={6}>
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    options={dataFetchCategory}
                    value={values.category}
                    size={formSize}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col span={9}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Link"}
                    title={"Link"}
                    type={"url"}
                    name={"url"}
                  />
                </Col>
              </Row>
              {!values.cover_image ? (
                <>
                  <label className="required">Upload files (Max. 2MB)</label>
                  <Dragger
                    {...{
                      name: "file",
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                  {errors.cover_image && (
                    <div className={"error"}>
                      {errors.cover_image.toString()}
                    </div>
                  )}
                </>
              ) : (
                <Col span={12}>
                  <label className="required">Image Cover</label>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={values.cover_image}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostInvestment}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update Investment Partner"
        open={isEditModalVisible}
        width={1200}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataInvestment}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleEditInvestmentSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Row
                style={{
                  marginBottom: 24,
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={[8, 8]}
              >
                <Col span={6}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Company Name"}
                    title={"Company Name"}
                    type={"text"}
                    name={"name"}
                  />
                </Col>
                <Col span={6}>
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    style={{
                      width: 280,
                      marginTop: 10,
                      marginBottom: 15,
                    }}
                    onChange={(value: string) =>
                      setFieldValue("category", value)
                    }
                    options={dataFetchCategory}
                    value={values.category}
                    size={formSize}
                  />
                  {errors.category && (
                    <div className={"error"}>{errors.category.toString()}</div>
                  )}
                </Col>
                <Col span={9}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Link"}
                    title={"Link"}
                    type={"url"}
                    name={"url"}
                  />
                </Col>
              </Row>
              {!values.cover_image ? (
                <>
                  <label className="required">Upload files (Max. 2MB)</label>
                  <Dragger
                    {...{
                      name: "file",
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-portal/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(info, setFieldValue, "cover_image"),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                  {errors.cover_image && (
                    <div className={"error"}>
                      {errors.cover_image.toString()}
                    </div>
                  )}
                </>
              ) : (
                <Col span={12}>
                  <label className="required">Image Cover</label>
                  <Space
                    style={{ position: "relative", marginTop: 5 }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={values.cover_image}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image", "");
                      }}
                    />
                  </Space>
                </Col>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostInvestment}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default InvestmentPartnerPage;
