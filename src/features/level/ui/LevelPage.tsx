import { DASHBOARD_LEVEL_DETAIL } from "@/shared/constants/path";
import {
  DeleteOutlined,
  EditTwoTone,
  EyeTwoTone,
  FolderOpenTwoTone,
  PlusOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Flex,
  message,
  Modal,
  Row,
  Space,
  Upload,
} from "antd";
import { useRouter } from "next/router";

import FormInputFormik from "@/components/FormInputFormik";
import HamburgerCard from "@/components/HamburgerCard";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { imageLMS } from "@/shared/constants/imageUrl";
import { IMAGE_FORMAT, USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { Field, Formik } from "formik";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React, { useState } from "react";
import { SketchPicker } from "react-color";
import "react-quill/dist/quill.snow.css";
import * as yup from "yup";
import useGetLevel from "../hooks/useGetLevel";
import usePostLevels from "../hooks/usePostLevels";
import usePutLevels from "../hooks/usePutLevels";
const { Dragger } = Upload;

const initialFormLevel = {
  badge_icon: "",
  color: "",
  name: "",
};
const LevelPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { fetchQuery } = useGetLevel();
  const { data: dataLevel, isLoading } = fetchQuery;
  console.log("dataLevel", dataLevel);
  const [formLevel, setFormLevel] = useState<any>(initialFormLevel);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const { handleUpdateLevel, mutationQuery: mutationQueryPutLevel } =
    usePutLevels();
  const { isPending: isLoadingUpdate, isSuccess: isSuccessUpdate } =
    mutationQueryPutLevel;
  const { handleCreateLevel, mutationQuery: mutationQueryPostLevel } =
    usePostLevels();
  const { isPending: isLoadingCreate, isSuccess: isSuccessCreate } =
    mutationQueryPostLevel;

  React.useEffect(() => {
    if (isSuccessUpdate) {
      setIsEditModalVisible(false);
    }
    if (isSuccessCreate) {
      setIsModalVisible(false);
    }
  }, [isSuccessUpdate, isSuccessCreate]);

  const handleNavigate = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("query", value);
    navigate.push(`${DASHBOARD_LEVEL_DETAIL}/${value.id}/${value.name}`);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const editModal = (e: any, card: any) => {
    console.log("card", card);

    setFormLevel((prev: any) => ({
      ...prev,
      name: card.name,
      color: card.color ?? "",
      badge_icon: card.badge_icon,
      id: card.id,
    }));
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleUpdate = (values: any) => {
    handleUpdateLevel(values.id, values);
  };

  const handleCreate = (values: any) => {
    handleCreateLevel(values);
  };

  const renderCards = () => {
    if (!dataLevel) return <LoaderSpinGif size="large" />;
    if (dataLevel && dataLevel.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataLevel?.map((card: any, index: any) => (
      <Card className="card" key={card.id}>
        <div className="management-portal-card">
          <div className="management-portal-content-card">
            <div>
              <p style={{ fontWeight: "bold" }}>
                {index + 1} - {card.name}
              </p>
              <p style={{ color: "gray" }}>{card.name}</p>
            </div>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              <span>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleNavigate(e, card)}
                />
              </span>
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => editModal(e, card)}
                />
              </span>
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleNavigate(e, card)}
                onEdit={(e) => editModal(e, card)}
                arrayItems={["0", "1"]}
              />
            </div>
          </div>
        </div>
      </Card>
    ));
  };

  const handleChangeColor = (color: any, setFieldValue: any) => {
    console.log("color", color.hex);
    setFieldValue("color", color.hex);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    badge_icon: yup.string().required(translate("BadgeIconRequired")),
    color: yup.string().required(translate("ColorRequired")),
    name: yup
      .string()
      .required(translate("RequireName"))
      .max(
        100,
        ({ max }) =>
          `${translate("NameLength")} ${max} ${translate("Characters")}`
      ),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row gutter={[12, 12]} style={{ marginLeft: 10 }} wrap>
        <Col>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Level</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Level",
              },
            ]}
          />
        </Col>
        <Col style={{ marginLeft: "auto" }}>
          <Button
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C" }}
            icon={<PlusOutlined />}
          >
            Add Level
          </Button>
        </Col>
      </Row>
      <div className="category-content">
        <div className="category-table">{renderCards()}</div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Level"
        open={isModalVisible}
        width={1000}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormLevel}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleCreate(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
            <>
              <Col span={24}>
                <Field
                  component={FormInputFormik}
                  placeholder={"Level Name"}
                  title={"Level Name"}
                  type={"text"}
                  name={"name"}
                />
              </Col>
              <Row
                gutter={[6, 18]}
                style={{ marginTop: 20, marginBottom: 25 }}
                wrap
              >
                <Col>
                  <label className="required" style={{ marginBottom: 10 }}>
                    Color
                  </label>
                  <SketchPicker
                    color={values.color}
                    onChange={(color) =>
                      handleChangeColor(color, setFieldValue)
                    }
                  />
                </Col>
                {values.badge_icon ? (
                  <Col span={18}>
                    <label className="required">Badge Icon</label>
                    <Space
                      style={{ position: "relative", marginTop: 10 }}
                      direction="horizontal"
                    >
                      <Image
                        alt="image edit"
                        width={200}
                        height={200}
                        style={{ width: 400, height: 200, borderRadius: 10 }}
                        src={
                          values.badge_icon.includes("https")
                            ? values.badge_icon
                            : `${imageLMS}/${values.badge_icon}`
                        }
                      />
                      <DeleteOutlined
                        style={{
                          fontSize: 20,
                          color: "red",
                          position: "absolute",
                          top: 10,
                          right: 10,
                          backgroundColor: "white",
                          padding: 5,
                          borderRadius: 100,
                        }}
                        type="primary"
                        onClick={() => {
                          setFieldValue("badge_icon", "");
                        }}
                      />
                    </Space>
                  </Col>
                ) : (
                  <Col span={18}>
                    <label className="required">
                      Upload Badge Icon (Max. 2MB)
                    </label>
                    <Dragger
                      {...{
                        name: "file",
                        multiple: true,
                        maxCount: 1,
                        action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                        headers: {
                          authorization: `Bearer ${dataUser.token}`,
                        },
                        accept: IMAGE_FORMAT,
                        beforeUpload: (file) => {
                          console.log("file beforeUpload", file);
                          const reader = new FileReader();
                          reader.readAsDataURL(file);
                          reader.addEventListener("load", (event: any) => {
                            const _loadedImageUrl = event.target.result;
                            const image = document.createElement("img");
                            image.src = _loadedImageUrl;
                            image.addEventListener("load", () => {
                              const { width, height } = image;
                              // set image width and height to your state here
                              console.log(width, height);
                              const isSameDimension = width === height;
                              console.log("isSame", isSameDimension);
                              const isSize = file.size / 1024 / 1024 < 2;

                              if (!isSize) {
                                message.error(
                                  `${file.name} size must be less than 2MB`
                                );
                              }

                              if (!isSameDimension) {
                                message.error(`${file.name} must be square`);
                              }

                              return (
                                (isSameDimension && isSize) ||
                                Upload.LIST_IGNORE
                              );
                            });
                          });
                        },
                        onChange: (info) =>
                          handleUploadChange(info, setFieldValue, "badge_icon"),
                      }}
                      style={{ marginTop: 10 }}
                    >
                      <p className="ant-upload-drag-icon">
                        <FolderOpenTwoTone twoToneColor="#737373" />
                      </p>
                      <p className="ant-upload-text">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint"></p>
                    </Dragger>
                    {errors.badge_icon && (
                      <div className={"error"}>
                        {errors.badge_icon.toString()}
                      </div>
                    )}
                  </Col>
                )}
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingCreate}
                >
                  {translate("Save")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update Level"
        open={isEditModalVisible}
        width={1000}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formLevel}
          enableReinitialize
          onSubmit={(values) => {
            console.log("values", values);
            handleUpdate(values);
          }}
        >
          {({ handleSubmit, isValid, setFieldValue, values, errors }) => (
            <>
              <Col span={24}>
                <Field
                  component={FormInputFormik}
                  placeholder={"Level Name"}
                  title={"Level Name"}
                  type={"text"}
                  name={"name"}
                />
              </Col>
              <Row
                gutter={[6, 18]}
                style={{ marginTop: 20, marginBottom: 25 }}
                wrap
              >
                <Col>
                  <label className="required" style={{ marginBottom: 10 }}>
                    Color
                  </label>
                  <SketchPicker
                    color={values.color}
                    onChange={(color) =>
                      handleChangeColor(color, setFieldValue)
                    }
                  />
                </Col>
                {values.badge_icon ? (
                  <Col span={18}>
                    <p>Badge Icon</p>
                    <Space
                      style={{ position: "relative", marginTop: 10 }}
                      direction="horizontal"
                    >
                      <Image
                        alt="image edit"
                        width={200}
                        height={200}
                        style={{ width: 400, height: 200, borderRadius: 10 }}
                        src={
                          values.badge_icon.includes("https")
                            ? values.badge_icon
                            : `${imageLMS}/${values.badge_icon}`
                        }
                      />
                      <DeleteOutlined
                        style={{
                          fontSize: 20,
                          color: "red",
                          position: "absolute",
                          top: 10,
                          right: 10,
                          backgroundColor: "white",
                          padding: 5,
                          borderRadius: 100,
                        }}
                        type="primary"
                        onClick={() => {
                          setFieldValue("badge_icon", "");
                        }}
                      />
                    </Space>
                  </Col>
                ) : (
                  <Col span={18}>
                    <p>Upload Badge Icon (Max. 2MB)</p>
                    <Dragger
                      {...{
                        name: "file",
                        multiple: true,
                        maxCount: 1,
                        action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                        headers: {
                          authorization: `Bearer ${dataUser.token}`,
                        },
                        accept: IMAGE_FORMAT,
                        beforeUpload: (file) => {
                          console.log("file beforeUpload", file);
                          const reader = new FileReader();
                          reader.readAsDataURL(file);
                          reader.addEventListener("load", (event: any) => {
                            const _loadedImageUrl = event.target.result;
                            const image = document.createElement("img");
                            image.src = _loadedImageUrl;
                            image.addEventListener("load", () => {
                              const { width, height } = image;
                              // set image width and height to your state here
                              console.log(width, height);
                              const isSameDimension = width === height;
                              console.log("isSame", isSameDimension);
                              const isSize = file.size / 1024 / 1024 < 2;

                              if (!isSize) {
                                message.error(
                                  `${file.name} size must be less than 2MB`
                                );
                              }

                              if (!isSameDimension) {
                                message.error(`${file.name} must be square`);
                              }

                              return (
                                (isSameDimension && isSize) ||
                                Upload.LIST_IGNORE
                              );
                            });
                          });
                        },
                        onChange: (info) =>
                          handleUploadChange(info, setFieldValue, "badge_icon"),
                      }}
                      style={{ marginTop: 10 }}
                    >
                      <p className="ant-upload-drag-icon">
                        <FolderOpenTwoTone twoToneColor="#737373" />
                      </p>
                      <p className="ant-upload-text">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint"></p>
                    </Dragger>
                    {errors.badge_icon && (
                      <div className={"error"}>
                        {errors.badge_icon.toString()}
                      </div>
                    )}
                  </Col>
                )}
              </Row>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingUpdate}
                >
                  {translate("Save")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default LevelPage;
