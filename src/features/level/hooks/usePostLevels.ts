import { postLevel } from "@/shared/api/fetch/level";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostLevels = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postLevel,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["level"] });
          notification.success({
            message: "Level berhasil dicreate",
          });
        }
      },
    },
  });

  const handleCreateLevel = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleCreateLevel,
  };
};

export default usePostLevels;
