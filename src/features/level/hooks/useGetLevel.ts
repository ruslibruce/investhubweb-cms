import { fetchAllLevel } from "@/shared/api/fetch/level";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchAllLevel();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetLevel = (initialData?: any) => {
  const fetchDataColors = fetchAllLevel();

  const fetchQuery = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetLevel;
