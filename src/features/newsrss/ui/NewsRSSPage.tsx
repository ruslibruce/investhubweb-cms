import HamburgerCard from "@/components/HamburgerCard";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  getStatusColor,
  isYoutubeUrl,
  regexUrl,
  validateUrl,
} from "@/shared/utils/helper";
import {
  ArrowRightOutlined,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Divider,
  Flex,
  Input,
  message,
  Modal,
  Space,
  Spin,
} from "antd";
import moment from "moment";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useDeleteNewsRss from "../hooks/useDeleteNewsRss";
import useGetNewsRss from "../hooks/useGetNewsRss";
import usePostNewsRSS from "../hooks/usePostNewsRSS";
import usePostUpdateNewsRSS from "../hooks/usePostUpdateNewsRSS";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  media_name: "",
  rss_link: "",
  is_active: "published",
};

const NewsRSSPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const { handlePostNewsRSS, mutationQuery: mutationQueryPostRSS } =
    usePostNewsRSS();
  const { isPending: isLoadingPostRSS, isSuccess: isSuccessPostRSS } =
    mutationQueryPostRSS;
  const { handlePostUpdateNewsRSS, mutationQuery } = usePostUpdateNewsRSS();
  const {
    isPending: isLoadingPostUpdateNewsRSS,
    isSuccess: isSuccessPostUpdateRSS,
  } = mutationQuery;
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [uploadedFileName, setUploadedFileName] = useState("");
  const { fetchQuery } = useGetNewsRss([], currentPage);
  const { isLoading, data: dataRSS } = fetchQuery;
  const { data: newsDataRss } = dataRSS;
  const { mutationQuery: mutationDelete, handleDeleteNewsRss } =
    useDeleteNewsRss();
  const [formDataNews, setFormDataNews] = useState<any>(initialFormData);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [fontSize, setFontSize] = useState("initial");
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessPostRSS) {
      setIsAddModalVisible(false);
    }

    if (isSuccessPostUpdateRSS) {
      setIsEditModalVisible(false);
    }
  }, [isSuccessPostRSS, isSuccessPostUpdateRSS]);

  const handleAddNewsRssSubmit = (values: any) => {
    handlePostNewsRSS(values);
  };

  const handleDeleteNewsRssSubmit = (id: string) => {
    handleDeleteNewsRss(id);
  };

  const addModal = () => {
    setFormDataNews(initialFormData);
    setIsAddModalVisible(true);
  };

  const editModal = (value: any) => {
    setIsEditModalVisible(true);
    setFormDataNews({
      id: value.id,
      media_name: value.media_name,
      rss_link: value.rss_link,
      is_active: value.is_active,
    });
  };

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };
  }, []);

  const handleCancel = () => {
    setIsAddModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const checkIsEmptyObject = (object: any) => {
    return Object.values(object).some((x) => x === null || x === "");
  };

  const handlePublish = (values: any, params: string) => {
    console.log("query", JSON.stringify(values, null, 2));
    handlePostUpdateNewsRSS(values.id, {
      ...values,
      is_active: params,
      publish_time: moment().format("YYYY-MM-DD"),
    });
  };

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const renderCards = () => {
    if (!newsDataRss) return <LoaderSpinGif size="large" />;
    if (newsDataRss && newsDataRss.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return newsDataRss?.map((card: any, index: any) => (
      <Card key={card.id}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(card.is_active),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {card.is_active}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              {/* <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleNews(e, { id: card.id })}
                />
              </span> */}
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => editModal(card)}
                />
              </span>
              {/* <span style={{ marginRight: 12 }}>
                <DeleteTwoTone
                  twoToneColor="#ff0000"
                  onClick={() =>
                    Modal.confirm({
                      title: "Delete User",
                      content: "Are you sure you want to delete this user?",
                      okText: "Yes",
                      cancelText: "No",
                      onOk: () => handleOnDelete(value.id),
                      okButtonProps: {
                        icon: <DeleteFilled />,
                        style: { backgroundColor: "#CB0B0C" },
                      },
                    })
                  }
                />
              </span> */}
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                isView={false}
                onEdit={() => editModal(card)}
                arrayItems={["1"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p style={{ fontWeight: "bold" }}>{card.media_name}</p>
            <p style={{ color: "gray" }}>
              {isMobile ? truncateTextToHalf(card.rss_link) : card.rss_link}
            </p>
          </div>
        </div>
      </Card>
    ));
  };

  const handleSetFormData = (event: any) => {
    // if (event.target.name === "rss_link" && !validateUrl(event.target.value)) {
    //   setFormDataNews((prev: any) => ({
    //     ...prev,
    //     [event.target.name]: "",
    //   }));
    //   return;
    // }

    // if (isYoutubeUrl(event.target.value)) {
    //   setFormDataNews((prev: any) => ({
    //     ...prev,
    //     [event.target.name]: "",
    //   }));
    //   return;
    // }

    setFormDataNews((prev: any) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    media_name: yup
      .string()
      .required(translate("MediaNameRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("MediaNameLength")} ${max} ${translate("Characters")}`
      ),
    rss_link: yup
      .string()
      .required(translate("LinkRequired"))
      .matches(regexUrl(), translate("LinkMatches"))
      .max(
        100,
        ({ max }) =>
          `${translate("LinkLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "40px" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>News Rss</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "News Rss",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={formSize}
            type="primary"
            style={{ backgroundColor: "#CB0B0C" }}
            onClick={addModal}
          >
            <PlusOutlined />
            Create News Rss
          </Button>
        </div>
      </div>
      <div className="category-content">
        {/* <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search RSS News</p>
            <p style={{ marginTop: "18px" }}>RSS News Name</p>
            <Input
              size={formSize}
              placeholder="RSS Name"
            />
            <p style={{ marginTop: 18 }}>Link RSS</p>
            <Input.TextArea
              size={formSize}
              placeholder="Link RSS"
            />
            <Button
              size={formSize}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Search
            </Button>
          </Card>
        </div> */}
        <div className="category-table">
          {renderCards()}
          {newsDataRss?.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <div>
                <div>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataRSS?.records}
                    limit={dataRSS?.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataRSS?.records}
                    limit={dataRSS?.limit}
                  />
                </div>
              </div>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add News Rss"
        open={isAddModalVisible}
        width={1100}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddNewsRssSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"RSS News Name"}
                title={"RSS News Name"}
                type={"text"}
                name={"media_name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Link RSS"}
                title={"Link RSS"}
                type={"url"}
                name={"rss_link"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostRSS}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Edit */}
      <Modal
        centered
        title="Form Edit RSS News"
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataNews}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handlePublish(values, "published");
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"RSS News Name"}
                title={"RSS News Name"}
                type={"text"}
                name={"media_name"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Link RSS"}
                title={"Link RSS"}
                type={"url"}
                name={"rss_link"}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostUpdateNewsRSS}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default NewsRSSPage;
