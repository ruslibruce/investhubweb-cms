import { deleteNewsRss } from "@/shared/api/mutation/newsRss";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteNewsRss = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteNewsRss,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["rss"]] });
          notification.success({
            message: "The news rss was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteNewsRss = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteNewsRss,
  };
};

export default useDeleteNewsRss;
