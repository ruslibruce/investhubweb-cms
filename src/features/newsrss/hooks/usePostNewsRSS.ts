import { postNewsRSS } from "@/shared/api/mutation/newsRss";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";
import { useRouter } from "next/router";

const usePostNewsRSS = () => {
  const queryClient = useQueryClient();
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: postNewsRSS,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          notification.success({
            message: "The news RSS was successfully created",
          });
          queryClient.invalidateQueries({ queryKey: [["rss"]] });
        }
      },
    },
  });

  const handlePostNewsRSS = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostNewsRSS,
  };
};

export default usePostNewsRSS;
