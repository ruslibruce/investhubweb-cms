import {
  PlusOutlined
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Checkbox,
  Col,
  Row,
  Space,
  Spin
} from "antd";
import { useRouter } from "next/router";

import NoData from "@/shared/components/NoData";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useGetDetailLevel from "../hooks/useGetDetailLevel";
import usePutLevels from "../hooks/usePutLevels";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";

const LevelPageDetail = () => {
  const navigate = useRouter();
  const {slug} = navigate.query;
  const id = slug?.[0];
  const name = slug?.[1];
  const { fetchQuery } = useGetDetailLevel(id as any);
  const { data: dataLevel, isLoading } = fetchQuery;
  const { mutationQuery, handleUpdateLevel } = usePutLevels();
  const [dataChecked, setDataChecked] = useState([]);
  console.log('dataLevel', dataLevel);
  

  React.useEffect(() => {
    if (dataLevel) {
      let temp = [] as any;
      dataLevel?.map((item: any) => {
        temp.push(item.checked);
      });
      // console.log("temp", temp);
      setDataChecked(temp);
    }
  }, [dataLevel]);

  const onChangeCheckbox = (e: any, ind: number) => {
    console.log("dataChecked", ind);

    console.log(`checked = ${e.target.checked}`);
    let temp: any = dataChecked.map((item: any, index: any) => {
      if (index === ind) {
        item = e.target.checked ? 1 : 0;
        console.log("item", item);
      }
      return item;
    });

    setDataChecked(temp);
  };

  const handleSaveLevel = () => {
    let temp: any = [];
    dataChecked.map((item: any, index: any) => {
      if (item === 1) {
        let data = dataLevel?.find((element: any, ind: any) => {
          return ind == index;
        });
        console.log("data", data);

        temp.push(data.id);
      }
    });
    console.log("temp", temp);

    handleUpdateLevel(`/requirement/${id}`, {
      requirements: temp,
    });
  };

  const renderCards = () => {
    if(isLoading) {
      return <LoaderSpinGif size="large" />
    }

    if (dataLevel && dataLevel.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataLevel?.map((card: any, index: any) => (
      <Card key={card.id}>
        <Row>
          <Col span={22}>
            {/* <p
              style={{
                color:
                  card.status.toLowerCase() === "active"
                    ? "green"
                    : card.status.toLowerCase() === "draft"
                    ? "#e5e500"
                    : "red",
                fontWeight: "bold",
              }}
            >
              {card.status}
            </p> */}
          </Col>
          <Col span={2}></Col>
        </Row>
        <Row style={{ display: "flex", alignItems: "center" }}>
          <Col span={22}>
            {/* <p style={{ fontWeight: "bold" }}>{card.id}</p> */}
            <p style={{ fontWeight: "bold" }}>
              {index + 1} - {card.title}
            </p>
            <p style={{ color: "gray" }}>{card.title}</p>
          </Col>
          <Col span={2}>
            <Checkbox
              style={{ transform: "scale(1.5)" }}
              checked={dataChecked[index]}
              onChange={(e) => onChangeCheckbox(e, index)}
            />
          </Col>
        </Row>
      </Card>
    ));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Row>
        <Col span={16}>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>
            Level Requirement - {name}
          </p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: `Level Requirement - ${name}`,
              },
            ]}
          />
        </Col>
        <Col span={8}>
          <Button
            type="primary"
            onClick={handleSaveLevel}
            style={{ backgroundColor: "#CB0B0C", marginLeft: 150 }}
          >
            <PlusOutlined />
            Save Requirements
          </Button>
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        {/* <Col span={6}>
          <Card style={{ width: "100%", marginTop: "30px" }}>
            <p style={{ fontWeight: "bold" }}>Search Course</p>
            <p style={{ marginTop: "18px" }}>Name</p>
            <Input style={{ height: 50 }} placeholder="Name" />
            <p style={{ marginTop: 18 }}>Description</p>
            <Input.TextArea style={{ height: 50 }} placeholder="Description" />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              defaultValue="active"
              style={{ width: "100%", height: "50px" }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-active", label: "Non-Active" },
              ]}
            />
            <Button
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Submit
            </Button>
          </Card>
        </Col> */}
        <Col span={24}>
          {renderCards()}
        </Col>
      </Row>
    </Space>
  );
};

export default LevelPageDetail;
