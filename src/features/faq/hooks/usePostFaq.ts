import { addFaq } from "@/shared/api/mutation/faq";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostFaq = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: addFaq,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["faq"]] });
          notification.success({
            message: "The FAQ was successfully created",
          });
        }
      },
    },
  });

  const handlePostFaq = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handlePostFaq,
  };
};

export { usePostFaq };
