import { editFaq } from "@/shared/api/mutation/faq";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useEditFaq = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: editFaq,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["faq"]] });
          notification.success({
            message: "FAQ berhasil diupdate",
          });
        }
      },
    },
  });

  const handleEditFaq = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleEditFaq,
  };
};

export { useEditFaq };
