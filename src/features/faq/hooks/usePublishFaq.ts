import { postUpdateFAQ } from "@/shared/api/mutation/faq";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useUpdateFAQ = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postUpdateFAQ,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables: any, context) {
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["faq"]] });
          notification.success({
            message:
              variables.type === "update"
                ? "FAQ berhasil diupdate"
                : variables.type === "unpublished"
                ? "FAQ berhasil diunpublish"
                : "FAQ berhasil dipublish",
          });
        }
      },
    },
  });

  const handleUpdateFAQ = (data: {}, id: string, type?: string) => {
    mutationQuery.mutate({ data, id, type });
  };

  return {
    mutationQuery,
    handleUpdateFAQ,
  };
};

export default useUpdateFAQ;
