import { deleteFaq } from "@/shared/api/mutation/faq";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDeleteFaq = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: deleteFaq,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["faq"]] });
          notification.success({
            message: "The FAQ was successfully deleted",
          });
        }
      },
    },
  });

  const handleDeleteFaq = (id: string) => {
    mutationQuery.mutate({ id });
  };

  return {
    mutationQuery,
    handleDeleteFaq,
  };
};

export { useDeleteFaq };
