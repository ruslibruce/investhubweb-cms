import HamburgerCard from "@/components/HamburgerCard";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { DASHBOARD_FAQ_DETAIL } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { checkIsEmptyObject, getStatusColor } from "@/shared/utils/helper";
import { paramsToString } from "@/shared/utils/string";
import {
  ArrowRightOutlined,
  DeleteOutlined,
  DeleteTwoTone,
  EditTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Divider,
  Flex,
  Input,
  Modal,
  Row,
  Select,
  Space,
  Spin,
} from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import { useDeleteFaq } from "../hooks/useDeleteFaq";
import { useEditFaq } from "../hooks/useEditFaq";
import useGetFaq from "../hooks/useGetFaq";
import { usePostFaq } from "../hooks/usePostFaq";
import useUpdateFAQ from "../hooks/usePublishFaq";
import NoData from "@/shared/components/NoData";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialCreateFaq = {
  question: "",
  answer: "",
  role_user: "",
};

const initialEditFaq = {
  question: "",
  answer: "",
  id: "",
  role_user: "",
};

const FaqPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [fontSize, setFontSize] = useState("initial");
  const [currentPage, setCurrentPage] = React.useState<number>(1);
  // Post
  const { mutationQuery: mutationPost, handlePostFaq } = usePostFaq();
  const { isPending: isLoadingPostFaq, isSuccess: isSuccessPostFaq } =
    mutationPost;
  // Delete
  const { handleDeleteFaq } = useDeleteFaq();
  // Edit
  const { mutationQuery: mutationUpdate, handleEditFaq } = useEditFaq();
  const { isPending: isLoadingEditFaq, isSuccess: isSuccessEditFaq } =
    mutationUpdate;
  // Filter
  const [dataFilter, setDataFilter] = React.useState({
    question: "",
    is_active: "",
    answer: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState({
    question: "",
    is_active: "",
    answer: "",
  });

  // Get
  const { fetchQuery } = useGetFaq(
    [],
    paramsToString({ page: currentPage, ...dataFilterApi })
  );
  const { data: dataResultFaq, isLoading } = fetchQuery;
  const { data: dataFaq } = dataResultFaq;
  const { handleUpdateFAQ } = useUpdateFAQ();
  const [formDataFaq, setFormDataFaq] = useState<any>(initialCreateFaq);
  const [editDataFaq, editFormDataFaq] = useState<any>(initialEditFaq);
  const [formSize, setFormSize] = React.useState<SizeType>("large");
  
  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };
  
    window.addEventListener("resize", handleResize);
  
    // Call the function initially to set the correct form size on load
    handleResize();
  
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    if (isSuccessEditFaq) {
      setIsEditModalVisible(false);
    }
    if (isSuccessPostFaq) {
      setIsModalVisible(false);
    }
  }, [isSuccessEditFaq, isSuccessPostFaq]);

  const handleChangeStatus = (value: string) => {
    console.log(`selected ${value}`);
    if (value === "all") {
      setDataFilter({
        ...dataFilter,
        is_active: "",
      });
      return;
    }
    setDataFilter({
      ...dataFilter,
      is_active: value,
    });
  };

  const handleSetFilter = () => {
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };

  React.useEffect(() => {
    console.log("Data Result FAQ:", dataResultFaq);
  }, [dataResultFaq]);

  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    handleUpdateFAQ(
      {
        ...value,
        is_active: params,
      },
      value.id,
      params
    );
  };

  const handleAddDataFaq = async (values: any) => {
    handlePostFaq(values);
  };

  const handleDelete = (id: string) => {
    handleDeleteFaq(id);
  };

  const editModal = (value: any) => {
    editFormDataFaq({
      ...initialEditFaq,
      id: value.id,
      question: value.question,
      answer: value.answer,
      role_user: value.role_user,
    });
    setIsEditModalVisible(true);
  };

  const handleEditSave = async (values: any) => {
    handleEditFaq(values.id, values);
  };

  const handleEditInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    editFormDataFaq((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleQuestionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormDataFaq((prev: any) => ({
      ...prev,
      question: e.target.value,
    }));
  };

  const handleAnswerChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setFormDataFaq((prev: any) => ({
      ...prev,
      answer: e.target.value,
    }));
  };

  const handleRoleChange = (value: string, setFieldValue: any) => {
    console.log(`selected ${value}`);
    setFieldValue("role_user", value);
  };

  const handleEditRoleChange = (value: string) => {
    console.log(`selected ${value}`);
    editFormDataFaq((prev: any) => ({
      ...prev,
      role_user: value,
    }));
  };

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };
  }, []);

  const showModal = () => {
    setFormDataFaq(initialCreateFaq);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleFaq = (event: React.MouseEvent<HTMLElement>, value: any) => {
    navigate.push(`${DASHBOARD_FAQ_DETAIL}/${value.id}`);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    question: yup
      .string()
      .required(translate("QuestionRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("QuestionLength")} ${max} ${translate("Characters")}`
      ),
    answer: yup
      .string()
      .required(translate("AnswerRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("AnswerLength")} ${max} ${translate("Characters")}`
      ),
    role_user: yup.string().required(translate("RoleUserRequired")),
  });

  const renderCards = () => {
    if (!dataFaq) return <LoaderSpinGif size="large" />;
    if (dataFaq && dataFaq.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataFaq?.map((item: any) => (
      <Card key={item.id} style={{ marginBottom: "15px" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(item.is_active),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {item.is_active}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              {/* <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleFaq(e, { id: item.id })}
                />
              </span> */}
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => editModal(item)}
                />
              </span>
              <span style={{ marginRight: 10 }}>
                <DeleteTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => {
                    Modal.confirm({
                      title: "Delete Data",
                      content: "Are you sure you want to delete this data?",
                      onOk: () => handleDelete(item.id),
                      okButtonProps: {
                        icon: <DeleteOutlined />,
                        style: { backgroundColor: "#CB0B0C" },
                      },
                    });
                  }}
                />
              </span>
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                isView={false}
                onEdit={() => editModal(item)}
                onDelete={() => {
                  Modal.confirm({
                    title: "Delete Data",
                    content: "Are you sure you want to delete this data?",
                    onOk: () => handleDelete(item.id),
                    okButtonProps: {
                      icon: <DeleteOutlined />,
                      style: { backgroundColor: "#CB0B0C" },
                    },
                  });
                }}
                arrayItems={["1", "2"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p style={{ fontWeight: "bold", marginBottom: 5 }}>
              {item.role_user}
            </p>
            <p style={{ fontWeight: 600 }}>{item.question}</p>
            <p style={{ fontWeight: 600 }}>{item.answer}</p>
          </div>
          <div>
            {item.is_active === "draft" && (
              <Button
                size={formSize}
                onClick={(e) => handlePublish(e, item, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: "green",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
            {item.is_active === "published" && (
              <Button
                size={formSize}
                onClick={(e) => handlePublish(e, item, "unpublished")}
                style={{
                  marginTop: 24,
                  backgroundColor: "#CB0B0C",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Unpublish
              </Button>
            )}
            {item.is_active === "pending" &&
              dataUser?.user?.role === "portal-admin" && (
                <Button
                  size={formSize}
                  onClick={(e) => handlePublish(e, item, "unpublished")}
                  style={{
                    marginTop: 24,
                    backgroundColor: "#CB0B0C",
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  iconPosition="end"
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                >
                  Unpublish
                </Button>
              )}
            {item.is_active === "pending" &&
              dataUser?.user?.role == "kurator" && (
                <Button
                  size={formSize}
                  onClick={(e) => handlePublish(e, item, "published")}
                  style={{
                    marginTop: 24,
                    backgroundColor: "green",
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  iconPosition="end"
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                >
                  Publish
                </Button>
              )}
            {item.is_active === "unpublished" && (
              <Button
                size={formSize}
                onClick={(e) => handlePublish(e, item, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: "green",
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
          </div>
        </div>
      </Card>
    ));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>FAQ</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "FAQ",
              },
            ]}
          />
        </div>
        <div className="button-post-category">
          <Button
            size={formSize}
            type="primary"
            onClick={showModal}
            style={{ backgroundColor: "#CB0B0C" }}
          >
            <PlusOutlined />
            Create FAQ
          </Button>
        </div>
      </div>

      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search FAQ</p>
            <p style={{ marginTop: "18px" }}>Question</p>
            <Input
              size={formSize}
              placeholder="Question"
              onChange={(e) =>
                setDataFilter({
                  ...dataFilter,
                  question: e.target.value,
                })
              }
            />
            <p style={{ marginTop: 18 }}>Answer</p>
            <Input
              size={formSize}
              placeholder="Answer"
              onChange={(e) =>
                setDataFilter({
                  ...dataFilter,
                  answer: e.target.value,
                })
              }
            />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              size={formSize}
              defaultValue="All"
              style={{ width: "100%" }}
              onChange={handleChangeStatus}
              options={[
                { value: "all", label: "All" },
                { value: "published", label: "Publish" },
                { value: "unpublished", label: "Unpublish" },
              ]}
            />
            <Button
              size={formSize}
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
              onClick={handleSetFilter}
            >
              Search
            </Button>
          </Card>
        </div>
        <div className="category-table">
          {renderCards()}
          {dataFaq && dataFaq.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <Row>
                <Col>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultFaq?.records}
                    limit={dataResultFaq?.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultFaq?.records}
                    limit={dataResultFaq?.limit}
                  />
                </Col>
              </Row>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add FAQ"
        open={isModalVisible}
        width={1200}
        onCancel={handleCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialCreateFaq}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddDataFaq(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, values, errors, setFieldValue }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Question"}
                title={"Question"}
                type={"text"}
                name={"question"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Answer"}
                title={"Answer"}
                type={"text"}
                name={"answer"}
                textArea={true}
              />
              <label className="required" style={{ marginTop: 18 }}>
                Role
              </label>
              <Select
                size={formSize}
                defaultValue="Select Role"
                style={{ width: "100%" }}
                onChange={(value) => handleRoleChange(value, setFieldValue)}
                options={[
                  { value: "portal-user", label: "User" },
                  { value: "cp-admin", label: "Content Provider" },
                ]}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPostFaq}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update FAQ"
        open={isEditModalVisible}
        width={1200}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialEditFaq}
          enableReinitialize
          onSubmit={(values) => {
            handleEditSave(values);
          }}
        >
          {({ handleSubmit, isValid, values, errors, setFieldValue }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Question"}
                title={"Question"}
                type={"text"}
                name={"question"}
              />
              <Field
                component={FormInputFormik}
                placeholder={"Answer"}
                title={"Answer"}
                type={"text"}
                name={"answer"}
                textArea={true}
              />
              <label className="required" style={{ marginTop: 18 }}>
                Role
              </label>
              <Select
                size={formSize}
                value={values.role_user}
                style={{ width: "100%" }}
                onChange={(value) => handleRoleChange(value, setFieldValue)}
                options={[
                  { value: "portal-user", label: "User" },
                  { value: "cp-admin", label: "Content Provider" },
                ]}
              />
              {errors.role_user && (
                <div className={"error"}>{errors.role_user.toString()}</div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingEditFaq}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default FaqPage;
