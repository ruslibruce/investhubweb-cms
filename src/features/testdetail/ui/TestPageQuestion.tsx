import {
  DASHBOARD_COURSE_DETAIL,
  DASHBOARD_TEST,
} from "@/shared/constants/path";
import { Breadcrumb, Card, Checkbox, Col, Flex, Row, Space, Spin } from "antd";
import { useRouter } from "next/router";

import NoData from "@/shared/components/NoData";
import { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useDelCheckQuestion from "../hooks/useDelCheckQuestion";
import useGetTestQuestion from "../hooks/useGetTestQuestion";
import usePostCheckQuestion from "../hooks/usePostCheckQuestion";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
const TestPageQuestion = () => {
  const navigate = useRouter();
  const { slug } = navigate.query;
  const id = slug?.[0];
  const name = slug?.[1];
  const idDetailCourse = slug?.[2];
  const isCourseDetail = slug?.[3];
  const { fetchQuery } = useGetTestQuestion(id as any);
  const { data: dataTestQuestion, isLoading } = fetchQuery;
  const { handleChecked, mutationQuery: mutationQueryPost } =
    usePostCheckQuestion();
  const { isPending: isLoadingPost } = mutationQueryPost;
  const { handleUnChecked, mutationQuery: mutationQueryDel } =
    useDelCheckQuestion();
  const { isPending: isLoadingDel } = mutationQueryDel;
  const [delaySecond, setDelaySecond] = useState(false);

  const delay = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  const methodDelay = async () => {
    await delay(2000); // 2 second delay
    setDelaySecond(false);
  };

  const onChangeCheckbox = async (e: any, idQuestion: string) => {
    console.log(`checked = ${e.target.checked}`);
    e.preventDefault();
    if (delaySecond) return;
    setDelaySecond(true);
    if (e.target.checked) {
      let data = {
        test_id: id,
        question_id: idQuestion,
      };
      handleChecked(data);
      methodDelay();
    } else {
      handleUnChecked(id, idQuestion);
      methodDelay();
    }
  };

  const renderCards = () => {
    if (!dataTestQuestion) return <LoaderSpinGif size="large" />;
    if (dataTestQuestion && dataTestQuestion.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataTestQuestion?.map((card: any, index: any) => {
      let dataOptions = JSON.parse(card.options);
      return (
        <Card key={card.id}>
          <Row>
            <Col span={22}>
              {/* <p
            style={{
              color:
                card.status.toLowerCase() === "active"
                  ? "green"
                  : card.status.toLowerCase() === "draft"
                  ? "#e5e500"
                  : "red",
              fontWeight: "bold",
            }}
          >
            {card.status}
          </p> */}
            </Col>
            <Col span={2}></Col>
          </Row>
          <Row style={{ display: "flex", alignItems: "center" }}>
            <Col span={22}>
              {/* <p style={{ fontWeight: "bold" }}>{card.id}</p> */}
              <p
                style={{ color: "gray" }}
                dangerouslySetInnerHTML={{ __html: card.question }}
              />
              {dataOptions.map((item: any, index: any) => {
                return (
                  <Flex key={index} style={{ marginTop: 10 }}>
                    <p
                      style={{ color: "gray" }}
                      dangerouslySetInnerHTML={{ __html: item.option }}
                    />
                    <p style={{ color: "gray", marginRight: 10 }}>{"."}</p>
                    <p
                      style={{ color: "gray" }}
                      dangerouslySetInnerHTML={{ __html: item.value }}
                    />
                  </Flex>
                );
              })}
            </Col>
            <Col span={2}>
              {(isLoadingDel || isLoadingPost) && delaySecond ? (
                <LoaderSpinGif size="large" />
              ) : (
                <>
                  {dataTestQuestion && dataTestQuestion[index].id != card.id ? (
                    <LoaderSpinGif size="large" />
                  ) : (
                    <Checkbox
                      style={{ transform: "scale(1.5)" }}
                      checked={card.check}
                      onChange={(e) => onChangeCheckbox(e, card.id)}
                    />
                  )}
                </>
              )}
            </Col>
          </Row>
        </Card>
      );
    });
  };

  return (
    <Space direction="vertical" style={{ display: "flex", marginTop: "40px" }}>
      <Row>
        <Col span={16}>
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>
            Test Question - {name}
          </p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: isCourseDetail ? "Course Detail" : "Test",
                onClick: () => {
                  if (!isCourseDetail) {
                    return;
                  }
                  navigate.push(`${DASHBOARD_COURSE_DETAIL}/${idDetailCourse}`);
                },
                href: isCourseDetail ? "#" : `/cms/${DASHBOARD_TEST}`,
              },
              {
                title: `Test Question - ${name}`,
              },
            ]}
          />
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        {/* <Col span={6}>
          <Card style={{ width: "100%", marginTop: "30px" }}>
            <p style={{ fontWeight: "bold" }}>Search Course</p>
            <p style={{ marginTop: "18px" }}>Name</p>
            <Input style={{ height: 50 }} placeholder="Name" />
            <p style={{ marginTop: 18 }}>Description</p>
            <Input.TextArea style={{ height: 50 }} placeholder="Description" />
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              defaultValue="active"
              style={{ width: "100%", height: "50px" }}
              onChange={handleChange}
              options={[
                { value: "active", label: "Active" },
                { value: "draft", label: "Draft" },
                { value: "non-active", label: "Non-Active" },
              ]}
            />
            <Button
              type="primary"
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
            >
              Submit
            </Button>
          </Card>
        </Col> */}
        <Col span={24}>{renderCards()}</Col>
      </Row>
    </Space>
  );
};

export default TestPageQuestion;
