import { delUncheckTest } from "@/shared/api/fetch/test";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useDelCheckQuestion = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: delUncheckTest,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({
          queryKey: ["testQuestion"],
          refetchType: 'active',
        });
        if (data) {
          notification.info({
            message: "Question berhasil dihapus",
          });
        }
      },
    },
  });

  const handleUnChecked = (idTest: any, idQuestion: string) => {
    mutationQuery.mutate({ idTest, idQuestion });
  };

  return {
    mutationQuery,
    handleUnChecked,
  };
};

export default useDelCheckQuestion;
