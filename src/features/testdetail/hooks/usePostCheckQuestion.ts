import { postCheckTest } from "@/shared/api/fetch/test";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePostCheckQuestion = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postCheckTest,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        queryClient.invalidateQueries({
          queryKey: ["testQuestion"],
          refetchType: 'active',
        });
        if (data) {
          notification.success({
            message: "Question berhasil ditambahkan",
          });
        }
      },
    },
  });

  const handleChecked = (data: {}) => {
    mutationQuery.mutate(data);
  };

  return {
    mutationQuery,
    handleChecked,
  };
};

export default usePostCheckQuestion;
