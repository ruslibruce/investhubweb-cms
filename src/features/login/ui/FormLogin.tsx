import { Form, Input, Space, Button, notification } from "antd";
import useLoginForm from "../hooks/useLoginForm";
import TextWelcomeLogin from "./components/TextWelcomeLogin";
import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import ForgorPasswordLoginComponent from "./components/ForgorPasswordLoginComponent";
import TextRegisterComponent from "./components/TextRegisterComponent";
import CustomButton from "@/shared/components/CustomButton";
import ReCAPTCHA from "react-google-recaptcha";
import React, { useState } from "react";
import { SizeType } from "antd/es/config-provider/SizeContext";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { jwtDecode } from "jwt-decode";
import useGetProfileMutate from "@/features/profile/hooks/useGetProfileMutate";

const FormLogin = () => {
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { loginsession } = navigate.query;
  const { mutationQuery, handleOnSubmit } = useLoginForm();
  const { isPending, data: dataLogin } = mutationQuery;
  const [token, setToken] = React.useState<any>("");
  const captchaRef = React.useRef<any>(null);
  const [formSize, setFormSize] = React.useState<SizeType>("large");
  const { handleGetProfile } = useGetProfileMutate();

  React.useEffect(() => {
    if (dataLogin) {
      let resultData: any = dataLogin;
      if (!resultData.data?.cp) {
        notification.error({
          message: "Error",
          description:
            "Your account role is not assigned yet. Please contact admin.",
        });
        return;
      }
      const decodedToken: any = jwtDecode(resultData.data.authorisation.token);
      handleGetProfile({
        token: resultData.data.authorisation.token,
        user: decodedToken.user,
        cp: resultData.data?.cp,
      });
    }
  }, [dataLogin]);

  React.useEffect(() => {
    if (loginsession) {
      notification.info({
        message: translate("EndSession"),
        description: translate("EndSessionDesc"),
      });
    }
  }, [loginsession]);

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else if (screenWidth <= 450) {
        setFormSize("small");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const onFinish = (values: any) => {
    if (!token) {
      notification.error({
        message: "reCAPTCHA",
        description: "Please verify the reCAPTCHA!",
      });
      return;
    }

    setToken("");
    handleOnSubmit({
      ...values,
      "g-recaptcha-response": captchaRef.current.getValue(),
    });
    captchaRef.current.reset();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const onVerify = () => {
    const tokenCaptcha = captchaRef.current.getValue();
    // alert(tokenCaptcha);
    setToken(tokenCaptcha);
  };

  const onExpire = () => {
    notification.info({
      message: "reCAPTCHA",
      description: "Token Recaptcha has expired",
    });
  };

  return (
    <Space
      direction="vertical"
      size={40}
      style={{ display: "flex", marginTop: "20px" }}
    >
      <TextWelcomeLogin />
      <Space direction="vertical" size={0} style={{ display: "flex" }}>
        <Form
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          size={formSize}
        >
          <CustomFormItem
            label="Email"
            name="email"
            rules={[
              {
                type: "email",
                required: true,
                message: "Please input your email!",
              },
            ]}
          >
            <Input style={{ height: 60 }} />
          </CustomFormItem>

          <CustomFormItem
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password style={{ height: 60 }} />
          </CustomFormItem>

          <ForgorPasswordLoginComponent />

          {/* <TextRegisterComponent /> */}
          <div className={"position-recaptcha"}>
            <ReCAPTCHA
              sitekey={process.env.NEXT_PUBLIC_SITE_KEY as string}
              onChange={onVerify}
              onExpired={onExpire}
              ref={captchaRef}
            />
          </div>

          <CustomFormItem>
            <Button
              type="primary"
              style={{ backgroundColor: "#CB0B0C" }}
              block
              spellCheck={true}
              loading={isPending}
              htmlType="submit"
            >
              Submit
            </Button>
            {/* <CustomButton
              typeButton="primary"
              loading={isPending}
              spellCheck={true}
              block
              htmlType="submit">
              Submit
            </CustomButton> */}
          </CustomFormItem>
        </Form>
      </Space>
    </Space>
  );
};

export default FormLogin;
