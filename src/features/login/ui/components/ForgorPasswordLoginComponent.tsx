
// antd
import { Typography } from 'antd';

// next
import { DASHBOARD_FORGOT } from '@/shared/constants/path';
import { useRouter } from 'next/router';

// path-url
// import { AUTH_FORGOT_PASSWORD_URL } from 'src/shared/constants/path';

const ForgorPasswordLoginComponent = () => {
  // router
  const router = useRouter();

  // func
  const hanleToForgotPassword = () => {
    router.push(DASHBOARD_FORGOT);
  };
  return (
    <Typography
      style={{
        color: "#9F0E0F",
        fontWeight: 700,
        textAlign: "right"
      }}>
      <span
        style={{
          cursor: "pointer",
          width: "fit-content",
          height: "fit-content"
        }}
        onClick={hanleToForgotPassword}>
        Forgot Your Password?
      </span>
    </Typography>
  );
};

export default ForgorPasswordLoginComponent;
