import { logIn } from "@/shared/api/mutation/login";
import {
  DASHBOARD_HOME,
  DASHBOARD_PROFILE,
  DASHBOARD_TERMS,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { storageSet } from "@/shared/utils/clientStorageUtils";
import { notification } from "antd";
import { useRouter } from "next/router";
import { jwtDecode } from "jwt-decode";

const useLoginForm = () => {
  const navigate = useRouter();

  const mutationQuery = useMutationHook({
    api: logIn,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        // console.log("data useLogin", data);
        // console.log("variables", variables);
        // console.log("context", context);
      },
    },
  });

  const handleOnSubmit = (values: any) => {
    mutationQuery.mutate(values);
  };

  return {
    mutationQuery,
    handleOnSubmit,
  };
};

export default useLoginForm;
