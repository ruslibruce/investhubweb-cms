import React from "react";
import { Space, Breadcrumb, Card, Spin, Button } from "antd";
import { useRouter } from "next/router";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { USER } from "@/shared/constants/storageStatis";
import usePutDetailFAQ from "../hooks/usePutDetailFaq";
import useGetDetailFAQ from "../hooks/useGetDetailFaq";
import { getStatusColor } from "@/shared/utils/helper";
import moment from "moment";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { DASHBOARD_FAQ } from "@/shared/constants/path";

const FAQDetailPage = () => {
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { id } = navigate.query;

  const { fetchQuery } = useGetDetailFAQ(id as string, {});
  const { data: faqDetail, isLoading } = fetchQuery;
  const { handleUpdateDetailFAQ, mutationQuery } = usePutDetailFAQ();
  const { isPending: isLoadingUpdateFAQ } = mutationQuery;

  if(isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(`${faqDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />
  }

  if(!faqDetail) {
    return <NoData />
  }

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <p style={{ fontWeight: "bold", fontSize: "24px" }}>Detail FAQ</p>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "FAQ",
            href: `/cms/${DASHBOARD_FAQ}`,
          },
          {
            title: "Detail FAQ",
          },
        ]}
      />
      <Card loading={faqDetail ? false : true}>
        <p style={{ fontWeight: "bold", fontSize: 18 }}>Detail FAQ</p>
        <p style={{ marginTop: 10 }}>
          <Button
            ghost
            disabled
            style={{
              color: "white",
              background: getStatusColor(faqDetail?.is_active),
              fontWeight: "bold",
              marginBottom: 12,
            }}
          >
            {faqDetail?.is_active}
          </Button>
        </p>
        <b>Question:</b>
        <p style={{ color: "gray", marginBottom: 13 }}>{faqDetail?.question}</p>
        <b>Answer:</b>
        <p style={{ color: "gray", marginBottom: 13 }}>{faqDetail?.answer}</p>
      </Card>
    </Space>
  );
};

export default FAQDetailPage;
