import { fetchDetailFAQ } from "@/shared/api/fetch/faqDetail";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchDetailFAQ();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetDetailFAQ = (id: string, initialData?: any) => {
  const fetchDataFAQ = fetchDetailFAQ(id);
  const fetchQuery = useFetchHook({
    keys: fetchDataFAQ.key,
    api: fetchDataFAQ.api,
    initialData,
    options: {}
  });

  return {
    fetchQuery
  };
};

export default useGetDetailFAQ;
