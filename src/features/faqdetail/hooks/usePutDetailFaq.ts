import { editFaq } from "@/shared/api/mutation/faq";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutDetailFAQ = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: editFaq,
    options: {
      onError(error: any, variables, context) {},

      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["faqDetail"]] });
          notification.success({
            message: "FAQ berhasil diupdate",
          });
        }
      },
    },
  });

  const handleUpdateDetailFAQ = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleUpdateDetailFAQ,
  };
};

export default usePutDetailFAQ;
