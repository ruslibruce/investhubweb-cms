import { fetchGeneralContent } from "@/shared/api/fetch/generalContent";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchGeneralContent();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

const useGetGeneralContent = (initialData?: any) => {
  const fetchDataColors = fetchGeneralContent();
  const fetchGetGeneralContent = useFetchHook({
    keys: fetchDataColors.key,
    api: fetchDataColors.api,
    initialData,
    options: {}
  });

  return {
    fetchGetGeneralContent
  };
};

export default useGetGeneralContent;
