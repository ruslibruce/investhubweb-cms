import { editGeneralContent } from "@/shared/api/mutation/generalContent";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useEditGeneralContent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: editGeneralContent,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["generalContent"] });
          notification.success({
            message: "General Content berhasil diupdate",
          });
        }
      },
    },
  });

  const handleUpdateGeneralContent = (id: string, data: {}) => {
    mutationQuery.mutate({ id, data });
  };

  return {
    mutationQuery,
    handleUpdateGeneralContent,
  };
};

export default useEditGeneralContent;
