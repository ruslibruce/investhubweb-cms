import React, { useState } from "react";
import {
  Space,
  Breadcrumb,
  Row,
  Col,
  Button,
  Card,
  Flex,
  Select,
  Modal,
  Spin,
  Divider,
} from "antd";
import {
  EditTwoTone,
  EyeTwoTone,
  PlusOutlined,
  SendOutlined,
} from "@ant-design/icons";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
// import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import useEditGeneralContent from "../hooks/usePutGeneralContent";
import useGetGeneralContent from "../hooks/useGetGeneralContent";
import HamburgerCard from "@/components/HamburgerCard";
import { DASHBOARD_GENERAL_CONTENT_DETAIL } from "@/shared/constants/path";
import {
  checkIsEmptyObject,
  isEmptyContent,
  ReactQuillNoSSR,
} from "@/shared/utils/helper";
import { TOOLBAR_OPTION_QUILL } from "@/shared/constants/storageStatis";
import NoData from "@/shared/components/NoData";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";

const GeneralContentPage = () => {
  const { t: translate } = useTranslation();
  const navigation = useRouter();
  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);
  const { mutationQuery, handleUpdateGeneralContent } = useEditGeneralContent();
  const { isPending: isLoadingUpdateGeneralContent, data } = mutationQuery;
  const [selectedContent, setSelectedContent] = useState<string | null>(null);
  const [content, setContent] = useState<any>({
    id: "",
    title: "",
    body: "",
  });
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const { fetchGetGeneralContent } = useGetGeneralContent();
  const { data: dataGeneralContent, isLoading } = fetchGetGeneralContent;
  const [isMobile, setIsMobile] = useState(window.innerWidth < 550);
  const [fontSize, setFontSize] = useState("initial");

  React.useEffect(() => {
    if (data) {
      setIsEditModalVisible(false);
    }
  }, [data]);

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleSubmit = (values: any) => {
    handleUpdateGeneralContent(values.id, values);
    console.log("lihat id:", values.id);
  };

  // const handleSubmit = () => {
  //   if (!selectedContent) return;

  //   const idMap: { [key: string]: number } = {
  //     "About Us": 1,
  //     Disclaimer: 2,
  //     "Syarat & Ketentuan": 3,
  //     "Kebijakan Privacy": 4,
  //   };

  //   const id = idMap[selectedContent];
  //   const data = {title:selectedContent, content};

  //   console.log("Submiting data:", data);

  //   editContent({ id, data });
  // };

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };
  }, []);

  React.useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 550);
    };

    window.addEventListener("resize", handleResize);

    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const truncateTextToHalf = (text: string) => {
    const halfLength = Math.floor(text.length / 4);
    return text.slice(0, halfLength) + "...";
  };

  const handleGeneralContent = (
    event: React.MouseEvent<HTMLElement>,
    value: any
  ) => {
    console.log("query", value);
    event.preventDefault();
    navigation.push(`${DASHBOARD_GENERAL_CONTENT_DETAIL}/${value.id}`);
  };

  const editModal = (values: any) => {
    console.log("values", values);

    setContent((prev: any) => ({
      ...prev,
      id: values.id,
      title: values.title,
      body: values.body,
    }));
    setIsEditModalVisible(true);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    body: yup.string().required(translate("ContentBodyRequired")),
  });

  const renderCards = () => {
    if (!dataGeneralContent) return <LoaderSpinGif size="large" />;
    if (dataGeneralContent && dataGeneralContent.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataGeneralContent?.map((card: any, index: any) => (
      <Card key={card.id} loading={isLoadingUpdateGeneralContent}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <Flex
              style={{ flexDirection: "row" }}
              className="curd-button"
              justify="flex-end"
              align="center"
            >
              <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleGeneralContent(e, { id: card.id })}
                />
              </span>
              <span style={{ marginRight: 10 }}>
                <EditTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={() => editModal(card)}
                />
              </span>
            </Flex>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleGeneralContent(e, { id: card.id })}
                onEdit={() => editModal(card)}
                arrayItems={["0", "1"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content" style={{ display: "flex", gap: 5 }}>
            <p style={{ fontWeight: "bold" }}>{`${card.id}  |  `}</p>
            <p
              style={{ fontWeight: "bold" }}
              dangerouslySetInnerHTML={{ __html: card.title }}
            />
            {/* <p
              style={{ color: "gray" }}
              dangerouslySetInnerHTML={{
                __html: isMobile ? truncateTextToHalf(card.body) : card.body,
              }}
            /> */}
          </div>
        </div>
      </Card>
    ));
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Card>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div>
            <p style={{ fontWeight: "bold", fontSize: "24px" }}>
              General Content
            </p>
            <Breadcrumb
              separator=">"
              items={[
                {
                  title: "General Content",
                },
              ]}
            />
          </div>
        </div>
      </Card>
      {renderCards()}

      {/* Modal Edit */}
      <Modal
        centered
        title={`Form Edit ${content.title}`}
        open={isEditModalVisible}
        width={800}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={content}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, errors, values, setFieldValue }) => (
            <>
              <Card style={{ width: "100%" }}>
                <label className="required" style={{ fontWeight: "bold" }}>
                  Body
                </label>
                <ReactQuillNoSSR
                  modules={TOOLBAR_OPTION_QUILL}
                  style={{ marginTop: 16, marginBottom: 55 }}
                  theme="snow"
                  value={values.body}
                  onChange={(value: string) => {
                    if (isEmptyContent(value)) {
                      setFieldValue("body", "");
                      return;
                    }
                    setFieldValue("body", value);
                  }}
                />
                {errors.body && (
                  <div className={"error"}>{errors.body.toString()}</div>
                )}
              </Card>
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingUpdateGeneralContent}
                  disabled={!isValid}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default GeneralContentPage;
