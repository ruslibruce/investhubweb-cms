import { putEvent } from "@/shared/api/mutation/event";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePutDetailEvent = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: putEvent,
    options: {
      
      onError(error: any, variables, context) {},
      onSuccess(data: any, variables, context) {
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: ["eventDetail"] });
          notification.success({
            message: "Event berhasil diupdate",
          });
        }
      },
    },
  });

  const handleUpdateDetailEvent = (params: string, data: {}) => {
    mutationQuery.mutate({ params, data });
  };

  return {
    mutationQuery,
    handleUpdateDetailEvent,
  };
};

export default usePutDetailEvent;
