import useGetEventCategory from "@/features/event/hooks/useGetEventCategory";
import { DASHBOARD_EVENT } from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { checkIsEmptyObject, getStatusColor } from "@/shared/utils/helper";
import {
  ArrowRightOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Modal,
  Row,
  Space,
  Spin,
  Typography,
} from "antd";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useState } from "react";
import useGetDetailEvent from "../hooks/useGetDetailEvent";
import usePutDetailEvent from "../hooks/usePutDetailEvent";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
import NoData from "@/shared/components/NoData";
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
const { Text } = Typography;

const EventDetailPage = () => {
  const { t: translate } = useTranslation();
  const dataUser = storageCheck(USER);
  const navigate = useRouter();
  const { id } = navigate.query;
  const { fetchQuery } = useGetDetailEvent(id as string, {});
  const { data: eventDetail, isLoading } = fetchQuery;
  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const { handleUpdateDetailEvent, mutationQuery: mutationUpdate } =
    usePutDetailEvent();
  const { isPending: isLoadingUpdateEvent, isSuccess: isSuccessUpdateEvent } =
    mutationUpdate;
  const { fetchQuery: fetchCategory } = useGetEventCategory([]);
  const { isLoading: isLoadingCategory, data: dataFetchCategory } =
    fetchCategory;
  const [isNoteModalVisible, setIsNoteModalVisible] = useState(false);
  const [editNoteData, editNoteFormData] = useState({
    note: "",
    id: "",
  });

  const handleNoteCancel = () => {
    setIsNoteModalVisible(false);
  };

  React.useEffect(() => {
    if (isSuccessUpdateEvent) {
      setIsNoteModalVisible(false);
    }
  }, [isSuccessUpdateEvent]);

  const noteModal = (eventDetail: any) => {
    editNoteFormData({
      note: eventDetail.note,
      id: eventDetail.id,
    });
    setIsNoteModalVisible(true);
  };

  const handleSubmitNote = async (values: any) => {
    handleUpdateDetailEvent(values.id, {
      ...values,
      is_active: "rejected",
    });
  };

  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    event.preventDefault();
    console.log("query", JSON.stringify(value, null, 2));
    let category = dataFetchCategory.find(
      (item: any) => item.name == value.category
    );
    handleUpdateDetailEvent(id as string, {
      ...value,
      is_active: params,
      category: category.id,
      publish_time: moment(value.publish_time).format("YYYY-MM-DD"),
      start_time: moment(value.start_time).format("YYYY-MM-DD"),
      end_time: moment(value.end_time).format("YYYY-MM-DD"),
      cover_image: value.cover_image.split("/").pop(),
    });
  };

  if (isLoading) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (`${eventDetail.id}` !== id) {
    return <LoaderSpinGif isFullScreen size="large" />;
  }

  if (!eventDetail) {
    return <NoData />;
  }

  const noteValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    note: yup
      .string()
      .required(translate("NoteRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("NoteLength")} ${max} ${translate("Characters")}`
      ),
  });

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <Text style={{ fontWeight: "bold", fontSize: "24px" }}>Detail Event</Text>
      <Breadcrumb
        separator=">"
        items={[
          {
            title: "Management Portal",
          },
          {
            title: "Event",
            href: `/cms/${DASHBOARD_EVENT}`,
          },
          {
            title: "Detail Event",
          },
        ]}
      />
      <Card loading={eventDetail ? false : true}>
        <Row
          style={{ justifyContent: "space-between", justifyItems: "center" }}
        >
          <Text style={{ fontWeight: "bold", fontSize: 18 }}>Detail Event</Text>
          <Button
            ghost
            disabled
            style={{
              color: "white",
              background: getStatusColor(eventDetail?.is_active),
              fontWeight: "bold",
              borderRadius: 5,
            }}
          >
            {eventDetail?.is_active}
          </Button>
        </Row>
        <Space direction="vertical">
          <Text style={{ fontWeight: "bold" }}>Category</Text>
          <Text style={{ color: "gray", marginBottom: 13 }}>
            {eventDetail?.category}
          </Text>
          <Text style={{ fontWeight: "bold" }}>Start Time</Text>
          <Text style={{ color: "gray", marginBottom: 13 }}>
            {moment(eventDetail?.start_time).format("DD MMM YYYY")}
          </Text>
          <Text style={{ fontWeight: "bold" }}>Title</Text>
          <article
            style={{ color: "gray", marginBottom: 13 }}
            dangerouslySetInnerHTML={{ __html: eventDetail?.title }}
          />
          <Text style={{ fontWeight: "bold" }}>Description</Text>
          <article
            style={{ color: "gray", marginBottom: 13 }}
            dangerouslySetInnerHTML={{ __html: eventDetail?.description }}
          />
          <Text style={{ fontWeight: "bold" }}>Images</Text>
          <Image
            src={eventDetail?.cover_image}
            alt="event-detail-image"
            className="flag-icon"
            width={300}
            height={300}
            style={{ borderRadius: 10 }}
          />
          {eventDetail?.note && eventDetail?.is_active !== "pending" && (
            <>
              <Text style={{ fontWeight: "bold" }}>Note</Text>
              <article
                style={{ color: "gray", marginBottom: 13 }}
                dangerouslySetInnerHTML={{ __html: eventDetail?.note }}
              />
            </>
          )}
          <Flex gap="small" justify="flex-end">
            {eventDetail?.is_active === "draft" && !isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, eventDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {eventDetail?.is_active === "unpublished" && !isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, eventDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {eventDetail?.is_active === "rejected" && !isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("pending"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, eventDetail, "pending")}
              >
                Publish
              </Button>
            )}
            {eventDetail?.is_active === "pending" && isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("published"),
                  color: "white",
                }}
                type="primary"
                iconPosition="end"
                onClick={(e) => handlePublish(e, eventDetail, "published")}
              >
                Publish
              </Button>
            )}
            {eventDetail?.is_active === "pending" && isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("rejected"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                type="primary"
                iconPosition="end"
                onClick={() => noteModal(eventDetail)}
              >
                Rejected
              </Button>
            )}
            {eventDetail?.is_active === "published" && isKurator && (
              <Button
                loading={isLoadingUpdateEvent}
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
                style={{
                  marginTop: 18,
                  backgroundColor: getStatusColor("rejected"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                type="primary"
                iconPosition="end"
                onClick={() => noteModal(eventDetail)}
              >
                Rejected
              </Button>
            )}
          </Flex>
        </Space>
      </Card>

      {/* modal note */}
      <Modal
        centered
        title="Note"
        open={isNoteModalVisible}
        width={800}
        onCancel={handleNoteCancel}
        footer={null}
      >
        <Formik
          validationSchema={noteValidationSchema}
          initialValues={editNoteData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleSubmitNote(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Enter your note here"}
                title={"Notes"}
                type={"text"}
                name={"note"}
                textArea={true}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingUpdateEvent}
                >
                  {translate("Submit")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default EventDetailPage;
