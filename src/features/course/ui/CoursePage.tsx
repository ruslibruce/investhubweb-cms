import { DASHBOARD_COURSE_DETAIL } from "@/shared/constants/path";
import {
  ArrowRightOutlined,
  DeleteOutlined,
  EditTwoTone,
  EyeTwoTone,
  FolderOpenTwoTone,
  PlusOutlined,
  SaveOutlined,
  SendOutlined,
} from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Divider,
  Flex,
  Input,
  Modal,
  Row,
  Select,
  Space,
  Spin,
  Upload,
  message,
} from "antd";
import { useRouter } from "next/router";

import HamburgerCard from "@/components/HamburgerCard";
import NoData from "@/shared/components/NoData";
import PaginationComponent from "@/shared/components/PaginationComponent";
import PaginationResponsive from "@/shared/components/PaginationResponsive";
import { imageLMS } from "@/shared/constants/imageUrl";
import {
  CATEGORY_STATUS,
  IMAGE_FORMAT,
  USER,
} from "@/shared/constants/storageStatis";
import iconCommentCard from "@/shared/images/icon/icon_comment_card.webp";
import iconLikeCard from "@/shared/images/icon/icon_like_card.webp";
import IconReportCard from "@/shared/images/icon/icon_report_card.webp";
import iconShareCard from "@/shared/images/icon/icon_share_card.webp";
import iconStudentCard from "@/shared/images/icon/icon_student_card.webp";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { handlingErrors } from "@/shared/utils/handlingErrors";
import { checkIsEmptyObject, getStatusColor } from "@/shared/utils/helper";
import { paramsToString } from "@/shared/utils/string";
import Image from "next/image";
import React, { useState } from "react";
import "react-quill/dist/quill.snow.css";
import useAddCourse from "../hooks/useAddCourse";
import useGetComment from "../hooks/useGetComment";
import useGetCourse from "../hooks/useGetCourse";
import useGetCourseCategory from "../hooks/useGetCourseCategory";
import useGetCourseContentType from "../hooks/useGetCourseContentType";
import useGetCourseLevel from "../hooks/useGetCourseLevel";
import useGetLike from "../hooks/useGetLike";
import useGetReport from "../hooks/useGetReport";
import usePublishCourse from "../hooks/usePublishCourse";
import LoaderSpinGif from "@/shared/components/LoaderSpinGif";
const { Dragger } = Upload;
import { Field, Formik } from "formik";
import FormInputFormik from "@/components/FormInputFormik";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import { SizeType } from "antd/lib/config-provider/SizeContext";

const initialFormData = {
  course_provider: "1",
  status: "draft",
  title: "",
  description: "",
  cover_image_file: "",
  content_type: "",
  category_id: "",
};
const CoursePage = () => {
  const dataUser = storageCheck(USER);
  const isKurator = dataUser?.user?.role === "kurator";
  const { t: translate } = useTranslation();
  const navigate = useRouter();
  const { page } = navigate.query;
  const [isModalCreateCourse, setIsModalCreateCourse] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [formDataCourse, setFormDataCourse] =
    React.useState<any>(initialFormData);
  const [currentPage, setCurrentPage] = React.useState<number>(
    (page as any) ?? 1
  );
  const [dataFilter, setDataFilter] = React.useState({
    status: "",
    category: "",
    level: "",
    content_type: "",
  });
  const [dataFilterApi, setDataFilterApi] = React.useState({
    status: "",
    category: "",
    level: "",
    content_type: "",
  });
  const { fetchQuery } = useGetCourse(
    {},
    paramsToString({ page: currentPage, ...dataFilterApi })
  );
  const { data: dataResultCourse, isLoading } = fetchQuery;
  const { data: dataCourse } = dataResultCourse;
  const { handleCreateCourse, mutationQuery: mutationCreate } = useAddCourse();
  const { isPending: isLoadingCreate, isSuccess: isSuccessCreate } =
    mutationCreate;
  const { handlePublishCourse, mutationQuery: mutationPublish } =
    usePublishCourse();
  const { isPending: isLoadingPublish, isSuccess: isSuccessPublish } =
    mutationPublish;
  const { fetchQuery: fetchQueryCategory } = useGetCourseCategory([]);
  const { data: dataCategory } = fetchQueryCategory;
  const { fetchContentType } = useGetCourseContentType([]);
  const { data: dataContentType } = fetchContentType;
  const { fetchLevelCourse } = useGetCourseLevel([]);
  const { data: dataLevel } = fetchLevelCourse;
  const { handleGetCommentCourse, mutationQuery: mutationGetComment } =
    useGetComment();
  const { data: dataApiComment, isPending: isLoadingComment } =
    mutationGetComment;
  const dataComment = dataApiComment as any;
  const [isModalVisibleComment, setIsModalVisibleComment] =
    React.useState(false);
  const { handleGetLikeCourse, mutationQuery: mutationGetLike } = useGetLike();
  const { data: dataApiLike, isPending: isLoadingLike } = mutationGetLike;
  const dataLike = dataApiLike as any;
  const [isModalVisibleLike, setIsModalVisibleLike] = React.useState(false);
  const { handleGetReportCourse, mutationQuery: mutationGetReport } =
    useGetReport();
  const { data: dataApiReport, isPending: isLoadingReport } = mutationGetReport;
  const dataReport = dataApiReport as any;
  const [isModalVisibleReport, setIsModalVisibleReport] = React.useState(false);
  // Note Modal
  const [isNoteModalVisible, setIsNoteModalVisible] = useState(false);
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleNoteCancel = () => {
    setIsNoteModalVisible(false);
  };

  console.log("dataCourse", dataCourse);

  React.useEffect(() => {
    if (isSuccessCreate) {
      setIsModalCreateCourse(false);
    }

    if (isSuccessPublish) {
      setIsEditModalVisible(false);
      setIsNoteModalVisible(false);
    }
  }, [isSuccessCreate, isSuccessPublish]);

  const handleChange = (value: string, name: string) => {
    if (value === "all") {
      setDataFilter({ ...dataFilter, [name]: "" });
      return;
    }
    setDataFilter({ ...dataFilter, [name]: value });
  };

  const editModal = (value: any) => {
    console.log("values", value);
    setFormDataCourse({
      id: value.id,
      course_provider: value.course_provider,
      status: value.status,
      title: value.title,
      description: value.description,
      cover_image_file: value.cover_image_file,
      content_type: value.content_type,
      category_id: value.category_id,
    });
    setIsEditModalVisible(true);
  };

  const handleEditCancel = () => {
    setIsEditModalVisible(false);
  };

  const handleCourse = (event: React.MouseEvent<HTMLElement>, value: any) => {
    console.log("value", value);
    navigate.push(`${DASHBOARD_COURSE_DETAIL}/${value.id}/${currentPage}`);
  };

  const handleHiddenCreateCourse = () => {
    setIsModalCreateCourse(false);
  };

  const showModalCreateCourse = () => {
    setFormDataCourse(initialFormData);
    setIsModalCreateCourse(true);
  };

  const handleAddCourse = (values: any) => {
    console.log("formDataCourse", values);
    handleCreateCourse(values);
  };

  const handlePublish = (
    event: React.MouseEvent<HTMLElement>,
    value: any,
    params: string
  ) => {
    console.log("value", value);
    handlePublishCourse(
      {
        ...value,
        status: params,
        notes: isKurator ? "" : formDataCourse.notes,
      },
      value.id,
      params
    );
  };

  const handleUpdateCourse = (values: any) => {
    handlePublishCourse(
      {
        ...values,
        status: "published",
      },
      values.id,
      "update"
    );
  };

  const [fontSize, setFontSize] = useState("initial");
  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 550) {
        setFontSize("10px");
      } else {
        setFontSize("initial");
      }
    };

    handleResize(); // Set initial font size
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const noteModal = (value: any) => {
    setFormDataCourse({
      id: value.id,
      course_provider: value.course_provider,
      status: value.status,
      title: value.title,
      description: value.description,
      cover_image_file: value.cover_image_file,
      content_type: value.content_type,
      category_id: value.category_id,
      notes: "",
    });
    setIsNoteModalVisible(true);
  };

  const handleSubmitNote = async (values: any) => {
    handlePublishCourse(
      {
        ...values,
        status: "rejected",
      },
      values.id,
      "rejected"
    );
  };

  const renderCards = () => {
    if (!dataCourse) return <LoaderSpinGif size="large" />;
    if (dataCourse && dataCourse.length === 0)
      return (
        <Card>
          <NoData />
        </Card>
      );
    return dataCourse?.map((item: any, index: any) => (
      <Card key={index}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <p
              style={{
                color: getStatusColor(item.status),
                fontWeight: "bold",
                textTransform: "capitalize",
                fontSize: fontSize,
              }}
            >
              {item.status}
            </p>
          </div>
          <div>
            <div className="curd-button" style={{ flexDirection: "row" }}>
              <span style={{ marginRight: "10px" }}>
                <EyeTwoTone
                  twoToneColor="#a6a6a6"
                  onClick={(e) => handleCourse(e, item)}
                />
              </span>
              {!isKurator && (
                <span style={{ marginRight: 10 }}>
                  <EditTwoTone
                    twoToneColor="#a6a6a6"
                    onClick={() => editModal(item)}
                  />
                </span>
              )}
            </div>
            <div className="hamburger-curd-button">
              <HamburgerCard
                onView={(e) => handleCourse(e, item)}
                onEdit={() => editModal(item)}
                arrayItems={["0", "1"]}
              />
            </div>
          </div>
        </div>
        <Divider />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <div className="course-content">
            <p style={{ fontWeight: "light", marginBottom: 5 }}>
              {item.category}
            </p>
            <p style={{ fontWeight: "bold" }}>{item.title}</p>
            <p style={{ color: "gray" }}>{item.description}</p>
            {item.status === "rejected" && (
              <Row>
                <Col>
                  <p>Notes</p>
                </Col>
                <Col style={{ marginLeft: 10 }}>
                  <p>{":"}</p>
                </Col>
                <Col style={{ marginLeft: 10 }}>
                  <p>{item.notes}</p>
                </Col>
              </Row>
            )}
            <Row gutter={10} style={{ marginTop: 10 }} wrap>
              <Col style={{ display: "flex", alignItems: "center", gap: 5 }}>
                <Image
                  width={20}
                  height={20}
                  src={iconStudentCard}
                  alt="student-card"
                />
                <span className="text-bottom-card">
                  {item.number_of_participants}
                </span>
              </Col>
              <Col
                onClick={() => {
                  handleGetLikeCourse(item.id);
                  setIsModalVisibleLike(true);
                }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: 5,
                  cursor: "pointer",
                }}
              >
                <Image
                  width={20}
                  height={20}
                  src={iconLikeCard}
                  alt="like-card"
                />
                <span className="text-bottom-card">{item.number_of_likes}</span>
              </Col>
              <Col
                onClick={() => {
                  handleGetCommentCourse(item.id);
                  setIsModalVisibleComment(true);
                }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: 5,
                  cursor: "pointer",
                }}
              >
                <Image
                  width={20}
                  height={20}
                  src={iconCommentCard}
                  alt="coment-card"
                />
                <span className="text-bottom-card">
                  {item.number_of_comments}
                </span>
              </Col>
              <Col
                onClick={() => {
                  handleGetReportCourse(item.id);
                  setIsModalVisibleReport(true);
                }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: 5,
                  cursor: "pointer",
                }}
              >
                <Image
                  width={20}
                  height={20}
                  src={IconReportCard}
                  alt="coment-card"
                />
                <span className="text-bottom-card">
                  {item.number_of_reports}
                </span>
              </Col>
              <div style={{ display: "flex", alignItems: "center", gap: 5 }}>
                <Image
                  width={20}
                  height={20}
                  src={iconShareCard}
                  alt="share-card"
                />
                <span className="text-bottom-card">
                  {item.number_of_shares}
                </span>
              </div>
            </Row>
          </div>
          <div>
            {item.status === "draft" && !isKurator && (
              <Button
                loading={isLoadingPublish}
                size={formSize}
                onClick={(e) => handlePublish(e, item, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: getStatusColor("published"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
            {item.status === "published" && !isKurator && (
              <Button
                loading={isLoadingPublish}
                size={formSize}
                onClick={(e) => handlePublish(e, item, "unpublished")}
                style={{
                  marginTop: 24,
                  backgroundColor: getStatusColor("unpublished"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Unpublish
              </Button>
            )}
            {item.status === "pending" &&
              isKurator &&
              dataUser?.user?.role !== "cp-admin" && (
                <Button
                  loading={isLoadingPublish}
                  size={formSize}
                  onClick={(e) => handlePublish(e, item, "published")}
                  style={{
                    marginTop: 24,
                    backgroundColor: getStatusColor("published"),
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  iconPosition="end"
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                >
                  Publish
                </Button>
              )}
            {item.status === "pending" &&
              isKurator &&
              dataUser?.user?.role !== "cp-admin" && (
                <Button
                  size={formSize}
                  onClick={() => noteModal(item)}
                  style={{
                    marginTop: 24,
                    backgroundColor: getStatusColor("rejected"),
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  iconPosition="end"
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                >
                  Reject
                </Button>
              )}
            {item.status === "published" &&
              isKurator &&
              dataUser?.user?.role !== "cp-admin" && (
                <Button
                  size={formSize}
                  onClick={() => noteModal(item)}
                  style={{
                    marginTop: 24,
                    backgroundColor: getStatusColor("rejected"),
                    color: "white",
                    display: "flex",
                    alignItems: "center",
                  }}
                  iconPosition="end"
                  icon={
                    <ArrowRightOutlined
                      style={{ color: "white", height: 15, width: 15 }}
                    />
                  }
                >
                  Reject
                </Button>
              )}
            {item.status === "unpublished" && !isKurator && (
              <Button
                loading={isLoadingPublish}
                size={formSize}
                onClick={(e) => handlePublish(e, item, "published")}
                style={{
                  marginTop: 24,
                  backgroundColor: getStatusColor("published"),
                  color: "white",
                  display: "flex",
                  alignItems: "center",
                }}
                iconPosition="end"
                icon={
                  <ArrowRightOutlined
                    style={{ color: "white", height: 15, width: 15 }}
                  />
                }
              >
                Publish
              </Button>
            )}
          </div>
        </div>
      </Card>
    ));
  };

  const handleSubmitSearch = () => {
    setCurrentPage(1);
    setDataFilterApi(dataFilter);
  };

  const handleCloseModalComment = () => {
    setIsModalVisibleComment(false);
  };

  const handleCloseModalReport = () => {
    setIsModalVisibleReport(false);
  };

  const handleCloseModalLike = () => {
    setIsModalVisibleLike(false);
  };

  const addValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    title: yup
      .string()
      .required(translate("TitleRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("TitleLength")} ${max} ${translate("Characters")}`
      ),
    description: yup.string().required(translate("DescriptionRequired")),
    cover_image_file: yup.string().required(translate("CoverPictureRequired")),
    content_type: yup.string().required(translate("ContentTypeRequired")),
    category_id: yup.string().required(translate("CategoryRequired")),
  });

  const noteValidationSchema: yup.ObjectSchema<any> = yup.object().shape({
    notes: yup
      .string()
      .required(translate("NoteRequired"))
      .max(
        100,
        ({ max }) =>
          `${translate("NoteLength")} ${max} ${translate("Characters")}`
      ),
  });

  const handleUploadChange = (info: any, setFieldValue: any, name: string) => {
    const { status, response } = info;
    if (status !== "uploading") {
      console.log("info.file", info.file);
      console.log("info.fileList", info.fileList);
      const dataFile = info.file;

      if (dataFile.response?.status === "success") {
        setFieldValue(name, dataFile.response.data.filename);
      }
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      console.log(
        `${info.file.name} file upload failed because of ${info.file.error}`
      );
      if (response && response?.message == "Unauthorized") {
        handlingErrors({
          error: response,
          text: "Upload Image",
        });
      }
    }
  };

  return (
    <Space direction="vertical" style={{ display: "flex" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
        }}
      >
        <div className="indicator">
          <p style={{ fontWeight: "bold", fontSize: "24px" }}>Course</p>
          <Breadcrumb
            separator=">"
            items={[
              {
                title: "Management Portal",
              },
              {
                title: "Course",
              },
            ]}
          />
        </div>
        {!isKurator && (
          <div className="button-post-category">
            <Button
              size={formSize}
              type="primary"
              onClick={showModalCreateCourse}
              style={{ backgroundColor: "#CB0B0C" }}
            >
              <PlusOutlined />
              Create Course
            </Button>
          </div>
        )}
      </div>
      <div className="category-content">
        <div>
          <Card className="category-search">
            <p style={{ fontWeight: "bold" }}>Search Course</p>
            <p style={{ marginTop: 18 }}>Status</p>
            <Select
              size={formSize}
              defaultValue="All"
              style={{ width: "100%", marginTop: 10 }}
              onChange={(value: string) => handleChange(value, "status")}
              options={CATEGORY_STATUS}
            />
            {/* <p style={{ marginTop: "18px" }}>Title</p>
            <Input
              size={formSize}
              placeholder="Title"
            /> */}
            <p style={{ marginTop: 18 }}>Category</p>
            <Select
              size={formSize}
              defaultValue="Select Category"
              style={{ width: "100%", marginTop: 10 }}
              onChange={(value: string) => handleChange(value, "category")}
              options={[
                {
                  key: "all",
                  label: "All",
                  value: "all",
                },
                ...dataCategory,
              ]}
            />
            <p style={{ marginTop: 18 }}>Content Type</p>
            <Select
              size={formSize}
              defaultValue="Select Content"
              style={{ width: "100%", marginTop: 10 }}
              onChange={(value: string) => handleChange(value, "content_type")}
              options={[
                {
                  key: "all",
                  label: "All",
                  value: "all",
                },
                ...dataContentType,
              ]}
            />
            <p style={{ marginTop: 18 }}>Level Course</p>
            <Select
              size={formSize}
              defaultValue="Select Level"
              style={{ width: "100%", marginTop: 10 }}
              onChange={(value: string) => handleChange(value, "level")}
              options={[
                {
                  key: "all",
                  label: "All",
                  value: "all",
                },
                ...dataLevel,
              ]}
            />
            <Button
              size={formSize}
              type="primary"
              loading={isLoading}
              style={{
                backgroundColor: "#CB0B0C",
                marginTop: 25,
                width: "100%",
              }}
              onClick={handleSubmitSearch}
            >
              Search
            </Button>
          </Card>
        </div>
        <div className="category-table">
          {renderCards()}
          {dataCourse && dataCourse.length > 0 && (
            <Card
              style={{
                width: "100%",
                height: 70,
                marginTop: "12px",
              }}
            >
              <Row>
                <Col>
                  <PaginationComponent
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultCourse?.records}
                    limit={dataResultCourse?.limit}
                  />
                  <PaginationResponsive
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    total={dataResultCourse?.records}
                    limit={dataResultCourse?.limit}
                  />
                </Col>
              </Row>
            </Card>
          )}
        </div>
      </div>

      {/* Modal Create */}
      <Modal
        centered
        title="Form Add Course"
        open={isModalCreateCourse}
        width={1200}
        onCancel={handleHiddenCreateCourse}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={initialFormData}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleAddCourse(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid, values, setFieldValue, errors }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={10}
              >
                <Col style={{width: 300}}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Title"}
                    title={"Title"}
                    type={"text"}
                    name={"title"}
                  />
                </Col>
                <Col
                  style={{
                    marginBottom: 18,
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    style={{ width: 290, marginTop: 10 }}
                    onChange={(value: string) => {
                      setFieldValue("category_id", value);
                    }}
                    options={dataCategory}
                    value={values.category_id}
                    size={formSize}
                  />
                  {errors.category_id && (
                    <div className={"error"}>
                      {errors.category_id.toString()}
                    </div>
                  )}
                </Col>
                <Col
                  style={{
                    marginBottom: 18,
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Content Type</label>
                  <Select
                    placeholder="Select Content Type"
                    style={{ width: 290, marginTop: 10 }}
                    onChange={(value: string) =>
                      setFieldValue("content_type", value)
                    }
                    options={dataContentType}
                    value={values.content_type}
                    size={formSize}
                  />
                  {errors.content_type && (
                    <div className={"error"}>
                      {errors.content_type.toString()}
                    </div>
                  )}
                </Col>
              </Row>
              <Row>
                <Col style={{width: 290}}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Description"}
                    title={"Description"}
                    type={"text"}
                    name={"description"}
                    textArea={true}
                  />
                </Col>
              </Row>
              <Col style={{ display: "flex", flexDirection: "column" }}>
                <label className="required" style={{marginBottom: 10}}>
                  {`Upload Course Image Cover (Max. 2MB) Image Only`}
                </label>
                {values.cover_image_file ? (
                  <Space
                    style={{ position: "relative"}}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={200}
                      height={200}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={`${imageLMS}/${values.cover_image_file}`}
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image_file", "");
                      }}
                    />
                  </Space>
                ) : (
                  <Dragger
                    {...{
                      name: "file",
                      multiple: true,
                      maxCount: 1,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(
                          info,
                          setFieldValue,
                          "cover_image_file"
                        ),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                )}
              </Col>
              {errors.cover_image_file && (
                <div className={"error"}>
                  {errors.cover_image_file.toString()}
                </div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingCreate}
                  disabled={!isValid}
                >
                  {translate("Save")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* modal edit */}
      <Modal
        centered
        title="Form Update Course"
        open={isEditModalVisible}
        width={1200}
        onCancel={handleEditCancel}
        footer={null}
      >
        <Formik
          validationSchema={addValidationSchema}
          initialValues={formDataCourse}
          enableReinitialize
          onSubmit={(values) => {
            handleUpdateCourse(values);
          }}
        >
          {({ handleSubmit, isValid, values, setFieldValue, errors }) => (
            <>
              <Row
                style={{
                  marginTop: 32,
                  alignItems: "center",
                }}
                gutter={10}
                wrap
              >
                <Col style={{width: 300}}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Title"}
                    title={"Title"}
                    type={"text"}
                    name={"title"}
                  />
                </Col>
                <Col
                  style={{
                    marginBottom: 18,
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Category</label>
                  <Select
                    placeholder="Select Category"
                    style={{ width: 290, marginTop: 10 }}
                    onChange={(value: string) => {
                      setFieldValue("category_id", value);
                    }}
                    options={dataCategory}
                    value={values.category_id}
                    size={formSize}
                  />
                  {errors.category_id && (
                    <div className={"error"}>
                      {errors.category_id.toString()}
                    </div>
                  )}
                </Col>
                <Col
                  style={{
                    marginBottom: 18,
                    flexDirection: "column",
                    display: "flex",
                  }}
                >
                  <label className="required">Content Type</label>
                  <Select
                    placeholder="Select Content Type"
                    style={{ width: 290, marginTop: 10 }}
                    onChange={(value: string) =>
                      setFieldValue("content_type", value)
                    }
                    options={dataContentType}
                    value={values.content_type}
                    size={formSize}
                  />
                  {errors.content_type && (
                    <div className={"error"}>
                      {errors.content_type.toString()}
                    </div>
                  )}
                </Col>
              </Row>
              <Row>
                <Col style={{width: 290}}>
                  <Field
                    component={FormInputFormik}
                    placeholder={"Input Description"}
                    title={"Description"}
                    type={"text"}
                    name={"description"}
                    textArea={true}
                  />
                </Col>
              </Row>
              <Col style={{ display: "flex", flexDirection: "column" }}>
                <label className="required" style={{ marginBottom: 10 }}>
                  Upload Course Image Cover (Max. 2MB)
                </label>
                {values.cover_image_file ? (
                  <Space
                    style={{ position: "relative" }}
                    direction="horizontal"
                  >
                    <Image
                      alt="image edit"
                      width={100}
                      height={100}
                      style={{ width: 400, height: 200, borderRadius: 10 }}
                      src={
                        values.cover_image_file.includes("https")
                          ? values.cover_image_file
                          : `${imageLMS}/${values.cover_image_file}`
                      }
                    />
                    <DeleteOutlined
                      style={{
                        fontSize: 20,
                        color: "red",
                        position: "absolute",
                        top: 10,
                        right: 10,
                        backgroundColor: "white",
                        padding: 5,
                        borderRadius: 100,
                      }}
                      type="primary"
                      onClick={() => {
                        setFieldValue("cover_image_file", "");
                      }}
                    />
                  </Space>
                ) : (
                  <Dragger
                    {...{
                      name: "file",
                      multiple: true,
                      maxCount: 1,
                      action: `${process.env.NEXT_PUBLIC_HOST_NAME}/api-lms/course/upload-file`,
                      headers: {
                        authorization: `Bearer ${dataUser.token}`,
                      },
                      accept: IMAGE_FORMAT,
                      beforeUpload: (file) => {
                        console.log("file beforeUpload", file);
                        const isSize = file.size / 1024 / 1024 < 2;

                        if (!isSize) {
                          message.error(
                            `${file.name} size must be less than 2 MB`
                          );
                        }

                        return isSize || Upload.LIST_IGNORE;
                      },
                      onChange: (info) =>
                        handleUploadChange(
                          info,
                          setFieldValue,
                          "cover_image_file"
                        ),
                    }}
                  >
                    <p className="ant-upload-drag-icon">
                      <FolderOpenTwoTone twoToneColor="#737373" />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint"></p>
                  </Dragger>
                )}
              </Col>
              {errors.cover_image_file && (
                <div className={"error"}>
                  {errors.cover_image_file.toString()}
                </div>
              )}
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  loading={isLoadingPublish}
                  disabled={!isValid}
                >
                  {translate("Update")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>

      {/* Modal Comment */}
      <Modal
        centered
        title="Information Comments"
        open={isModalVisibleComment}
        width={1100}
        onCancel={handleCloseModalComment}
        footer={[
          <Button
            style={{
              backgroundColor: "#CB0B0C",
              color: "white",
            }}
            type="primary"
            onClick={handleCloseModalComment}
          >
            OK
          </Button>,
        ]}
      >
        <Card loading={isLoadingComment}>
          {dataComment?.length === 0 && <NoData />}
          {dataComment?.map((item: any) => (
            <Col style={{ marginTop: 10 }}>
              <p style={{ fontWeight: "bold", fontSize: 14 }}>{item.name}</p>
              <p style={{ fontSize: 14 }}>{item.comment}</p>
            </Col>
          ))}
        </Card>
      </Modal>

      {/* Modal Report */}
      <Modal
        centered
        title="Information Reports"
        open={isModalVisibleReport}
        width={1100}
        onCancel={handleCloseModalReport}
        footer={[
          <Button
            style={{
              backgroundColor: "#CB0B0C",
              color: "white",
            }}
            type="primary"
            onClick={handleCloseModalReport}
          >
            OK
          </Button>,
        ]}
      >
        <Card loading={isLoadingReport}>
          {dataReport?.length === 0 && <NoData />}
          {dataReport?.map((item: any) => (
            <Col style={{ marginTop: 10 }}>
              <p style={{ fontWeight: "bold", fontSize: 14 }}>{item.name}</p>
              <p style={{ fontSize: 14 }}>{item.report}</p>
            </Col>
          ))}
        </Card>
      </Modal>

      {/* Modal Like */}
      <Modal
        centered
        title="Information Likes"
        open={isModalVisibleLike}
        width={1100}
        onCancel={handleCloseModalLike}
        footer={[
          <Button
            style={{
              backgroundColor: "#CB0B0C",
              color: "white",
            }}
            type="primary"
            onClick={handleCloseModalLike}
          >
            OK
          </Button>,
        ]}
      >
        <Card loading={isLoadingLike}>
          {dataLike?.length === 0 && <NoData />}
          {dataLike?.map((item: any, index: any) => (
            <Col style={{ marginTop: 10, display: "flex" }}>
              <p style={{ fontSize: 14 }}>{`${index + 1}. `}</p>
              <p style={{ fontWeight: "bold", fontSize: 14 }}>{item.name}</p>
            </Col>
          ))}
        </Card>
      </Modal>

      {/* modal note */}
      <Modal
        centered
        title="Note"
        open={isNoteModalVisible}
        width={800}
        onCancel={handleNoteCancel}
        footer={null}
      >
        <Formik
          validationSchema={noteValidationSchema}
          initialValues={formDataCourse}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log("values", values);
            handleSubmitNote(values);
            resetForm();
          }}
        >
          {({ handleSubmit, isValid }) => (
            <>
              <Field
                component={FormInputFormik}
                placeholder={"Enter your note here"}
                title={"Notes"}
                type={"text"}
                name={"notes"}
                textArea={true}
              />
              <Flex justify="flex-end">
                <Button
                  icon={<SendOutlined />}
                  style={{
                    marginTop: 18,
                    backgroundColor: !isValid ? "#a6a6a6" : "#CB0B0C",
                    color: "white",
                  }}
                  type="primary"
                  onClick={(event: any) => handleSubmit(event)}
                  disabled={!isValid}
                  loading={isLoadingPublish}
                >
                  {translate("Create")}
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      </Modal>
    </Space>
  );
};

export default CoursePage;
