import { getCommentCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useGetComment = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: getCommentCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        // queryClient.invalidateQueries({ queryKey: ["course_certificate"] });
        if (data) {
        }
      },
    },
  });

  const handleGetCommentCourse = (id: string) => {
    mutationQuery.mutate(id);
  };

  return {
    mutationQuery,
    handleGetCommentCourse,
  };
};

export default useGetComment;
