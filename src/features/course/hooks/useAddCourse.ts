import { postCreateCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const useAddCourse = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postCreateCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["course"]] });
          notification.success({
            message: "Course berhasil dibuat",
          });
        }
      },
    },
  });

  const handleCreateCourse = (object: {}) => {
    mutationQuery.mutate(object);
  };

  return {
    mutationQuery,
    handleCreateCourse,
  };
};

export default useAddCourse;
