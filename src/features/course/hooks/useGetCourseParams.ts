import { fetchAllCourses } from "@/shared/api/fetch/course";
import useFetchHook from "@/shared/hooks/useFetchHook";
import { DehydratedState, QueryClient, dehydrate } from "@tanstack/react-query";

/**
 *
 * @param queryClient
 * @param params // the optional params
 */
export const prefetchListQuery = async (
  queryClient: QueryClient,
  // eslint-disable-next-line no-unused-vars
  params?: any
): Promise<DehydratedState> => {
  const fetchData = fetchAllCourses();

  await queryClient.prefetchQuery({
    queryKey: fetchData.key,
    queryFn: fetchData.api,
  });

  return dehydrate(queryClient);
};

/**
 * the optional initial data used for SSR
 * @param initialData
 */
const useGetCourseParams = (
  params?: string,
  initialData?: any,
  page?: number
) => {
  const fetchDataColors = fetchAllCourses(params);

  const fetchQuery = useFetchHook({
    keys: [fetchDataColors.key, page],
    api: fetchDataColors.api,
    initialData,
    options: {},
  });

  return {
    fetchQuery,
  };
};

export default useGetCourseParams;
