import { postPublishCourse } from "@/shared/api/fetch/course";
import useMutationHook from "@/shared/hooks/useMutationHook";
import { useQueryClient } from "@tanstack/react-query";
import { notification } from "antd";

const usePublishCourse = () => {
  const queryClient = useQueryClient();

  const mutationQuery = useMutationHook({
    api: postPublishCourse,
    options: {
      // eslint-disable-next-line no-unused-vars
      onError(error: any, variables, context) {},

      // eslint-disable-next-line no-unused-vars
      onSuccess(data: any, variables: any, context) {
        // Create Token cookie
        console.log("data", data);
        console.log("variables", variables);
        console.log("context", context);
        if (data) {
          queryClient.invalidateQueries({ queryKey: [["course"]] });
          notification.success({
            message:
              variables.type === "update"
                ? "Course berhasil diupdate"
                : variables.type === "unpublished"
                ? "Course berhasil diunpublish"
                : variables.type === "rejected"
                ? "Course berhasil direject"
                : "Course berhasil dipublish",
          });
        }
      },
    },
  });

  const handlePublishCourse = (data: {}, id: string, type?: string) => {
    mutationQuery.mutate({ data, id, type });
  };

  return {
    mutationQuery,
    handlePublishCourse,
  };
};

export default usePublishCourse;
