import { Drawer, DrawerBody, DrawerContent, InputGroup, InputLeftElement, Input, Button } from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react";
import React, { useState } from 'react';
import { SearchIcon } from "@chakra-ui/icons";

function ResponsiveSearch() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const placement = "top";
  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 550);

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    window.addEventListener('resize', handleResize);

    // Cleanup function to remove event listener
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <>
      <SearchIcon cursor="pointer" w={isSmallScreen ? '15px' : '25px'} h={isSmallScreen ? '15px' : '25px'} color="#999" aria-label="Options" onClick={onOpen} />
      <Drawer placement={placement} onClose={onClose} isOpen={isOpen}>
        <DrawerContent>
          <DrawerBody>
            <div className="responsive-search-container">
              <div className="search-component-container">
                <div className="search-responsive-component">
                  <InputGroup>
                    <InputLeftElement pointerEvents="none" pt="8px" pl="3px">
                      <SearchIcon color="#999" />
                    </InputLeftElement>
                    <Input
                      placeholder="Search Here"
                      size="large"
                      _focus={{
                        boxShadow: "none",
                        outline: "none",
                      }}
                    />
                  </InputGroup>
                </div>
                <div className="search-responsive-close">
                  <Button colorScheme="teal" onClick={onClose}>
                    Batalkan
                  </Button>
                </div>
              </div>
            </div>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default ResponsiveSearch;
