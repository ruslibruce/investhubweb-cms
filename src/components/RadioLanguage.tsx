import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";

const optionsWithDisabled = [
  { label: "EN", value: "en" },
  { label: "ID", value: "id" },
];

const RadioGroupPage: React.FC = () => {
  const [value, setValue] = useState("EN");
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const navigate = useRouter();
  const { asPath } = navigate;

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    // Pertama kali, panggil handleResize untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // Dependensi kosong agar efek hanya berjalan sekali saat komponen dipasang

  return (
    <div className="radio-language">
      {optionsWithDisabled.map((option, index) => {
        const buttonStyle = {
          padding: "8px 9px",
          backgroundColor: value === option.value ? "#e8e8e8" : "#fff",
          color: value === option.value ? "#9F0E0F" : "#000",
          border: "1px solid #e8e8e8",
          borderRadius: index === 0 ? "4px 0 0 4px" : "0 4px 4px 0",
          cursor: "pointer",
          ...(isSmallScreen && {
            padding: "2px 2px",
            // Tambahkan penyesuaian gaya khusus untuk layar kecil di sini
          }),
        };

        return (
          <React.Fragment key={option.value}>
            <Link href={asPath} locale={option.value}>
              <button style={buttonStyle}>{option.label}</button>
            </Link>
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default RadioGroupPage;
