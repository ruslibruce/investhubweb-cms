import { CustomFormItem } from "@/shared/components/Form/CustomForm";
import { Input } from "antd";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import React, { useState } from "react";
import { CSSProperties } from "styled-components";

type PropsForm = {
  styleForm: CSSProperties;
  title: string;
  type: string;
  placeholder: string;
  input?: [key: string];
  onChangeArea?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  textArea?: boolean;
  readOnly?: boolean;
  field: {
    name: any;
    onBlur: any;
    onChange: () => void;
    value: string | number;
  };
  form: {
    errors: string;
    touched: any;
    setFieldTouched: any;
  };
  isRequired: boolean;
};

function FormInputFormik(props: PropsForm) {
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    styleForm,
    title,
    type,
    placeholder,
    input,
    textArea,
    readOnly,
    isRequired = true,
  } = props;
  const [visible, setVisible] = React.useState(true);
  const hasError = errors[name] && touched[name];
  const [formSize, setFormSize] = React.useState<SizeType>("large");

  React.useEffect(() => {
    const handleResize = () => {
      const screenWidth = window.innerWidth;
      if (screenWidth >= 1200) {
        setFormSize("middle");
      } else {
        setFormSize("large"); // Default size if needed
      }
    };

    window.addEventListener("resize", handleResize);

    // Call the function initially to set the correct form size on load
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  let inputElement;
  if (type === "password") {
    inputElement = (
      <>
        <div style={{ display: "flex", alignItems: "center", marginTop: 10 }}>
          <Input
            type={visible ? type : "text"}
            name={name}
            value={value}
            placeholder={placeholder}
            {...input}
            onChange={onChange}
            readOnly={readOnly}
            onBlur={() => {
              onBlur(name);
              setFieldTouched(name);
            }}
            size={formSize}
            className="input-formik"
          />
          <button
            onClick={() => setVisible(!visible)}
            className={"text-show"}
            style={{ cursor: "pointer" }}
            tabIndex={0}
          >
            {"Show"}
          </button>
        </div>
      </>
    );
  } else if (textArea) {
    inputElement = (
      <Input.TextArea
        name={name}
        value={value}
        placeholder={placeholder}
        {...input}
        onChange={onChange}
        readOnly={readOnly}
        onBlur={() => {
          onBlur(name);
          setFieldTouched(name);
        }}
        rows={4}
        size={formSize}
        style={{ marginTop: 10 }}
      />
    );
  } else {
    inputElement = (
      <Input
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        {...input}
        onChange={onChange}
        readOnly={readOnly}
        onBlur={() => {
          onBlur(name);
          setFieldTouched(name);
        }}
        size={formSize}
        style={{ marginTop: 10 }}
      />
    );
  }

  return (
    <CustomFormItem style={styleForm}>
      <label className={isRequired ? "required" : ""}>{title}</label>
      {inputElement}
      {hasError && <div className={"error"}>{errors[name]}</div>}
    </CustomFormItem>
  );
}

export default FormInputFormik;
