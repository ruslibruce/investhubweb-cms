// components/PdfViewer.jsx
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Viewer, Worker } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import { Col } from "antd";

type PdfViewerProps = {
  url: string;
};
const PDFViewer = ({ url }: PdfViewerProps) => {
  const dataUser = storageCheck(USER);
  console.log("url PDF", url);

  return (
    <Col
      span={24}
      style={{
        border: "1px solid rgba(0, 0, 0, 0.3)",
        height: "750px",
      }}
    >
      <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.4.120/build/pdf.worker.min.js">
        <Viewer
          fileUrl={url}
          httpHeaders={{
            Authorization: `Bearer ${dataUser?.token}`,
          }}
          withCredentials={true}
        />
      </Worker>
    </Col>
  );
};
export default PDFViewer;
