import React from "react";
import type { MenuProps } from "antd";
import { Dropdown, Space } from "antd";
import { TbDotsVertical } from "react-icons/tb";
import { EditTwoTone, EyeTwoTone, DeleteTwoTone } from "@ant-design/icons";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { USER } from "@/shared/constants/storageStatis";

type HamburgerMenuProps = {
  onEdit?: (e: any) => void;
  onDelete?: (e: any) => void;
  onView?: (e: any) => void;
  isView?: boolean;
  arrayItems?: string[];
};

const HamburgerCard: React.FC<HamburgerMenuProps> = ({
  onEdit,
  onDelete,
  onView,
  isView = true,
  arrayItems = ["0"],
}) => {
  const dataUser = storageCheck(USER);
  const isKurator = dataUser?.user?.role === "kurator";

  const data = [
    {
      label: (
        <span onClick={onView}>
          <EyeTwoTone twoToneColor="#a6a6a6" />
          View
        </span>
      ),
      key: "0",
    },
    {
      label: (
        <span onClick={onEdit}>
          <EditTwoTone twoToneColor="#a6a6a6" />
          Edit
        </span>
      ),
      key: "1",
    },
    {
      label: (
        <span onClick={onDelete}>
          <DeleteTwoTone twoToneColor="#a6a6a6" />
          Delete
        </span>
      ),
      key: "2",
    },
  ];

  const filterData = data.filter((item) => arrayItems.includes(item.key));

  const itemsKurator: MenuProps["items"] = [
    {
      label: (
        <span onClick={onView}>
          <EyeTwoTone twoToneColor="#a6a6a6" />
          View
        </span>
      ),
      key: "0",
    },
  ];

  return (
    <Dropdown
      menu={{
        items: isKurator && isView ? itemsKurator : filterData,
      }}
      trigger={["click"]}
    >
      <a onClick={(e) => e.preventDefault()}>
        <Space>
          <TbDotsVertical />
        </Space>
      </a>
    </Dropdown>
  );
};

export default HamburgerCard;
