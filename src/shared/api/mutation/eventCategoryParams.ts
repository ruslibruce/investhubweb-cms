import { EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";
import { paramsToString } from "@/shared/utils/string";

const getCategoryEvent = async (params: any) => {
  const dataUser = storageCheck(USER);
  console.log("params", params);

  const res = await getData({
    endpoint: EVENT(
      paramsToString({
        category: params.value
      })
    ),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`
      }
    }
  });
  return res?.data.data;
};

export { getCategoryEvent };
