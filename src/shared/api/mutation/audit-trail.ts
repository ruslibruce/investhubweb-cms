import {
  AUDIT_TRAIL_LOG,
  EXPORT_AUDIT_TRAIL_LOG,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchAuditTrail(params?: string): Props {
  const KEY = ["audit_trail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataAudit = await getData({
      endpoint: AUDIT_TRAIL_LOG(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataAudit?.data;
  };

  return { key: KEY, api: API };
}

const getExportAuditTrail = async (params: string) => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: EXPORT_AUDIT_TRAIL_LOG(params),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

export { fetchAuditTrail, getExportAuditTrail };
