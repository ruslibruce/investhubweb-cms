import {
  PROFILE,
  UPDATE_USERS,
  USER_API
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { deleteData, getData, postData } from "@/shared/utils/http/axiosHelper";

const deleteUser = async (id: string) => {
  const dataUser = storageCheck(USER);
  const res = await deleteData({
    endpoint: USER_API(id),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

const updateProfile = async (data: {}) => {
  const dataUser = storageCheck(USER);

  const dataUserUpdate = await postData({
    endpoint: UPDATE_USERS(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataUserUpdate?.data;
};



const getProfileInfoMutation = async (data: any) => {
  const dataProfile = await getData({
    endpoint: PROFILE(),
    config: {
      headers: {
        Authorization: `Bearer ${data.token}`,
      },
    },
  });

  return dataProfile?.data.data;
};

export { deleteUser, updateProfile, getProfileInfoMutation };
