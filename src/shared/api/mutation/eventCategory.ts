import { CATEGORY_EVENT, EDIT_CATEGORY_EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { postData, deleteData, putData } from "@/shared/utils/http/axiosHelper";


const addEventCategory = async (data: any) => {
    try {
      const dataUser = storageCheck(USER);
      const res = await postData({
        endpoint: CATEGORY_EVENT(),
        data: data,
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
      return res?.data;
    } catch (error: any) {
      const errorMessage = error.response?.data?.message || "Failed to add event category. Please try again later.";
      throw new Error(errorMessage);
    }
  };

const deleteEventCategory = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: EDIT_CATEGORY_EVENT(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete event Category. Please try again later.");
  }
};

async function editEventCategory({ params, data }: { params: string; data: any }) {
  const dataUser = storageCheck(USER);

  const dataEditEventCategory = await putData({
    endpoint: EDIT_CATEGORY_EVENT(`/id/${params}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataEditEventCategory?.data;
}

export { addEventCategory, deleteEventCategory, editEventCategory };