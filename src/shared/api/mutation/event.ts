import { EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData, putData, deleteData } from "@/shared/utils/http/axiosHelper";

const addEvent = async (data: any) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await postData({
      endpoint: EVENT(),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      }, 
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Add News. Please try again later.");
  }
}

const getEvent = async (data: any) => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: EVENT(),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`
      }
    }
  });
  return res?.data;
};

const getEventDetail = async ({ id }: { id: string }) => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: EVENT(`/id/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`
      }
    }
  });
  return res?.data;
};

const deleteEvent = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: EVENT(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete Event. Please try again later.");
  }
};

async function putEvent({ params, data }: { params: string; data: {} }) {
  const dataUser = storageCheck(USER);

  const dataPutEvent = await putData({
    endpoint: EVENT(`/id/${params}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPutEvent?.data.data;
}

export { getEvent, getEventDetail, addEvent, deleteEvent, putEvent };
