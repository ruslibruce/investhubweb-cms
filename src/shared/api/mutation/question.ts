import { QUESTION, TERMS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { postData, putData } from "@/shared/utils/http/axiosHelper";

const postQuestionBank = async (data: {}) => {
  const dataUser = storageCheck(USER);
  console.log("token====>", dataUser.token);

  const res = await postData({
    endpoint: QUESTION(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

const putQuestionBank = async ({
  id,
  data,
}: {
  id: string;
  data: {};
}) => {
  const dataUser = storageCheck(USER);
  console.log("token====>", dataUser.token);

  const res = await putData({
    endpoint: QUESTION(`/${id}`),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

export { postQuestionBank, putQuestionBank };
