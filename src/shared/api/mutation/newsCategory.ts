import { CATEGORY_NEWS, DELETE_CATEGORY_NEWS, EDIT_CATEGORY_NEWS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { postData, deleteData, putData } from "@/shared/utils/http/axiosHelper";

const addNewsCategory = async (data: any) => {
    try {
      const dataUser = storageCheck(USER);
      const res = await postData({
        endpoint: CATEGORY_NEWS(),
        data: data,
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
      return res?.data;
    } catch (error: any) {
      const errorMessage = error.response?.data?.message || "Failed to add news category. Please try again later.";
      throw new Error(errorMessage);
    }
  };

const deleteNewsCategory = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: DELETE_CATEGORY_NEWS(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete News Category. Please try again later.");
  }
};

async function editNewsCategory({ params, data }: { params: string; data: any }) {
  const dataUser = storageCheck(USER);

  const dataEditNewsCategory = await putData({
    endpoint: EDIT_CATEGORY_NEWS(`/id/${params}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataEditNewsCategory?.data;
}

export { addNewsCategory, deleteNewsCategory, editNewsCategory };
