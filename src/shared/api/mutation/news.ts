import { NEWS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "@/shared/utils/http/axiosHelper";
import { paramsToString } from "@/shared/utils/string";

const addNews = async (data: any) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await postData({
      endpoint: NEWS(),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Add News. Please try again later.");
  }
};

const getNews = async (data: any) => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: NEWS(),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

const getNewsDetail = async ({ id }: { id: string }) => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: NEWS(`/id/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

const deleteNews = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: NEWS(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete News. Please try again later.");
  }
};

async function putNews({ params, data }: { params: string; data: {} }) {
  const dataUser = storageCheck(USER);

  const datagetPage = await putData({
    endpoint: NEWS(`/id/${params}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return datagetPage?.data.data;
}

const getCategoryNews = async (params: any) => {
  const dataUser = storageCheck(USER);
  console.log("params", params);

  const res = await getData({
    endpoint: NEWS(
      paramsToString({
        category: params.value,
      })
    ),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

export {
  addNews,
  deleteNews,
  getNews,
  getNewsDetail,
  putNews,
  getCategoryNews,
};
