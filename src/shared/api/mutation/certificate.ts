import { CERTIFICATE } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { putData } from "@/shared/utils/http/axiosHelper";

async function publishCertificate({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);

  const dataPostUpdate = await putData({
    endpoint: CERTIFICATE(`/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpdate?.data;
}

  export { publishCertificate };
