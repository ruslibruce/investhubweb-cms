import {
  CHANGE_PASSWORD
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  postData
} from "@/shared/utils/http/axiosHelper";

const changePass = async (data: any) => {
  const dataUser = storageCheck(USER);
  const res = await postData({
    endpoint: CHANGE_PASSWORD(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

export { changePass };
