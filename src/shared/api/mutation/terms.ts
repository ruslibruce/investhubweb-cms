import {
  TERMS
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { postData } from "@/shared/utils/http/axiosHelper";

const postTerms = async (data: {}) => {
  const dataUser = storageCheck(USER);
  console.log("token====>", dataUser.token);
  
  const res = await postData({
    endpoint: TERMS(),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

export {
  postTerms
};
