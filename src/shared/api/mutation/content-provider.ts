import {
  CONTENT_PROVIDER,
  CONTENT_PROVIDER_INVITE,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { deleteData, postData, putData } from "@/shared/utils/http/axiosHelper";

const addContentProvider = async (data: any) => {
  const datadataUser = storageCheck(USER);
  const res = await postData({
    endpoint: CONTENT_PROVIDER(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${datadataUser.token}`,
      },
    },
  });
  return res?.data;
};

const inviteUser = async (data: any) => {
  const datadataUser = storageCheck(USER);
  const res = await postData({
    endpoint: CONTENT_PROVIDER_INVITE(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${datadataUser.token}`,
      },
    },
  });
  return res?.data;
};

const updateContentProvider = async ({
  data,
  id,
}: {
  data: any;
  id: string;
}) => {
  const dataUser = storageCheck(USER);
  const res = await putData({
    endpoint: CONTENT_PROVIDER(`/${id}`),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

const deleteContentProvider = async (id: string) => {
  const dataUser = storageCheck(USER);
  const res = await deleteData({
    endpoint: CONTENT_PROVIDER(`/id/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

export {
  addContentProvider,
  deleteContentProvider,
  inviteUser,
  updateContentProvider,
};
