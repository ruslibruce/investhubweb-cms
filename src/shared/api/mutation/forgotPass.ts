import {
  FORGOT_PASSWORD,
  RESET_PASSWORD,
  VALIDATE_RESET_TOKEN
} from "@/shared/constants/endpoint";
import { postData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

const postResetPassword = async (data: {}) => {
  const res = await postData({
    endpoint: RESET_PASSWORD(),
    data,
  });
  return res?.data;
};

const postValidateResetToken = async (data: {}) => {
  const res = await postData({
    endpoint: VALIDATE_RESET_TOKEN(),
    data,
  });
  return res?.data;
};

const postForgotPassword = async (data: {}) => {
  const res = await postData({
    endpoint: FORGOT_PASSWORD(),
    data,
  });
  return res?.data;
};

export { postForgotPassword, postResetPassword, postValidateResetToken };

