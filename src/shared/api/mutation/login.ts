import { LOGIN } from '@/shared/constants/endpoint';
import { postData } from '@/shared/utils/http/axiosHelper';

const logIn = async (data: any) => {
  const res = await postData({
    endpoint: LOGIN,
    data
  });
  return res?.data;
};

export { logIn };
