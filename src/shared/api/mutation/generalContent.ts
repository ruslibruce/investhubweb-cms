import { GENERAL_CONTENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { putData

  
} from "@/shared/utils/http/axiosHelper";

const editGeneralContent = async ({id, data}: {id:string;data:{}}) => {
  try {
    const token = storageCheck(USER)?.token;
    if (!token) throw new Error("User not authenticated");

    const endpoint = GENERAL_CONTENT(`/id/${id}`);
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const response = await putData({ endpoint, data, config });
    return response;
  } catch (error) {
    console.error("Failed to edit general content:", error);
    throw error;
  }
};



export {editGeneralContent};