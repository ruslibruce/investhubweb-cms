import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { INVESTMENT_PARTNER, INVESTMENT_PARTNER_DETAIL } from "@/shared/constants/endpoint";
import { deleteData, postData, putData } from "@/shared/utils/http/axiosHelper";

const addInvestment = async (data: any) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await postData({
      endpoint: INVESTMENT_PARTNER_DETAIL(),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Add Investment. Please try again later.");
  }
};

const deleteInvestment = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: INVESTMENT_PARTNER_DETAIL(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete Investment. Please try again later.");
  }
};

async function putInvestment({ params, data }: { params: string; data: any }) {
    const dataUser = storageCheck(USER);
  
    const dataPutInvestment = await putData({
      endpoint: INVESTMENT_PARTNER_DETAIL(`/id/${params}`),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return dataPutInvestment?.data;
  }

  async function publishInvestment({ data, id }: { data: any; id: string }) {
    const dataUser = storageCheck(USER);
  
    const dataPublishUpdate = await putData({
      endpoint: INVESTMENT_PARTNER_DETAIL(`/id/${id}`),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return dataPublishUpdate?.data;
  }

export { addInvestment, deleteInvestment, putInvestment, publishInvestment };
