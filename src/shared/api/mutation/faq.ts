import { FAQ, DELETE_FAQ, EDIT_FAQ } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { postData, deleteData, putData } from "@/shared/utils/http/axiosHelper";

async function postUpdateFAQ({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);

  const dataPostUpdate = await putData({
    endpoint: EDIT_FAQ(`${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpdate?.data;
}


  async function editFaq({ params, data }: { params: string; data: any }) {
    const dataUser = storageCheck(USER);

    const dataEditFaq = await putData({
      endpoint: DELETE_FAQ(`/id/${params}`),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return dataEditFaq?.data;
  }

const deleteFaq = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: DELETE_FAQ(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete FAQ. Please try again later.");
  }
};

const addFaq = async (data: any) => {
    try {
      const dataUser = storageCheck(USER);
      const res = await postData({
        endpoint: FAQ(),
        data: data,
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
      return res?.data;
    } catch (error: any) {
      const errorMessage = error.response?.data?.message || "Failed to add FAQ. Please try again later.";
      throw new Error(errorMessage);
    }
  };

  export {addFaq,deleteFaq, editFaq, postUpdateFAQ};