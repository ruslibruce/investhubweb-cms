import { FETCH_RSS_NEWS, NEWS_RSS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  getData,
  postData,
  putData,
  deleteData,
} from "@/shared/utils/http/axiosHelper";
import { paramsToString } from "@/shared/utils/string";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchNewsRss(params?: string): Props {
  const KEY = ["rss"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataNewsRss = await getData({
      endpoint: NEWS_RSS(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataNewsRss?.data;
  };

  return { key: KEY, api: API };
}

const fetchUpdateRSSToNews = async () => {
  const dataUser = storageCheck(USER);
  const res = await getData({
    endpoint: FETCH_RSS_NEWS(),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

const postNewsRSS = async (data: any) => {
  const dataUser = storageCheck(USER);
  const res = await postData({
    endpoint: NEWS_RSS(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data;
};

const updateNewsRss = async ({ id, data }: { id: string; data: {} }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await putData({
      endpoint: NEWS_RSS(`/id/${id}`),
      data: data,
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete News Rss. Please try again later.");
  }
};

const deleteNewsRss = async ({ id }: { id: string }) => {
  try {
    const dataUser = storageCheck(USER);
    const res = await deleteData({
      endpoint: NEWS_RSS(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    return res?.data;
  } catch (error) {
    throw new Error("Failed to Delete News Rss. Please try again later.");
  }
};

const getCategoryNewsRss = async (params: any) => {
  const dataUser = storageCheck(USER);
  console.log("params", params);

  const res = await getData({
    endpoint: NEWS_RSS(
      paramsToString({
        category: params.value,
      })
    ),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return res?.data.data;
};

export {
  deleteNewsRss,
  postNewsRSS,
  fetchNewsRss,
  getCategoryNewsRss,
  updateNewsRss,
  fetchUpdateRSSToNews,
};
