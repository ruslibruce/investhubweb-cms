import { EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchDetailEvent(params?: string): Props {
  const KEY = ["eventDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataEvent = await getData({
      endpoint: EVENT(`/id/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataEvent?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchDetailEvent };
