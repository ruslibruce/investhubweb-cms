import { EVENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchEvent(params?: string): Props {
  const KEY = ["event"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataEvent = await getData({
      endpoint: EVENT(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataEvent?.data;
  };

  return { key: KEY, api: API };
}

export { fetchEvent };
