import { COUNTRY } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchLocation(params?: string): Props {
  const KEY = ["location"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataLocation = await getData({
      endpoint: COUNTRY(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    dataLocation.data.data.map((item: any) => {
      item.value = item.name;
      item.label = item.name;
    });

    return dataLocation?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchLocation };
