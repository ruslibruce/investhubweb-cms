import { INVESTMENT_PARTNER_DETAIL} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchInvestmentDetail(params?: string): Props {
    const KEY = ["investmentDetail"];
    const dataUser = storageCheck(USER);
  
    const API = async () => {
      const dataInvestmentDetail = await getData({
        endpoint: INVESTMENT_PARTNER_DETAIL(`/id/${params}`),
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
  
      return dataInvestmentDetail?.data.data;
    };
  
    return { key: KEY, api: API };
  }
  

export { fetchInvestmentDetail};
