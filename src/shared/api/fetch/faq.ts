import { FAQ } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchFaq(params?: string): Props {
    const KEY = ["faq"];
    const dataUser = storageCheck(USER);
  
    const API = async () => {
      const dataFaq = await getData({
        endpoint: FAQ(params),
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
  
      return dataFaq?.data;
    };
  
    return { key: KEY, api: API };
  }

  
  

export { fetchFaq};
