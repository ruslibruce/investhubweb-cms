import {
  CONTENT_PROVIDER,
  CONTENT_PROVIDER_TYPE,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchContentProvider(params?: string): Props {
  const KEY = ["content-provider"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataContentProvider = await getData({
      endpoint: CONTENT_PROVIDER(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataContentProvider.data.data.map((item: any, index: any) => {
      item.no = index + 1;
      item.value = item.id;
      item.label = item.name;
    });

    return dataContentProvider.data.data;
  };

  return { key: KEY, api: API };
}

function fetchContentProviderType(params?: string): Props {
  const KEY = ["content-provider-type"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataContentProvider = await getData({
      endpoint: CONTENT_PROVIDER_TYPE(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataContentProvider.data.data.map((item: any, index: any) => {
      item.no = index + 1;
      item.key = item.id;
      item.label = item.name;
      item.value = item.name;
    });

    // Sorting by id in ascending order
    dataContentProvider.data.data.sort((a: any, b: any) => a.id - b.id);

    return dataContentProvider.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchContentProvider, fetchContentProviderType };
