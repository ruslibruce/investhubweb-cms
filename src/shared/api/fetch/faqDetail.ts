import { DELETE_FAQ } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchDetailFAQ(id?: string): Props {
  const KEY = ["faqDetail"];
  const dataUser = storageCheck(USER);
  

  const API = async () => {
    const dataFAQ = await getData({
      endpoint: DELETE_FAQ(`/id/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataFAQ?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchDetailFAQ };
