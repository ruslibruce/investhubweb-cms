import { NEWS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "@/shared/utils/http/axiosHelper";
import { paramsToString } from "@/shared/utils/string";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchNews(params?: string): Props {
  const KEY = ["news"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataNews = await getData({
      endpoint: NEWS(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataNews?.data;
  };

  return { key: KEY, api: API };
}

export { fetchNews };
