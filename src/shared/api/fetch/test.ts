import { TEST, TEST_QUESTION, TEST_TYPE } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchAllTest(params?: string): Props {
  const KEY = ["test"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataTest = await getData({
      endpoint: TEST(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });
    console.log("cek dataTest", dataTest?.data);

    return dataTest?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailTest(id?: string): Props {
  const KEY = ["testQuestion"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataTestDetail = await getData({
      endpoint: TEST_QUESTION(`/${id}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataTestDetail?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchTestType(params?: string): Props {
  const KEY = ["testType"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataTestType = await getData({
      endpoint: TEST_TYPE(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    let dataTest = [] as any;

    dataTestType?.data.data.map((item: any) => {
      if (item.id === 1 || item.id === 4) {
        dataTest.push({ value: item.id, label: item.name });
      }
    });

    return dataTest;
  };

  return { key: KEY, api: API };
}

async function postTest(data: {}) {
  const dataUser = storageCheck(USER);

  const dataPostTest = await postData({
    endpoint: TEST(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostTest?.data;
}

async function postCheckTest(data: {}) {
  const dataUser = storageCheck(USER);

  const dataPostCheckTest = await postData({
    endpoint: TEST_QUESTION(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostCheckTest?.data;
}

async function delUncheckTest({
  idTest,
  idQuestion,
}: {
  idTest: string;
  idQuestion: string;
}) {
  const dataUser = storageCheck(USER);

  const dataDelete = await deleteData({
    endpoint: TEST_QUESTION(`/${idTest}/${idQuestion}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataDelete?.data;
}

async function putTest({ data, id }: { data: {}; id: string }) {
  const dataUser = storageCheck(USER);

  const dataPostTest = await putData({
    endpoint: TEST(`/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostTest?.data;
}

async function deleteTest(id: string) {
  const dataUser = storageCheck(USER);

  const dataPostTest = await deleteData({
    endpoint: TEST(`/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostTest?.data;
}

export {
  delUncheckTest,
  fetchAllTest,
  fetchDetailTest,
  fetchTestType,
  postCheckTest,
  postTest,
  putTest,
  deleteTest,
};
