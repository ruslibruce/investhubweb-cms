import {  GENERAL_CONTENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchGeneralContent(params?: string): Props {
  const KEY = ["generalContent"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataGeneralContent = await getData({
      endpoint: GENERAL_CONTENT(),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataGeneralContent?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchGeneralContentDetail(params?: string): Props {
  const KEY = ["generalContentDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataGeneralContent = await getData({
      endpoint: GENERAL_CONTENT(`/id/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataGeneralContent?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchGeneralContent, fetchGeneralContentDetail };
