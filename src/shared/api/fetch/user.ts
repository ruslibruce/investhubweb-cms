import { USER_API } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
	key: string[];
	api: () => Promise<any>;
};

function getDetailUser(params?: string): Props {
  const KEY = ["detailUser"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataUsers = await getData({
      endpoint: USER_API(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataUsers?.data.data;
  };

  return { key: KEY, api: API };
}

export {
  getDetailUser
};
