import { CERTIFICATE } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchCertificate(params?: string): Props {
  const KEY = ["certificate"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCertificate = await getData({
      endpoint: CERTIFICATE(`/list${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataCertificate?.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailCertificate(params?: string): Props {
  const KEY = ["certificateDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCertificate = await getData({
      endpoint: CERTIFICATE(`/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataCertificate?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchCertificate, fetchDetailCertificate };
