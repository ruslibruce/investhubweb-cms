import { PORTAL_USERS_COLLECTION,PORTAL_USERS_COLLECTION_DETAIL } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchCollectionUsers(params?: string): Props {
  const KEY = ["portal-users-collection"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCollectionUsers = await getData({
      endpoint: PORTAL_USERS_COLLECTION(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataCollectionUsers.data.data;
  };

  return { key: KEY, api: API };
}

function fetchCollectionDetail(params?: string): Props {
  const KEY = ["portal-users-collection-detail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCollectionDetail = await getData({
      endpoint: PORTAL_USERS_COLLECTION_DETAIL(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataCollectionDetail.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchCollectionUsers,fetchCollectionDetail };
