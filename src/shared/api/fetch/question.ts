import {
  CONTENT_PROVIDER,
  CONTENT_PROVIDER_TYPE,
  QUESTION,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchQuestionApi(params?: string): Props {
  const KEY = ["question"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataQuestion = await getData({
      endpoint: QUESTION(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataQuestion.data.data.map((item: any, index: any) => {
      item.no = index + 1;
    });

    return dataQuestion.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchQuestionApi };
