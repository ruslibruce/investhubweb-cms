import { COUNTRY, PORTAL_USERS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchPortalUsers(params?: string): Props {
  const KEY = ["portal-users"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataPortalUsers = await getData({
      endpoint: PORTAL_USERS(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataPortalUsers.data.data.map((item: any, index: any) => {
      item.key = item.id;
      item.no = index + 1;
    });

    return dataPortalUsers.data.data;
  };

  return { key: KEY, api: API };
}

function getCountry(params?: string): Props {
  const KEY = ["country"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCountry = await getData({
      endpoint: COUNTRY(),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataCountry?.data.data.map((item: any) => {
      item.key = item.id;
      item.value = item.name;
      item.label = item.name;
    });

    return dataCountry?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchPortalUsers, getCountry };
