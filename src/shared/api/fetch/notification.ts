import { NOTIFICATION } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, putData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchNotification(params?: string): Props {
  const KEY = ["notification"];
  const user = storageCheck(USER);

  const API = async () => {
    const dataAbout = await getData({
      endpoint: NOTIFICATION(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    return dataAbout?.data;
  };

  return { key: KEY, api: API };
}

const putReadDataNotif = async ({ id, data }: { id: string; data: {} }) => {
  const user = storageCheck(USER);
  const res = await putData({
    endpoint: NOTIFICATION(`/${id}`),
    data,
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

export { fetchNotification, putReadDataNotif };
