import {
  CATEGORY_CONTENT_TYPE,
  COURSE,
  COURSE_CATEGORY,
  COURSE_COMMENT,
  COURSE_LIKE,
  COURSE_REPORT,
  LEVEL,
  SUBTITLE,
  TEST_TYPE,
} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchAllCourses(params?: string): Props {
  const KEY = ["course"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: COURSE(`/list${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataCourse?.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailCourses(params?: string): Props {
  const KEY = ["courseDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: COURSE(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

async function postCreateCourse(data: any) {
  const dataUser = storageCheck(USER);

  const dataPostUpload = await postData({
    endpoint: COURSE(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function postPublishCourse({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);

  const dataPostUpload = await putData({
    endpoint: COURSE(`/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function postCreateCourseSection(data: {}) {
  const dataUser = storageCheck(USER);

  const dataPostUpload = await postData({
    endpoint: COURSE(`/section`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function postCreateCourseContent(data: {}) {
  console.log("cek postCreateCourseContent", data);
  const dataUser = storageCheck(USER);

  const dataPostContent = await postData({
    endpoint: COURSE(`/content`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

function fetchTestTypeCourse(params?: string): Props {
  const KEY = ["testType"];
  const dataUser = storageCheck(USER);
  const API = async () => {
    const dataTestType = await getData({
      endpoint: TEST_TYPE(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    let dataTest = [] as any;

    dataTestType?.data.data.map((item: any) => {
      if (item.id === 2 || item.id === 3) {
        dataTest.push({ value: item.id, label: item.name });
      }
    });

    return dataTest;
  };

  return { key: KEY, api: API };
}

function getCategoryCourse(params?: string): Props {
  const KEY = ["course-categories"];
  const dataUser = storageCheck(USER);
  const API = async () => {
    const dataCategory = await getData({
      endpoint: COURSE_CATEGORY(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataCategory?.data.data.map((item: any, index: any) => {
      item.key = item.id;
      item.value = item.id;
      item.label = item.name;
      item.no = index + 1;
    });

    return dataCategory?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchCourseContentType(params?: string): Props {
  const KEY = ["courseContentType"];
  const dataUser = storageCheck(USER);
  const API = async () => {
    const dataCourse = await getData({
      endpoint: CATEGORY_CONTENT_TYPE(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataCourse?.data.data.map((item: any) => {
      item.key = item.id;
      item.label = item.name;
      item.value = item.name;
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchLevelCourses(params?: string): Props {
  const KEY = ["courseLevel"];
  const user = storageCheck(USER);
  const API = async () => {
    const dataCourse = await getData({
      endpoint: LEVEL(params),
      config: {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      },
    });

    dataCourse?.data.data.map((item: any) => {
      item.key = item.id;
      item.label = item.name;
      item.value = item.id;
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

async function deleteCourseSection(id: string) {
  const dataUser = storageCheck(USER);
  const dataPostUpload = await deleteData({
    endpoint: COURSE(`/section/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function deleteCourseContent(id: string) {
  const dataUser = storageCheck(USER);
  const dataPostContent = await deleteData({
    endpoint: COURSE(`/content/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

async function putCourseContent({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);
  const dataPostContent = await putData({
    endpoint: COURSE(`/content/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

async function putCourseSection({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);
  const dataPostUpload = await putData({
    endpoint: COURSE(`/section/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function putCourseCategory({ data, id }: { data: any; id: string }) {
  const dataUser = storageCheck(USER);

  const dataPostUpload = await putData({
    endpoint: COURSE_CATEGORY(`/${id}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostUpload?.data;
}

async function deleteCourseCategory(id: string) {
  const dataUser = storageCheck(USER);
  const dataPostContent = await deleteData({
    endpoint: COURSE_CATEGORY(`/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

async function postCourseCategory(data: {}) {
  const dataUser = storageCheck(USER);

  const dataPostContent = await postData({
    endpoint: COURSE_CATEGORY(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

const getCommentCourse = async (id?: string) => {
  const user = storageCheck(USER);
  const res = await getData({
    endpoint: COURSE_COMMENT(`/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const getCommentLike = async (id?: string) => {
  const user = storageCheck(USER);
  const res = await getData({
    endpoint: COURSE_LIKE(`/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

const getCommentReport = async (id?: string) => {
  const user = storageCheck(USER);
  const res = await getData({
    endpoint: COURSE_REPORT(`/${id}`),
    config: {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    },
  });
  return res?.data.data;
};

async function postCreateCourseSubtitle(data: {}) {
  console.log("cek postCreateCourseContent", data);
  const dataUser = storageCheck(USER);

  const dataPostContent = await postData({
    endpoint: SUBTITLE(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostContent?.data;
}

export {
  deleteCourseCategory,
  deleteCourseContent,
  deleteCourseSection,
  fetchAllCourses,
  fetchCourseContentType,
  fetchDetailCourses,
  fetchLevelCourses,
  fetchTestTypeCourse,
  getCategoryCourse,
  getCommentCourse,
  postCourseCategory,
  postCreateCourse,
  postCreateCourseContent,
  postCreateCourseSection,
  postPublishCourse,
  putCourseCategory,
  putCourseContent,
  putCourseSection,
  getCommentLike,
  getCommentReport,
  postCreateCourseSubtitle,
};
