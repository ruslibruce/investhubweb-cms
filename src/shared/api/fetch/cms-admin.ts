import { CMS_ADMIN } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchCmsAdmin(params?: string): Props {
  const KEY = ["cms-admin"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCmsAdmin = await getData({
      endpoint: CMS_ADMIN(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataCmsAdmin.data.data.map((item: any, index: any) => {
      item.key = index + 1;
      item.no = index + 1;
    });

    if (dataUser?.user?.role === "cp-admin") {
      return dataCmsAdmin.data.data.filter(
        (item: any) => item.role === "cp-admin"
      );
    }

    return dataCmsAdmin.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchCmsAdmin };
