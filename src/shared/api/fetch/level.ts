import { LEVEL, LEVEL_DETAIL } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData, postData, putData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchAllLevel(params?: string): Props {
  const KEY = ["level"];
  const datadataUser = storageCheck(USER);

  const API = async () => {
    const dataLevel = await getData({
      endpoint: LEVEL(params),
      config: {
        headers: {
          Authorization: `Bearer ${datadataUser.token}`,
        },
      },
    });
    console.log("cek dataLevel", dataLevel?.data);

    return dataLevel?.data.data;
  };

  return { key: KEY, api: API };
}

function fetchDetailLevel(params?: string): Props {
  const KEY = ["levelDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCourse = await getData({
      endpoint: LEVEL_DETAIL(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    return dataCourse?.data.data;
  };

  return { key: KEY, api: API };
}

async function putLevel({ params, data }: { params: string; data: any }) {
  const dataUser = storageCheck(USER);

  const dataPutLevel = await putData({
    endpoint: LEVEL(`/${params}`),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPutLevel?.data;
}

async function postLevel(data: {}) {
  const dataUser = storageCheck(USER);

  const dataPostLevel = await postData({
    endpoint: LEVEL(),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPostLevel?.data;
}

function fetchLevelType(params?: string): Props {
  const KEY = ["levelType"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataLevelype = await getData({
      endpoint: LEVEL(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    let dataLevelResult = [] as any;

    dataLevelype?.data.data.map((item: any) => {
      dataLevelResult.push({ value: item.id, label: item.name });
    });

    return dataLevelResult;
  };

  return { key: KEY, api: API };
}

async function putDetailLevel({ params, data }: { params: string; data: any }) {
  const dataUser = storageCheck(USER);

  const dataPutLevel = await putData({
    endpoint: LEVEL(params),
    data: data,
    config: {
      headers: {
        Authorization: `Bearer ${dataUser.token}`,
      },
    },
  });
  return dataPutLevel?.data;
}

export {
  fetchAllLevel,
  fetchDetailLevel,
  fetchLevelType,
  postLevel,
  putDetailLevel,
  putLevel,
};
