import { INVESTMENT_PARTNER} from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchInvestment(params?: string): Props {
    const KEY = ["investment-partner"];
    const dataUser = storageCheck(USER);
  
    const API = async () => {
      const dataInvestment = await getData({
        endpoint: INVESTMENT_PARTNER(params),
        config: {
          headers: {
            Authorization: `Bearer ${dataUser.token}`,
          },
        },
      });
  
      return dataInvestment?.data;
    };
  
    return { key: KEY, api: API };
  }
  

export { fetchInvestment};
