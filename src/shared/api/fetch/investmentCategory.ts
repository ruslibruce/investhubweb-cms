import { CATEGORY_INVESTMENT } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchInvestmentCategory(params?: string): Props {
  const KEY = ["category-investment"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataCategory = await getData({
      endpoint: CATEGORY_INVESTMENT(params),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`,
        },
      },
    });

    dataCategory?.data.data.map((item: any) => {
      item.key = item.id;
      item.value = item.id;
      item.label = item.name;
    });

    return dataCategory?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchInvestmentCategory };
