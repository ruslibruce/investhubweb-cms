import { NEWS } from "@/shared/constants/endpoint";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { getData } from "@/shared/utils/http/axiosHelper";

type Props = {
  key: string[];
  api: () => Promise<any>;
};

function fetchDetailNews(params?: string): Props {
  const KEY = ["newsDetail"];
  const dataUser = storageCheck(USER);

  const API = async () => {
    const dataNews = await getData({
      endpoint: NEWS(`/id/${params}`),
      config: {
        headers: {
          Authorization: `Bearer ${dataUser.token}`
        }
      }
    });

    return dataNews?.data.data;
  };

  return { key: KEY, api: API };
}

export { fetchDetailNews };
