import axios, { AxiosRequestConfig } from "axios";
import type { AxiosInstance } from "axios"; // Change the import to a type-only import
import { BASE_URL } from "../constants/endpoint";

const axiosConfig: AxiosRequestConfig = {
  timeout: 50000,
  headers: {
    "Content-Type": "application/json",
    'Accept': "application/json"
  },
  baseURL: BASE_URL
};

const AxiosInstance: AxiosInstance = axios.create(axiosConfig);

export default AxiosInstance;
