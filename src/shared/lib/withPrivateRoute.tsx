/* eslint-disable react/display-name */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-anonymous-default-export */
import { useRouter } from "next/router";
import React from "react";
import { DASHBOARD_HOME, DASHBOARD_LOGIN, INVALID_ACCESS_PAGE } from "../constants/path";
import { USER } from "../constants/storageStatis";
import { storageCheck } from "../utils/clientStorageUtils";
import {
  checkIsAuthRoute,
  checkIsCPRoute,
  checkIsKuratorRoute,
  checkIsPortalAdminRoute,
  checkIsPrivateRoute,
} from "../utils/route";

/**
 *
 * @param WrappedComponent
 * @returns
 * Component with additonal props auth
 * and checking of authentication
 */
export default function withPrivateRoute(WrappedComponent: any) {
  return (props: any) => {
    const router = useRouter();
    const dataUser = storageCheck(USER);
    const isCpAdmin = dataUser?.user?.role === "cp-admin";
    const isPortalAdmin = dataUser?.user?.role === "portal-admin";
    const isKurator = dataUser?.user?.role === "kurator";

    const [initialRenderComplete, setInitialRenderComplete] =
      React.useState(false);

      React.useEffect(() => {
        // Updating a state causes a re-render
        // Render client complete => true
        setInitialRenderComplete(true);

        // token not authenticated & accessing private route
        if (!dataUser.token && checkIsPrivateRoute(router.route)) {
          router.replace(DASHBOARD_LOGIN);
          return;
        }
        // token authenticated accessing private route then redirect to dashboard home
        if (dataUser.token && checkIsAuthRoute(router.route)) {
          router.replace(DASHBOARD_HOME);
        }
      }, []);

    // Returning null will prevent the component from rendering, so the content will simply be missing from
    // the server HTML and also wont render during the first client-side render.
    if (!initialRenderComplete) {
      return null;
    }

    // if not authenticated & token accessing private route, render nothing
    // render nothing to wait redirection
    if (!dataUser.token && checkIsPrivateRoute(router.route)) {
      return null;
    }

    // if authenticated & token accessing auth route, render nothing
    // render nothing to wait redirection
    if (dataUser.token && checkIsAuthRoute(router.route)) {
      return null;
    }

    // if authenticated & token not accessing kurator route, render nothing
    // render nothing to wait redirection
    if (dataUser.token && isKurator) {
      if (!checkIsKuratorRoute(router.route)) {
        router.replace(INVALID_ACCESS_PAGE);
        return;
      }
    }

    // if authenticated & token not accessing cp route, render nothing
    // render nothing to wait redirection
    if (dataUser.token && isCpAdmin) {
      if (!checkIsCPRoute(router.route)) {
        router.replace(INVALID_ACCESS_PAGE);
        return;
      }
    }

    if (dataUser.token && isPortalAdmin) {
      if (!checkIsPortalAdminRoute(router.route)) {
        router.replace(INVALID_ACCESS_PAGE);
        return;
      }
    }

    return <WrappedComponent {...props} auth={dataUser.token} />;
  };
}
