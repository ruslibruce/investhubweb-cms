import React, { useState } from "react";
import { Pagination, PaginationProps } from "antd";
type PaginationComponentProps = {
  total: number;
  setCurrentPage: (value: any) => void;
  currentPage: number;
  limit?: number;
};

export default function PaginationComponent({
  total,
  currentPage,
  setCurrentPage,
  limit = 10,
}: PaginationComponentProps) {
  const onChange: PaginationProps["onChange"] = (page) => {
    console.log("Pagination", page);
    setCurrentPage(page);
  };

  const showTotal: PaginationProps["showTotal"] = (total) =>
    `Total ${total} items`;

  return (
    <Pagination
      responsive
      size="small"
      className="bottom-pagination-card"
      total={total}
      current={currentPage}
      onChange={onChange}
      pageSize={limit}
      showSizeChanger={false}
      showTotal={showTotal}
      showQuickJumper
    />
  );
} // Path: src/shared/utils/pagination.ts
