import React from "react";
import { useMediaQuery } from "react-responsive";

type PropsText = {
  children: React.ReactNode;
  desktop: number;
  bigScreen?: number;
  mobile: number;
  retina?: number;
  color: string;
  fontWeight: string | number;
  isSpan?: boolean;
};
export default function TextResponsive({
  children,
  desktop,
  bigScreen,
  mobile,
  retina,
  color,
  fontWeight,
  isSpan,
}: PropsText) {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const isBigScreen = useMediaQuery({ query: "(min-width: 1824px)" });
  const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });
  const isRetina = useMediaQuery({ query: "(min-resolution: 2dppx)" });
  //   console.log('isRetina', isRetina);
  //   console.log('isPortrait', isPortrait);
  //   console.log('isTabletOrMobile', isTabletOrMobile);
  //   console.log('isDesktopOrLaptop', isDesktopOrLaptop);
  //   console.log('isBigScreen', isBigScreen);

  return (
    <div>
      {isDesktopOrLaptop && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: desktop,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: desktop,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )}
      {isTabletOrMobile && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: mobile,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: mobile,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )}
      {/* {isBigScreen && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: bigScreen,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: bigScreen,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )}
      {isRetina && (
        <>
          {isSpan ? (
            <span
              style={{
                fontSize: retina,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </span>
          ) : (
            <p
              style={{
                fontSize: retina,
                color: color,
                fontWeight: fontWeight,
              }}
            >
              {children}
            </p>
          )}
        </>
      )} */}
    </div>
  );
}
