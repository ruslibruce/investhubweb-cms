import { Layout } from "antd";
import ContentLayoutAuth from "./auth-layout/content";
import SiderLayoutAuth from "./auth-layout/sider";
import React, { useState } from "react";

type AuthLayoutProps = {
  children: React.ReactNode;
  title?: string;
  description?: string;
};

const AuthLayout = (props: AuthLayoutProps) => {
  const [siderWidth, setSiderWidth] = useState(640);
  const [collapsed, setCollapsed] = useState(false);

  React.useEffect(() => {
    const handleResize = () => {
      const { innerWidth } = window;
      const newWidth = innerWidth * 0.4;

      if (newWidth < 640) {
        setSiderWidth(newWidth);
      } else {
        setSiderWidth(640);
      }
    };

    window.addEventListener("resize", handleResize);
    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleBreakpoint = (broken: boolean | ((prevState: boolean) => boolean)) => {
    setCollapsed(broken);
  };

  return (
    <Layout>
      <Layout.Content>
        <ContentLayoutAuth>{props.children}</ContentLayoutAuth>
      </Layout.Content>

      <Layout.Sider
        width={siderWidth}
        style={{ backgroundColor: "transparent" }}
        breakpoint="md"
        collapsedWidth="0"
        onBreakpoint={handleBreakpoint}
      >
        <SiderLayoutAuth />
      </Layout.Sider>
    </Layout>
  );
};

export default AuthLayout;
