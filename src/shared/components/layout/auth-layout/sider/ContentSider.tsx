import React, { useState } from 'react';

// styled
import styled from 'styled-components';

// style
import StylesSider from './Style';

const StyledDiv = styled.div`
	// display: flex;
	// width: 540px;
	// height: 456px;
	// padding: 120px 50px 50px 50px;
	// flex-direction: column;
	// justify-content: flex-end;
	// align-items: flex-start;
	// gap: 20px;
	// flex-shrink: 0;
	// border-radius: 20px;
	// background: rgba(255, 255, 255, 0.3);
	// backdrop-filter: blur(10px);
	// position: relative;
	// border-color: transparent;
	// margin-top: 100px;
`;

const ContentSider = () => {
  // style
  const stylesSider = StylesSider();

  // state
  const [scale, setScale] = useState(1);

  // effect
  React.useEffect(() => {
    const handleScale = () => {
      const { innerWidth } = window;

      const width40 = innerWidth * 0.4;

      if (width40 < 640) {
        setScale(Math.ceil(width40) / 640);
      } else {
        if (scale !== 1) {
          setScale(1);
        }
      }
    };

    window.addEventListener('resize', handleScale);

    handleScale();

    return () => window.removeEventListener('resize', handleScale);
  }, [scale]);

  return (
    <StyledDiv style={{ transform: `scale(${scale})` }}>
    </StyledDiv>
  );
};

export default ContentSider;
