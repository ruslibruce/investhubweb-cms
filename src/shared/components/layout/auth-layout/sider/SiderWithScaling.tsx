import React, { useState } from "react";
import styled from "styled-components";

// Styled component for Sider
const StyledSider = styled.div`
  // Styles for the Sider
`;

type SiderWithScalingProps = {
  children: React.ReactNode;
};

const SiderWithScaling: React.FC<SiderWithScalingProps> = ({ children }) => {
  const [scale, setScale] = useState(1);

  React.useEffect(() => {
    const handleScale = () => {
      const { innerWidth } = window;
      const width40 = innerWidth * 0.4;

      if (width40 < 640) {
        setScale(Math.ceil(width40) / 640);
      } else {
        if (scale !== 1) {
          setScale(1);
        }
      }
    };

    window.addEventListener("resize", handleScale);
    handleScale();

    return () => window.removeEventListener("resize", handleScale);
  }, [scale]);

  return (
    <StyledSider style={{ transform: `scale(${scale})` }}>
      {children}
    </StyledSider>
  );
};

export default SiderWithScaling;
