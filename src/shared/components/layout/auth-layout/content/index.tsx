import React from "react";

// next
import Image from "next/image";

// styled
import styled from "styled-components";

// image
import logoLogin from "@/shared/images/logo/logo_login.webp";

// hooks
import useWindowResize from "@/shared/hooks/useWindowResize";
import { queries } from "@storybook/testing-library";
import { Space } from "antd";

type ContentLayoutAuthProps = {
  children: React.ReactNode;
};

const StyledDiv = styled.div`
  min-height: 100vh;
  display: flex;
  background: #ffffff;
  justify-content: center;
  align-items: center;
`;

const ContentLayoutAuth = (props: ContentLayoutAuthProps) => {
  // hooks

  return (
    <StyledDiv>
      <div
        className="login-container"
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Space style={{ width: 259, height: 65 }}>
          <Image
            src={logoLogin}
            alt={"logo-cms"}
            width={100}
            height={100}
            quality={100}
            style={{ marginBottom: "30px", width: "auto", height: "auto" }}
          />
        </Space>

        {props.children}
      </div>
    </StyledDiv>
  );
};

export default ContentLayoutAuth;
