import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import {
  checkIsAuthRoute,
  checkIsPrivateRoute
} from "@/shared/utils/route";
import { useRouter } from "next/router";
import React, { useState } from "react";
import AuthLayout from "./AuthLayout";
import PrivateLayout from "./PrivateLayout";

type LayoutProps = {
  children: React.ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
  const router = useRouter();
  const dataUser = storageCheck(USER);
  console.log("dataUser in layout", dataUser);

  const [initialRenderComplete, setInitialRenderComplete] = useState(false);

  React.useEffect(() => {
    setInitialRenderComplete(true);
  }, []);

  if (!initialRenderComplete) {
    return null;
  }

  if (
    dataUser.token &&
    dataUser?.profile &&
    checkIsPrivateRoute(router.route)
  ) {
    console.log('Masukkk Private Layout');
    return <PrivateLayout>{children}</PrivateLayout>;
  }

  if (
    !dataUser.token &&
    (dataUser?.profile === null || dataUser?.profile === undefined) &&
    checkIsAuthRoute(router.route)
  ) {
    console.log('Masukkk Auth Layout');
    return <AuthLayout>{children}</AuthLayout>;
  }

  return <>{children}</>;
};

export default Layout;
