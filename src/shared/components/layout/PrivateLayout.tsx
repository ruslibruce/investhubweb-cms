import { Grid, Layout } from "antd";
import SidebarPrivateLayout from "./private-layout/SidebarPrivateLayout";
import FooterContentPrivateLayout from "./private-layout/FooterContentPrivateLayout";
import HeaderContentPrivateLayout from "./private-layout/HeaderContentPrivateLayout";
import styled from "styled-components";
import React, { useState } from "react";
import DrawerPrivateLayout from "./private-layout/DrawerPrivateLayout";

const { Content } = Layout;

const CustomLayout = styled(Layout)`
  background: #f8f8f8;
  min-height: 100vh;
`;

type Props = {
  children: React.ReactNode;
};

const PrivateLayout = ({ children }: Props) => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [open, setOpen] = useState(false);

  React.useEffect(() => {
    const handleResize = () => setWindowWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleClose = () => {
    setOpen(!open);
  };

  return (
    <Layout hasSider>
      {windowWidth >= 950 ? (
        <SidebarPrivateLayout open={open} onClose={handleClose} />
      ) : (
        <DrawerPrivateLayout open={open} onClose={handleClose} />
      )}
      <CustomLayout>
        <Content
          style={{
            padding: 0,
            margin: 0,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <HeaderContentPrivateLayout onClose={handleClose} />

          <div
            style={{ padding: "0px 30px", minHeight: "calc(100vh - 132px)" }}
          >
            {children}
          </div>

          <FooterContentPrivateLayout />
        </Content>
      </CustomLayout>
    </Layout>
  );
};

export default PrivateLayout;
