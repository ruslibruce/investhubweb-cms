import { DoubleLeftOutlined, DoubleRightOutlined } from "@ant-design/icons";
import { Button, Layout, theme } from "antd";
import styled from "styled-components";
import React, { useState } from "react";
import MenuSideMenuPrivateLayout from "./MenuSideMenuPrivateLayout";
import Image from "next/image";
import InvestHubLogo from "@/shared/images/logo/logo_investhub.webp";
import InvesthubSiderLogo from "/public/android-chrome-192x192.png";

const CustomSider = styled(Layout.Sider)`
  scrollbar-color: #bebfcf #f8f8f8;
  scrollbar-width: thin;
  &::-webkit-scrollbar {
    width: 0.5vw;
    height: 0.5vw;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #bebfcf;
    border-radius: 0px;
  }
  &::-webkit-scrollbar-track {
    background-color: #f8f8f8;
  }
`;

type Props = {
  open: boolean;
  onClose: () => void;
};

const SidebarPrivateLayout = ({ open, onClose }: Props) => {
  
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const collapsedWidth = 80; // Set default value for collapsedWidth

  return (
    <CustomSider
      style={{
        background: colorBgContainer,
        overflowY: "auto",
        overflowX: "hidden",
        zIndex: 10,
        padding: "25px 0px 10px 4px",
        userSelect: "none",
      }}
      collapsible
      collapsed={open}
      collapsedWidth={collapsedWidth}
      trigger={null}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBlockEnd: "16px",
          marginInlineEnd: "16px",
        }}
      >
        {/* LOGO HERE */}
        {open ? (
          <Image
            src={InvesthubSiderLogo}
            alt={"logo-cms"}
            width={25}
            height={25}
            quality={100}
            style={{ marginLeft: "18px", marginRight: "4px" }}
          />
        ) : (
          <Image
            src={InvestHubLogo}
            alt={"logo-cms"}
            width={150}
            height={40}
            quality={100}
            style={{ marginLeft: "28px" }}
          />
        )}

        {/* BUTTON COLLAPSE */}
        <Button
          type="text"
          size="small"
          icon={open ? <DoubleRightOutlined /> : <DoubleLeftOutlined />}
          onClick={onClose}
          style={{
            fontSize: "16px",
          }}
        />
      </div>

      {/* SIDE MENU */}
      <MenuSideMenuPrivateLayout />
    </CustomSider>
  );
};

export default SidebarPrivateLayout;
