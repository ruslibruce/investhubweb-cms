import {
  DASHBOARD_CONTENT_PROVIDER,
  DASHBOARD_COURSE,
  DASHBOARD_COURSE_RSS,
  DASHBOARD_EVENT,
  DASHBOARD_EXTERNAL_CERTIFICATE,
  DASHBOARD_FAQ,
  DASHBOARD_INVESTMENT_PARTNER,
  DASHBOARD_FORUM,
  DASHBOARD_GENERAL_CONTENT,
  DASHBOARD_HOME,
  DASHBOARD_LEVEL,
  DASHBOARD_NEWS,
  DASHBOARD_NEWS_RSS,
  DASHBOARD_QUESTION,
  DASHBOARD_TEST,
  MASTER_DATA_CATEGORY_COURSE,
  MASTER_DATA_CATEGORY_EVENT,
  MASTER_DATA_CATEGORY_NEWS,
  USER_MANAGEMENT_CMSADMIN,
  USER_MANAGEMENT_PORTAL_USERS,
  USER_MANAGEMENT_ROLE,
} from "@/shared/constants/path";
import { USER } from "@/shared/constants/storageStatis";
import ContentProviderLogo from "@/shared/images/logo/logo_content_provider.webp";
import HomeLogo from "@/shared/images/logo/logo_home.webp";
import ManagementPortalLogo from "@/shared/images/logo/logo_management_portal.webp";
import MasterDataLogo from "@/shared/images/logo/logo_master_data.webp";
import UserManagementLogo from "@/shared/images/logo/logo_user_management.webp";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { Menu, MenuProps, Tooltip } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styled from "styled-components";

const CustomMenus = styled(Menu)`
  padding-bottom: 20px;

  & li,
  li div {
    margin: 0 !important;
    outline: none !important;
  }

  & li.ant-menu-item,
  .ant-menu-submenu-title,
  .ant-menu-item-selected,
  .ant-menu-submenu-selected .ant-menu-submenu-title {
    height: fit-content;
    width: calc(100% + 1px);

    & .ant-menu-title-content {
      margin-inline-start: 16px !important;
      margin-right: 16px;
    }

    &:not(.ant-menu-item-only-child) {
      padding: 8px 16px !important;
    }
  }

  & .ant-menu-submenu.ant-menu-submenu-inline .ant-menu-submenu-title {
    min-height: 48px !important;
  }

  &.ant-menu-root > li.ant-menu-item,
  ul.ant-menu.ant-menu-sub .ant-menu-item {
    padding-block: 4px !important;
  }

  &.ant-menu-inline-collapsed {
    & li.ant-menu-item,
    .ant-menu-submenu-title,
    .ant-menu-item-selected {
      padding: 4px 16px !important;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    & .ant-menu-item-group-title {
      color: #fff;
      position: relative;
      height: 10px;
      margin-top: -5px !important;
      margin-bottom: 5px !important;

      &:before {
        content: " ";
        position: absolute;
        top: 50%;
        left: 10px;
        width: 38px;
        height: 2px;
        background: #e2e1e6;
      }
    }

    & li.ant-menu-item-selected,
    .ant-menu-submenu-selected > .ant-menu-submenu-title {
      &:not(.ant-menu-item-selected.ant-menu-item-only-child) {
        padding: 20px 16px 10px !important;
      }
    }
  }

  &.ant-menu-root > li.ant-menu-item-selected,
  &.ant-menu-root > .ant-menu-submenu-selected > .ant-menu-submenu-title {
    color: #cb0b0c;

    & img {
      filter: /* invert(40%) sepia(77%) saturate(270%) hue-rotate(208deg)
				brightness(96%) contrast(205%) */ invert(
          24%
        )
        sepia(36%) saturate(246%) hue-rotate(238deg) brightness(97%)
        contrast(97%);
    }

    &:not(.ant-menu-item-selected.ant-menu-item-only-child) {
      background: #ffcbcc;
      // border-radius: 35% 0px 0px 35%;
      padding: 20px 16px !important;

      &:hover {
        color: #cb0b0c;
        background: #ffcbcc;
      }

      &:before {
        content: " ";
        position: absolute;
        top: 0;
        left: 0;
        width: calc(100% - 1px);
        height: 12px;
        background: #fff;
        z-index: 20;
        // border-radius: 0px 0px 20px 0px;
      }

      &:after {
        content: " ";
        position: absolute;
        bottom: 0;
        left: 0;
        width: calc(100% - 1px);
        height: 12px;
        background: #fff;
        z-index: 20;
        margin-top: 68px;
        // border-radius: 0px 20px 0px 0px;
      }
    }
  }

  & .ant-menu-item-only-child .point,
  & ul.ant-menu-sub > .ant-menu-submenu > .ant-menu-submenu-title .point {
    /* padding: 0px 16px !important; */
    /* margin-inline-start: -40px !important; */

    background: transparent;

    & span {
      height: fit-content;
    }

    &:before {
      content: "•";
      color: rgba(110, 110, 110, 1);
      /* margin-inline-start: 8px; */
      margin-inline-end: 16px;
    }

    /* &:hover {
			background: #0000000f;
		} */
  }

  &
    ul.ant-menu-sub
    > .ant-menu-submenu.ant-menu-submenu-selected
    > .ant-menu-submenu-title:hover {
    color: #cb0b0c;
    background: #666cff0d;
  }

  /* & ul.ant-menu-sub > .ant-menu-submenu > .ant-menu-submenu-title::before {
		margin-inline-start: 16px;
	} */

  & .ant-menu-item-selected.ant-menu-item-only-child .point:before,
  .ant-menu-submenu-selected > .ant-menu-submenu-title .point:before {
    content: "•";
    scale: 1.4;
    color: #cb0b0c !important;
  }

  & .ant-menu-inline {
    background: transparent !important;
  }

  & .ant-menu-item-group-title {
    text-transform: capitalize;
    color: #212121;
    font-weight: 700;
    padding: 10px;
  }

  & .ant-menu-submenu-arrow {
    transform: rotate(-90deg);

    &:before {
      height: 1px;
    }

    &:after {
      height: 1px;
    }
  }

  &.ant-menu
    .ant-menu-submenu-open.ant-menu-submenu-inline
    > .ant-menu-submenu-title
    > .ant-menu-submenu-arrow {
    rotate: 180deg;
    margin-right: 6px;
  }

  /* & .ant-menu-item-only-child,
	ul.ant-menu-sub > .ant-menu-submenu > .ant-menu-submenu-title,
	.ant-menu-item {
		padding-left: 0px !important;
	} */

  & .ant-menu-item.ant-menu-item-selected.ant-menu-item-only-child:hover,
  ul.ant-menu-sub li.ant-menu-item.ant-menu-item-selected:hover {
    background: #666cff0d;
  }

  /* collapse menu */
  &.ant-menu-vertical {
    & .ant-menu-item {
      line-height: 20px !important;
    }

    & .ant-menu-submenu-title {
      display: flex;
      flex-wrap: wrap;
    }

    & .ant-menu-title-content {
      display: none !important;
    }
  }
`;

/**
 * MENU ITEMS
 */
type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: "group"
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const MenuSideMenuPrivateLayout = () => {
  const router = useRouter();
  const dataUser = storageCheck(USER);

  const isCpAdmin = dataUser?.user?.role === "cp-admin";
  const isPortalAdmin = dataUser?.user?.role === "portal-admin";
  const isKurator = dataUser?.user?.role === "kurator";
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    // !dataUser?.profile?.is_active
    //       ? true
    //       : dataUser?.user?.first_login
    //       ? true
    //       : false
    if (!dataUser?.profile?.is_active) {
      if (dataUser?.user?.first_login) {
        setIsDisabled(true);
      } else {
        setIsDisabled(false);
      }
    } else {
      setIsDisabled(false);
    }
  }, [dataUser])
  

  const itemsPortal: MenuItem[] = [
    getItem(
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>Home</Link>,
      DASHBOARD_HOME,
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>
        <Image
          src={HomeLogo}
          alt={"home-logo"}
          width={20}
          height={20}
          quality={100}
        />
      </Link>
    ),
    getItem(
      "Master Data",
      "/master-data",
      <Tooltip title="Master Data">
        <Image
          src={MasterDataLogo}
          alt={"master-data"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem("Category", "/master-data/category/", null, [
          getItem(
            <Link href={MASTER_DATA_CATEGORY_NEWS}>News</Link>,
            MASTER_DATA_CATEGORY_NEWS
          ),
          getItem(
            <Link href={MASTER_DATA_CATEGORY_EVENT}>Event</Link>,
            MASTER_DATA_CATEGORY_EVENT
          ),
          getItem(
            <Link href={MASTER_DATA_CATEGORY_COURSE}>Course</Link>,
            MASTER_DATA_CATEGORY_COURSE
          ),
          getItem(
            <Link href="/master-data/category/content-provider">
              Content Provider
            </Link>,
            "/master-data/category/content-provider"
          ),
        ]),
      ]
    ),
    getItem(
      "User Management",
      "/users",
      <Tooltip title="User Management">
        <Image
          src={UserManagementLogo}
          alt={"user-management"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem(
          <Link href={USER_MANAGEMENT_ROLE}>Role</Link>,
          USER_MANAGEMENT_ROLE
        ),
        getItem(
          <Link href={USER_MANAGEMENT_CMSADMIN}>CMS Admins</Link>,
          USER_MANAGEMENT_CMSADMIN
        ),
        getItem(
          <Link href={USER_MANAGEMENT_PORTAL_USERS}>Portal Users</Link>,
          USER_MANAGEMENT_PORTAL_USERS
        ),
      ]
    ),
    getItem(
      <Link href={isDisabled ? "#" : DASHBOARD_CONTENT_PROVIDER}>Content Provider</Link>,
      DASHBOARD_CONTENT_PROVIDER,
      <Link href={isDisabled ? "#" : DASHBOARD_CONTENT_PROVIDER}>
        <Image
          src={ContentProviderLogo}
          alt={"content-provider"}
          width={20}
          height={20}
          quality={100}
        />
      </Link>
    ),
    getItem(
      <Tooltip title="Management Content">Management Content</Tooltip>,
      "/management-portal",
      <Tooltip title="Management Content">
        <Image
          src={ManagementPortalLogo}
          alt={"management-portal"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        // getItem(<Link href={DASHBOARD_LEVEL}>Level</Link>, DASHBOARD_LEVEL),
        // getItem("Learning", "/learning/", null, [
        //   getItem("E - book", "7"),
        //   getItem("Glosarium", "8"),
        // ]),
        getItem("Course", "/management-portal/group-course/", null, [
          getItem(
            <Link href={DASHBOARD_COURSE}>All Course</Link>,
            DASHBOARD_COURSE
          ),
          getItem(
            <Link href={DASHBOARD_QUESTION}>Question Bank</Link>,
            DASHBOARD_QUESTION
          ),
          getItem(<Link href={DASHBOARD_TEST}>Test</Link>, DASHBOARD_TEST),
          // getItem(
          //   <Link href={DASHBOARD_COURSE_RSS}>RSS Course Feed</Link>,
          //   DASHBOARD_COURSE_RSS
          // ),
        ]),
        getItem("News", "/management-portal/group-news/", null, [
          getItem(<Link href={DASHBOARD_NEWS}>News</Link>, DASHBOARD_NEWS),
          getItem(
            <Link href={DASHBOARD_NEWS_RSS}>RSS News Feed</Link>,
            DASHBOARD_NEWS_RSS
          ),
        ]),
        getItem(<Link href={DASHBOARD_EVENT}>Event</Link>, DASHBOARD_EVENT),
        // getItem(<Link href={DASHBOARD_FORUM}>Forum</Link>, DASHBOARD_FORUM),
        getItem(<Link href={DASHBOARD_FAQ}>FAQ</Link>, DASHBOARD_FAQ),
        getItem(
          <Link href={DASHBOARD_EXTERNAL_CERTIFICATE}>External Certificate</Link>,
          DASHBOARD_EXTERNAL_CERTIFICATE
        ),
        getItem(
          <Link href={DASHBOARD_INVESTMENT_PARTNER}>Investment Partner</Link>,
          DASHBOARD_INVESTMENT_PARTNER
        ),
      ]
    ),
    getItem(
      <Link href={isDisabled ? "#" : DASHBOARD_GENERAL_CONTENT}>General Content</Link>,
      DASHBOARD_GENERAL_CONTENT,
      <Link href={isDisabled ? "#" : DASHBOARD_GENERAL_CONTENT}>
        <Image
          src={ManagementPortalLogo}
          alt={"general-content"}
          width={20}
          height={20}
          quality={100}
        />
      </Link>
    ),
  ];
  
  const itemsCP: MenuItem[] = [
    getItem(
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>Home</Link>,
      DASHBOARD_HOME,
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>
        <Image
          src={HomeLogo}
          alt={"home-logo"}
          width={20}
          height={20}
          quality={100}
        />
      </Link>
    ),
    getItem(
      "User Management",
      "/users",
      <Tooltip title="User Management">
        <Image
          src={UserManagementLogo}
          alt={"user-management"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem(
          <Link href={USER_MANAGEMENT_CMSADMIN}>CMS Admins</Link>,
          USER_MANAGEMENT_CMSADMIN
        ),
      ]
    ),
    getItem(
      "Content Management",
      "/management-portal",
      <Tooltip title="Content Management">
        <Image
          src={ManagementPortalLogo}
          alt={"management-portal"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem(<Link href={DASHBOARD_COURSE}>Course</Link>, DASHBOARD_COURSE),
        getItem("News", "/management-portal/group-news/", null, [
          getItem(<Link href={DASHBOARD_NEWS}>News</Link>, DASHBOARD_NEWS),
          getItem(
            <Link href={DASHBOARD_NEWS_RSS}>RSS News Feed</Link>,
            DASHBOARD_NEWS_RSS
          ),
        ]),
        getItem(<Link href={DASHBOARD_EVENT}>Event</Link>, DASHBOARD_EVENT),
      ]
    ),
  ];
  
  const itemsKurator: MenuItem[] = [
    getItem(
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>Home</Link>,
      DASHBOARD_HOME,
      <Link href={isDisabled ? "#" : DASHBOARD_HOME}>
        <Image
          src={HomeLogo}
          alt={"home-logo"}
          width={20}
          height={20}
          quality={100}
        />
      </Link>
    ),
    getItem(
      "Master Data",
      "/master-data",
      <Tooltip title="Master Data">
        <Image
          src={MasterDataLogo}
          alt={"master-data"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem("Category", "/master-data/category/", null, [
          getItem(
            <Link href={MASTER_DATA_CATEGORY_NEWS}>News</Link>,
            MASTER_DATA_CATEGORY_NEWS
          ),
          getItem(
            <Link href={MASTER_DATA_CATEGORY_EVENT}>Event</Link>,
            MASTER_DATA_CATEGORY_EVENT
          ),
          getItem(
            <Link href={MASTER_DATA_CATEGORY_COURSE}>Course</Link>,
            MASTER_DATA_CATEGORY_COURSE
          ),
        ]),
      ]
    ),
    getItem(
      <Tooltip title="Management Content">Management Content</Tooltip>,
      "/management-portal",
      <Tooltip title="Management Content">
        <Image
          src={ManagementPortalLogo}
          alt={"management-portal"}
          width={20}
          height={20}
          quality={100}
        />
      </Tooltip>,
      [
        getItem(<Link href={DASHBOARD_LEVEL}>Level</Link>, DASHBOARD_LEVEL),
        // getItem("Learning", "/learning/", null, [
        //   getItem("E - book", "7"),
        //   getItem("Glosarium", "8"),
        // ]),
        getItem("Course", "/management-portal/group-course/", null, [
          getItem(
            <Link href={DASHBOARD_COURSE}>All Course</Link>,
            DASHBOARD_COURSE
          ),
          getItem(
            <Link href={DASHBOARD_QUESTION}>Question Bank</Link>,
            DASHBOARD_QUESTION
          ),
          getItem(<Link href={DASHBOARD_TEST}>Test</Link>, DASHBOARD_TEST),
          // getItem(
          //   <Link href={DASHBOARD_COURSE_RSS}>RSS Course Feed</Link>,
          //   DASHBOARD_COURSE_RSS
          // ),
        ]),
        getItem(<Link href={DASHBOARD_NEWS}>News</Link>, DASHBOARD_NEWS),
        getItem(<Link href={DASHBOARD_EVENT}>Event</Link>, DASHBOARD_EVENT),
        // getItem(<Link href={DASHBOARD_FORUM}>Forum</Link>, DASHBOARD_FORUM),
        getItem(
          <Link href={DASHBOARD_EXTERNAL_CERTIFICATE}>External Certificate</Link>,
          DASHBOARD_EXTERNAL_CERTIFICATE
        ),
      ]
    ),
  ];

  return (
    <CustomMenus
      // defaultSelectedKeys={["dashboard"]}
      selectedKeys={[router.asPath]} // menu actived by asPath
      mode="inline"
      inlineIndent={8}
      items={isCpAdmin ? itemsCP : isKurator ? itemsKurator : itemsPortal}
      // defaultOpenKeys={["dashboard"]}
      disabled={isDisabled}
    />
  );
};

export default MenuSideMenuPrivateLayout;
