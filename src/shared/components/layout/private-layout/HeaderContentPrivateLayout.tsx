import RadioButton from "@/components/RadioLanguage";
import React, { useState } from "react";

// chakra ui
import { Avatar, AvatarBadge } from "@chakra-ui/react";
import { FaBell } from "react-icons/fa";

// antd
import { DASHBOARD_PROFILE } from "@/shared/constants/path";
import useLogoutUser from "@/shared/hooks/useLogoutUser";
import {
  DownOutlined,
  MenuUnfoldOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Badge, Col, Divider, Dropdown, Flex, Input, Row, Space } from "antd";
import Link from "next/link";

import ResponsiveSearch from "@/components/ResponsiveSearch";
import { USER } from "@/shared/constants/storageStatis";
import { storageCheck } from "@/shared/utils/clientStorageUtils";
import { useTranslation } from "react-i18next";
import TextResponsive from "../../TextResponsive";
import { useRouter } from "next/router";
import { COLORS } from "@/shared/styles/color";
import PaginationComponent from "../../PaginationComponent";
import { paramsToString } from "@/shared/utils/string";
import useGetNotification from "@/features/dashboard/hooks/useGetNotification";
import usePutNotification from "@/features/dashboard/hooks/usePutNotification";

type Props = {
  onClose: () => void;
};

const HeaderContentPrivateLayout = ({ onClose }: Props) => {
  const navigate = useRouter();
  const { t: translation } = useTranslation();
  const dataUser = storageCheck(USER);
  const { handleSignout } = useLogoutUser();
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const { fetchNotif } = useGetNotification(
    [],
    paramsToString({ page: currentPage, limit: 10, app: "cms" })
  );
  const { data: dataResultNotification } = fetchNotif;
  const { data: dataNotification } = dataResultNotification || {};
  console.log("dataNotification", dataNotification);

  const { handleReadNotification } = usePutNotification();

  const handleMarkAllRead = () => {
    dataNotification?.map((item: any) => {
      handleReadNotification(item.id, { is_read: true });
    });
  };

  React.useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth <= 550);
    };

    // Pertama kali, panggil handleResize untuk mengatur isSmallScreen
    handleResize();

    // Tambahkan event listener untuk memantau perubahan ukuran layar
    window.addEventListener("resize", handleResize);

    // Clean up event listener saat komponen dibongkar
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const items: MenuProps["items"] = [
    {
      label: (
        <Link rel="noopener noreferrer" href={DASHBOARD_PROFILE}>
          My Profile
        </Link>
      ),
      key: "0",
    },
    {
      label: (
        <Link onClick={handleSignout} rel="noopener noreferrer" href="">
          Logout
        </Link>
      ),
      key: "1",
    },
  ];

  return (
    <div className="navbar">
      <Row
        gutter={10}
        style={{
          alignItems: "center",
          border: "1px solid #8692a6",
          borderRadius: 10,
          padding: 12,
        }}
        justify={"space-between"}
      >
        {/* <div className="navbar-search">
        <InputGroup>
          <InputLeftElement pointerEvents="none" pt="22px">
            <SearchIcon color="#999" w="18px" h="18px" />
          </InputLeftElement>
          <Input
            placeholder="Search Here"
            height="44px"
            size="lg"
            _focus={{
              boxShadow: "none",
              outline: "none",
            }}
          />
        </InputGroup>
        <>
          <Input size="large" placeholder="Search" prefix={<SearchOutlined />} />
        </>
      </div> */}
        {window.innerWidth >= 950 ? (
          <div />
        ) : (
          <MenuUnfoldOutlined onClick={onClose} />
        )}
        <Flex align="center" justify="flex-end" gap={10}>
          <div className="responsive-search">
            <ResponsiveSearch />
          </div>
          <Dropdown
            className="navbar-notification"
            placement="bottom"
            dropdownRender={(menu) => (
              <Flex
                justify="space-between"
                style={{
                  backgroundColor: "white",
                  flexDirection: "column",
                  padding: 10,
                  width: 400,
                }}
              >
                <Row
                  style={{
                    justifyContent:
                      dataNotification && dataNotification.length > 0
                        ? "space-between"
                        : "center",
                    alignItems: "center",
                    marginBottom: 10,
                  }}
                  gutter={[16, 16]}
                >
                  <Col>
                    <TextResponsive
                      bigScreen={18}
                      mobile={14}
                      isSpan
                      fontWeight={700}
                      desktop={16}
                      color={COLORS.BLACK}
                    >
                      {translation("Notifications")}
                    </TextResponsive>
                  </Col>
                  {dataNotification && dataNotification.length > 0 && (
                    <Col
                      style={{ cursor: "pointer" }}
                      onClick={handleMarkAllRead}
                    >
                      <TextResponsive
                        bigScreen={16}
                        mobile={12}
                        isSpan
                        fontWeight={400}
                        desktop={14}
                        color={COLORS.PRIMARY}
                      >
                        {translation("Mark_All")}
                      </TextResponsive>
                    </Col>
                  )}
                </Row>
                <Divider style={{ margin: 0 }} />
                <Row
                  style={{
                    width: "100%",
                    overflowY: "scroll",
                    maxHeight: 300,
                    marginTop: 10,
                    padding: "15px 10px",
                    // Hide scrollbar for WebKit-based browsers (Chrome, Safari, etc.)
                    scrollbarWidth: "none", // For Firefox
                    msOverflowStyle: "none", // For Internet Explorer and Edge
                  }}
                >
                  {dataNotification && dataNotification.length === 0 && (
                    <Col span={24}>
                      <TextResponsive
                        bigScreen={16}
                        mobile={12}
                        isSpan
                        fontWeight={400}
                        desktop={14}
                        color={COLORS.WHITE}
                      >
                        {translation("No_Notification")}
                      </TextResponsive>
                    </Col>
                  )}
                  {dataNotification?.map((item: any, index: any) => {
                    return (
                      <Col
                        style={{
                          backgroundColor: COLORS.WHITE,
                          justifyContent: "space-between",
                          padding: 10,
                          cursor: "pointer",
                          boxShadow: "0 8px 16px rgba(0, 0, 0, 0.05)",
                          marginBottom: 15,
                        }}
                        onClick={() => {
                          handleReadNotification(item.id, {
                            is_read: true,
                          });
                          console.log("url notif", item.url);
                          if (item.url) {
                            navigate.push(item.url);
                          }
                        }}
                        key={item.id}
                        span={24}
                      >
                        {item.is_read ? (
                          <TextResponsive
                            bigScreen={16}
                            mobile={12}
                            isSpan
                            fontWeight={400}
                            desktop={14}
                            color={COLORS.BLACK}
                          >
                            {item.message}
                          </TextResponsive>
                        ) : (
                          <TextResponsive
                            bigScreen={16}
                            mobile={12}
                            isSpan
                            fontWeight={800}
                            desktop={14}
                            color={COLORS.BLACK}
                          >
                            {item.message}
                          </TextResponsive>
                        )}
                        {/* <Space style={{ flexDirection: "row" }}>
                                <span className="textfs14-fw400-gray">
                                  {moment(item.created_at).fromNow()}
                                </span>
                                <LuDot />
                                <span className="textfs14-fw600-gray">
                                  {item.category}
                                </span>
                              </Space> */}
                      </Col>
                    );
                  })}
                </Row>
                {dataNotification && dataNotification.length > 0 && (
                  <Col span={24} style={{ marginTop: "0 auto" }}>
                    <PaginationComponent
                      currentPage={currentPage}
                      setCurrentPage={setCurrentPage}
                      total={dataResultNotification.records}
                      limit={dataResultNotification.limit}
                    />
                  </Col>
                )}
              </Flex>
            )}
          >
            {dataResultNotification.unread > 0 ? (
              <Badge count={dataResultNotification.unread}>
                <FaBell color={COLORS.PRIMARY} size={isSmallScreen ? 15 : 25} />
              </Badge>
            ) : (
              <FaBell color={COLORS.PRIMARY} size={isSmallScreen ? 15 : 25} />
            )}
          </Dropdown>
          <RadioButton />
          <Dropdown menu={{ items }} placement="bottom">
            <a onClick={(e) => e.preventDefault()}>
              <div className="dropdown-content">
                <div className="dropdown-avatar">
                  <Avatar size="lg">
                    <AvatarBadge boxSize="1.25em" bg="green.500" />
                  </Avatar>
                </div>
                <div className="dropdown-profile">
                  <h3>{dataUser?.user?.email}</h3>
                  <h4>{dataUser?.user?.role}</h4>
                </div>
                <DownOutlined className="down-outlined-account" />
              </div>
            </a>
          </Dropdown>
        </Flex>
      </Row>
    </div>
  );
};

export default HeaderContentPrivateLayout;
