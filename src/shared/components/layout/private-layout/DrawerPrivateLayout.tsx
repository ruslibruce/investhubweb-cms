import InvestHubLogo from "@/shared/images/logo/logo_investhub.webp";
import { DoubleLeftOutlined, DoubleRightOutlined } from "@ant-design/icons";
import { Button, Drawer, theme } from "antd";
import Image from "next/image";
import styled from "styled-components";
import MenuSideMenuPrivateLayout from "./MenuSideMenuPrivateLayout";

const CustomDrawer = styled(Drawer)`
  scrollbar-color: #bebfcf #f8f8f8;
  scrollbar-width: thin;
  &::-webkit-scrollbar {
    width: 0.5vw;
    height: 0.5vw;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #bebfcf;
    border-radius: 0px;
  }
  &::-webkit-scrollbar-track {
    background-color: #f8f8f8;
  }
`;

type Props = {
  open: boolean;
  onClose: () => void;
};

const DrawerPrivateLayout = ({ open, onClose }: Props) => {

  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <CustomDrawer
      open={open}
      closeIcon={null}
      placement="left"
      style={{
        background: colorBgContainer,
        overflowY: "auto",
        overflowX: "hidden",
        position: "fixed",
        left: 0,
        top: 0,
        bottom: 0,
        zIndex: 10,
        padding: "25px 0px 10px 4px",
        userSelect: "none",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBlockEnd: "16px",
          marginInlineEnd: "16px",
        }}
      >
        {/* LOGO HERE */}
        <Image
          src={InvestHubLogo}
          alt={"logo-cms"}
          width={150}
          height={40}
          quality={100}
          style={{ marginLeft: "28px" }}
        />

        {/* BUTTON COLLAPSE */}
        <span>
          <Button
            type="text"
            size="small"
            icon={<DoubleLeftOutlined />}
            onClick={onClose}
            style={{
              fontSize: "16px",
            }}
          />
        </span>
      </div>

      {/* SIDE MENU */}
      <MenuSideMenuPrivateLayout />
    </CustomDrawer>
  );
};

export default DrawerPrivateLayout;
