import React, { useState } from "react";

// antd
import { Typography, Skeleton } from "antd";

// hooks
// import useGetListSettings from 'src/features/konfigurasi/settings/hooks/useGetListSettings';

const FooterContentPrivateLayout = () => {
  // hooks
  // const { isLoading, stateListSettings } = useGetListSettings(
  // 	'FOOTER_CMS',
  // 	true,
  // );
  // eslint-disable-next-line no-unused-vars
  const [rendered, setRendered] = useState(false);

  React.useEffect(() => {}, []);

  return (
    <footer
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "15px 30px"
      }}>
      <Typography.Title level={5} style={{ fontWeight: 300, margin: 0 }}>
        ©2022{" "}
        {rendered ? (
          <Skeleton.Input
            active={true}
            style={{
              width: "100px",
              height: "30px",
              minWidth: "unset",
              marginTop: "-5px"
            }}
          />
        ) : (
          <span>Bursa Efek Indonesia</span>
        )}
      </Typography.Title>

      <Typography.Text>{"InvestHub"} V.1.0.0</Typography.Text>
    </footer>
  );
};

export default FooterContentPrivateLayout;
