import { Spin } from "antd";
import GIFGraph from "@/shared/images/gif/graph.gif";
import Image from "next/image";
import { useTranslation } from "next-i18next";

type Props = {
  isFullScreen?: boolean;
  size?: "default" | "small" | "large";
};

export default function LoaderSpinGif({
  isFullScreen,
  size = "default",
}: Props) {
  const { t: translate } = useTranslation();
  return (
    <Spin
      fullscreen={isFullScreen}
      size={size}
      indicator={<Image src={GIFGraph} alt="loading" />}
      tip={isFullScreen ? translate("Loading") : null}
    />
  );
}
