import Image from "next/image";
import iconEmpty from "@/shared/images/emptyData/icon_empty_table.webp";
import { Space, Spin } from "antd";
import { useTranslation } from "next-i18next";
import LoaderSpinGif from "./LoaderSpinGif";

function NoData({ loading }: { loading?: boolean }) {
  const { t: translate } = useTranslation();
  return (
    <Space
      style={{ minWidth: "60dvw", width: "60dvw", alignItems: "center" }}
      direction="vertical"
    >
      {loading ? (
        <LoaderSpinGif size="large" />
      ) : (
        <>
          <Image src={iconEmpty} alt="empty-data" />
          <div style={{ height: 10 }} />
          <span style={{ color: "#9f0e0f", fontWeight: 24 }}>
            {translate("SorryNoData")}
          </span>
          <div style={{ height: 30 }} />
        </>
      )}
    </Space>
  );
}

export default NoData;
