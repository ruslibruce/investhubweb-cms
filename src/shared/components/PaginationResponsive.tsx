import React, { useState } from "react";
import { Pagination, PaginationProps } from "antd";
type PaginationResponsiveProps = {
  total: number;
  setCurrentPage: (value: any) => void;
  currentPage: number;
  limit?: number;
}

export default function PaginationResponsive({
  total,
  currentPage,
  setCurrentPage,
  limit = 10,
}: PaginationResponsiveProps) {
  const onChange: PaginationProps["onChange"] = (page) => {
    console.log("Pagination", page);
    setCurrentPage(page);
  };

  return (
    <Pagination
      size="small"
      simple
      className="bottom-pagination-responsive"
      total={total}
      current={currentPage}
      onChange={onChange}
      pageSize={limit}
      showSizeChanger={false}
    />
  );
} // Path: src/shared/utils/pagination.ts
