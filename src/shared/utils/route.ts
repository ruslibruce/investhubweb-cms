import {
  AUTH_ROUTES,
  CP_ROUTES,
  KURATOR_ROUTES,
  PORTAL_ADMIN_ROUTES,
  PRIVATE_ROUTES,
} from "../constants/path";

export const checkIsPrivateRoute = (currentRoute: string): boolean => {
  return Boolean(
    PRIVATE_ROUTES.find((r: string) => {
      return currentRoute.includes(r);
    })
  );
};

export const checkIsAuthRoute = (currentRoute: string): boolean => {
  return Boolean(
    AUTH_ROUTES.find((r: string) => {
      return r.includes(currentRoute);
    })
  );
};

export const checkIsKuratorRoute = (currentRoute: string): boolean => {
  return Boolean(
    KURATOR_ROUTES.find((r: string) => {
      return currentRoute.includes(r);
    })
  );
};

export const checkIsCPRoute = (currentRoute: string): boolean => {
  return Boolean(
    CP_ROUTES.find((r: string) => {
      return currentRoute.includes(r);
    })
  );
};

export const checkIsPortalAdminRoute = (currentRoute: string): boolean => {
  return Boolean(
    PORTAL_ADMIN_ROUTES.find((r: string) => {
      return currentRoute.includes(r);
    })
  );
};
