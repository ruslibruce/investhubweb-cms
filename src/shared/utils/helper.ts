import dynamic from "next/dynamic";
import { CATEGORY_STATUS } from "../constants/storageStatis";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
function createAction(type: any, payload: any) {
  return {
    type,
    payload,
  };
}
function validateEmail(email: string) {
  return String(email)
    .toLowerCase()
    .match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
}
function validateUrl(url: string) {
  const urlPattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-zA-Z\\d]([a-zA-Z\\d-]*[a-zA-Z\\d])*)\\.)+[a-zA-Z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-zA-Z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-zA-Z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-zA-Z\\d_]*)?$', 'i' // fragment locator
  );
  return urlPattern.test(url);
}

function isYoutubeUrl(url: string) {
  const youtubePattern =
    /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/; // fragment locator
  return youtubePattern.test(url);
}

function validatePhoneNumber(noTlpn: string) {
  // const pattern = /^[0-9]{9,15}$/;
  const pattern = /^08[0-9]{8,11}$/;
  return pattern.test(noTlpn);
  // return true;
}
function normalisasiNomorHP(phone: string) {
  phone = String(phone).trim();
  if (phone.startsWith("+62")) {
    phone = "0" + phone.slice(3);
  } else if (phone.startsWith("62")) {
    phone = "0" + phone.slice(2);
  }
  return phone.replace(/[- .]/g, "");
}

function isHTML(str: string) {
  const htmlRegex =
    /<(br|basefont|hr|input|source|frame|param|area|meta|!--|col|link|option|base|img|wbr|!DOCTYPE).*?>|<(a|abbr|acronym|address|applet|article|aside|audio|b|bdi|bdo|big|blockquote|body|button|canvas|caption|center|cite|code|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frameset|head|header|hgroup|h1|h2|h3|h4|h5|h6|html|i|iframe|ins|kbd|keygen|label|legend|li|map|mark|menu|meter|nav|noframes|noscript|object|ol|optgroup|output|p|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video).*?<\/\2>/i;
  return htmlRegex.test(str);
}

function checkIsEmptyObject(object: any) {
  return Object.values(object).some((x) => x === null || x === "");
}

function checkIsBooleanObject(object: any, value: boolean) {
  return Object.values(object).some((x) => x === value);
}

const getStatusColor = (status: string) => {
  const data = CATEGORY_STATUS.find((item) => item.value == status);
  return data?.color;
};

const isEmptyContent = (str: string) => {
  const regex = /^(\s*<p><br><\/p>\s*)+$/;
  return regex.test(str);
};

const ReactQuillNoSSR = dynamic(() => import("react-quill"), {
  ssr: false,
});

const arrayContainsEmptyString = (array: []) => {
  return array.some((object: {}) =>
    Object.values(object).some(
      (value) => typeof value === "string" && value.trim() === ""
    )
  );
};

const handleOpenLink = async (value: string) => {
  window.open(value, "_blank");
};

const regexUrl = () => {
  return /^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})$/;
};

const regexEmail = () => {
  return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
};

const regexPhoneNumber = () => {
  return /^08[0-9]{8,11}$/;
};

const regexYoutubeUrl = () => {
  return /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/;
};

const regexFacebookUrl = () => {
  return /^(https?:\/\/)?(www\.)?(facebook\.com)\/.+$/;
};

const regexInstagramUrl = () => {
  return /^(https?:\/\/)?(www\.)?(instagram\.com)\/.+$/;
};

const regexTwitterUrl = () => {
  return /^(https?:\/\/)?(www\.)?(twitter\.com)\/.+$/;
};

const regexTiktokUrl = () => {
  return /^(https?:\/\/)?(www\.)?(tiktok\.com)\/.+$/;
};

export {
  arrayContainsEmptyString,
  checkIsBooleanObject,
  checkIsEmptyObject,
  createAction,
  getStatusColor,
  handleOpenLink,
  isEmptyContent,
  isHTML,
  isYoutubeUrl,
  normalisasiNomorHP,
  ReactQuillNoSSR,
  regexEmail,
  regexPhoneNumber,
  regexUrl,
  sleep,
  validateEmail,
  validatePhoneNumber,
  validateUrl,
  regexYoutubeUrl,
  regexFacebookUrl,
  regexInstagramUrl,
  regexTwitterUrl,
  regexTiktokUrl,
};
