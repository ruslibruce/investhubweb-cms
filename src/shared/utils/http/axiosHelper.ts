import AxiosInstance from "@/shared/api/axiosInstance";
import { AxiosRequestConfig } from "axios";
import { handlingErrors } from "../handlingErrors";

async function getData({
  endpoint, config
}: { endpoint: string, config?: AxiosRequestConfig<any> }): Promise<any> {
  try {
    const response = await AxiosInstance.get(endpoint, config);
    return response;
  } catch (error: any) {
    handlingErrors({ error, text: "Get Data" });
    throw error;
  }
}

async function postData({ 
  endpoint, data, config
}: { endpoint: string, data: any, config?: AxiosRequestConfig<any> }) {
  try {
    const response = await AxiosInstance.post(
      endpoint,
      data,
      config
    );
    return response;
  } catch (error: any) {
    handlingErrors({ error, text: "Post Data" });
    throw error;
  }
}

async function putData({ 
  endpoint, data, config
}: { endpoint: string, data: any, config?: AxiosRequestConfig<any> }) {
  try {
    const response = await AxiosInstance.put(
      endpoint,
      data,
      config
    );
    return response;
  } catch (error: any) {
    handlingErrors({ error, text: "Put Data" });
    throw error;
  }
}

async function deleteData({
  endpoint, config
}: { endpoint: string, config?: AxiosRequestConfig<any> }): Promise<any> {
  try {
    const response = await AxiosInstance.delete(endpoint, config);
    return response;
  } catch (error: any) {
    handlingErrors({ error, text: "Delete Data" });
    throw error;
  }
}

export {
  getData,
  postData,
  putData,
  deleteData,
};