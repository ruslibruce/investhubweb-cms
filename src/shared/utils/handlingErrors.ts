import { notification } from "antd";
import Router from "next/router";
import { AUTH_ROUTES, DASHBOARD_LOGIN } from "../constants/path";
import { storageRemove } from "./clientStorageUtils";
import { USER } from "../constants/storageStatis";
import { i18n } from "next-i18next";

type props = {
  error: any;
  text: string;
};

export function handlingErrors({ error, text }: props) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log("error.response.data", error.response.data);
    console.log("error.response.status", error.response.status);
    console.log("error.response.headers", error.response.headers);
    if (error.response.status === 401) {
      let check = false;
      AUTH_ROUTES.forEach((route) => {
        if (Router.asPath === route) {
          check = true;
        }
      });
      console.log("check asPath", check);
      console.log('Router.asPath', Router.asPath)
      if (check) {
        notification.error({
          message: error.response?.data?.message ?? `${text} Failed`,
          description: error?.message,
        });
        return;
      }
      storageRemove(USER);
      Router.replace({
        pathname: DASHBOARD_LOGIN,
        query: { loginsession: true },
      });
      return;
    }
    notification.error({
      message: error.response?.data?.message ?? `${text} Failed`,
      description: error?.message,
    });
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log("error.request", error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log("Error message", error.message);
    if (
      error.message === "Unauthorized" ||
      error.message === "Unauthenticated"
    ) {
      storageRemove(USER);
      Router.replace({
        pathname: DASHBOARD_LOGIN,
        query: { loginsession: true },
      });
      // notification.error({
      //   message: i18n?.t("EndSession"),
      //   description: i18n?.t("EndSessionDesc"),
      // });
      return;
    }
    notification.error({
      message: `${text} Failed`,
      description: error.message ?? `Your ${text} Failed`,
    });
  }
  console.log("error.config", error.config);
  console.log("error", error);
}
