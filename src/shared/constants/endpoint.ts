/**
 * BASE URL
 */
const BASE_URL: string = `${process.env.NEXT_PUBLIC_HOST_NAME}`;

/**
 * ENDPOINTS Template
 */
const LOGIN = "/api-iam/cms-login";
const LOGOUT = "api-iam/logout";
const REGISTER = "/register";

// ENDPOINT
export const GENERAL_CONTENT = (params: string = ""): string =>
  `/api-portal/general-content${params}`;
const INVESTMENT_PARTNER = (params: string = ""): string =>
  `/api-portal/investment-partner${params}`;
const INVESTMENT_PARTNER_DETAIL = (params: string = ""): string =>
  `/api-portal/investment-partner${params}`;
const FAQ = (params: string = ""): string => `/api-portal/faqs${params}`;
const CERTIFICATE = (params: string = ""): string =>
  `/api-lms/external-certificate${params}`;
const NEWS = (params: string = ""): string => `/api-portal/news${params}`;
const DELETE_FAQ = (params: string = ""): string => `/api-portal/faqs${params}`;
const EDIT_FAQ = (params: string = ""): string =>
  `/api-portal/faqs/id/${params}`;
const SEARCH_FAQ = (params: string = ""): string =>
  `/api-portal/search-faq${params}`;
const NEWS_RSS = (params: string = ""): string =>
  `/api-portal/news-rss${params}`;
const EDIT_CATEGORY_NEWS = (params: string = ""): string =>
  `/api-portal/news-category${params}`;
const FETCH_RSS_NEWS = (params: string = ""): string =>
  `api-portal/rss-feed${params}`;
const EDIT_CATEGORY_EVENT = (params: string = ""): string =>
  `/api-portal/event-category${params}`;
const DELETE_CATEGORY_NEWS = (params: string = ""): string =>
  `/api-portal/news-category${params}`;
const CATEGORY_NEWS = (params: string = ""): string =>
  `/api-portal/news-category`;
const UPADATE_EVENT = (params: string = ""): string =>
  `/api-portal/event/id/${params}`;
const DELETE_EVENT = (params: string = ""): string =>
  `/api-portal/event/id/${params}`;
const DETAIL_EVENT = (params: string = ""): string =>
  `/api-portal/event/id/${params}`;
const EVENT = (params: string = ""): string => `/api-portal/event${params}`;
const CATEGORY_EVENT = (params: string = ""): string =>
  `/api-portal/event-category`;
const CATEGORY_INVESTMENT = (params: string = ""): string =>
  `/api-portal/investment-partner-category`;

const CMS_ADMIN = (params: string = ""): string => "/api-iam/cms-users";
const PORTAL_USERS = (params: string = ""): string => "/api-iam/portal-users";
const USER_API = (params: string = ""): string => `/api-iam/user/${params}`;
const PORTAL_USERS_COLLECTION = (params: string = ""): string =>
  `/api-lms/collection/user/${params}`;
const PORTAL_USERS_COLLECTION_DETAIL = (params: string = ""): string =>
  `/api-lms/collection-content/id/${params}`;

const CONTENT_PROVIDER = (params: string = ""): string =>
  `/api-iam/content-provider${params}`;
const CONTENT_PROVIDER_INVITE = (params: string = ""): string =>
  "/api-iam/create-user";
const CONTENT_PROVIDER_TYPE = (params: string = ""): string =>
  "api-iam/content-provider-type";

const TERMS = (params: string = ""): string =>
  "/api-iam/content-provider/agree";

const COURSE = (params: string = ""): string => `/api-lms/course${params}`;
const COURSE_COMMENT = (params: string = ""): string =>
  `/api-lms/course-comment${params}`;
const COURSE_REPORT = (params: string = ""): string =>
  `/api-lms/course-report${params}`;
const COURSE_LIKE = (params: string = ""): string =>
  `/api-lms/course-like${params}`;
const COURSE_CATEGORY = (params: string = ""): string =>
  `/api-lms/category${params}`;

const LEVEL = (params: string = ""): string => `/api-lms/level${params}`;
const LEVEL_DETAIL = (params: string = ""): string =>
  `/api-lms/level-requirement/${params}`;

const CHANGE_PASSWORD = (params: string = ""): string =>
  `/api-iam/change-password`;

const AUDIT_TRAIL_LOG = (params: string = ""): string =>
  `/api-iam/audit-trail-logs${params}`;

const EXPORT_AUDIT_TRAIL_LOG = (params: string = ""): string =>
  `/api-iam/audit-trail-logs/export${params}`;

const COUNTRY = (params: string = ""): string => "/api-portal/countries";

const UPDATE_USERS = (params: string = ""): string => `/api-iam/update-profile`;

const FORGOT_PASSWORD = (params: string = ""): string =>
  `/api-iam/forgot-password`;

const VALIDATE_RESET_TOKEN = (params: string = ""): string =>
  `/api-iam/validate-reset-token`;

const RESET_PASSWORD = (params: string = ""): string =>
  `/api-iam/reset-password`;

const ID_PROVINCE = (params: string = ""): string =>
  `/api-portal/provinces/countriesId/ID`;

const QUESTION = (params: string = ""): string => `/api-lms/question${params}`;

const TEST = (params: string = ""): string => `/api-lms/test${params}`;

const TEST_QUESTION = (params: string = ""): string =>
  `/api-lms/test-question${params}`;

const TEST_TYPE = (params: string = ""): string =>
  `/api-lms/test-type${params}`;

const CATEGORY_CONTENT_TYPE = (params: string = ""): string =>
  "/api-lms/content-type";

const NOTIFICATION = (params: string = ""): string =>
  `/api-iam/notification${params}`;

const SUBTITLE = (params: string = ""): string =>
  `/api-lms/content-subtitle${params}`;

const PROFILE = (params: string = ""): string => "/api-iam/profile-info";

export {
  AUDIT_TRAIL_LOG,
  BASE_URL,
  CATEGORY_CONTENT_TYPE,
  CATEGORY_EVENT,
  CATEGORY_INVESTMENT,
  CATEGORY_NEWS,
  CERTIFICATE,
  CHANGE_PASSWORD,
  CMS_ADMIN,
  CONTENT_PROVIDER,
  CONTENT_PROVIDER_INVITE,
  CONTENT_PROVIDER_TYPE,
  COUNTRY,
  COURSE,
  COURSE_CATEGORY,
  COURSE_COMMENT,
  COURSE_LIKE,
  COURSE_REPORT,
  DELETE_CATEGORY_NEWS,
  DELETE_FAQ,
  EDIT_CATEGORY_EVENT,
  EDIT_CATEGORY_NEWS,
  EDIT_FAQ,
  EVENT,
  EXPORT_AUDIT_TRAIL_LOG,
  FAQ,
  FETCH_RSS_NEWS,
  FORGOT_PASSWORD,
  ID_PROVINCE,
  INVESTMENT_PARTNER,
  INVESTMENT_PARTNER_DETAIL,
  LEVEL,
  LEVEL_DETAIL,
  LOGIN,
  LOGOUT,
  NEWS,
  NEWS_RSS,
  NOTIFICATION,
  PORTAL_USERS,
  PORTAL_USERS_COLLECTION,
  PORTAL_USERS_COLLECTION_DETAIL,
  PROFILE,
  QUESTION,
  REGISTER,
  RESET_PASSWORD,
  SEARCH_FAQ,
  SUBTITLE,
  TERMS,
  TEST,
  TEST_QUESTION,
  TEST_TYPE,
  UPDATE_USERS,
  USER_API,
  VALIDATE_RESET_TOKEN,
};
