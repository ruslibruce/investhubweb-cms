const USER = "user-cms-idx";

const BUTTON_COLOR = [
  {
    id: 1,
    color: "#9F0E0F",
  },
  {
    id: 2,
    color: "#23B56B",
  },
  {
    id: 3,
    color: "#0097C7",
  },
  {
    id: 4,
    color: "#D0C802",
  },
  {
    id: 5,
    color: "#FFA500",
  },
  {
    id: 6,
    color: "#80669d",
  },
  {
    id: 7,
    color: "#CB0125",
  },
];

const CATEGORY_STATUS = [
  {
    value: "all",
    label: "All",
    color: "#9F0E0F",
  },
  {
    value: "published",
    label: "Published",
    color: "#23B56B",
  },
  {
    value: "pending",
    label: "Pending",
    color: "#D0C802",
  },
  {
    value: "unpublished",
    label: "Unpublished",
    color: "#FFA500",
  },
  {
    value: "draft",
    label: "Draft",
    color: "#0097C7",
  },
  {
    value: "rejected",
    label: "Rejected",
    color: "#CB0125",
  },
];

const CERTIFICATE_STATUS = [
  {
    value: "",
    label: "All",
  },
  {
    value: "true",
    label: "Verified",
  },
  {
    value: "false",
    label: "Unverified",
  },
];

const TOOLBAR_OPTION_QUILL = {
  toolbar: [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    ["link", "image", "video"], // add's image support
    ["clean"], // remove formatting button
  ],
};

const VIDEO_FORMAT = ".mp4,.avi,.flv,.wmv,.mov,.webm,.mkv";
const IMAGE_FORMAT = ".jpg,.jpeg,.png,.gif,.bmp,.svg,.webp";
const DOCUMENT_FORMAT = ".pdf";
const AUDIO_FORMAT = ".mp3,.wav,.wma,.ogg,.aac,.flac";
const SUBTITLE_FORMAT = ".vtt";

export {
  USER,
  BUTTON_COLOR,
  CATEGORY_STATUS,
  TOOLBAR_OPTION_QUILL,
  CERTIFICATE_STATUS,
  VIDEO_FORMAT,
  IMAGE_FORMAT,
  DOCUMENT_FORMAT,
  AUDIO_FORMAT,
  SUBTITLE_FORMAT,
};
// Path: src/shared/constants/storage.ts
