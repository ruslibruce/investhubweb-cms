import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const MenuTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  colorPrimary: "#9F0E0F",
  controlItemBgActive: "transparent"
};
