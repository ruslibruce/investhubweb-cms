import { ComponentToken } from 'antd/es/app/style';
import { AliasToken } from 'antd/es/theme/internal';

export const RadioTheme: Partial<ComponentToken> & Partial<AliasToken> = {
  paddingXS: 9,
  marginXS: 2,
};
