import { AliasToken } from 'antd/es/theme/internal';

export const InputTheme: Partial<{}> & Partial<AliasToken> = {
  borderRadius: 8,
  controlHeight: 60,
  colorBorder: '#E5E5F0',
  colorBgContainer: 'transparent',
  colorTextPlaceholder: '#BABABA',
  colorPrimaryHover: 'rgba(247, 10, 26, 0.8)',
  colorBgContainerDisabled: '#F8F8F8',
  controlOutline: 'transparent',
};
