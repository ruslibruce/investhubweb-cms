// next
import localFont from "next/font/local";

export const PFDinTextPro = localFont({
  src: [
    {
      path: "../font/PFDinTextPro-Thin.otf",
      weight: "100",
      style: "normal",
    },
    {
      path: "../font/PFDinTextPro-Light.otf",
      weight: "300",
      style: "normal",
    },
    {
      path: "../font/PFDinTextPro-Regular.otf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../font/PFDinTextPro-Medium.otf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../font/PFDinTextPro-Bold.otf",
      weight: "700",
      style: "normal",
    },
    {
      path: "../font/PFDinTextPro-ThinItalic.otf",
      weight: "100",
      style: "italic",
    },
    {
      path: "../font/PFDinTextPro-LightItalic.otf",
      weight: "300",
      style: "italic",
    },
    {
      path: "../font/PFDinTextPro-Italic.otf",
      weight: "400",
      style: "italic",
    },
    {
      path: "../font/PFDinTextPro-MedItalic.otf",
      weight: "500",
      style: "italic",
    },
    {
      path: "../font/PFDinTextPro-BoldItal.otf",
      weight: "700",
      style: "italic",
    },
  ],
  display: "swap",
});

export const OpenFont = localFont({
  src: [
    {
      path: "../font/OpenSans-Light.ttf",
      weight: "300",
      style: "normal",
    },
    {
      path: "../font/OpenSans-Regular.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../font/OpenSans-Medium.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../font/OpenSans-SemiBold.ttf",
      weight: "600",
      style: "normal",
    },
    {
      path: "../font/OpenSans-Bold.ttf",
      weight: "700",
      style: "normal",
    },
    {
      path: "../font/OpenSans-ExtraBold.ttf",
      weight: "800",
      style: "normal",
    },
    {
      path: "../font/OpenSans-LightItalic.ttf",
      weight: "300",
      style: "normal",
    },
    {
      path: "../font/OpenSans-Italic.ttf",
      weight: "400",
      style: "italic",
    },
    {
      path: "../font/OpenSans-MediumItalic.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../font/OpenSans-SemiBoldItalic.ttf",
      weight: "600",
      style: "italic",
    },
    {
      path: "../font/OpenSans-BoldItalic.ttf",
      weight: "700",
      style: "italic",
    },
    {
      path: "../font/OpenSans-ExtraBoldItalic.ttf",
      weight: "800",
      style: "italic",
    }
  ],
});
