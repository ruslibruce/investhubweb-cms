import { useRouter } from "next/router";
import { storageRemove } from "@/shared/utils/clientStorageUtils";
import { DASHBOARD_LOGIN } from "../constants/path";
import { USER } from "../constants/storageStatis";

const useLogoutUser = () => {
  const navigate = useRouter();

  // func
  const handleSignout = () => {
    storageRemove(USER);
    navigate.push(DASHBOARD_LOGIN);
  };

  return {
    handleSignout
  };
};

export default useLogoutUser;
