// antd
import { Typography } from "antd";

// next
import ImageNext from "next/image";
import { useRouter } from "next/router";

// image
import Bg500Page from "@/shared/images/bg-500-page.svg";

// helper
import CustomButton from "@/shared/components/CustomButton";
import { DASHBOARD_HOME } from "@/shared/constants/path";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import MetaHead from "@/shared/components/MetaHead";

// component

function Custom505() {
  // router
  const navigate = useRouter();

  // func
  const toDashboard = () => {
    navigate.push(DASHBOARD_HOME);
  };

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        position: "relative",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MetaHead
        title={"Page Not Found"}
        description={"Not Found Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />

      <ImageNext
        style={{ zIndex: 4, objectFit: "contain", width: "100%", height: "70%" }}
        src={Bg500Page}
        alt="backgound-404-page"
        placeholder="blur"
        blurDataURL="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDACgcHiMeGSgjISMtKygwPGRBPDc3PHtYXUlkkYCZlo+AjIqgtObDoKrarYqMyP/L2u71////m8H////6/+b9//j/2wBDASstLTw1PHZBQXb4pYyl+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj/wAARCAEMAhcDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwX/xAAVEAEBAAAAAAAAAAAAAAAAAAAAEf/EABcBAQEBAQAAAAAAAAAAAAAAAAABAwL/xAAVEQEBAAAAAAAAAAAAAAAAAAAAEf/aAAwDAQACEQMRAD8A84BsAAAAAKAAAAAAACgAAAAAAIqAAAAAgqAIqAAIIACAAiKIiIqAgqAIqIADlQBAAAAAAAAAVFAAQABVAAAB2FG6IKAigAAAAAKCooAAAAAAAAAAAIoCCoIIoCAAgqICKgCKgIKiCCoIiKAgCCCoigCAAgAAAAAoACACigAACgADuA2AFBBQEFAAAAABQEUAAAAAAAEUBBQEABBQERQEFRBBUEQVAQVEEFQERpARGkREFQEFEEFEVBRBBQAFBFAAFBFBVAAAAdwGoAoIKAgoAAAAoCgIKAigAAAigIKAgqAAIIKgCKAgqAgqIIKgIKgiCoggqAgogyKAgCCCgIKAAIAoAAACggoAAKAA7gNQAUAUEFARQAAAAAFAQUBBQEFAQUBAAEUBBUQEUBAAQVAQVEEFQEFRBBUEEUBEUQQVAAEAAAAAFBFAAAAFBBQEFAdwGqgAAAAAAoKAAAAAAAoIKAgAAAAAIKAgAiCogIoCAAgCCAAgqIIKgIKgCKIiAAAAAIAAAAAKCKAAAAAAAO4DV0AAAoAAAAAAAKAAAAAAAAAigIKgAACKgACCACIKgCKiAioAioAiiCAAgCAigiAAAAAICgAAAAAAAAoAA7gNHQAAqKAAAAAAAqAKAAAAAAAAAAgAAAIqAAIIAAioAioiCKgAIAAggAIAgAAgAgAgAAoigAAAAAAAKAAO4DR0AAAAKgCiKAAAAAACiAKIAogAAAAACAAIAIAACAAICAgCCKiAioACAAICAAAgACAAAACoAoigAAAAAA7ANHSiAKIAogCgAKgCiCiiAKAAAAAAIAoggAACAAICoACAgCAAIiAIACAAiAAAIIAAAAAAgqAKIAoigAAAAAA7ANHQACiAKIAogCgAogCiAKIAogCiAKIAqAACAqAAIIKggKgAICAgCCAAgIAgAIIAhUFEoUUQSiiBRRAqKrKlFECiiC0UQBRAHcQaOlEAUQBRAFVkBoQBRAFEAUQBRAFEAUQBRAFQQFEAVBAURAUQqAJUoKJUqCoVKCpREFQRKiiCAAIAAAAAAAAAAogCiCCiAKIA7CUrV0qs0oNDNKDVKzVoLSpSgtVmlBqjNKDVKlKC0qUqi0SlEWlSlFUqUoLSs0qC0qUoKJUoKVKlBqpUqUGqlSpUGqlQAohUqAglFQEAAQAAAAAAAAAAAAAAAAAAAAABulZpVrpqlZpVo1Ss0pRqrWKUo3Ss0pRqlZpVqN0rFWlGqVmlBqlZpQapWaUGqVmlBqlZpQapWalBqlZpQaqVKlKNVKlKUWjNKlKtKglKtSgiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQApQApQBaVBaLSoFFpUCi0qBQpQKFKCUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z"
      />

      <Typography.Title
        level={4}
        style={{
          position: "relative",
          zIndex: 5,
          color: "var(--Primary, #9F0E0F)",
          marginTop: "20px",
          fontSize: "20px",
        }}
      >
        oops, looks like you a wrong turn{" "}
      </Typography.Title>

      <CustomButton
        typeButton="outline"
        style={{
          position: "relative",
          zIndex: 4,
          background: "transparent",
          borderColor: "var(--Primary, #9F0E0F)",
          color: "var(--Primary, #9F0E0F)",
          marginTop: "20px",
          fontWeight: 700,
        }}
        onClick={toDashboard}
      >
        Back
      </CustomButton>
    </div>
  );
}

export default Custom505;

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
