import GlobalStyles from "@/shared/styles/GlobalStyles";
import "@/shared/styles/globals.css";
import withTheme from "@/shared/styles/theme";
import ThemeProviderComponents from "@/shared/styles/theme/theme-provider";
import { ChakraProvider } from "@chakra-ui/react";
import {
  HydrationBoundary,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import "antd/dist/reset.css";
import { appWithTranslation } from "next-i18next";
import type { AppProps } from "next/app";
import React from "react";

const App = ({ Component, pageProps }: AppProps) => {
  const [queryClient] = React.useState(() => new QueryClient());

  return (
    <QueryClientProvider client={queryClient}>
      <HydrationBoundary state={pageProps.dehydratedState}>
        <ThemeProviderComponents>
          <ChakraProvider>
            {withTheme(<Component {...pageProps} />)}
            <GlobalStyles />
          </ChakraProvider>
        </ThemeProviderComponents>
      </HydrationBoundary>
      {/* <ReactQueryDevtools initialIsOpen={false} buttonPosition="top-right" /> */}
    </QueryClientProvider>
  );
};

export default appWithTranslation(App);
