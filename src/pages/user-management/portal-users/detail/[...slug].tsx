import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import PortalUserDetail from "@/features/portal-users/detail/PortalUserDetail";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function PortalUserDetailPage() {
  return (
    <>
      <MetaHead
        title={"Portal User Detail Page"}
        description={"Welcome to Portal User Detail Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <PortalUserDetail />
    </>
  );
}

export default withPrivateRoute(PortalUserDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}