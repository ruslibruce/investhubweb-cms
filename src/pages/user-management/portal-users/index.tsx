import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import PortalUsers from "@/features/portal-users/ui/PortalUserPage";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function PortalUsersPage() {
  return (
    <>
      <MetaHead
        title={"Portal Users Page"}
        description={"Welcome to Portal Users Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <PortalUsers />
    </>
  );
}

export default withPrivateRoute(PortalUsersPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}