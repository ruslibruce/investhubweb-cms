import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import RoleManagement from "@/features/master-data/role/ui/RolePage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function RoleManagementPage() {
  return (
    <>
      <MetaHead
        title={"Role Management Page"}
        description={"Welcome to Role Management Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <RoleManagement />
    </>
  );
}

export default withPrivateRoute(RoleManagementPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}