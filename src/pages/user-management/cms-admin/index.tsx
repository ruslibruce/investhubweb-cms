import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import CMSAdmin from "@/features/cms-admin/ui/CMSAdminPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CMSAdminPage() {
  return (
    <>
      <MetaHead
        title={"CMS Admin Page"}
        description={"Welcome to CMS Admin Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CMSAdmin />
    </>
  );
}

export default withPrivateRoute(CMSAdminPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}