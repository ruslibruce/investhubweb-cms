import CMSAdminDetail from "@/features/cms-admin/detail/CMSAdminDetail";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function PortalUserDetailPage() {
  return (
    <>
      <MetaHead
        title={"Portal CMS User Detail Page"}
        description={"Welcome to Portal CMS User Detail Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CMSAdminDetail />
    </>
  );
}

export default withPrivateRoute(PortalUserDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}