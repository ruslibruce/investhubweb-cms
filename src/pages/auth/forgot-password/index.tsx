// import { Inter } from 'next/font/google'
// import styles from '@/styles/Home.module.css'
import ForgotPassword from "@/features/forgotpassword/ForgotPassword";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";

function ForgotPasswordMainPage() {
  return (
    <>
      <Head>
        <title>Forgot Password Page</title>
        <meta name="description" content="Welcome to Forgot Password Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ForgotPassword />
    </>
  );
}

export default withPrivateRoute(ForgotPasswordMainPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
