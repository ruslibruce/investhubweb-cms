import ResetPasswordForm from "@/features/forgotpasswordform/ResetPasswordForm";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";

function ResetPasswordMain() {
  return (
    <>
      <Head>
        <title>Reset Password Page</title>
        <meta name="description" content="Welcome to Reset Password Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ResetPasswordForm />
    </>
  );
}

export default withPrivateRoute(ResetPasswordMain);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
