import ValidateTokenForm from "@/features/validatetokenform/ValidateTokenForm";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";

function ValidateTokenMain() {
  return (
    <>
      <Head>
        <title>Validate Token Page</title>
        <meta name="description" content="Welcome to Validate Token Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ValidateTokenForm />
    </>
  );
}

export default withPrivateRoute(ValidateTokenMain);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
