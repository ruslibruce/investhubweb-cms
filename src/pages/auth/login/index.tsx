// import { Inter } from 'next/font/google'
// import styles from '@/styles/Home.module.css'
import Head from 'next/head';
import FormLogin from '@/features/login/ui/FormLogin';
import withPrivateRoute from '@/shared/lib/withPrivateRoute';
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function Home() {
  return (
    <>
      <Head>
        <title>Login Page</title>
        <meta name="description" content="Welcome to dashboard" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <FormLogin />
    </>
  );
}

export default withPrivateRoute(Home);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}