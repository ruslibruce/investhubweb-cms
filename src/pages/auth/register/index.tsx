import FormRegister from "@/features/register/ui/FormRegister";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function Register() {
  return (
    <>
      <Head>
        <title>Register Page</title>
        <meta name="description" content="Welcome to dashboard" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <FormRegister />
    </>
  );
}

export default withPrivateRoute(Register);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}