import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import Link from "next/link";
import DashboardPage from "@/features/dashboard/ui/DashboardPage";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function Dashboard() {
  return (
    <>
      <MetaHead
        title={"Home Page"}
        description={"Welcome to Home page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <DashboardPage />
    </>
  );
}

export default withPrivateRoute(Dashboard);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}