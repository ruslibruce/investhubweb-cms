import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import QuizCreate from "@/features/questionbank/ui/QuestionBankPage";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function QuizCreatePage() {
  return (
    <>
      <MetaHead
        title={"Create Quiz Page"}
        description={"Welcome to Quiz Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <QuizCreate />
    </>
  );
}

export default withPrivateRoute(QuizCreatePage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}