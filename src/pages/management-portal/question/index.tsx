import QuestionBankPage from "@/features/questionbank/ui/QuestionBankPage";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function QuestionPage() {
  return (
    <>
      <MetaHead
        title={"Quiz Page"}
        description={"Welcome to Quiz Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <QuestionBankPage />
    </>
  );
}

export default withPrivateRoute(QuestionPage);


export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}