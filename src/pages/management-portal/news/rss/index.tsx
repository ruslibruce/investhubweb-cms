import NewsRSSPage from "@/features/newsrss/ui/NewsRSSPage";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function News() {
  return (
    <>
      <MetaHead
        title={"News Page"}
        description={"Welcome to News page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <NewsRSSPage />
    </>
  );
}

export default withPrivateRoute(News);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}