import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import NewsDetailPage from "@/features/newsdetail/ui/NewsDetailPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function NewsDetail() {
  return (
    <>
      <MetaHead
        title={"News Detail Page"}
        description={"Welcome to News Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <NewsDetailPage />
    </>
  );
}

export default withPrivateRoute(NewsDetail);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}