import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import InvestmentPartnerDetail from "@/features/investment-partner-detail/ui/InvestmentPartnerDetail";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function InvestmentPartnerDetailPage() {
  return (
    <>
      <MetaHead
        title={"Investment Partner Detail Page"}
        description={"Welcome to Investment Partner Detail Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <InvestmentPartnerDetail />
    </>
  );
}

export default withPrivateRoute(InvestmentPartnerDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}