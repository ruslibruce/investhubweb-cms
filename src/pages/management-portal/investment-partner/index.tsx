import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import InvestmentPartner from "@/features/investment-partner/ui/InvestmentPartner";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function InvestmentPartnerPage() {
  return (
    <>
      <MetaHead
        title={"Investment Partner Page"}
        description={"Welcome to Investment Partner Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <InvestmentPartner />
    </>
  );
}

export default withPrivateRoute(InvestmentPartnerPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}