import TestPage from "@/features/test/ui/TestPage";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function LevelMainPage() {
  return (
    <>
      <MetaHead
        title={"Level Page"}
        description={"Welcome to Level Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <TestPage />
    </>
  );
}

export default withPrivateRoute(LevelMainPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}