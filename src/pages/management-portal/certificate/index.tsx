import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import Sertifikat from "@/features/sertifikat/ui/SertifikatPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function SertifikatPage() {
  return (
    <>
      <MetaHead
        title={"External Certificate Page"}
        description={"Welcome to External Certificate Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <Sertifikat />
    </>
  );
}

export default withPrivateRoute(SertifikatPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}