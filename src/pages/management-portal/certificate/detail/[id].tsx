import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import SertifikatDetail from "@/features/sertifikat/detail/ui/SertifikatDetail";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function SertifikatDetailPage() {
  return (
    <>
      <MetaHead
        title={"Sertifikat Detail Page"}
        description={"Welcome to Sertifikat Detail Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <SertifikatDetail />
    </>
  );
}

export default withPrivateRoute(SertifikatDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}