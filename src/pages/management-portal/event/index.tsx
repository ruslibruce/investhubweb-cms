import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import EventPage from "@/features/event/ui/EventPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function Event() {
  return (
    <>
      <MetaHead
        title={"Event Page"}
        description={"Welcome to Event page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <EventPage />
    </>
  );
}

export default withPrivateRoute(Event);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}