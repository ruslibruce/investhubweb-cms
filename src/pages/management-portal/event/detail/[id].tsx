import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import EventDetailPage from "@/features/eventdetail/ui/EventDetailPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function EventDetail() {
  return (
    <>
      <MetaHead
        title={"Event Detail Page"}
        description={"Welcome to Event Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <EventDetailPage />
    </>
  );
}

export default withPrivateRoute(EventDetail);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}