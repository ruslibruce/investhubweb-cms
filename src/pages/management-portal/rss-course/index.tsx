import CourseRSSPage from "@/features/courserss/ui/CourseRSSPage";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CourseRSS() {
  return (
    <>
      <MetaHead
        title={"Course RSS Page"}
        description={"Welcome to Course RSS page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CourseRSSPage />
    </>
  );
}

export default withPrivateRoute(CourseRSS);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}