import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import FaqDetailPage from "@/features/faqdetail/ui/FaqDetailPage";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function FaqDetail() {
  return (
    <>
      <MetaHead
        title={"FAQ Detail Page"}
        description={"Welcome to FAQ Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <FaqDetailPage />
    </>
  );
}

export default withPrivateRoute(FaqDetail);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}