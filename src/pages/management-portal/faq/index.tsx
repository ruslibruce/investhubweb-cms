import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import FAQ from "@/features/faq/ui/FaqPage";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function FaqPage() {
    return (
      <>
        <MetaHead
          title={"FAQ Page"}
          description={"Welcome to FAQ page"}
          url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
          image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
        />
        <FAQ />
      </>
    );
  }

  export default withPrivateRoute(FaqPage);

  export async function getStaticProps({ locale }: any) {
    return {
      props: {
        ...(await serverSideTranslations(locale, ["common"])),
        // Will be passed to the page component as props
      },
    };
  }