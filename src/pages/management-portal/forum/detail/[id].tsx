import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import ForumDetail from "@/features/forumdetail/ui/ForumDetail";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function ForumDetailPage() {
  return (
    <>
      <MetaHead
        title={"Forum Detail Page"}
        description={"Welcome to Forum Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <ForumDetail />
    </>
  );
}

export default withPrivateRoute(ForumDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}