import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import Forum from "@/features/forum/ui/Forum";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function ForumPage() {
  return (
    <>
      <MetaHead
        title={"Forum Page"}
        description={"Welcome to Forum page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <Forum />
    </>
  );
}

export default withPrivateRoute(ForumPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}