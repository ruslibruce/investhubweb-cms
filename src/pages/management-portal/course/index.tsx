import CoursePage from "@/features/course/ui/CoursePage";
import MetaHead from "@/shared/components/MetaHead";
import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CourseMainPage() {
  return (
    <>
      <MetaHead
        title={"Course Page"}
        description={"Welcome to Course page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CoursePage />
    </>
  );
}

export default withPrivateRoute(CourseMainPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
