import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import CourseDetail from "@/features/coursedetail/ui/CourseDetailPage";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CourseDetailPage() {
  return (
    <>
      <MetaHead
        title={"Course Detail Page"}
        description={"Welcome to Course Detail page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CourseDetail />
    </>
  );
}

export default withPrivateRoute(CourseDetailPage);

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}
