import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import Link from "next/link";
import GeneralContentPage from "@/features/general-content/ui/GeneralContent";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function GeneralContent() {
  return (
    <>
      <MetaHead
        title={"General Content Page"}
        description={"Welcome to General Content page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <GeneralContentPage />
    </>
  );
}

export default withPrivateRoute(GeneralContent);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}