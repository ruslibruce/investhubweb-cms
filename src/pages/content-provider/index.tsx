import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import ContentProvider from "@/features/content-provider/ui/ContentProviderPage"
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function ContentProviderPage() {
  return (
    <>
      <MetaHead
        title={"Content Provider Page"}
        description={"Welcome to Content Provider Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <ContentProvider />
    </>
  );
}

export default withPrivateRoute(ContentProviderPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}