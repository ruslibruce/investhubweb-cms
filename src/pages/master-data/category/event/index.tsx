import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import CategoryEvent from "@/features/master-data/category/event/ui/CategoryEvent";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CategoryEventPage() {
  return (
    <>
      <MetaHead
        title={"Category Event Page"}
        description={"Welcome to Category Event Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CategoryEvent />
    </>
  );
}

export default withPrivateRoute(CategoryEventPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}