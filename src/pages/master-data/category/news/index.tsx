import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import CategoryNews from "@/features/master-data/category/news/ui/CategoryNews";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CategoryNewsPage() {
  return (
    <>
      <MetaHead
        title={"Category News Page"}
        description={"Welcome to Category News Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CategoryNews />
    </>
  );
}

export default withPrivateRoute(CategoryNewsPage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}