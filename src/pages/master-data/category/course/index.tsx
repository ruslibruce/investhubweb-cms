import withPrivateRoute from "@/shared/lib/withPrivateRoute";
import CategoryCourse from "@/features/master-data/category/course/ui/CategoryCourse";
import Link from "next/link";
import MetaHead from "@/shared/components/MetaHead";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

function CategoryCoursePage() {
  return (
    <>
      <MetaHead
        title={"Category Course Page"}
        description={"Welcome to Category Course Page"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/image/favicon1.ico`}
      />
      <CategoryCourse />
    </>
  );
}

export default withPrivateRoute(CategoryCoursePage);

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}